

# ProdutoTabelaPreco

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**nome** | **String** |  |  [optional]
**usarComoDesconto** | **Boolean** |  |  [optional]
**preco** | **Double** |  |  [optional]



