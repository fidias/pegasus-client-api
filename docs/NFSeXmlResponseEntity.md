

# NFSeXmlResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**loteRpsXml** | **String** |  |  [optional]
**nfseXml** | **String** |  |  [optional]



