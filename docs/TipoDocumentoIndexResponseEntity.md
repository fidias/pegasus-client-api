

# TipoDocumentoIndexResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;TipoDocumentoItemLista&gt;**](TipoDocumentoItemLista.md) |  |  [optional]



