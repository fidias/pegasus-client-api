

# TaxaListarResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;ItemTaxa&gt;**](ItemTaxa.md) |  |  [optional]



