

# ItemPOS

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**serialPos** | **String** |  |  [optional]
**descricao** | **String** |  |  [optional]
**adquirenteId** | **Integer** |  |  [optional]
**chaveRequisicao** | **String** |  |  [optional]
**adquirente** | **String** |  |  [optional]
**cnpj** | **String** |  |  [optional]
**codigoAdquirente** | **Integer** |  |  [optional]



