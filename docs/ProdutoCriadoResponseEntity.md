

# ProdutoCriadoResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**lucro** | **Double** |  |  [optional]
**custoExtraPercentual** | **Double** |  |  [optional]



