

# Evento

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**usuario** | [**Usuario2**](Usuario2.md) |  | 
**dataEvento** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**tipoEvento** | [**TipoEvento**](TipoEvento.md) |  | 
**mensagemAuxiliar** | **String** |  |  [optional]
**sistema** | [**Sistema**](Sistema.md) |  | 



