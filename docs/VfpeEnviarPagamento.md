

# VfpeEnviarPagamento

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**serialPos** | **String** |  | 
**origemPagamento** | **String** |  | 
**valorTotalVenda** | **Double** |  | 



