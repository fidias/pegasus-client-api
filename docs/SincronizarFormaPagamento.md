

# SincronizarFormaPagamento

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienteFormaId** | **Long** |  | 
**idTipoPagamento** | **Integer** |  | 
**valor** | **Double** |  | 
**data** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**quantParcela** | **Integer** |  |  [optional]
**fiscal** | **Boolean** |  |  [optional]
**codigoTransferencia** | **String** |  |  [optional]
**serialPos** | **String** |  |  [optional]
**contaId** | **Integer** |  |  [optional]
**taxaD** | **Double** |  |  [optional]
**taxaP** | **Double** |  |  [optional]
**planoContaFinanceiroId** | **Long** |  |  [optional]
**parcelas** | [**List&lt;SincronizarParcelaPrazo&gt;**](SincronizarParcelaPrazo.md) |  |  [optional]



