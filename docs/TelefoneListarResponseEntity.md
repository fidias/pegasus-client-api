

# TelefoneListarResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;ItemTelefone&gt;**](ItemTelefone.md) |  |  [optional]
**id** | **Long** |  |  [optional]
**telefonePadrao** | **Long** |  |  [optional]



