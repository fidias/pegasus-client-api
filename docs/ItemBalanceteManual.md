

# ItemBalanceteManual

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**dataContagem** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**responsavelId** | **Long** |  | 
**responsavel** | **String** |  | 
**tipoId** | **Long** |  | 
**tipoDescricao** | **String** |  | 
**motivoInventarioId** | **Long** |  | 
**motivoSped** | **String** |  | 



