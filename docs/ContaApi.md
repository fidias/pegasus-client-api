# ContaApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**autocomplete1**](ContaApi.md#autocomplete1) | **GET** /api/conta/autocomplete | 
[**autocompleteBancosSuportados**](ContaApi.md#autocompleteBancosSuportados) | **GET** /api/conta/autocomplete/bancos-suportados | 


<a name="autocomplete1"></a>
# **autocomplete1**
> ContaListaResponseEntity autocomplete1(termo, pagina)



Solicita lista de Contas que contenham o termo solicitado no nome

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContaApi apiInstance = new ContaApi(defaultClient);
    String termo = "termo_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      ContaListaResponseEntity result = apiInstance.autocomplete1(termo, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContaApi#autocomplete1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  | [optional]
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**ContaListaResponseEntity**](ContaListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de Contas |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="autocompleteBancosSuportados"></a>
# **autocompleteBancosSuportados**
> ContaListaResponseEntity autocompleteBancosSuportados(termo, pagina)



Solicita lista de Contas que contenham o termo solicitado no nome

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContaApi apiInstance = new ContaApi(defaultClient);
    String termo = "termo_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      ContaListaResponseEntity result = apiInstance.autocompleteBancosSuportados(termo, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContaApi#autocompleteBancosSuportados");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  | [optional]
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**ContaListaResponseEntity**](ContaListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de Contas |  -  |
**500** | Erro desconhecido no servidor |  -  |

