

# NFeNaturezaOperacaoListResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**list** | [**List&lt;NFeNaturezaOperacaoSelect2&gt;**](NFeNaturezaOperacaoSelect2.md) |  |  [optional]



