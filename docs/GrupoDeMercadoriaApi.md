# GrupoDeMercadoriaApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deletar7**](GrupoDeMercadoriaApi.md#deletar7) | **DELETE** /api/grupo-mercadoria/{id} | 
[**index5**](GrupoDeMercadoriaApi.md#index5) | **GET** /api/grupo-mercadoria | 
[**listarSelect2**](GrupoDeMercadoriaApi.md#listarSelect2) | **GET** /api/grupo-mercadoria/select2 | 


<a name="deletar7"></a>
# **deletar7**
> deletar7(id)



Deleta grupo de mercadoria

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.GrupoDeMercadoriaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    GrupoDeMercadoriaApi apiInstance = new GrupoDeMercadoriaApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      apiInstance.deletar7(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling GrupoDeMercadoriaApi#deletar7");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**404** | Grupo de Mercadoria não encontrado |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="index5"></a>
# **index5**
> GrupoMercadoriaIndexResponseEntity index5(descricao)



Solicita lista de grupos de mercadoria, com opção de busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.GrupoDeMercadoriaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    GrupoDeMercadoriaApi apiInstance = new GrupoDeMercadoriaApi(defaultClient);
    String descricao = "descricao_example"; // String | 
    try {
      GrupoMercadoriaIndexResponseEntity result = apiInstance.index5(descricao);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling GrupoDeMercadoriaApi#index5");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **descricao** | **String**|  | [optional]

### Return type

[**GrupoMercadoriaIndexResponseEntity**](GrupoMercadoriaIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de grupos de mercadoria |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="listarSelect2"></a>
# **listarSelect2**
> GrupoMercadoriaListaResponseEntity listarSelect2(termo)



Solicita lista de grupos de mercadorias que contenham o termo solicitado no nome

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.GrupoDeMercadoriaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    GrupoDeMercadoriaApi apiInstance = new GrupoDeMercadoriaApi(defaultClient);
    String termo = "termo_example"; // String | 
    try {
      GrupoMercadoriaListaResponseEntity result = apiInstance.listarSelect2(termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling GrupoDeMercadoriaApi#listarSelect2");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  |

### Return type

[**GrupoMercadoriaListaResponseEntity**](GrupoMercadoriaListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de Grupos de Mercadoria |  -  |
**500** | Erro desconhecido no servidor |  -  |

