

# ClienteCreditoListaItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**tipoTransacao** | [**TipoTransacao**](TipoTransacao.md) |  | 
**valor** | **Double** |  | 
**saldo** | **Double** |  | 
**venda** | [**Venda**](Venda.md) |  |  [optional]
**usuario** | [**Usuario**](Usuario.md) |  | 
**historico** | **String** |  | 
**cadastro** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**notaEntrada** | [**NotaEntrada**](NotaEntrada.md) |  |  [optional]



