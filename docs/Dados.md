

# Dados

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienteId** | **Long** |  | 
**tipo** | [**Tipo**](Tipo.md) |  | 
**enderecoEntregaId** | **Long** |  |  [optional]
**dataEntrega** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**dataValidade** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**comissionario** | **Long** |  |  [optional]
**observacao** | **String** |  |  [optional]
**cliente** | [**Cliente**](Cliente.md) |  | 
**ignorarQtdDescPromo** | **Boolean** |  | 
**produtoProducao** | [**ProdutoProducao**](ProdutoProducao.md) |  |  [optional]



