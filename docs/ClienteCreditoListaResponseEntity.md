

# ClienteCreditoListaResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;ClienteCreditoListaItem&gt;**](ClienteCreditoListaItem.md) |  |  [optional]



