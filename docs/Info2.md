

# Info2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**ambiente** | **String** |  | 
**versaoAplicativo** | **String** |  | 
**uf** | **String** |  | 
**status** | **String** |  | 
**motivo** | **String** |  | 
**chave** | **String** |  |  [optional]
**tipoEvento** | **String** |  |  [optional]
**evento** | **String** |  |  [optional]
**numSequencia** | **String** |  |  [optional]
**dataHoraEvento** | **String** |  |  [optional]
**numProtocolo** | **String** |  |  [optional]



