

# NFSeConfiguracaoResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**configuracoes** | **String** |  |  [optional]
**codigoServico** | **String** |  |  [optional]



