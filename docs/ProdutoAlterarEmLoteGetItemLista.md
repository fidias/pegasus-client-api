

# ProdutoAlterarEmLoteGetItemLista

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**produtoId** | **Long** |  | 
**descricao** | **String** |  | 
**novoCusto** | **Double** |  | 
**novoPreco** | **Double** |  | 
**novoLucro** | **Double** |  | 
**custoAnterior** | **Double** |  | 
**precoAnterior** | **Double** |  | 
**lucroAnterior** | **Double** |  | 



