

# InlineObject95

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**statusId** | [**StatusIdEnum**](#StatusIdEnum) | Status para o qual será modificado |  [optional]



## Enum: StatusIdEnum

Name | Value
---- | -----
EM_ANDAMENTO | &quot;EM_ANDAMENTO&quot;
FINALIZADO | &quot;FINALIZADO&quot;



