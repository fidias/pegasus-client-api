

# ListaImpressorasResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**impressoras** | **List&lt;String&gt;** |  |  [optional]



