# IncioApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**aviso**](IncioApi.md#aviso) | **GET** /api/inicio/calendario/aviso | 
[**contasPagar**](IncioApi.md#contasPagar) | **GET** /api/inicio/calendario/contas-pagar | 
[**contasReceber**](IncioApi.md#contasReceber) | **GET** /api/inicio/calendario/contas-receber | 


<a name="aviso"></a>
# **aviso**
> CalendarioEventosResponseEntity aviso(dataInicial, dataFinal)



Solicita lista de avisos entre datas

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.IncioApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    IncioApi apiInstance = new IncioApi(defaultClient);
    OffsetDateTime dataInicial = new OffsetDateTime(); // OffsetDateTime | 
    OffsetDateTime dataFinal = new OffsetDateTime(); // OffsetDateTime | 
    try {
      CalendarioEventosResponseEntity result = apiInstance.aviso(dataInicial, dataFinal);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling IncioApi#aviso");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dataInicial** | **OffsetDateTime**|  |
 **dataFinal** | **OffsetDateTime**|  |

### Return type

[**CalendarioEventosResponseEntity**](CalendarioEventosResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de avisos |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="contasPagar"></a>
# **contasPagar**
> CalendarioEventosResponseEntity contasPagar(dataInicial, dataFinal)



Solicita lista de contas a pagar entre datas

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.IncioApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    IncioApi apiInstance = new IncioApi(defaultClient);
    OffsetDateTime dataInicial = new OffsetDateTime(); // OffsetDateTime | 
    OffsetDateTime dataFinal = new OffsetDateTime(); // OffsetDateTime | 
    try {
      CalendarioEventosResponseEntity result = apiInstance.contasPagar(dataInicial, dataFinal);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling IncioApi#contasPagar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dataInicial** | **OffsetDateTime**|  |
 **dataFinal** | **OffsetDateTime**|  |

### Return type

[**CalendarioEventosResponseEntity**](CalendarioEventosResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de contas a pagar |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="contasReceber"></a>
# **contasReceber**
> CalendarioEventosResponseEntity contasReceber(dataInicial, dataFinal)



Solicita lista de contas a receber entre datas

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.IncioApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    IncioApi apiInstance = new IncioApi(defaultClient);
    OffsetDateTime dataInicial = new OffsetDateTime(); // OffsetDateTime | 
    OffsetDateTime dataFinal = new OffsetDateTime(); // OffsetDateTime | 
    try {
      CalendarioEventosResponseEntity result = apiInstance.contasReceber(dataInicial, dataFinal);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling IncioApi#contasReceber");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dataInicial** | **OffsetDateTime**|  |
 **dataFinal** | **OffsetDateTime**|  |

### Return type

[**CalendarioEventosResponseEntity**](CalendarioEventosResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de contas a receber |  -  |
**500** | Erro desconhecido no servidor |  -  |

