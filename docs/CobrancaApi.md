# CobrancaApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**salvar**](CobrancaApi.md#salvar) | **POST** /api/configuracao-cobranca | 


<a name="salvar"></a>
# **salvar**
> ConfiguracaoCobrancaResponseEntity salvar(situacao, token, urlCobranca)



Salva os dados iniciais da Configuração de Cobrança.

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CobrancaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CobrancaApi apiInstance = new CobrancaApi(defaultClient);
    String situacao = "situacao_example"; // String | 
    String token = "token_example"; // String | 
    String urlCobranca = "urlCobranca_example"; // String | 
    try {
      ConfiguracaoCobrancaResponseEntity result = apiInstance.salvar(situacao, token, urlCobranca);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CobrancaApi#salvar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **situacao** | **String**|  |
 **token** | **String**|  |
 **urlCobranca** | **String**|  |

### Return type

[**ConfiguracaoCobrancaResponseEntity**](ConfiguracaoCobrancaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna dados da Configuração de Cobrança |  -  |
**500** | Erro desconhecido no servidor |  -  |

