

# NFCeIntegradorResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**xml** | **String** |  |  [optional]
**codigoUf** | **Integer** |  |  [optional]
**numeroNFCe** | **Integer** |  |  [optional]
**dataHoraNFCeGerado** | **Long** |  |  [optional]
**valorNFCe** | **Double** |  |  [optional]
**retEnviNFe** | **String** | Representação JSON de um retorno bem sucedido de uma transmissão de NFC-e. Sua presença indica que o NFC-e já foi transmitido corretamente. |  [optional]
**codigoNumerico** | **Integer** |  |  [optional]



