

# ContratoCobrancaVisualizarItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**qtdMeses** | **Long** |  | 
**dataInicial** | **String** |  | 
**dataFinal** | **String** |  |  [optional]
**cliente** | [**Cliente1**](Cliente1.md) |  | 
**renovacaoAutomatica** | **Boolean** |  | 
**valorParcela** | **Double** |  | 
**taxaReajusteAnual** | **Double** |  | 
**diaVencimento** | **Long** |  | 
**contratoStatus** | [**ContratoStatus**](ContratoStatus.md) |  | 
**contratoTipo** | [**ContratoTipo2**](ContratoTipo2.md) |  | 
**contratoPeriodicidade** | [**ContratoPeriodicidade**](ContratoPeriodicidade.md) |  | 
**numParcelasNaoPagas** | **Long** |  | 



