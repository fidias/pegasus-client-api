

# DavSimplificado

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**dataEdicao** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**statusId** | **Long** |  | 
**usuario** | [**Usuario**](Usuario.md) |  | 
**usuarioEdicao** | [**UsuarioEdicao**](UsuarioEdicao.md) |  | 
**isOrcamento** | **Boolean** |  | 



