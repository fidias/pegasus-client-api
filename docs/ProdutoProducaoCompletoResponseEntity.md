

# ProdutoProducaoCompletoResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**dados** | **String** |  |  [optional]
**itens** | **String** |  |  [optional]
**status** | **String** |  |  [optional]
**eventos** | **String** |  |  [optional]
**listaNotas** | **String** |  |  [optional]
**idUltimoItemCriado** | **Long** |  |  [optional]



