

# NFeTransmissaoListaItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**retorno** | [**RetEnvEvento**](RetEnvEvento.md) |  | 
**schemaXml** | **String** |  | 



