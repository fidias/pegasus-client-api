

# NFeDestinatarioAutocompleteItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**value** | **String** |  | 
**nome** | **String** |  | 
**nomeFantasia** | **String** |  |  [optional]
**ramo** | **String** |  |  [optional]
**cpfCnpjFormatado** | **String** |  |  [optional]
**indConsumidor** | **Long** |  | 



