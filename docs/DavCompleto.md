

# DavCompleto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enderecosCliente** | [**List&lt;EnderecoCliente&gt;**](EnderecoCliente.md) |  |  [optional]
**id** | **Long** |  | 
**clienteId** | **Long** |  | 
**dataCadastro** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**dataEdicao** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**statusId** | **Long** |  | 
**arquivado** | **Boolean** |  | 
**usuario** | [**Usuario1**](Usuario1.md) |  | 
**usuarioEdicao** | [**UsuarioEdicao1**](UsuarioEdicao1.md) |  | 
**dados** | [**Dados**](Dados.md) |  | 
**itens** | [**List&lt;Item&gt;**](Item.md) |  |  [optional]
**objeto** | [**Objeto**](Objeto.md) |  |  [optional]
**totais** | [**Totais**](Totais.md) |  | 
**isOrcamento** | **Boolean** |  | 
**tabelaPreco** | [**TabelaPreco**](TabelaPreco.md) |  |  [optional]
**eventos** | [**List&lt;Evento&gt;**](Evento.md) |  |  [optional]



