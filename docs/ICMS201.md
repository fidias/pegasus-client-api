

# ICMS201

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pICMSST** | **Double** |  | 
**pMVAST** | **Double** |  |  [optional]
**pRedBCST** | **Double** |  |  [optional]
**pCredSN** | **Double** |  | 



