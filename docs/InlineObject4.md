

# InlineObject4

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**descricao** | **String** |  | 
**chaveRequisicao** | **String** |  | 
**idAdquirente** | [**IntegerParam**](IntegerParam.md) |  | 
**posicao** | [**IntegerParam**](IntegerParam.md) |  |  [optional]



