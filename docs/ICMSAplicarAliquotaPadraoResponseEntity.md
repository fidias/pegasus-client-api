

# ICMSAplicarAliquotaPadraoResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eventos** | [**List&lt;ICMSAliquotaPadraoEventoItem&gt;**](ICMSAliquotaPadraoEventoItem.md) |  |  [optional]



