

# NCMItemLista

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**descricao** | **String** |  | 
**codigo** | **String** |  | 
**disponivelUso** | **Boolean** |  | 



