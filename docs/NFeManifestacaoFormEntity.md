

# NFeManifestacaoFormEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chaveNfe** | **String** |  | 
**justificativa** | **String** |  |  [optional]
**manifestacaoTipoId** | **Integer** |  | 



