

# Veiculo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marca** | [**Marca**](Marca.md) |  | 
**placa** | **String** |  |  [optional]
**modelo** | **String** |  | 
**renavam** | **String** |  | 
**anoFabricacao** | **String** |  | 



