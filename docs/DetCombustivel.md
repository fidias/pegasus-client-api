

# DetCombustivel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**produtoId** | **Long** |  | 
**cProdAnp** | **Long** |  | 
**pGlp** | **Double** |  |  [optional]
**pGnn** | **Double** |  |  [optional]
**pGni** | **Double** |  |  [optional]
**vPart** | **Double** |  |  [optional]
**qTemp** | **Double** |  |  [optional]
**ufCons** | **Long** |  | 
**descAnp** | **String** |  | 



