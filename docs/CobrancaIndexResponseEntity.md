

# CobrancaIndexResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;ContratoCobrancaListaItem&gt;**](ContratoCobrancaListaItem.md) |  |  [optional]



