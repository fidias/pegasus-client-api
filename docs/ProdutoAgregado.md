

# ProdutoAgregado

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**produtoAgregadoId** | **Long** |  | 
**descricao** | **String** |  | 
**quantidade** | **Double** |  | 
**unidade** | **String** |  | 
**preco** | **Double** |  | 



