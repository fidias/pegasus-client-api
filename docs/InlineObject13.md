

# InlineObject13

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**habilitado** | **Boolean** |  |  [optional]
**isGerarFatura** | **Boolean** |  |  [optional]
**isAlterarQuantidade** | **Boolean** |  |  [optional]
**isAlterarCusto** | **Boolean** |  |  [optional]
**isAlterarPreco** | **Boolean** |  |  [optional]
**hasFormaPagamento** | **Boolean** |  |  [optional]
**isAlterarIdUltimaNota** | **Boolean** |  |  [optional]



