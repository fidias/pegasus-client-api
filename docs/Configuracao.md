

# Configuracao

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**grupoMercadoriaId** | **Long** |  | 
**grupoMercadoria** | **String** |  | 
**nomeImpressora** | **String** |  | 
**espacamentoVertical** | **Long** |  | 
**modelo** | **String** |  | 
**marca** | **String** |  | 
**habilitada** | **Boolean** |  | 
**impressaoAutomatica** | **Boolean** |  | 



