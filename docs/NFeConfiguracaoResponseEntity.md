

# NFeConfiguracaoResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**configuracoesNfe** | [**NFeCarregarConfiguracoes**](NFeCarregarConfiguracoes.md) |  |  [optional]
**configuracoesDist** | **String** |  |  [optional]



