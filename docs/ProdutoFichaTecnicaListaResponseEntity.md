

# ProdutoFichaTecnicaListaResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;ProdutoFichaTecnicaItemLista&gt;**](ProdutoFichaTecnicaItemLista.md) |  |  [optional]
**produtoBase** | [**ProdutoBaseFichaTecnica**](ProdutoBaseFichaTecnica.md) |  |  [optional]



