

# InlineObject69

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codigoUfEmitente** | [**IntegerParam**](IntegerParam.md) |  | 
**dataEmissao** | **String** |  | 
**cnpjEmitente** | [**NumericOnlyParam**](NumericOnlyParam.md) |  | 
**serie** | [**IntegerParam**](IntegerParam.md) |  | 
**numeroDocumentoFiscal** | [**LongParam**](LongParam.md) |  | 
**modelo** | **Integer** |  | 



