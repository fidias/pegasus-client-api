# NfcEApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**nfceAutorizarLoteImportarVenda**](NfcEApi.md#nfceAutorizarLoteImportarVenda) | **POST** /api/nfce/autorizar-lote-importar-venda | 
[**nfceCancelarPorIdVenda**](NfcEApi.md#nfceCancelarPorIdVenda) | **POST** /api/nfce/cancelar-por-id-venda | 
[**nfceConfiguracaoSalvar**](NfcEApi.md#nfceConfiguracaoSalvar) | **POST** /api/nfce/configuracao | 
[**nfceConsultarProtocolo**](NfcEApi.md#nfceConsultarProtocolo) | **GET** /api/nfce/{id}/consultar-protocolo | 
[**nfceConsultarStatusServico**](NfcEApi.md#nfceConsultarStatusServico) | **GET** /api/nfce/consultar-status-servico | 
[**nfceIntegradorCancelarVenda**](NfcEApi.md#nfceIntegradorCancelarVenda) | **POST** /api/nfce/integrador/cancelar-venda | 
[**nfceIntegradorImportarVenda**](NfcEApi.md#nfceIntegradorImportarVenda) | **POST** /api/nfce/integrador/importar-venda | 
[**nfceIntegradorSalvarRetCancelarVenda**](NfcEApi.md#nfceIntegradorSalvarRetCancelarVenda) | **POST** /api/nfce/integrador/salvar-ret-cancelar-venda | 
[**nfceIntegradorSalvarRetConsultarSituacao**](NfcEApi.md#nfceIntegradorSalvarRetConsultarSituacao) | **POST** /api/nfce/integrador/{id}/salvar-ret-consultar-situacao | 
[**nfceIntegradorSalvarRetVenda**](NfcEApi.md#nfceIntegradorSalvarRetVenda) | **POST** /api/nfce/integrador/salvar-ret-venda | 
[**nfceListar**](NfcEApi.md#nfceListar) | **GET** /api/nfce | 


<a name="nfceAutorizarLoteImportarVenda"></a>
# **nfceAutorizarLoteImportarVenda**
> NFCeIntegradorResponseEntity nfceAutorizarLoteImportarVenda(vendaBalcaoIntegradorEntity)



Solicita a importação de uma Venda para NFC-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfcEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfcEApi apiInstance = new NfcEApi(defaultClient);
    VendaBalcaoIntegradorEntity vendaBalcaoIntegradorEntity = new VendaBalcaoIntegradorEntity(); // VendaBalcaoIntegradorEntity | 
    try {
      NFCeIntegradorResponseEntity result = apiInstance.nfceAutorizarLoteImportarVenda(vendaBalcaoIntegradorEntity);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfcEApi#nfceAutorizarLoteImportarVenda");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vendaBalcaoIntegradorEntity** | [**VendaBalcaoIntegradorEntity**](VendaBalcaoIntegradorEntity.md)|  | [optional]

### Return type

[**NFCeIntegradorResponseEntity**](NFCeIntegradorResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Resposta da autorização de NFC-e importado de uma Venda |  -  |
**400** | Erro na validação do XML |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="nfceCancelarPorIdVenda"></a>
# **nfceCancelarPorIdVenda**
> NFCeCancelarResponseEntity nfceCancelarPorIdVenda(idCaixa, clienteVendaId, justificativa)



Solicita o cancelamento de um NFC-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfcEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfcEApi apiInstance = new NfcEApi(defaultClient);
    Integer idCaixa = 56; // Integer | 
    Long clienteVendaId = 56L; // Long | 
    String justificativa = "justificativa_example"; // String | 
    try {
      NFCeCancelarResponseEntity result = apiInstance.nfceCancelarPorIdVenda(idCaixa, clienteVendaId, justificativa);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfcEApi#nfceCancelarPorIdVenda");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idCaixa** | **Integer**|  |
 **clienteVendaId** | **Long**|  |
 **justificativa** | **String**|  |

### Return type

[**NFCeCancelarResponseEntity**](NFCeCancelarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Resposta do cancelamento de um NFC-e a partir de uma Venda |  -  |
**400** | Erro na validação do XML |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="nfceConfiguracaoSalvar"></a>
# **nfceConfiguracaoSalvar**
> nfceConfiguracaoSalvar(numeroNfce, serieNfce, idTokenCscNfce, codCscNfce)



Solicita atualização dos dados de Configuração para o NFC-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfcEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfcEApi apiInstance = new NfcEApi(defaultClient);
    LongParam numeroNfce = new LongParam(); // LongParam | 
    ShortParam serieNfce = new ShortParam(); // ShortParam | 
    IntegerParam idTokenCscNfce = new IntegerParam(); // IntegerParam | 
    String codCscNfce = "codCscNfce_example"; // String | 
    try {
      apiInstance.nfceConfiguracaoSalvar(numeroNfce, serieNfce, idTokenCscNfce, codCscNfce);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfcEApi#nfceConfiguracaoSalvar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **numeroNfce** | [**LongParam**](LongParam.md)|  |
 **serieNfce** | [**ShortParam**](ShortParam.md)|  |
 **idTokenCscNfce** | [**IntegerParam**](IntegerParam.md)|  |
 **codCscNfce** | **String**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="nfceConsultarProtocolo"></a>
# **nfceConsultarProtocolo**
> NFeConsultarProtocoloResponseEntity nfceConsultarProtocolo(id)



Requisita consulta de protocolo (situação da NFC-e) na Sefaz

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfcEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfcEApi apiInstance = new NfcEApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      NFeConsultarProtocoloResponseEntity result = apiInstance.nfceConsultarProtocolo(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfcEApi#nfceConsultarProtocolo");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**NFeConsultarProtocoloResponseEntity**](NFeConsultarProtocoloResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados da situação do NFC-e |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="nfceConsultarStatusServico"></a>
# **nfceConsultarStatusServico**
> NFeConsultarStatusServicoResponseEntity nfceConsultarStatusServico()



Requisita consulta do status do serviço na Sefaz

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfcEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfcEApi apiInstance = new NfcEApi(defaultClient);
    try {
      NFeConsultarStatusServicoResponseEntity result = apiInstance.nfceConsultarStatusServico();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfcEApi#nfceConsultarStatusServico");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NFeConsultarStatusServicoResponseEntity**](NFeConsultarStatusServicoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados do status do serviço |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="nfceIntegradorCancelarVenda"></a>
# **nfceIntegradorCancelarVenda**
> NFCeCancelarResponseEntity nfceIntegradorCancelarVenda(idCaixa, clienteVendaId, justificativa)



Solicita o cancelamento de um NFC-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfcEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfcEApi apiInstance = new NfcEApi(defaultClient);
    Integer idCaixa = 56; // Integer | 
    Long clienteVendaId = 56L; // Long | 
    String justificativa = "justificativa_example"; // String | 
    try {
      NFCeCancelarResponseEntity result = apiInstance.nfceIntegradorCancelarVenda(idCaixa, clienteVendaId, justificativa);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfcEApi#nfceIntegradorCancelarVenda");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idCaixa** | **Integer**|  |
 **clienteVendaId** | **Long**|  |
 **justificativa** | **String**|  |

### Return type

[**NFCeCancelarResponseEntity**](NFCeCancelarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Gera XML de cancelamento a partir do Id do Caixa e Id da Venda Local. |  -  |
**400** | Erro na validação do XML |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="nfceIntegradorImportarVenda"></a>
# **nfceIntegradorImportarVenda**
> NFCeIntegradorResponseEntity nfceIntegradorImportarVenda(vendaBalcaoIntegradorEntity)



Solicita a importação de uma Venda para NFC-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfcEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfcEApi apiInstance = new NfcEApi(defaultClient);
    VendaBalcaoIntegradorEntity vendaBalcaoIntegradorEntity = new VendaBalcaoIntegradorEntity(); // VendaBalcaoIntegradorEntity | 
    try {
      NFCeIntegradorResponseEntity result = apiInstance.nfceIntegradorImportarVenda(vendaBalcaoIntegradorEntity);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfcEApi#nfceIntegradorImportarVenda");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vendaBalcaoIntegradorEntity** | [**VendaBalcaoIntegradorEntity**](VendaBalcaoIntegradorEntity.md)|  |

### Return type

[**NFCeIntegradorResponseEntity**](NFCeIntegradorResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Solicita geração de XML NFC-e para ser enviado ao Integrador |  -  |
**400** | Erro na validação do XML |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="nfceIntegradorSalvarRetCancelarVenda"></a>
# **nfceIntegradorSalvarRetCancelarVenda**
> nfceIntegradorSalvarRetCancelarVenda(xml, idCaixa, clienteVendaId)



Salva os dados de retorno do Integrador ao cancelar um NFC-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfcEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfcEApi apiInstance = new NfcEApi(defaultClient);
    String xml = "xml_example"; // String | xml
    Integer idCaixa = 56; // Integer | id_caixa
    Long clienteVendaId = 56L; // Long | cliente_venda_id
    try {
      apiInstance.nfceIntegradorSalvarRetCancelarVenda(xml, idCaixa, clienteVendaId);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfcEApi#nfceIntegradorSalvarRetCancelarVenda");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xml** | **String**| xml |
 **idCaixa** | **Integer**| id_caixa |
 **clienteVendaId** | **Long**| cliente_venda_id |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="nfceIntegradorSalvarRetConsultarSituacao"></a>
# **nfceIntegradorSalvarRetConsultarSituacao**
> nfceIntegradorSalvarRetConsultarSituacao(id, xml)



Salva os dados de retorno do Integrador ao consultar protocolo de NFC-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfcEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfcEApi apiInstance = new NfcEApi(defaultClient);
    Long id = 56L; // Long | id
    String xml = "xml_example"; // String | xml
    try {
      apiInstance.nfceIntegradorSalvarRetConsultarSituacao(id, xml);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfcEApi#nfceIntegradorSalvarRetConsultarSituacao");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id |
 **xml** | **String**| xml |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="nfceIntegradorSalvarRetVenda"></a>
# **nfceIntegradorSalvarRetVenda**
> nfceIntegradorSalvarRetVenda(xml, idCaixa, clienteVendaId)



Salva os dados de retorno do Integrador ao importar uma Venda para NFC-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfcEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfcEApi apiInstance = new NfcEApi(defaultClient);
    String xml = "xml_example"; // String | xml
    Integer idCaixa = 56; // Integer | id_caixa
    Long clienteVendaId = 56L; // Long | cliente_venda_id
    try {
      apiInstance.nfceIntegradorSalvarRetVenda(xml, idCaixa, clienteVendaId);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfcEApi#nfceIntegradorSalvarRetVenda");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xml** | **String**| xml |
 **idCaixa** | **Integer**| id_caixa |
 **clienteVendaId** | **Long**| cliente_venda_id |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="nfceListar"></a>
# **nfceListar**
> NFeListarResponseEntity nfceListar(numeroDocumento, dataEmissaoInicial, dataEmissaoFinal, status, pagina)



Solicita lista de NFC-e&#39;s, com suporte a busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfcEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfcEApi apiInstance = new NfcEApi(defaultClient);
    Integer numeroDocumento = 56; // Integer | 
    OffsetDateTime dataEmissaoInicial = 01/01/2019; // OffsetDateTime | Data no formato DD/MM/AAAA
    OffsetDateTime dataEmissaoFinal = 31/01/2019; // OffsetDateTime | Data no formato DD/MM/AAAA
    String status = "status_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      NFeListarResponseEntity result = apiInstance.nfceListar(numeroDocumento, dataEmissaoInicial, dataEmissaoFinal, status, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfcEApi#nfceListar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **numeroDocumento** | **Integer**|  | [optional]
 **dataEmissaoInicial** | **OffsetDateTime**| Data no formato DD/MM/AAAA | [optional]
 **dataEmissaoFinal** | **OffsetDateTime**| Data no formato DD/MM/AAAA | [optional]
 **status** | **String**|  | [optional] [enum: em_digitacao, autorizada, cancelada, denegada, validada_assinada, arquivada]
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**NFeListarResponseEntity**](NFeListarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de NFC-e&#39;s |  -  |
**500** | Erro desconhecido no servidor |  -  |

