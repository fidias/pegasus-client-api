

# MDFeSituacaoResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retorno** | **String** |  |  [optional]
**leiauteXml** | **String** |  |  [optional]
**statusAtual** | **Integer** |  |  [optional]



