

# TabelasPreco

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tabelaPrecoItemId** | **Long** |  | 
**tabelaId** | **Long** |  | 
**nome** | **String** |  | 
**margemP** | **Double** |  | 
**preco** | **Double** |  | 



