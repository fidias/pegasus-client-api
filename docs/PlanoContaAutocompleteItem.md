

# PlanoContaAutocompleteItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**parentId** | **Long** |  |  [optional]
**descricao** | **String** |  | 
**identificador** | **String** |  |  [optional]
**fluxoOperacao** | [**FluxoOperacaoEnum**](#FluxoOperacaoEnum) |  |  [optional]
**disabled** | **Boolean** |  | 
**$isDisabled** | **Boolean** |  | 



## Enum: FluxoOperacaoEnum

Name | Value
---- | -----
ENTRADA | &quot;Entrada&quot;
SAIDA | &quot;Saida&quot;



