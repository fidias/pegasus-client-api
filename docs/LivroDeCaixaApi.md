# LivroDeCaixaApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**atualizar6**](LivroDeCaixaApi.md#atualizar6) | **PUT** /api/livro-caixa/centro-custo/{id} | 
[**autocomplete8**](LivroDeCaixaApi.md#autocomplete8) | **GET** /api/livro-caixa/centro-custo/autocomplete | 
[**cancelarReferenciaLivroCaixa**](LivroDeCaixaApi.md#cancelarReferenciaLivroCaixa) | **DELETE** /api/livro-caixa/conciliar-conta/{id}/item/{conciliar_conta_item_id}/livro-caixa/{livro_caixa_id} | 
[**criar9**](LivroDeCaixaApi.md#criar9) | **POST** /api/livro-caixa/centro-custo | 
[**deletar8**](LivroDeCaixaApi.md#deletar8) | **DELETE** /api/livro-caixa/centro-custo/{id} | 
[**get6**](LivroDeCaixaApi.md#get6) | **GET** /api/livro-caixa/conciliar-conta/{id} | 
[**importarOFX**](LivroDeCaixaApi.md#importarOFX) | **POST** /api/livro-caixa/conciliar-conta/importar/ofx | 
[**index6**](LivroDeCaixaApi.md#index6) | **GET** /api/livro-caixa/centro-custo | 
[**index7**](LivroDeCaixaApi.md#index7) | **GET** /api/livro-caixa/conciliar-conta | 
[**referenciarLivroCaixa**](LivroDeCaixaApi.md#referenciarLivroCaixa) | **GET** /api/livro-caixa/conciliar-conta/{id}/item/{conciliar_conta_item_id}/livro-caixa/{livro_caixa_id} | 


<a name="atualizar6"></a>
# **atualizar6**
> atualizar6(id, inlineObject40)



Requisita atualização de um Centro de Custo

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.LivroDeCaixaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    LivroDeCaixaApi apiInstance = new LivroDeCaixaApi(defaultClient);
    Integer id = 56; // Integer | 
    InlineObject40 inlineObject40 = new InlineObject40(); // InlineObject40 | 
    try {
      apiInstance.atualizar6(id, inlineObject40);
    } catch (ApiException e) {
      System.err.println("Exception when calling LivroDeCaixaApi#atualizar6");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **inlineObject40** | [**InlineObject40**](InlineObject40.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**404** | Centro de Custo {0} não encontrado. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="autocomplete8"></a>
# **autocomplete8**
> CentroCustoAutocompleteResponseEntity autocomplete8(termo)



Solicita lista de Centro de Custo que contenham o termo solicitado na descrição

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.LivroDeCaixaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    LivroDeCaixaApi apiInstance = new LivroDeCaixaApi(defaultClient);
    String termo = "termo_example"; // String | 
    try {
      CentroCustoAutocompleteResponseEntity result = apiInstance.autocomplete8(termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling LivroDeCaixaApi#autocomplete8");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  | [optional]

### Return type

[**CentroCustoAutocompleteResponseEntity**](CentroCustoAutocompleteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de Centro de Custo |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="cancelarReferenciaLivroCaixa"></a>
# **cancelarReferenciaLivroCaixa**
> ConciliarContaCompletoResponseEntity cancelarReferenciaLivroCaixa(id, conciliarContaItemId, livroCaixaId)



Solicita cancelamento de referencia um Item da Conciliação de Conta para um registro do Livro de Caixa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.LivroDeCaixaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    LivroDeCaixaApi apiInstance = new LivroDeCaixaApi(defaultClient);
    Long id = 56L; // Long | 
    Long conciliarContaItemId = 56L; // Long | 
    Long livroCaixaId = 56L; // Long | 
    try {
      ConciliarContaCompletoResponseEntity result = apiInstance.cancelarReferenciaLivroCaixa(id, conciliarContaItemId, livroCaixaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling LivroDeCaixaApi#cancelarReferenciaLivroCaixa");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **conciliarContaItemId** | **Long**|  |
 **livroCaixaId** | **Long**|  |

### Return type

[**ConciliarContaCompletoResponseEntity**](ConciliarContaCompletoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de uma Concilição de Conta |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar9"></a>
# **criar9**
> criar9(inlineObject41)



Requisita criação de um Centro de Custo

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.LivroDeCaixaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    LivroDeCaixaApi apiInstance = new LivroDeCaixaApi(defaultClient);
    InlineObject41 inlineObject41 = new InlineObject41(); // InlineObject41 | 
    try {
      apiInstance.criar9(inlineObject41);
    } catch (ApiException e) {
      System.err.println("Exception when calling LivroDeCaixaApi#criar9");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject41** | [**InlineObject41**](InlineObject41.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Retorna URI do Centro de Custo criado |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar8"></a>
# **deletar8**
> deletar8(id)



Deleta Centro de Custo

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.LivroDeCaixaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    LivroDeCaixaApi apiInstance = new LivroDeCaixaApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      apiInstance.deletar8(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling LivroDeCaixaApi#deletar8");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**404** | Centro de Custo não encontrado |  -  |
**409** | Não foi possível deletar este Centro de Custo, pois ele é referenciado por uma Conta a Pagar/Receber/Livro de Caixa. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="get6"></a>
# **get6**
> ConciliarContaCompletoResponseEntity get6(id)



Solicita uma Concilição de Conta completa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.LivroDeCaixaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    LivroDeCaixaApi apiInstance = new LivroDeCaixaApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      ConciliarContaCompletoResponseEntity result = apiInstance.get6(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling LivroDeCaixaApi#get6");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**ConciliarContaCompletoResponseEntity**](ConciliarContaCompletoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de uma Concilição de Conta |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="importarOFX"></a>
# **importarOFX**
> CodigoConciliarContaResponseEntity importarOFX(arquivo)



Importar arquivo do OFX para Concilição de Conta

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.LivroDeCaixaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    LivroDeCaixaApi apiInstance = new LivroDeCaixaApi(defaultClient);
    List<byte[]> arquivo = null; // List<byte[]> | Arquivo OFX
    try {
      CodigoConciliarContaResponseEntity result = apiInstance.importarOFX(arquivo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling LivroDeCaixaApi#importarOFX");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **arquivo** | [**List&lt;byte[]&gt;**](byte[].md)| Arquivo OFX | [optional]

### Return type

[**CodigoConciliarContaResponseEntity**](CodigoConciliarContaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Código da Concilição de Conta criada |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="index6"></a>
# **index6**
> CentroCustoIndexResponseEntity index6(descricao)



Solicita lista de Centro de Custo, com opção de busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.LivroDeCaixaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    LivroDeCaixaApi apiInstance = new LivroDeCaixaApi(defaultClient);
    String descricao = "descricao_example"; // String | 
    try {
      CentroCustoIndexResponseEntity result = apiInstance.index6(descricao);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling LivroDeCaixaApi#index6");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **descricao** | **String**|  | [optional]

### Return type

[**CentroCustoIndexResponseEntity**](CentroCustoIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de Centro de Custo |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="index7"></a>
# **index7**
> ConciliarContaIndexResponseEntity index7(contaId, dataInicial, dataFinal, pagina)



Solicita lista de Metadados de Concilição de Contas, com opção de busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.LivroDeCaixaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    LivroDeCaixaApi apiInstance = new LivroDeCaixaApi(defaultClient);
    Integer contaId = 56; // Integer | 
    OffsetDateTime dataInicial = 02/01/2019; // OffsetDateTime | Data no formato DD/MM/AAAA
    OffsetDateTime dataFinal = 02/01/2019; // OffsetDateTime | Data no formato DD/MM/AAAA
    Integer pagina = 1; // Integer | 
    try {
      ConciliarContaIndexResponseEntity result = apiInstance.index7(contaId, dataInicial, dataFinal, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling LivroDeCaixaApi#index7");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contaId** | **Integer**|  |
 **dataInicial** | **OffsetDateTime**| Data no formato DD/MM/AAAA | [optional]
 **dataFinal** | **OffsetDateTime**| Data no formato DD/MM/AAAA | [optional]
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**ConciliarContaIndexResponseEntity**](ConciliarContaIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de Metadados de Concilição de Contas |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="referenciarLivroCaixa"></a>
# **referenciarLivroCaixa**
> ConciliarContaCompletoResponseEntity referenciarLivroCaixa(id, conciliarContaItemId, livroCaixaId)



Referencia um Item da Conciliação de Conta para um registro do Livro de Caixa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.LivroDeCaixaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    LivroDeCaixaApi apiInstance = new LivroDeCaixaApi(defaultClient);
    Long id = 56L; // Long | 
    Long conciliarContaItemId = 56L; // Long | 
    Long livroCaixaId = 56L; // Long | 
    try {
      ConciliarContaCompletoResponseEntity result = apiInstance.referenciarLivroCaixa(id, conciliarContaItemId, livroCaixaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling LivroDeCaixaApi#referenciarLivroCaixa");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **conciliarContaItemId** | **Long**|  |
 **livroCaixaId** | **Long**|  |

### Return type

[**ConciliarContaCompletoResponseEntity**](ConciliarContaCompletoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de uma Concilição de Conta |  -  |
**500** | Erro desconhecido no servidor |  -  |

