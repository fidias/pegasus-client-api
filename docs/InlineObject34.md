

# InlineObject34

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**produtoId** | **Integer** |  | 
**quantidade** | **Double** |  | 
**observacaoAdicional** | **String** |  |  [optional]
**preco** | **Double** |  |  [optional]



