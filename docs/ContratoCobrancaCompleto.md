

# ContratoCobrancaCompleto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**dados** | [**Dados**](Dados.md) |  | 
**lista** | [**List&lt;Listum&gt;**](Listum.md) |  | 
**eventos** | [**List&lt;Evento&gt;**](Evento.md) |  | 
**mensagemAuxiliar** | **String** |  |  [optional]



