

# InlineObject6

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**taxaP** | [**DoubleParam**](DoubleParam.md) |  | 
**numParcelasIni** | [**IntegerParam**](IntegerParam.md) |  | 
**numParcelasFim** | [**IntegerParam**](IntegerParam.md) |  | 



