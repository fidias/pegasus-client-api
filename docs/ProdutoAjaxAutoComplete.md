

# ProdutoAjaxAutoComplete

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**value** | **String** |  | 
**codBarra** | **String** |  |  [optional]
**referencia** | **String** |  |  [optional]
**preco** | **Double** |  | 
**lucro** | **Double** |  | 
**custo** | **Double** |  | 
**unidade** | **String** |  | 
**quantidade** | **Double** |  | 
**produtoTipo** | [**ProdutoTipo**](ProdutoTipo.md) |  | 
**custoExtraPercentual** | **Double** |  | 



