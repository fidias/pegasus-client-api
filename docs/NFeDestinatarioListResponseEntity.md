

# NFeDestinatarioListResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**list** | [**List&lt;NFeDestinatarioAutocompleteItem&gt;**](NFeDestinatarioAutocompleteItem.md) |  |  [optional]



