

# NFeConsultarProtocoloResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**list** | [**List&lt;NFeTransmissaoListaItem&gt;**](NFeTransmissaoListaItem.md) |  |  [optional]
**retorno** | [**RetEnvEvento**](RetEnvEvento.md) |  |  [optional]
**schemaXml** | **String** |  |  [optional]



