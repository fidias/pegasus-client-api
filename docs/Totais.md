

# Totais

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalProduto** | **Double** |  | 
**totalServico** | **Double** |  | 
**totalBrutoProduto** | **Double** |  | 
**totalBrutoServico** | **Double** |  | 
**totalLiquido** | **Double** |  | 
**totalBruto** | **Double** |  | 
**variacaoD** | **Double** |  | 
**variacaoProdutoD** | **Double** |  | 
**variacaoServicoD** | **Double** |  | 



