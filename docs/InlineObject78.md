

# InlineObject78

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**titulo** | **String** |  | 
**numero** | [**NumericOnlyParam**](NumericOnlyParam.md) |  | 
**contato** | **String** |  |  [optional]



