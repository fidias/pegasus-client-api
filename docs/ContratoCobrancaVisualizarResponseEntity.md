

# ContratoCobrancaVisualizarResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;ContratoCobrancaVisualizarItem&gt;**](ContratoCobrancaVisualizarItem.md) |  |  [optional]
**zeradas** | [**List&lt;ContratoCobrancaVisualizarItem&gt;**](ContratoCobrancaVisualizarItem.md) |  |  [optional]



