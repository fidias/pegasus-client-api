# ConciliarContaApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cancelarReferenciaLivroCaixa**](ConciliarContaApi.md#cancelarReferenciaLivroCaixa) | **DELETE** /api/livro-caixa/conciliar-conta/{id}/item/{conciliar_conta_item_id}/livro-caixa/{livro_caixa_id} | 
[**get6**](ConciliarContaApi.md#get6) | **GET** /api/livro-caixa/conciliar-conta/{id} | 
[**importarOFX**](ConciliarContaApi.md#importarOFX) | **POST** /api/livro-caixa/conciliar-conta/importar/ofx | 
[**index7**](ConciliarContaApi.md#index7) | **GET** /api/livro-caixa/conciliar-conta | 
[**referenciarLivroCaixa**](ConciliarContaApi.md#referenciarLivroCaixa) | **GET** /api/livro-caixa/conciliar-conta/{id}/item/{conciliar_conta_item_id}/livro-caixa/{livro_caixa_id} | 


<a name="cancelarReferenciaLivroCaixa"></a>
# **cancelarReferenciaLivroCaixa**
> ConciliarContaCompletoResponseEntity cancelarReferenciaLivroCaixa(id, conciliarContaItemId, livroCaixaId)



Solicita cancelamento de referencia um Item da Conciliação de Conta para um registro do Livro de Caixa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ConciliarContaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ConciliarContaApi apiInstance = new ConciliarContaApi(defaultClient);
    Long id = 56L; // Long | 
    Long conciliarContaItemId = 56L; // Long | 
    Long livroCaixaId = 56L; // Long | 
    try {
      ConciliarContaCompletoResponseEntity result = apiInstance.cancelarReferenciaLivroCaixa(id, conciliarContaItemId, livroCaixaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ConciliarContaApi#cancelarReferenciaLivroCaixa");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **conciliarContaItemId** | **Long**|  |
 **livroCaixaId** | **Long**|  |

### Return type

[**ConciliarContaCompletoResponseEntity**](ConciliarContaCompletoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de uma Concilição de Conta |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="get6"></a>
# **get6**
> ConciliarContaCompletoResponseEntity get6(id)



Solicita uma Concilição de Conta completa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ConciliarContaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ConciliarContaApi apiInstance = new ConciliarContaApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      ConciliarContaCompletoResponseEntity result = apiInstance.get6(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ConciliarContaApi#get6");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**ConciliarContaCompletoResponseEntity**](ConciliarContaCompletoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de uma Concilição de Conta |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="importarOFX"></a>
# **importarOFX**
> CodigoConciliarContaResponseEntity importarOFX(arquivo)



Importar arquivo do OFX para Concilição de Conta

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ConciliarContaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ConciliarContaApi apiInstance = new ConciliarContaApi(defaultClient);
    List<byte[]> arquivo = null; // List<byte[]> | Arquivo OFX
    try {
      CodigoConciliarContaResponseEntity result = apiInstance.importarOFX(arquivo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ConciliarContaApi#importarOFX");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **arquivo** | [**List&lt;byte[]&gt;**](byte[].md)| Arquivo OFX | [optional]

### Return type

[**CodigoConciliarContaResponseEntity**](CodigoConciliarContaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Código da Concilição de Conta criada |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="index7"></a>
# **index7**
> ConciliarContaIndexResponseEntity index7(contaId, dataInicial, dataFinal, pagina)



Solicita lista de Metadados de Concilição de Contas, com opção de busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ConciliarContaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ConciliarContaApi apiInstance = new ConciliarContaApi(defaultClient);
    Integer contaId = 56; // Integer | 
    OffsetDateTime dataInicial = 02/01/2019; // OffsetDateTime | Data no formato DD/MM/AAAA
    OffsetDateTime dataFinal = 02/01/2019; // OffsetDateTime | Data no formato DD/MM/AAAA
    Integer pagina = 1; // Integer | 
    try {
      ConciliarContaIndexResponseEntity result = apiInstance.index7(contaId, dataInicial, dataFinal, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ConciliarContaApi#index7");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contaId** | **Integer**|  |
 **dataInicial** | **OffsetDateTime**| Data no formato DD/MM/AAAA | [optional]
 **dataFinal** | **OffsetDateTime**| Data no formato DD/MM/AAAA | [optional]
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**ConciliarContaIndexResponseEntity**](ConciliarContaIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de Metadados de Concilição de Contas |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="referenciarLivroCaixa"></a>
# **referenciarLivroCaixa**
> ConciliarContaCompletoResponseEntity referenciarLivroCaixa(id, conciliarContaItemId, livroCaixaId)



Referencia um Item da Conciliação de Conta para um registro do Livro de Caixa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ConciliarContaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ConciliarContaApi apiInstance = new ConciliarContaApi(defaultClient);
    Long id = 56L; // Long | 
    Long conciliarContaItemId = 56L; // Long | 
    Long livroCaixaId = 56L; // Long | 
    try {
      ConciliarContaCompletoResponseEntity result = apiInstance.referenciarLivroCaixa(id, conciliarContaItemId, livroCaixaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ConciliarContaApi#referenciarLivroCaixa");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **conciliarContaItemId** | **Long**|  |
 **livroCaixaId** | **Long**|  |

### Return type

[**ConciliarContaCompletoResponseEntity**](ConciliarContaCompletoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de uma Concilição de Conta |  -  |
**500** | Erro desconhecido no servidor |  -  |

