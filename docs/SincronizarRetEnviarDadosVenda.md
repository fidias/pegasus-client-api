

# SincronizarRetEnviarDadosVenda

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienteVendaId** | **Long** |  | 
**numeroSessao** | **Long** |  | 
**codigoRetorno** | **String** |  | 
**mensagem** | **String** |  | 
**codigoReferencia** | **String** |  |  [optional]
**mensagemSefaz** | **String** |  |  [optional]
**arquivoCfe** | **String** |  |  [optional]
**dataHoraEmissao** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**chaveConsulta** | **String** |  |  [optional]
**valorTotalCfe** | **Double** |  |  [optional]
**cpfCnpjAdquirente** | **Long** |  |  [optional]
**assinaturaQrcode** | **String** |  |  [optional]
**dataHoraCriacao** | [**OffsetDateTime**](OffsetDateTime.md) |  | 



