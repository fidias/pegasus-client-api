

# OperacaoFinanceiraAutocompleteResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;CfopOperacaoFinanceiraItemAutocomplete&gt;**](CfopOperacaoFinanceiraItemAutocomplete.md) |  |  [optional]



