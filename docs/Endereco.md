

# Endereco

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**titulo** | **String** |  | 
**logradouro** | **String** |  | 
**cep** | **String** |  | 
**pontoReferencia** | **String** |  |  [optional]
**complemento** | **String** |  |  [optional]
**bairro** | [**Bairro**](Bairro.md) |  | 
**municipio** | [**Municipio**](Municipio.md) |  | 
**uf** | [**Uf**](Uf.md) |  | 
**numero** | **String** |  | 



