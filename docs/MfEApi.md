# MfEApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**gerarAssinaturaAC**](MfEApi.md#gerarAssinaturaAC) | **POST** /api/mfe/aux-config/gerar-assinatura-ac | 
[**gerarGUID**](MfEApi.md#gerarGUID) | **POST** /api/mfe/aux-config/gerar-guid | 


<a name="gerarAssinaturaAC"></a>
# **gerarAssinaturaAC**
> gerarAssinaturaAC(cnpjEstabelecimento)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MfEApi apiInstance = new MfEApi(defaultClient);
    String cnpjEstabelecimento = "cnpjEstabelecimento_example"; // String | 
    try {
      apiInstance.gerarAssinaturaAC(cnpjEstabelecimento);
    } catch (ApiException e) {
      System.err.println("Exception when calling MfEApi#gerarAssinaturaAC");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cnpjEstabelecimento** | **String**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="gerarGUID"></a>
# **gerarGUID**
> gerarGUID(cnpjEmitente, cnpjAdquirente)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MfEApi apiInstance = new MfEApi(defaultClient);
    String cnpjEmitente = "cnpjEmitente_example"; // String | 
    String cnpjAdquirente = "cnpjAdquirente_example"; // String | 
    try {
      apiInstance.gerarGUID(cnpjEmitente, cnpjAdquirente);
    } catch (ApiException e) {
      System.err.println("Exception when calling MfEApi#gerarGUID");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cnpjEmitente** | **String**|  | [optional]
 **cnpjAdquirente** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

