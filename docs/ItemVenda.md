

# ItemVenda

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idProduto** | **Long** |  | 
**tipoId** | **Integer** |  | 
**descricao** | **String** |  | 
**idNcm** | **Long** |  |  [optional]
**codigoNcm** | **String** |  |  [optional]
**quantidade** | **Double** |  | 
**preco** | **Double** |  |  [optional]
**codigoBarra** | **String** |  |  [optional]
**pafDescDinheiro** | **Double** |  |  [optional]
**pafAcrescDinheiro** | **Double** |  |  [optional]
**cfop** | [**Cfop**](Cfop.md) |  |  [optional]
**pafUnidade** | [**PafUnidade**](PafUnidade.md) |  | 
**cest** | [**Cest**](Cest.md) |  |  [optional]
**impostoICMS** | **String** | Valor do imposto ICMS em JSON |  [optional]
**impostoPIS** | **String** | Valor do imposto PIS em JSON |  [optional]
**impostoCOFINS** | **String** | Valor do imposto COFINS em JSON |  [optional]
**totalAproximadoTributos** | **Double** |  |  [optional]
**iat** | **String** |  | 
**pafCasaDecimalQuant** | **Integer** |  | 
**pafCasaDecimalValor** | **Integer** |  | 



