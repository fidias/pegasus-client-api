

# InlineObject100

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cfopSaidaMesmoEstado** | [**IntegerParam**](IntegerParam.md) |  |  [optional]
**cfopSaidaInterestadual** | [**IntegerParam**](IntegerParam.md) |  |  [optional]



