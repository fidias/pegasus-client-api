# DavApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**adicionar**](DavApi.md#adicionar) | **POST** /api/dav/{dav_id}/item/{ordem_item}/responsavel | 
[**ajustarStatusDavJaFaturado**](DavApi.md#ajustarStatusDavJaFaturado) | **GET** /api/dav/ajustar-dav-faturado | 
[**alternarHabilitada**](DavApi.md#alternarHabilitada) | **PUT** /api/dav/config/impressao-comanda/{grupo_mercadoria_id}/alternar-habilitada | 
[**alternarOrcamento**](DavApi.md#alternarOrcamento) | **PUT** /api/dav/{id}/alternar-orcamento | 
[**aplicarVariacaoPercentual**](DavApi.md#aplicarVariacaoPercentual) | **PUT** /api/dav/{id}/variacao-percentual | 
[**aplicarVariacaoPercentual1**](DavApi.md#aplicarVariacaoPercentual1) | **PUT** /api/dav/{dav_id}/item/{ordem_item}/variacao-percentual | 
[**arquivar**](DavApi.md#arquivar) | **PUT** /api/dav/{id}/arquivar | 
[**atualizar4**](DavApi.md#atualizar4) | **PUT** /api/dav/{id} | 
[**atualizar5**](DavApi.md#atualizar5) | **PUT** /api/dav/{dav_id}/item/{ordem_item} | 
[**atualizarVeiculo**](DavApi.md#atualizarVeiculo) | **PUT** /api/dav/{dav_id}/objeto/veiculo | 
[**autocomplete3**](DavApi.md#autocomplete3) | **GET** /api/dav/produto-servico/autocomplete | 
[**autocomplete4**](DavApi.md#autocomplete4) | **GET** /api/dav/marca/autocomplete | 
[**autocomplete5**](DavApi.md#autocomplete5) | **GET** /api/dav/objeto/autocomplete | 
[**cancelar**](DavApi.md#cancelar) | **PUT** /api/dav/{dav_id}/item/{ordem_item}/cancelar | 
[**carregar5**](DavApi.md#carregar5) | **GET** /api/dav/config/impressao | 
[**carregar6**](DavApi.md#carregar6) | **GET** /api/dav/{dav_id}/objeto/{objeto_id} | 
[**criar7**](DavApi.md#criar7) | **POST** /api/dav | 
[**criar8**](DavApi.md#criar8) | **POST** /api/dav/{dav_id}/item | 
[**criarVeiculo**](DavApi.md#criarVeiculo) | **POST** /api/dav/{dav_id}/objeto/veiculo | 
[**davImpressaoManual**](DavApi.md#davImpressaoManual) | **GET** /api/dav/{id}/impressao-manual | 
[**definirObjeto**](DavApi.md#definirObjeto) | **POST** /api/dav/{id}/definir-objeto | 
[**deletar5**](DavApi.md#deletar5) | **DELETE** /api/dav/config/impressao-comanda/{grupo_mercadoria_id} | 
[**deletar6**](DavApi.md#deletar6) | **DELETE** /api/dav/{dav_id}/item/{ordem_item}/responsavel/{funcionario_id} | 
[**gerarProducao**](DavApi.md#gerarProducao) | **GET** /api/dav/{id}/gerar-producao | 
[**get5**](DavApi.md#get5) | **GET** /api/dav/{id} | 
[**listaTempoReal**](DavApi.md#listaTempoReal) | **GET** /api/dav/tempo-real | 
[**listarImpressorasDisponiveis**](DavApi.md#listarImpressorasDisponiveis) | **GET** /api/dav/config/impressao-comanda/impressoras-disponiveis | 
[**modificarSituacao**](DavApi.md#modificarSituacao) | **PUT** /api/dav/{id}/modificar-situacao | 
[**remover**](DavApi.md#remover) | **PUT** /api/dav/{dav_id}/item/{ordem_item}/remover | 
[**salvar4**](DavApi.md#salvar4) | **POST** /api/dav/config/impressao | 
[**salvar5**](DavApi.md#salvar5) | **POST** /api/dav/config/impressao-comanda | 
[**salvarObservacaoAdicional**](DavApi.md#salvarObservacaoAdicional) | **PUT** /api/dav/{dav_id}/item/{ordem_item}/observacao-adicional | 


<a name="adicionar"></a>
# **adicionar**
> DavCompleto adicionar(davId, ordemItem, funcionarioId)



Requisita adição de Responsável ao item de um DAV

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    DavApi apiInstance = new DavApi(defaultClient);
    Long davId = 56L; // Long | 
    Integer ordemItem = 56; // Integer | 
    Integer funcionarioId = 56; // Integer | 
    try {
      DavCompleto result = apiInstance.adicionar(davId, ordemItem, funcionarioId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#adicionar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **davId** | **Long**|  |
 **ordemItem** | **Integer**|  |
 **funcionarioId** | **Integer**|  | [optional]

### Return type

[**DavCompleto**](DavCompleto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Dados completos de um DAV |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="ajustarStatusDavJaFaturado"></a>
# **ajustarStatusDavJaFaturado**
> ajustarStatusDavJaFaturado()



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    try {
      apiInstance.ajustarStatusDavJaFaturado();
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#ajustarStatusDavJaFaturado");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | Erro desconhecido no servidor |  -  |

<a name="alternarHabilitada"></a>
# **alternarHabilitada**
> ListarConfigComandaResponseEntity alternarHabilitada(grupoMercadoriaId)



Alterna uma impressora relacionada a um Grupo de Mercadoria, habilitada ou desabilitada

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    IntegerParam grupoMercadoriaId = new IntegerParam(); // IntegerParam | 
    try {
      ListarConfigComandaResponseEntity result = apiInstance.alternarHabilitada(grupoMercadoriaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#alternarHabilitada");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **grupoMercadoriaId** | [**IntegerParam**](.md)|  |

### Return type

[**ListarConfigComandaResponseEntity**](ListarConfigComandaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Indica que a configuração foi alternada com sucesso |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="alternarOrcamento"></a>
# **alternarOrcamento**
> DavCompleto alternarOrcamento(id, isOrcamento)



Requisita alternar flag para identificar se um DAV é Orçamento

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Long id = 56L; // Long | 
    Boolean isOrcamento = true; // Boolean | Flag para a qual o DAV será modificado para Orçamento ou não
    try {
      DavCompleto result = apiInstance.alternarOrcamento(id, isOrcamento);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#alternarOrcamento");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **isOrcamento** | **Boolean**| Flag para a qual o DAV será modificado para Orçamento ou não | [optional]

### Return type

[**DavCompleto**](DavCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados simplificados do Dav |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="aplicarVariacaoPercentual"></a>
# **aplicarVariacaoPercentual**
> DavCompleto aplicarVariacaoPercentual(id, operacao, valorTextual)



Requisita aplicação da variação percentual - acréscimo ou desconto - de um DAV, que será diluído entre os itens

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Long id = 56L; // Long | 
    String operacao = "operacao_example"; // String | Define em qual dos totais a variação percentual será realizada
    String valorTextual = "valorTextual_example"; // String | 
    try {
      DavCompleto result = apiInstance.aplicarVariacaoPercentual(id, operacao, valorTextual);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#aplicarVariacaoPercentual");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **operacao** | **String**| Define em qual dos totais a variação percentual será realizada | [enum: total, produtos, servicos]
 **valorTextual** | **String**|  |

### Return type

[**DavCompleto**](DavCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um DAV |  -  |
**404** | Registro não encontrado  * DAV não informado. |  -  |
**412** | Pré-condição não satisfeita  * DAV &#39;&#39;{0}&#39;&#39; já foi arquivado e não pode ser alterado. * DAV &#39;&#39;{0}&#39;&#39; não pode ser alterado, pois já foi Faturado ou está Aguardando Faturamento. * Operação não reconhecida * DAV com total igual a zero. * Descontos / Acréscimo de 100% não são permitidos. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="aplicarVariacaoPercentual1"></a>
# **aplicarVariacaoPercentual1**
> DavCompleto aplicarVariacaoPercentual1(davId, ordemItem, valorTextual)



Requisita aplicação da variação percentual - acréscimo ou desconto - em um item do DAV

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Long davId = 56L; // Long | 
    Integer ordemItem = 56; // Integer | 
    String valorTextual = "valorTextual_example"; // String | 
    try {
      DavCompleto result = apiInstance.aplicarVariacaoPercentual1(davId, ordemItem, valorTextual);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#aplicarVariacaoPercentual1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **davId** | **Long**|  |
 **ordemItem** | **Integer**|  |
 **valorTextual** | **String**|  |

### Return type

[**DavCompleto**](DavCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um DAV |  -  |
**404** | Registro não encontrado  * DAV não informado. |  -  |
**412** | Pré-condição não satisfeita  * DAV &#39;&#39;{0}&#39;&#39; já foi arquivado e não pode ser alterado. * DAV &#39;&#39;{0}&#39;&#39; não pode ser alterado, pois já foi Faturado ou está Aguardando Faturamento. * Operação não reconhecida * Descontos / Acréscimo de 100% não são permitidos. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="arquivar"></a>
# **arquivar**
> arquivar(id, motivo)



Requisita arquivamento de um DAV, Em andamento ou Aguardando Faturamento.

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Long id = 56L; // Long | 
    String motivo = "motivo_example"; // String | Status para o qual será modificado
    try {
      apiInstance.arquivar(id, motivo);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#arquivar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **motivo** | **String**| Status para o qual será modificado |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="atualizar4"></a>
# **atualizar4**
> CodigoDavResponseEntity atualizar4(id, clienteId, davTipo, dataEntrega, horaEntrega, dataValidade, comissionarioId, observacao, enderecoId, ignorarQtdDescPromo)



Requisita atualização de um DAV

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Long id = 56L; // Long | 
    Integer clienteId = 56; // Integer | 
    String davTipo = "davTipo_example"; // String | 
    String dataEntrega = "dataEntrega_example"; // String | Data no formato DD/MM/AAAA
    String horaEntrega = "horaEntrega_example"; // String | Horário no formato HH:MM
    String dataValidade = "dataValidade_example"; // String | Data no formato DD/MM/AAAA
    Long comissionarioId = 56L; // Long | 
    String observacao = "observacao_example"; // String | 
    Long enderecoId = 56L; // Long | 
    Boolean ignorarQtdDescPromo = false; // Boolean | 
    try {
      CodigoDavResponseEntity result = apiInstance.atualizar4(id, clienteId, davTipo, dataEntrega, horaEntrega, dataValidade, comissionarioId, observacao, enderecoId, ignorarQtdDescPromo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#atualizar4");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **clienteId** | **Integer**|  |
 **davTipo** | **String**|  | [enum: PE, OS, OC, PC, DAV]
 **dataEntrega** | **String**| Data no formato DD/MM/AAAA | [optional]
 **horaEntrega** | **String**| Horário no formato HH:MM | [optional]
 **dataValidade** | **String**| Data no formato DD/MM/AAAA | [optional]
 **comissionarioId** | **Long**|  | [optional]
 **observacao** | **String**|  | [optional]
 **enderecoId** | **Long**|  | [optional]
 **ignorarQtdDescPromo** | **Boolean**|  | [optional] [default to false]

### Return type

[**CodigoDavResponseEntity**](CodigoDavResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Código do DAV atualizado |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="atualizar5"></a>
# **atualizar5**
> DavCompleto atualizar5(ordemItem, davId, inlineObject34)



Requisita edição de item de um DAV

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Integer ordemItem = 56; // Integer | 
    Long davId = 56L; // Long | 
    InlineObject34 inlineObject34 = new InlineObject34(); // InlineObject34 | 
    try {
      DavCompleto result = apiInstance.atualizar5(ordemItem, davId, inlineObject34);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#atualizar5");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ordemItem** | **Integer**|  |
 **davId** | **Long**|  |
 **inlineObject34** | [**InlineObject34**](InlineObject34.md)|  | [optional]

### Return type

[**DavCompleto**](DavCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um DAV |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="atualizarVeiculo"></a>
# **atualizarVeiculo**
> ObjetoResponseEntity atualizarVeiculo(davId, placa, marcaId, objetoId, modelo, anoFabricacao, renavam, infoExtra)



Requisita atualização de Objeto Veículo

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Long davId = 56L; // Long | 
    String placa = "placa_example"; // String | 
    Integer marcaId = 56; // Integer | 
    Long objetoId = 56L; // Long | 
    String modelo = "modelo_example"; // String | 
    String anoFabricacao = "anoFabricacao_example"; // String | 
    String renavam = "renavam_example"; // String | 
    String infoExtra = "infoExtra_example"; // String | 
    try {
      ObjetoResponseEntity result = apiInstance.atualizarVeiculo(davId, placa, marcaId, objetoId, modelo, anoFabricacao, renavam, infoExtra);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#atualizarVeiculo");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **davId** | **Long**|  |
 **placa** | **String**|  |
 **marcaId** | **Integer**|  |
 **objetoId** | **Long**|  | [optional]
 **modelo** | **String**|  | [optional]
 **anoFabricacao** | **String**|  | [optional]
 **renavam** | **String**|  | [optional]
 **infoExtra** | **String**|  | [optional]

### Return type

[**ObjetoResponseEntity**](ObjetoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna dados de um objeto |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="autocomplete3"></a>
# **autocomplete3**
> ProdutoServicoAutocompleteResponseEntity autocomplete3(termo, produtoTabelaPrecoId)



Solicita lista de produtos/serviços que contenham o termo solicitado na descrição, código do produto ou código de barras

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    String termo = "termo_example"; // String | 
    Integer produtoTabelaPrecoId = 0; // Integer | 
    try {
      ProdutoServicoAutocompleteResponseEntity result = apiInstance.autocomplete3(termo, produtoTabelaPrecoId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#autocomplete3");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  |
 **produtoTabelaPrecoId** | **Integer**|  | [optional] [default to 0]

### Return type

[**ProdutoServicoAutocompleteResponseEntity**](ProdutoServicoAutocompleteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de produtos/serviços |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="autocomplete4"></a>
# **autocomplete4**
> MarcaAutocompleteResponseEntity autocomplete4(termo)



Solicita lista de marcas que contenham o termo solicitado

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    String termo = "termo_example"; // String | 
    try {
      MarcaAutocompleteResponseEntity result = apiInstance.autocomplete4(termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#autocomplete4");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  |

### Return type

[**MarcaAutocompleteResponseEntity**](MarcaAutocompleteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de marcas |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="autocomplete5"></a>
# **autocomplete5**
> ObjetoAutocompleteResponseEntity autocomplete5(termo, objetoTipo)



Solicita lista de objetos que contenham o termo solicitado na descrição

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    String termo = "termo_example"; // String | 
    String objetoTipo = "objetoTipo_example"; // String | 
    try {
      ObjetoAutocompleteResponseEntity result = apiInstance.autocomplete5(termo, objetoTipo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#autocomplete5");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  |
 **objetoTipo** | **String**|  | [enum: veiculo, outro]

### Return type

[**ObjetoAutocompleteResponseEntity**](ObjetoAutocompleteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de objetos |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="cancelar"></a>
# **cancelar**
> DavCompleto cancelar(davId, ordemItem)



Requisita cancelamento de item de um DAV

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Long davId = 56L; // Long | 
    Integer ordemItem = 56; // Integer | 
    try {
      DavCompleto result = apiInstance.cancelar(davId, ordemItem);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#cancelar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **davId** | **Long**|  |
 **ordemItem** | **Integer**|  |

### Return type

[**DavCompleto**](DavCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um DAV |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="carregar5"></a>
# **carregar5**
> DAVConfiguracaoResponseEntity carregar5()



Retorna conjunto de configurações relacionadas a DAV - Impressão

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    try {
      DAVConfiguracaoResponseEntity result = apiInstance.carregar5();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#carregar5");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**DAVConfiguracaoResponseEntity**](DAVConfiguracaoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Conjunto de configurações de DAV - Impressão |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="carregar6"></a>
# **carregar6**
> ObjetoResponseEntity carregar6(davId, objetoId)



Solicita dados de um objeto

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Long davId = 56L; // Long | 
    Long objetoId = 56L; // Long | 
    try {
      ObjetoResponseEntity result = apiInstance.carregar6(davId, objetoId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#carregar6");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **davId** | **Long**|  |
 **objetoId** | **Long**|  |

### Return type

[**ObjetoResponseEntity**](ObjetoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna dados de um objeto |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar7"></a>
# **criar7**
> CodigoDavResponseEntity criar7(clienteId, davTipo, dataEntrega, horaEntrega, dataValidade, comissionarioId, observacao, enderecoId, ignorarQtdDescPromo)



Requisita criação de um DAV

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Integer clienteId = 56; // Integer | 
    String davTipo = "davTipo_example"; // String | 
    String dataEntrega = "dataEntrega_example"; // String | Data no formato DD/MM/AAAA
    String horaEntrega = "horaEntrega_example"; // String | Horário no formato HH:MM
    String dataValidade = "dataValidade_example"; // String | Data no formato DD/MM/AAAA
    Long comissionarioId = 56L; // Long | 
    String observacao = "observacao_example"; // String | 
    Long enderecoId = 56L; // Long | 
    Boolean ignorarQtdDescPromo = false; // Boolean | 
    try {
      CodigoDavResponseEntity result = apiInstance.criar7(clienteId, davTipo, dataEntrega, horaEntrega, dataValidade, comissionarioId, observacao, enderecoId, ignorarQtdDescPromo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#criar7");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clienteId** | **Integer**|  |
 **davTipo** | **String**|  | [enum: PE, OS, OC, PC, DAV]
 **dataEntrega** | **String**| Data no formato DD/MM/AAAA | [optional]
 **horaEntrega** | **String**| Horário no formato HH:MM | [optional]
 **dataValidade** | **String**| Data no formato DD/MM/AAAA | [optional]
 **comissionarioId** | **Long**|  | [optional]
 **observacao** | **String**|  | [optional]
 **enderecoId** | **Long**|  | [optional]
 **ignorarQtdDescPromo** | **Boolean**|  | [optional] [default to false]

### Return type

[**CodigoDavResponseEntity**](CodigoDavResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Código do DAV criado |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar8"></a>
# **criar8**
> DavCompleto criar8(davId, produtoId, quantidade, observacaoAdicional, preco)



Requisita adição de item a um DAV

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Long davId = 56L; // Long | 
    Integer produtoId = 56; // Integer | 
    Double quantidade = 3.4D; // Double | 
    String observacaoAdicional = "observacaoAdicional_example"; // String | 
    Double preco = 3.4D; // Double | 
    try {
      DavCompleto result = apiInstance.criar8(davId, produtoId, quantidade, observacaoAdicional, preco);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#criar8");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **davId** | **Long**|  |
 **produtoId** | **Integer**|  |
 **quantidade** | **Double**|  |
 **observacaoAdicional** | **String**|  | [optional]
 **preco** | **Double**|  | [optional]

### Return type

[**DavCompleto**](DavCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Dados completos de um DAV |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criarVeiculo"></a>
# **criarVeiculo**
> ObjetoResponseEntity criarVeiculo(davId, placa, marcaId, objetoId, modelo, anoFabricacao, renavam, infoExtra)



Requisita criação e relação de Objeto Veículo

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Long davId = 56L; // Long | 
    String placa = "placa_example"; // String | 
    Integer marcaId = 56; // Integer | 
    Long objetoId = 56L; // Long | 
    String modelo = "modelo_example"; // String | 
    String anoFabricacao = "anoFabricacao_example"; // String | 
    String renavam = "renavam_example"; // String | 
    String infoExtra = "infoExtra_example"; // String | 
    try {
      ObjetoResponseEntity result = apiInstance.criarVeiculo(davId, placa, marcaId, objetoId, modelo, anoFabricacao, renavam, infoExtra);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#criarVeiculo");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **davId** | **Long**|  |
 **placa** | **String**|  |
 **marcaId** | **Integer**|  |
 **objetoId** | **Long**|  | [optional]
 **modelo** | **String**|  | [optional]
 **anoFabricacao** | **String**|  | [optional]
 **renavam** | **String**|  | [optional]
 **infoExtra** | **String**|  | [optional]

### Return type

[**ObjetoResponseEntity**](ObjetoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Retorna dados de um objeto |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="davImpressaoManual"></a>
# **davImpressaoManual**
> davImpressaoManual(id)



Requisita alternar flag para identificar se um DAV é Orçamento

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      apiInstance.davImpressaoManual(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#davImpressaoManual");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="definirObjeto"></a>
# **definirObjeto**
> ObjetoResponseEntity definirObjeto(id, objetoId)



Requisita associação de Objeto com DAV

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Long id = 56L; // Long | 
    Long objetoId = 56L; // Long | 
    try {
      ObjetoResponseEntity result = apiInstance.definirObjeto(id, objetoId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#definirObjeto");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **objetoId** | **Long**|  | [optional]

### Return type

[**ObjetoResponseEntity**](ObjetoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados do Objeto |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar5"></a>
# **deletar5**
> ListarConfigComandaResponseEntity deletar5(grupoMercadoriaId)



Deleta configurações de uma impressora relacionada a um Grupo de Mercadoria

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    IntegerParam grupoMercadoriaId = new IntegerParam(); // IntegerParam | 
    try {
      ListarConfigComandaResponseEntity result = apiInstance.deletar5(grupoMercadoriaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#deletar5");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **grupoMercadoriaId** | [**IntegerParam**](.md)|  |

### Return type

[**ListarConfigComandaResponseEntity**](ListarConfigComandaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Indicar que a configuração do Grupo de Mercadoria foi deletada com sucesso |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar6"></a>
# **deletar6**
> DavCompleto deletar6(davId, ordemItem, funcionarioId)



Requisita remoção de Responsável ao item de um DAV

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    DavApi apiInstance = new DavApi(defaultClient);
    Long davId = 56L; // Long | 
    Integer ordemItem = 56; // Integer | 
    Integer funcionarioId = 56; // Integer | 
    try {
      DavCompleto result = apiInstance.deletar6(davId, ordemItem, funcionarioId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#deletar6");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **davId** | **Long**|  |
 **ordemItem** | **Integer**|  |
 **funcionarioId** | **Integer**|  |

### Return type

[**DavCompleto**](DavCompleto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um DAV |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="gerarProducao"></a>
# **gerarProducao**
> DavCompleto gerarProducao(id)



Solicita a geração de uma Produção a partir de um DAV

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      DavCompleto result = apiInstance.gerarProducao(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#gerarProducao");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**DavCompleto**](DavCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um DAV |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="get5"></a>
# **get5**
> DavCompleto get5(id)



Solicita um DAV

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      DavCompleto result = apiInstance.get5(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#get5");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**DavCompleto**](DavCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um DAV |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="listaTempoReal"></a>
# **listaTempoReal**
> DAVTempoRealResponseEntity listaTempoReal(davTipo, buscarPor, termo, isOrcamento, opcoesArquivamento)



Solicita lista de DAV, com opção de busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    String davTipo = "davTipo_example"; // String | 
    String buscarPor = "buscarPor_example"; // String | 
    String termo = "termo_example"; // String | 
    Boolean isOrcamento = false; // Boolean | 
    String opcoesArquivamento = "somente_nao_arquivados"; // String | 
    try {
      DAVTempoRealResponseEntity result = apiInstance.listaTempoReal(davTipo, buscarPor, termo, isOrcamento, opcoesArquivamento);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#listaTempoReal");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **davTipo** | **String**|  | [enum: PE, OS, OC, PC, DAV]
 **buscarPor** | **String**|  | [optional] [enum: cliente, codigo_dav, objeto, produto_id]
 **termo** | **String**|  | [optional]
 **isOrcamento** | **Boolean**|  | [optional] [default to false]
 **opcoesArquivamento** | **String**|  | [optional] [default to somente_nao_arquivados] [enum: todos, somente_arquivados, somente_nao_arquivados]

### Return type

[**DAVTempoRealResponseEntity**](DAVTempoRealResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de DAV |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="listarImpressorasDisponiveis"></a>
# **listarImpressorasDisponiveis**
> ListaImpressorasResponseEntity listarImpressorasDisponiveis()



Solicita lista de impressoras disponíveis através do spooler do Windows

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    try {
      ListaImpressorasResponseEntity result = apiInstance.listarImpressorasDisponiveis();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#listarImpressorasDisponiveis");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ListaImpressorasResponseEntity**](ListaImpressorasResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de impressoras disponíveis, instaladas no servidor |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="modificarSituacao"></a>
# **modificarSituacao**
> DavCompleto modificarSituacao(id, newStatusId)



Requisita modificação da Situação de um DAV, seja para Em andamento ou Aguardando faturamento

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Long id = 56L; // Long | 
    Integer newStatusId = 56; // Integer | Status para o qual será modificado
    try {
      DavCompleto result = apiInstance.modificarSituacao(id, newStatusId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#modificarSituacao");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **newStatusId** | **Integer**| Status para o qual será modificado | [optional]

### Return type

[**DavCompleto**](DavCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados simplificados do Dav |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="remover"></a>
# **remover**
> DavCompleto remover(davId, ordemItem)



Requisita remoção de item de um DAV

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Long davId = 56L; // Long | 
    Integer ordemItem = 56; // Integer | 
    try {
      DavCompleto result = apiInstance.remover(davId, ordemItem);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#remover");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **davId** | **Long**|  |
 **ordemItem** | **Integer**|  |

### Return type

[**DavCompleto**](DavCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um DAV |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar4"></a>
# **salvar4**
> salvar4(modoItem, mensagemCabecalho1, mensagemCabecalho2)



Salva conjunto de configurações do DAV - Impressão

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    String modoItem = "condensado"; // String | 
    String mensagemCabecalho1 = "mensagemCabecalho1_example"; // String | 
    String mensagemCabecalho2 = "mensagemCabecalho2_example"; // String | 
    try {
      apiInstance.salvar4(modoItem, mensagemCabecalho1, mensagemCabecalho2);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#salvar4");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **modoItem** | **String**|  | [optional] [default to condensado] [enum: condensado, expandido_preco_liquido]
 **mensagemCabecalho1** | **String**|  | [optional]
 **mensagemCabecalho2** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar5"></a>
# **salvar5**
> ListarConfigComandaResponseEntity salvar5(grupoMercadoriaId, nomeImpressora, espacamentoVertical, ecfModeloId, impressaoAutomatica)



Salva configurações de uma impressora relacionada a um Grupo de Mercadoria

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    IntegerParam grupoMercadoriaId = new IntegerParam(); // IntegerParam | 
    String nomeImpressora = "nomeImpressora_example"; // String | 
    IntegerParam espacamentoVertical = new IntegerParam(); // IntegerParam | 
    IntegerParam ecfModeloId = new IntegerParam(); // IntegerParam | 
    Boolean impressaoAutomatica = true; // Boolean | 
    try {
      ListarConfigComandaResponseEntity result = apiInstance.salvar5(grupoMercadoriaId, nomeImpressora, espacamentoVertical, ecfModeloId, impressaoAutomatica);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#salvar5");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **grupoMercadoriaId** | [**IntegerParam**](IntegerParam.md)|  |
 **nomeImpressora** | **String**|  |
 **espacamentoVertical** | [**IntegerParam**](IntegerParam.md)|  |
 **ecfModeloId** | [**IntegerParam**](IntegerParam.md)|  |
 **impressaoAutomatica** | **Boolean**|  | [default to true]

### Return type

[**ListarConfigComandaResponseEntity**](ListarConfigComandaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Indica que as configurações foram atualizadas com sucesso |  -  |
**201** | Indica que as configurações foram criadas com sucesso |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvarObservacaoAdicional"></a>
# **salvarObservacaoAdicional**
> DavCompleto salvarObservacaoAdicional(davId, ordemItem, observacaoAdicional)



Requisita atualizar a observação adicional do item

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DavApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DavApi apiInstance = new DavApi(defaultClient);
    Long davId = 56L; // Long | 
    Integer ordemItem = 56; // Integer | 
    String observacaoAdicional = "observacaoAdicional_example"; // String | 
    try {
      DavCompleto result = apiInstance.salvarObservacaoAdicional(davId, ordemItem, observacaoAdicional);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DavApi#salvarObservacaoAdicional");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **davId** | **Long**|  |
 **ordemItem** | **Integer**|  |
 **observacaoAdicional** | **String**|  |

### Return type

[**DavCompleto**](DavCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um DAV |  -  |
**404** | Registro não encontrado  * DAV não informado. |  -  |
**412** | Pré-condição não satisfeita  * DAV &#39;&#39;{0}&#39;&#39; já foi arquivado e não pode ser alterado. * DAV &#39;&#39;{0}&#39;&#39; não pode ser alterado, pois já foi Faturado ou está Aguardando Faturamento. * Operação não reconhecida |  -  |
**500** | Erro desconhecido no servidor |  -  |

