

# GrupoMercadoriaItemLista

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idgrupo** | **Long** |  | 
**nome** | **String** |  | 
**isAtivo** | **Boolean** |  | 



