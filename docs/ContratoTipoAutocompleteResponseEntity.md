

# ContratoTipoAutocompleteResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;ContratoTipoAutocompleteItem&gt;**](ContratoTipoAutocompleteItem.md) |  |  [optional]



