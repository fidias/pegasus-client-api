

# InlineObject50

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**funcionarioId** | [**IntegerParam**](IntegerParam.md) |  | 
**dataHoraEvento** | [**OffsetDateTime**](OffsetDateTime.md) | Data no formato DD/MM/AAAA HH:MM:SS | 



