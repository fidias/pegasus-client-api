

# ValoresExtra

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**historico** | **String** |  | 
**valor** | **Double** |  | 
**dataCriacao** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**criadoPor** | [**CriadoPor1**](CriadoPor1.md) |  | 



