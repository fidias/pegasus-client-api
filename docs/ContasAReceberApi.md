# ContasAReceberApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**atualizar3**](ContasAReceberApi.md#atualizar3) | **PUT** /api/contas_receber/financiamento/{id} | 
[**carregar3**](ContasAReceberApi.md#carregar3) | **GET** /api/contas_receber/financiamento/configuracao | 
[**criar5**](ContasAReceberApi.md#criar5) | **POST** /api/contas_receber/financiamento | 
[**criarValorExtra**](ContasAReceberApi.md#criarValorExtra) | **POST** /api/contas_receber/financiamento/{financiamento_id}/valor-extra | 
[**deletar4**](ContasAReceberApi.md#deletar4) | **DELETE** /api/contas_receber/financiamento/{id} | 
[**deletarValorExtra**](ContasAReceberApi.md#deletarValorExtra) | **DELETE** /api/contas_receber/financiamento/{financiamento_id}/valor-extra/{id} | 
[**gerar**](ContasAReceberApi.md#gerar) | **GET** /api/contas_receber/financiamento/{id}/gerar | 
[**get3**](ContasAReceberApi.md#get3) | **GET** /api/contas_receber/financiamento/{id} | 
[**index2**](ContasAReceberApi.md#index2) | **GET** /api/contas_receber/financiamento | 
[**salvar2**](ContasAReceberApi.md#salvar2) | **POST** /api/contas_receber/financiamento/configuracao | 
[**salvarIndicadorParcelaJuros**](ContasAReceberApi.md#salvarIndicadorParcelaJuros) | **PUT** /api/contas_receber/financiamento/{financiamento_id}/parcela/{conta_receber_id}/indicador-juros | 


<a name="atualizar3"></a>
# **atualizar3**
> FinanciamentoCompleto atualizar3(id, clienteId, descricaoObjeto, totalASerFinanciado, qtdParcelas, valorParcela, margemPercentual, totalFinanciamento, dataPrimeiraParcela, diaParaVencimento, entradaContaId, entradaLivroCaixaFormaPagamentoId, saidaContaId, saidaLivroCaixaFormaPagamentoId, entradaPlanoContaFinanceiroId, saidaPlanoContaFinanceiroId, isCriarContaPagar, vencimento, idFornecedor)



Atualizar Financiamento

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContasAReceberApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContasAReceberApi apiInstance = new ContasAReceberApi(defaultClient);
    Integer id = 56; // Integer | 
    Integer clienteId = 56; // Integer | 
    String descricaoObjeto = "descricaoObjeto_example"; // String | 
    Double totalASerFinanciado = 3.4D; // Double | 
    Integer qtdParcelas = 56; // Integer | 
    Double valorParcela = 3.4D; // Double | 
    Double margemPercentual = 3.4D; // Double | 
    Double totalFinanciamento = 3.4D; // Double | 
    OffsetDateTime dataPrimeiraParcela = new OffsetDateTime(); // OffsetDateTime | 
    Integer diaParaVencimento = 56; // Integer | 
    Integer entradaContaId = 56; // Integer | 
    Integer entradaLivroCaixaFormaPagamentoId = 56; // Integer | 
    Integer saidaContaId = 56; // Integer | 
    Integer saidaLivroCaixaFormaPagamentoId = 56; // Integer | 
    Long entradaPlanoContaFinanceiroId = 56L; // Long | 
    Long saidaPlanoContaFinanceiroId = 56L; // Long | 
    Boolean isCriarContaPagar = false; // Boolean | 
    OffsetDateTime vencimento = new OffsetDateTime(); // OffsetDateTime | 
    Integer idFornecedor = 56; // Integer | 
    try {
      FinanciamentoCompleto result = apiInstance.atualizar3(id, clienteId, descricaoObjeto, totalASerFinanciado, qtdParcelas, valorParcela, margemPercentual, totalFinanciamento, dataPrimeiraParcela, diaParaVencimento, entradaContaId, entradaLivroCaixaFormaPagamentoId, saidaContaId, saidaLivroCaixaFormaPagamentoId, entradaPlanoContaFinanceiroId, saidaPlanoContaFinanceiroId, isCriarContaPagar, vencimento, idFornecedor);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContasAReceberApi#atualizar3");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **clienteId** | **Integer**|  |
 **descricaoObjeto** | **String**|  |
 **totalASerFinanciado** | **Double**|  |
 **qtdParcelas** | **Integer**|  |
 **valorParcela** | **Double**|  |
 **margemPercentual** | **Double**|  |
 **totalFinanciamento** | **Double**|  |
 **dataPrimeiraParcela** | **OffsetDateTime**|  |
 **diaParaVencimento** | **Integer**|  |
 **entradaContaId** | **Integer**|  |
 **entradaLivroCaixaFormaPagamentoId** | **Integer**|  |
 **saidaContaId** | **Integer**|  |
 **saidaLivroCaixaFormaPagamentoId** | **Integer**|  |
 **entradaPlanoContaFinanceiroId** | **Long**|  | [optional]
 **saidaPlanoContaFinanceiroId** | **Long**|  | [optional]
 **isCriarContaPagar** | **Boolean**|  | [optional] [default to false]
 **vencimento** | **OffsetDateTime**|  | [optional]
 **idFornecedor** | **Integer**|  | [optional]

### Return type

[**FinanciamentoCompleto**](FinanciamentoCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um Financiamento |  -  |
**400** | Erro ao salvar Financiamento |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="carregar3"></a>
# **carregar3**
> FinanciamentoConfiguracaoResponseEntity carregar3()



Retorna conjunto de configurações relacionadas ao Financiamento

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContasAReceberApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContasAReceberApi apiInstance = new ContasAReceberApi(defaultClient);
    try {
      FinanciamentoConfiguracaoResponseEntity result = apiInstance.carregar3();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContasAReceberApi#carregar3");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**FinanciamentoConfiguracaoResponseEntity**](FinanciamentoConfiguracaoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Conjunto de configurações do Financiamento |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar5"></a>
# **criar5**
> CriarFinanciamentoResponseEntity criar5(clienteId, descricaoObjeto, totalASerFinanciado, qtdParcelas, valorParcela, margemPercentual, totalFinanciamento, dataPrimeiraParcela, diaParaVencimento, entradaContaId, entradaLivroCaixaFormaPagamentoId, saidaContaId, saidaLivroCaixaFormaPagamentoId, entradaPlanoContaFinanceiroId, saidaPlanoContaFinanceiroId, isCriarContaPagar, vencimento, idFornecedor)



Criar Financiamento

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContasAReceberApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContasAReceberApi apiInstance = new ContasAReceberApi(defaultClient);
    Integer clienteId = 56; // Integer | 
    String descricaoObjeto = "descricaoObjeto_example"; // String | 
    Double totalASerFinanciado = 3.4D; // Double | 
    Integer qtdParcelas = 56; // Integer | 
    Double valorParcela = 3.4D; // Double | 
    Double margemPercentual = 3.4D; // Double | 
    Double totalFinanciamento = 3.4D; // Double | 
    OffsetDateTime dataPrimeiraParcela = new OffsetDateTime(); // OffsetDateTime | 
    Integer diaParaVencimento = 56; // Integer | 
    Integer entradaContaId = 56; // Integer | 
    Integer entradaLivroCaixaFormaPagamentoId = 56; // Integer | 
    Integer saidaContaId = 56; // Integer | 
    Integer saidaLivroCaixaFormaPagamentoId = 56; // Integer | 
    Long entradaPlanoContaFinanceiroId = 56L; // Long | 
    Long saidaPlanoContaFinanceiroId = 56L; // Long | 
    Boolean isCriarContaPagar = false; // Boolean | 
    OffsetDateTime vencimento = new OffsetDateTime(); // OffsetDateTime | 
    Integer idFornecedor = 56; // Integer | 
    try {
      CriarFinanciamentoResponseEntity result = apiInstance.criar5(clienteId, descricaoObjeto, totalASerFinanciado, qtdParcelas, valorParcela, margemPercentual, totalFinanciamento, dataPrimeiraParcela, diaParaVencimento, entradaContaId, entradaLivroCaixaFormaPagamentoId, saidaContaId, saidaLivroCaixaFormaPagamentoId, entradaPlanoContaFinanceiroId, saidaPlanoContaFinanceiroId, isCriarContaPagar, vencimento, idFornecedor);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContasAReceberApi#criar5");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clienteId** | **Integer**|  |
 **descricaoObjeto** | **String**|  |
 **totalASerFinanciado** | **Double**|  |
 **qtdParcelas** | **Integer**|  |
 **valorParcela** | **Double**|  |
 **margemPercentual** | **Double**|  |
 **totalFinanciamento** | **Double**|  |
 **dataPrimeiraParcela** | **OffsetDateTime**|  |
 **diaParaVencimento** | **Integer**|  |
 **entradaContaId** | **Integer**|  |
 **entradaLivroCaixaFormaPagamentoId** | **Integer**|  |
 **saidaContaId** | **Integer**|  |
 **saidaLivroCaixaFormaPagamentoId** | **Integer**|  |
 **entradaPlanoContaFinanceiroId** | **Long**|  | [optional]
 **saidaPlanoContaFinanceiroId** | **Long**|  | [optional]
 **isCriarContaPagar** | **Boolean**|  | [optional] [default to false]
 **vencimento** | **OffsetDateTime**|  | [optional]
 **idFornecedor** | **Integer**|  | [optional]

### Return type

[**CriarFinanciamentoResponseEntity**](CriarFinanciamentoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Retorna id do Financiamento |  -  |
**400** | Erro ao salvar Financiamento |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criarValorExtra"></a>
# **criarValorExtra**
> FinanciamentoCompleto criarValorExtra(financiamentoId, historico, valor)



Salva um valor extra relacionado a um Financiamento

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContasAReceberApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContasAReceberApi apiInstance = new ContasAReceberApi(defaultClient);
    Integer financiamentoId = 56; // Integer | 
    String historico = "historico_example"; // String | 
    Double valor = 3.4D; // Double | 
    try {
      FinanciamentoCompleto result = apiInstance.criarValorExtra(financiamentoId, historico, valor);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContasAReceberApi#criarValorExtra");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **financiamentoId** | **Integer**|  |
 **historico** | **String**|  |
 **valor** | **Double**|  |

### Return type

[**FinanciamentoCompleto**](FinanciamentoCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um Financiamento |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar4"></a>
# **deletar4**
> deletar4(id)



Requisita remoção de Financiamento e suas parcelas.

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContasAReceberApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContasAReceberApi apiInstance = new ContasAReceberApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      apiInstance.deletar4(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContasAReceberApi#deletar4");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletarValorExtra"></a>
# **deletarValorExtra**
> FinanciamentoCompleto deletarValorExtra(financiamentoId, id)



Requisita remoção de um valor extra relacionado a um Financiamento

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContasAReceberApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContasAReceberApi apiInstance = new ContasAReceberApi(defaultClient);
    Integer financiamentoId = 56; // Integer | 
    Long id = 56L; // Long | 
    try {
      FinanciamentoCompleto result = apiInstance.deletarValorExtra(financiamentoId, id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContasAReceberApi#deletarValorExtra");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **financiamentoId** | **Integer**|  |
 **id** | **Long**|  |

### Return type

[**FinanciamentoCompleto**](FinanciamentoCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um Financiamento |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="gerar"></a>
# **gerar**
> FinanciamentoCompleto gerar(id)



Solicita geração de um financiamento, encerrando suas operações e criando parcelas no Contas a Receber.

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContasAReceberApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContasAReceberApi apiInstance = new ContasAReceberApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      FinanciamentoCompleto result = apiInstance.gerar(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContasAReceberApi#gerar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

[**FinanciamentoCompleto**](FinanciamentoCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um Financiamento |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="get3"></a>
# **get3**
> FinanciamentoCompleto get3(id)



Solicita um Financiamento

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContasAReceberApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContasAReceberApi apiInstance = new ContasAReceberApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      FinanciamentoCompleto result = apiInstance.get3(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContasAReceberApi#get3");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

[**FinanciamentoCompleto**](FinanciamentoCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um Financiamento |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="index2"></a>
# **index2**
> FinanciamentoIndexResponseEntity index2(clienteId, descricaoObjeto, pagina)



Solicita lista de Metadados de Financiamentos, com opção de busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContasAReceberApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContasAReceberApi apiInstance = new ContasAReceberApi(defaultClient);
    Integer clienteId = 56; // Integer | 
    String descricaoObjeto = "descricaoObjeto_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      FinanciamentoIndexResponseEntity result = apiInstance.index2(clienteId, descricaoObjeto, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContasAReceberApi#index2");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clienteId** | **Integer**|  | [optional]
 **descricaoObjeto** | **String**|  | [optional]
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**FinanciamentoIndexResponseEntity**](FinanciamentoIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de Metadados de Financiamentos |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar2"></a>
# **salvar2**
> FinanciamentoConfiguracaoResponseEntity salvar2(estornoFinanciamentoPlanoContaPadrao)



Salva conjunto de configurações do Financiamento

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContasAReceberApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContasAReceberApi apiInstance = new ContasAReceberApi(defaultClient);
    Long estornoFinanciamentoPlanoContaPadrao = 56L; // Long | 
    try {
      FinanciamentoConfiguracaoResponseEntity result = apiInstance.salvar2(estornoFinanciamentoPlanoContaPadrao);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContasAReceberApi#salvar2");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **estornoFinanciamentoPlanoContaPadrao** | **Long**|  |

### Return type

[**FinanciamentoConfiguracaoResponseEntity**](FinanciamentoConfiguracaoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Conjunto de configurações do Financiamento |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvarIndicadorParcelaJuros"></a>
# **salvarIndicadorParcelaJuros**
> FinanciamentoCompleto salvarIndicadorParcelaJuros(financiamentoId, contaReceberId, inlineObject18)



Requisita alterar o Indicador de Juros de uma Parcela de um Financiamento. Esse indicador faz com que o valor total recebido seja usado como lucro total da parcela.

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContasAReceberApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContasAReceberApi apiInstance = new ContasAReceberApi(defaultClient);
    Integer financiamentoId = 56; // Integer | 
    Integer contaReceberId = 56; // Integer | 
    InlineObject18 inlineObject18 = new InlineObject18(); // InlineObject18 | 
    try {
      FinanciamentoCompleto result = apiInstance.salvarIndicadorParcelaJuros(financiamentoId, contaReceberId, inlineObject18);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContasAReceberApi#salvarIndicadorParcelaJuros");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **financiamentoId** | **Integer**|  |
 **contaReceberId** | **Integer**|  |
 **inlineObject18** | [**InlineObject18**](InlineObject18.md)|  | [optional]

### Return type

[**FinanciamentoCompleto**](FinanciamentoCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um Financiamento |  -  |
**500** | Erro desconhecido no servidor |  -  |

