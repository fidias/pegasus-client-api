

# InlineObject84

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**digitosIniciais** | **String** |  | 
**totalDigitos** | **Integer** |  | 
**parteADigitoInicial** | **Integer** |  | 
**parteADigitoFinal** | **Integer** |  | 
**parteBDigitoInicial** | **Integer** |  | 
**parteBDigitoFinal** | **Integer** |  | 
**parteBQuantidadeCasas** | **Integer** |  | 
**parteBUtilizarComo** | [**ParteBUtilizarComoEnum**](#ParteBUtilizarComoEnum) |  | 
**tratamentoDecimal** | [**TratamentoDecimalEnum**](#TratamentoDecimalEnum) |  | 



## Enum: ParteBUtilizarComoEnum

Name | Value
---- | -----
PESO | &quot;peso&quot;
VALOR | &quot;valor&quot;



## Enum: TratamentoDecimalEnum

Name | Value
---- | -----
ARREDONDAMENTO | &quot;arredondamento&quot;
TRUNCAMENTO | &quot;truncamento&quot;



