

# ProdutoAlterarEmLoteItemLista

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**dataCriacao** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**usuarioCriacao** | [**UsuarioCriacao1**](UsuarioCriacao1.md) |  | 
**dataAplicacao** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**usuarioAplicacao** | [**UsuarioAplicacao1**](UsuarioAplicacao1.md) |  |  [optional]
**alteracaoAplicada** | **Boolean** |  | 



