

# InlineObject47

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fornecedorId** | **Integer** |  | 
**valor** | **Double** |  | 
**vencimento** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**contaId** | **Integer** |  | 
**operacao** | [**OperacaoEnum**](#OperacaoEnum) |  | 
**livroCaixaFormaPagamentoId** | **Integer** |  | 



## Enum: OperacaoEnum

Name | Value
---- | -----
FRETE | &quot;mdfe_contas_pagar__frete&quot;
IMPOSTO_FRETE | &quot;mdfe_contas_pagar__imposto_frete&quot;



