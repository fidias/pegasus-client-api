

# ProdutoTabelaPrecoItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**produtoTabelaPrecoId** | **Long** |  | 
**nome** | **String** |  | 
**produtoId** | **Long** |  | 
**margemP** | **Double** |  |  [optional]
**preco** | **Double** |  |  [optional]
**itemId** | **Long** |  |  [optional]



