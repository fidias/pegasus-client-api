

# SincronizarParcelaPrazo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**valor** | **Double** |  |  [optional]
**vencimento** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**emissao** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**competencia** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]



