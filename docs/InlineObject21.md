

# InlineObject21

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dataInicial** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**dataFinal** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**contratoTipoId** | **Integer** |  | 
**descricao** | **String** |  |  [optional]
**contaId** | **Integer** |  | 
**planoContaFinanceiroId** | **Long** |  |  [optional]



