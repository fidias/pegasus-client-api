

# VendaBalcaoIntegradorEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienteVendaId** | **Long** |  | 
**idCaixa** | **Integer** |  | 
**dataVenda** | **Long** |  | 
**totalLiquido** | **Double** |  | 
**cliente** | [**Cliente**](Cliente.md) |  |  [optional]
**formasPagamentos** | [**List&lt;FormaPagamento&gt;**](FormaPagamento.md) |  | 
**itensVenda** | [**List&lt;ItemVenda&gt;**](ItemVenda.md) |  | 
**totalAproximadoTributos** | **Double** |  |  [optional]



