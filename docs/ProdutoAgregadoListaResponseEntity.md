

# ProdutoAgregadoListaResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;ProdutoAgregado&gt;**](ProdutoAgregado.md) |  |  [optional]



