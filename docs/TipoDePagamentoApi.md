# TipoDePagamentoApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**autocomplete15**](TipoDePagamentoApi.md#autocomplete15) | **GET** /api/tipo-pagamento/autocomplete | 


<a name="autocomplete15"></a>
# **autocomplete15**
> TipoPagamentoAutocompleteResponseEntity autocomplete15(termo)



Solicita lista de Tipos de Pagamento que contenham o termo solicitado na descrição

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.TipoDePagamentoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    TipoDePagamentoApi apiInstance = new TipoDePagamentoApi(defaultClient);
    String termo = "termo_example"; // String | 
    try {
      TipoPagamentoAutocompleteResponseEntity result = apiInstance.autocomplete15(termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TipoDePagamentoApi#autocomplete15");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  | [optional]

### Return type

[**TipoPagamentoAutocompleteResponseEntity**](TipoPagamentoAutocompleteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de Tipo de Pagamento |  -  |
**500** | Erro desconhecido no servidor |  -  |

