

# ClienteAjaxAutoComplete

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**value** | **String** |  | 
**nome** | **String** |  | 
**nomeFantasia** | **String** |  |  [optional]
**cpfCnpjFormatado** | **String** |  |  [optional]



