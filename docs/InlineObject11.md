

# InlineObject11

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tipoRecolhimentoIcms** | **Integer** |  |  [optional]
**tipoSubstituicao** | **Integer** |  |  [optional]
**tipoTributacaoIpi** | **Integer** |  |  [optional]
**balanceteOperacaoFinanceiraEntrada** | **Integer** |  |  [optional]
**balanceteOperacaoFinanceiraSaida** | **Integer** |  |  [optional]
**devolucaoOperacaoFinanceiraPadrao** | **Integer** |  |  [optional]
**lucroPadrao** | **Double** |  |  [optional]
**custoExtraPercPadrao** | **Double** |  |  [optional]
**margemLucroPercPadrao** | **Double** |  |  [optional]
**idGeneroPadrao** | **Integer** |  |  [optional]



