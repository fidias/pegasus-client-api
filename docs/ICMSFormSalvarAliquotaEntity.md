

# ICMSFormSalvarAliquotaEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pCredSN** | **Double** |  | 
**pICMSST** | **Double** |  | 
**pMVAST** | **Double** |  |  [optional]
**pRedBCST** | **Double** |  |  [optional]



