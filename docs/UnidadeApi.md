# UnidadeApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**alternarDisponivel2**](UnidadeApi.md#alternarDisponivel2) | **PUT** /api/unidade/{id}/alternar-disponivel | 
[**listar16**](UnidadeApi.md#listar16) | **GET** /api/unidade | 


<a name="alternarDisponivel2"></a>
# **alternarDisponivel2**
> alternarDisponivel2(id, isDisponivel)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.UnidadeApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    UnidadeApi apiInstance = new UnidadeApi(defaultClient);
    Integer id = 56; // Integer | 
    Boolean isDisponivel = true; // Boolean | 
    try {
      apiInstance.alternarDisponivel2(id, isDisponivel);
    } catch (ApiException e) {
      System.err.println("Exception when calling UnidadeApi#alternarDisponivel2");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **isDisponivel** | **Boolean**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="listar16"></a>
# **listar16**
> listar16(cbDescricao, descricao, pagina)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.UnidadeApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    UnidadeApi apiInstance = new UnidadeApi(defaultClient);
    Boolean cbDescricao = false; // Boolean | 
    String descricao = "descricao_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      apiInstance.listar16(cbDescricao, descricao, pagina);
    } catch (ApiException e) {
      System.err.println("Exception when calling UnidadeApi#listar16");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cbDescricao** | **Boolean**|  | [optional] [default to false]
 **descricao** | **String**|  | [optional]
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

