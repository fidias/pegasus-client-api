

# FormasPagamento

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**idtipoPagamento** | **Long** |  | 
**valor** | **Double** |  | 
**quantParcela** | **Long** |  | 
**nome** | **String** |  | 
**cartao** | **Boolean** |  | 



