# CaixaApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**definirCliente**](CaixaApi.md#definirCliente) | **PUT** /api/caixa/vendas/{venda_id}/definir-cliente | 
[**importarNFSe**](CaixaApi.md#importarNFSe) | **GET** /api/caixa/vendas/importar-nfse/{venda_id} | 
[**importarNFe**](CaixaApi.md#importarNFe) | **GET** /api/caixa/vendas/importar-nfe/{venda_id} | 
[**ler**](CaixaApi.md#ler) | **GET** /api/caixa/vendas/{venda_id} | 
[**listar**](CaixaApi.md#listar) | **GET** /api/caixa/vendas | 
[**vendaSincronizar**](CaixaApi.md#vendaSincronizar) | **POST** /api/caixa/vendas/sincronizar | 


<a name="definirCliente"></a>
# **definirCliente**
> DefinirClienteResponseEntity definirCliente(vendaId, clienteId)



Define um cliente para a venda

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CaixaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CaixaApi apiInstance = new CaixaApi(defaultClient);
    Long vendaId = 56L; // Long | 
    Integer clienteId = 56; // Integer | 
    try {
      DefinirClienteResponseEntity result = apiInstance.definirCliente(vendaId, clienteId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CaixaApi#definirCliente");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vendaId** | **Long**|  |
 **clienteId** | **Integer**|  |

### Return type

[**DefinirClienteResponseEntity**](DefinirClienteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados do cliente definido para a venda |  -  |
**404** | Registro não encontrado  * Venda {0} não encontrada. * Cliente/Fornecedor não encontrado. |  -  |
**412** | Pré-condição não satisfeita  * Venda ainda não foi concluída. * Venda já foi cancelada. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="importarNFSe"></a>
# **importarNFSe**
> NFSeImportarVendaResponseEntity importarNFSe(vendaId)



Importa uma Venda para NFS-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CaixaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CaixaApi apiInstance = new CaixaApi(defaultClient);
    Long vendaId = 56L; // Long | 
    try {
      NFSeImportarVendaResponseEntity result = apiInstance.importarNFSe(vendaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CaixaApi#importarNFSe");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vendaId** | **Long**|  |

### Return type

[**NFSeImportarVendaResponseEntity**](NFSeImportarVendaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Indica que NFS-e foi criada a partir da venda |  -  |
**400** | Erro de validação dos dados da venda para importar para NFS-e |  -  |
**412** | Venda ainda não concluída ou já cancelada |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="importarNFe"></a>
# **importarNFe**
> NFeResponseEntity importarNFe(vendaId)



Importa uma Venda para NF-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CaixaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CaixaApi apiInstance = new CaixaApi(defaultClient);
    Long vendaId = 56L; // Long | 
    try {
      NFeResponseEntity result = apiInstance.importarNFe(vendaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CaixaApi#importarNFe");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vendaId** | **Long**|  |

### Return type

[**NFeResponseEntity**](NFeResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Indica que NF-e foi criada a partir da venda |  -  |
**400** | Erro de validação dos dados da venda para importar para NF-e |  -  |
**412** | Venda ainda não concluída ou já cancelada |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="ler"></a>
# **ler**
> VendaResponseEntity ler(vendaId)



Recupera uma venda através do ID

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CaixaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CaixaApi apiInstance = new CaixaApi(defaultClient);
    Long vendaId = 56L; // Long | 
    try {
      VendaResponseEntity result = apiInstance.ler(vendaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CaixaApi#ler");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vendaId** | **Long**|  |

### Return type

[**VendaResponseEntity**](VendaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos da venda, junto com possíveis dados do CF-e |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="listar"></a>
# **listar**
> ListaVendasResponseEntity listar(caixaId, dataVenda)



Solicita lista de vendas de um caixa, em uma determinada data

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CaixaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CaixaApi apiInstance = new CaixaApi(defaultClient);
    Integer caixaId = 56; // Integer | 
    OffsetDateTime dataVenda = 02/01/2019; // OffsetDateTime | Data no formato DD/MM/AAAA
    try {
      ListaVendasResponseEntity result = apiInstance.listar(caixaId, dataVenda);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CaixaApi#listar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caixaId** | **Integer**|  |
 **dataVenda** | **OffsetDateTime**| Data no formato DD/MM/AAAA |

### Return type

[**ListaVendasResponseEntity**](ListaVendasResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de vendas |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="vendaSincronizar"></a>
# **vendaSincronizar**
> VendaSincronizarResponseEntity vendaSincronizar(vendaBalcaoCompletaEntity)



Solicita a sincronização de uma Venda

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CaixaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CaixaApi apiInstance = new CaixaApi(defaultClient);
    VendaBalcaoCompletaEntity vendaBalcaoCompletaEntity = new VendaBalcaoCompletaEntity(); // VendaBalcaoCompletaEntity | 
    try {
      VendaSincronizarResponseEntity result = apiInstance.vendaSincronizar(vendaBalcaoCompletaEntity);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CaixaApi#vendaSincronizar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vendaBalcaoCompletaEntity** | [**VendaBalcaoCompletaEntity**](VendaBalcaoCompletaEntity.md)|  | [optional]

### Return type

[**VendaSincronizarResponseEntity**](VendaSincronizarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Sincronização realizada com sucesso. |  -  |
**409** | Conflito ao sincronizar venda. Verificar resposta e decidir se a venda já havia sido sincronizada ou realmente é uma venda duplicada. |  -  |
**500** | Erro desconhecido no servidor |  -  |

