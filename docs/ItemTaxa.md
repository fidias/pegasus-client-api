

# ItemTaxa

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**serialPos** | **String** |  |  [optional]
**taxaP** | **Double** |  |  [optional]
**numParcelasIni** | **Integer** |  |  [optional]
**numParcelasFim** | **Integer** |  |  [optional]



