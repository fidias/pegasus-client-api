

# Item

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ordemItem** | **Long** |  | 
**dataCadastro** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**dataEdicao** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**produtoId** | **Long** |  | 
**descricao** | **String** |  | 
**quantidade** | **Double** |  | 
**preco** | **Double** |  | 
**custo** | **Double** |  |  [optional]
**acrescimoD** | **Double** |  | 
**acrescimoP** | **Double** |  | 
**descontoD** | **Double** |  | 
**descontoP** | **Double** |  | 
**totalBruto** | **Double** |  | 
**totalLiquido** | **Double** |  | 
**cancelado** | **Boolean** |  | 
**removido** | **Boolean** |  | 
**casasDecValor** | **Long** |  | 
**casasDecQuant** | **Long** |  | 
**situacaoTrib** | **String** |  |  [optional]
**unidade** | **String** |  | 
**aliquota** | **Double** |  |  [optional]
**codBarra** | **String** |  |  [optional]
**referencia** | **String** |  |  [optional]
**pafFracionario** | **Boolean** |  | 
**tipo** | [**Tipo1**](Tipo1.md) |  | 
**responsaveis** | [**List&lt;Responsavel&gt;**](Responsavel.md) |  |  [optional]
**observacaoAdicional** | **String** |  |  [optional]



