

# FinanciamentoCompleto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**dados** | [**Dados**](Dados.md) |  | 
**parcelas** | [**List&lt;Parcela&gt;**](Parcela.md) |  |  [optional]
**valoresExtra** | [**List&lt;ValoresExtra&gt;**](ValoresExtra.md) |  |  [optional]



