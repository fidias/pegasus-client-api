

# VendasBalcao

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**idcaixa** | **Long** |  | 
**clienteVendaId** | **Long** |  | 
**davTipoDocumentoId** | **Long** |  | 
**davTipoDocumento** | **String** |  | 



