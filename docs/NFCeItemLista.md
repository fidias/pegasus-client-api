

# NFCeItemLista

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**anoMes** | **String** |  | 
**mod** | **Long** |  | 
**serie** | **Long** |  | 
**nNf** | **Long** |  | 
**tpEmis** | **Long** |  | 
**cNf** | **Long** |  | 
**tpAmb** | **Long** |  | 
**cUf** | **Long** |  | 
**versao** | **String** |  | 
**destinatario** | [**Destinatario**](Destinatario.md) |  |  [optional]
**situacao** | [**Situacao**](Situacao.md) |  | 
**valorTotal** | **Double** |  | 
**dhEmi** | [**OffsetDateTime**](OffsetDateTime.md) |  | 



