# EntradaSadaApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**atualizar2**](EntradaSadaApi.md#atualizar2) | **PUT** /api/operacao-financeira/{cfop} | 
[**carregar**](EntradaSadaApi.md#carregar) | **GET** /api/compra/configuracao | 
[**carregar1**](EntradaSadaApi.md#carregar1) | **GET** /api/entrada-saida/{entrada_saida_id}/fatura/parcelas | 
[**carregar2**](EntradaSadaApi.md#carregar2) | **GET** /api/operacao-financeira/{cfop} | 
[**criar3**](EntradaSadaApi.md#criar3) | **POST** /api/entrada-saida/{entrada_saida_id}/fatura/parcelas | 
[**criar4**](EntradaSadaApi.md#criar4) | **POST** /api/operacao-financeira | 
[**deletar3**](EntradaSadaApi.md#deletar3) | **DELETE** /api/entrada-saida/{entrada_saida_id}/fatura/parcelas/{id} | 
[**salvar1**](EntradaSadaApi.md#salvar1) | **POST** /api/compra/configuracao | 


<a name="atualizar2"></a>
# **atualizar2**
> atualizar2(cfop, habilitado, isGerarFatura, isAlterarQuantidade, isAlterarCusto, isAlterarPreco, hasFormaPagamento, isAlterarIdUltimaNota)



Salva opções de uma Operação Financeira

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.EntradaSadaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    EntradaSadaApi apiInstance = new EntradaSadaApi(defaultClient);
    Integer cfop = 56; // Integer | 
    Boolean habilitado = false; // Boolean | 
    Boolean isGerarFatura = false; // Boolean | 
    Boolean isAlterarQuantidade = false; // Boolean | 
    Boolean isAlterarCusto = false; // Boolean | 
    Boolean isAlterarPreco = false; // Boolean | 
    Boolean hasFormaPagamento = false; // Boolean | 
    Boolean isAlterarIdUltimaNota = false; // Boolean | 
    try {
      apiInstance.atualizar2(cfop, habilitado, isGerarFatura, isAlterarQuantidade, isAlterarCusto, isAlterarPreco, hasFormaPagamento, isAlterarIdUltimaNota);
    } catch (ApiException e) {
      System.err.println("Exception when calling EntradaSadaApi#atualizar2");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cfop** | **Integer**|  |
 **habilitado** | **Boolean**|  | [optional] [default to false]
 **isGerarFatura** | **Boolean**|  | [optional] [default to false]
 **isAlterarQuantidade** | **Boolean**|  | [optional] [default to false]
 **isAlterarCusto** | **Boolean**|  | [optional] [default to false]
 **isAlterarPreco** | **Boolean**|  | [optional] [default to false]
 **hasFormaPagamento** | **Boolean**|  | [optional] [default to false]
 **isAlterarIdUltimaNota** | **Boolean**|  | [optional] [default to false]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="carregar"></a>
# **carregar**
> CompraConfiguracaoResponseEntity carregar()



Retorna conjunto de configurações relacionadas a Entrada/Saída

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.EntradaSadaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    EntradaSadaApi apiInstance = new EntradaSadaApi(defaultClient);
    try {
      CompraConfiguracaoResponseEntity result = apiInstance.carregar();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling EntradaSadaApi#carregar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CompraConfiguracaoResponseEntity**](CompraConfiguracaoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Conjunto de configurações de Entrada/Saída |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="carregar1"></a>
# **carregar1**
> CarregarParcelasResponseEntity carregar1(entradaSaidaId)



Retorna lista de parcelas relacionadas a uma Entrada/Saída

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.EntradaSadaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    EntradaSadaApi apiInstance = new EntradaSadaApi(defaultClient);
    Long entradaSaidaId = 56L; // Long | 
    try {
      CarregarParcelasResponseEntity result = apiInstance.carregar1(entradaSaidaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling EntradaSadaApi#carregar1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entradaSaidaId** | **Long**|  |

### Return type

[**CarregarParcelasResponseEntity**](CarregarParcelasResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de parcelas de uma Entrada/Saída |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="carregar2"></a>
# **carregar2**
> OperacaoFinanceiraResponseEntity carregar2(cfop)



Retorna uma Operação Financeira

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.EntradaSadaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    EntradaSadaApi apiInstance = new EntradaSadaApi(defaultClient);
    Integer cfop = 56; // Integer | 
    try {
      OperacaoFinanceiraResponseEntity result = apiInstance.carregar2(cfop);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling EntradaSadaApi#carregar2");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cfop** | **Integer**|  |

### Return type

[**OperacaoFinanceiraResponseEntity**](OperacaoFinanceiraResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Operação Financeira |  -  |
**404** | Registro não encontrado |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar3"></a>
# **criar3**
> CarregarParcelasResponseEntity criar3(entradaSaidaId, dataVencimento, numeroParcela, valorParcela)



Salva uma parcela relacionada a uma Entrada/Saída

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.EntradaSadaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    EntradaSadaApi apiInstance = new EntradaSadaApi(defaultClient);
    Long entradaSaidaId = 56L; // Long | 
    OffsetDateTime dataVencimento = new OffsetDateTime(); // OffsetDateTime | Data no formato DD/MM/AAAA
    String numeroParcela = "numeroParcela_example"; // String | 
    Double valorParcela = 3.4D; // Double | 
    try {
      CarregarParcelasResponseEntity result = apiInstance.criar3(entradaSaidaId, dataVencimento, numeroParcela, valorParcela);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling EntradaSadaApi#criar3");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entradaSaidaId** | **Long**|  |
 **dataVencimento** | **OffsetDateTime**| Data no formato DD/MM/AAAA |
 **numeroParcela** | **String**|  |
 **valorParcela** | **Double**|  |

### Return type

[**CarregarParcelasResponseEntity**](CarregarParcelasResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de parcelas de uma Entrada/Saída |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar4"></a>
# **criar4**
> criar4(cfop)



Adiciona uma nova Operação Financeira a partir de um CFOP

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.EntradaSadaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    EntradaSadaApi apiInstance = new EntradaSadaApi(defaultClient);
    Integer cfop = 56; // Integer | 
    try {
      apiInstance.criar4(cfop);
    } catch (ApiException e) {
      System.err.println("Exception when calling EntradaSadaApi#criar4");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cfop** | **Integer**|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Operação Financeira criada com sucesso |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar3"></a>
# **deletar3**
> CarregarParcelasResponseEntity deletar3(entradaSaidaId, id)



Requisita remoção de uma parcela relacionada a uma Entrada/Saída

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.EntradaSadaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    EntradaSadaApi apiInstance = new EntradaSadaApi(defaultClient);
    Long entradaSaidaId = 56L; // Long | 
    Long id = 56L; // Long | 
    try {
      CarregarParcelasResponseEntity result = apiInstance.deletar3(entradaSaidaId, id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling EntradaSadaApi#deletar3");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entradaSaidaId** | **Long**|  |
 **id** | **Long**|  |

### Return type

[**CarregarParcelasResponseEntity**](CarregarParcelasResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de parcelas de uma Entrada/Saída |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar1"></a>
# **salvar1**
> salvar1(tipoRecolhimentoIcms, tipoSubstituicao, tipoTributacaoIpi, balanceteOperacaoFinanceiraEntrada, balanceteOperacaoFinanceiraSaida, devolucaoOperacaoFinanceiraPadrao, lucroPadrao, custoExtraPercPadrao, margemLucroPercPadrao, idGeneroPadrao)



Salva conjunto de configurações de Entrada/Saída

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.EntradaSadaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    EntradaSadaApi apiInstance = new EntradaSadaApi(defaultClient);
    Integer tipoRecolhimentoIcms = 56; // Integer | 
    Integer tipoSubstituicao = 56; // Integer | 
    Integer tipoTributacaoIpi = 56; // Integer | 
    Integer balanceteOperacaoFinanceiraEntrada = 56; // Integer | 
    Integer balanceteOperacaoFinanceiraSaida = 56; // Integer | 
    Integer devolucaoOperacaoFinanceiraPadrao = 56; // Integer | 
    Double lucroPadrao = 3.4D; // Double | 
    Double custoExtraPercPadrao = 3.4D; // Double | 
    Double margemLucroPercPadrao = 3.4D; // Double | 
    Integer idGeneroPadrao = 56; // Integer | 
    try {
      apiInstance.salvar1(tipoRecolhimentoIcms, tipoSubstituicao, tipoTributacaoIpi, balanceteOperacaoFinanceiraEntrada, balanceteOperacaoFinanceiraSaida, devolucaoOperacaoFinanceiraPadrao, lucroPadrao, custoExtraPercPadrao, margemLucroPercPadrao, idGeneroPadrao);
    } catch (ApiException e) {
      System.err.println("Exception when calling EntradaSadaApi#salvar1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tipoRecolhimentoIcms** | **Integer**|  | [optional]
 **tipoSubstituicao** | **Integer**|  | [optional]
 **tipoTributacaoIpi** | **Integer**|  | [optional]
 **balanceteOperacaoFinanceiraEntrada** | **Integer**|  | [optional]
 **balanceteOperacaoFinanceiraSaida** | **Integer**|  | [optional]
 **devolucaoOperacaoFinanceiraPadrao** | **Integer**|  | [optional]
 **lucroPadrao** | **Double**|  | [optional]
 **custoExtraPercPadrao** | **Double**|  | [optional]
 **margemLucroPercPadrao** | **Double**|  | [optional]
 **idGeneroPadrao** | **Integer**|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

