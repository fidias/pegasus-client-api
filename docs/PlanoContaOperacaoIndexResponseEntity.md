

# PlanoContaOperacaoIndexResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**planoConta** | [**PlanoConta**](PlanoConta.md) |  |  [optional]
**lista** | **String** |  |  [optional]



