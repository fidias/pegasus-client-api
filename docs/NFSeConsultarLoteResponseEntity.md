

# NFSeConsultarLoteResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**numeroRps** | **Long** |  |  [optional]
**nfseXml** | **String** |  |  [optional]
**listaTransmissao** | **String** |  |  [optional]
**statusInterno** | **String** |  |  [optional]



