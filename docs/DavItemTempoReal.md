

# DavItemTempoReal

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**dataCadastro** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**dataEdicao** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**totalLiquido** | **Double** |  | 
**observacao** | **String** |  |  [optional]
**arquivado** | **Boolean** |  | 
**cliente** | [**Cliente1**](Cliente1.md) |  | 
**objeto** | [**Objeto1**](Objeto1.md) |  |  [optional]
**davTipo** | [**DavTipo**](DavTipo.md) |  | 
**status** | [**Status**](Status.md) |  | 
**isOrcamento** | **Boolean** |  | 
**operador** | **String** |  | 
**operadorEdicao** | **String** |  | 
**vendasBalcao** | [**List&lt;VendasBalcao&gt;**](VendasBalcao.md) |  |  [optional]



