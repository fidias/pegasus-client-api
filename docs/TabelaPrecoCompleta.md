

# TabelaPrecoCompleta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**nome** | **String** |  | 
**usarComoDesconto** | **Boolean** |  | 
**tiposPagamento** | [**List&lt;TabelaPrecoTipoPagamento&gt;**](TabelaPrecoTipoPagamento.md) |  | 



