

# DavObjetoCarregar

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**objetoTipoId** | **Long** |  | 
**descricao** | **String** |  | 
**infoExtra** | **String** |  |  [optional]
**veiculo** | [**Veiculo**](Veiculo.md) |  |  [optional]
**outro** | [**Outro**](Outro.md) |  |  [optional]



