# FormaDePagamentoApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**atualizar7**](FormaDePagamentoApi.md#atualizar7) | **PUT** /api/livro-caixa/forma-pagamento/{id} | 
[**autocomplete9**](FormaDePagamentoApi.md#autocomplete9) | **GET** /api/livro-caixa/forma-pagamento/autocomplete | 
[**criar10**](FormaDePagamentoApi.md#criar10) | **POST** /api/livro-caixa/forma-pagamento | 
[**deletar9**](FormaDePagamentoApi.md#deletar9) | **DELETE** /api/livro-caixa/forma-pagamento/{id} | 
[**index8**](FormaDePagamentoApi.md#index8) | **GET** /api/livro-caixa/forma-pagamento | 


<a name="atualizar7"></a>
# **atualizar7**
> atualizar7(id, inlineObject42)



Requisita atualização de uma forma de pagamento

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.FormaDePagamentoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    FormaDePagamentoApi apiInstance = new FormaDePagamentoApi(defaultClient);
    Integer id = 56; // Integer | 
    InlineObject42 inlineObject42 = new InlineObject42(); // InlineObject42 | 
    try {
      apiInstance.atualizar7(id, inlineObject42);
    } catch (ApiException e) {
      System.err.println("Exception when calling FormaDePagamentoApi#atualizar7");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **inlineObject42** | [**InlineObject42**](InlineObject42.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**404** | Forma de Pagamento {0} não encontrada. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="autocomplete9"></a>
# **autocomplete9**
> TipoPagamentoAutocompleteResponseEntity autocomplete9(termo)



Solicita lista de Formas de Pagamento que contenham o termo solicitado na descrição

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.FormaDePagamentoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    FormaDePagamentoApi apiInstance = new FormaDePagamentoApi(defaultClient);
    String termo = "termo_example"; // String | 
    try {
      TipoPagamentoAutocompleteResponseEntity result = apiInstance.autocomplete9(termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling FormaDePagamentoApi#autocomplete9");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  | [optional]

### Return type

[**TipoPagamentoAutocompleteResponseEntity**](TipoPagamentoAutocompleteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de Formas de Pagamento |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar10"></a>
# **criar10**
> criar10(inlineObject43)



Requisita criação de uma forma de pagamento

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.FormaDePagamentoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    FormaDePagamentoApi apiInstance = new FormaDePagamentoApi(defaultClient);
    InlineObject43 inlineObject43 = new InlineObject43(); // InlineObject43 | 
    try {
      apiInstance.criar10(inlineObject43);
    } catch (ApiException e) {
      System.err.println("Exception when calling FormaDePagamentoApi#criar10");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject43** | [**InlineObject43**](InlineObject43.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar9"></a>
# **deletar9**
> deletar9(id)



Deleta forma de pagamento

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.FormaDePagamentoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    FormaDePagamentoApi apiInstance = new FormaDePagamentoApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      apiInstance.deletar9(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling FormaDePagamentoApi#deletar9");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**404** | Forma de pagamento não encontrada |  -  |
**409** | Não foi possível deletar esta Forma de pagamento, pois ele é referenciado por uma Conta a Pagar/Receber/Livro de Caixa. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="index8"></a>
# **index8**
> FormaPagamentoIndexResponseEntity index8(descricao)



Solicita lista de formas de pagamento, com opção de busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.FormaDePagamentoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    FormaDePagamentoApi apiInstance = new FormaDePagamentoApi(defaultClient);
    String descricao = "descricao_example"; // String | 
    try {
      FormaPagamentoIndexResponseEntity result = apiInstance.index8(descricao);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling FormaDePagamentoApi#index8");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **descricao** | **String**|  | [optional]

### Return type

[**FormaPagamentoIndexResponseEntity**](FormaPagamentoIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de formas de pagamento |  -  |
**500** | Erro desconhecido no servidor |  -  |

