

# PlanoContaListaResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;PlanoContaListaItem&gt;**](PlanoContaListaItem.md) |  |  [optional]
**newId** | **Long** |  |  [optional]



