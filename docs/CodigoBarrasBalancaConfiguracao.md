

# CodigoBarrasBalancaConfiguracao

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**habilitarCodBarrasBalanca** | [**HabilitarCodBarrasBalanca**](HabilitarCodBarrasBalanca.md) |  | 
**digitosIniciais** | [**DigitosIniciais**](DigitosIniciais.md) |  | 
**totalDigitos** | [**TotalDigitos**](TotalDigitos.md) |  | 
**parteADigitoInicial** | [**ParteADigitoInicial**](ParteADigitoInicial.md) |  | 
**parteADigitoFinal** | [**ParteADigitoFinal**](ParteADigitoFinal.md) |  | 
**parteBDigitoInicial** | [**ParteBDigitoInicial**](ParteBDigitoInicial.md) |  | 
**parteBDigitoFinal** | [**ParteBDigitoFinal**](ParteBDigitoFinal.md) |  | 
**parteBQuantidadeCasas** | [**ParteBQuantidadeCasas**](ParteBQuantidadeCasas.md) |  | 
**parteBUtilizarComo** | [**ParteBUtilizarComo**](ParteBUtilizarComo.md) |  | 
**tratamentoDecimal** | [**TratamentoDecimal**](TratamentoDecimal.md) |  | 
**parteABuscarPor** | [**ParteABuscarPor**](ParteABuscarPor.md) |  | 



