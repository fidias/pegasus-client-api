

# BalanceteManualCompletoResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**dados** | **String** |  |  [optional]
**lista** | **String** |  |  [optional]
**listaNotas** | **String** |  |  [optional]



