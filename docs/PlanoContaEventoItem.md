

# PlanoContaEventoItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**usuario** | [**Usuario**](Usuario.md) |  | 
**dataEvento** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**tipoEvento** | [**TipoEvento**](TipoEvento.md) |  | 
**mensagemAuxiliar** | **String** |  |  [optional]



