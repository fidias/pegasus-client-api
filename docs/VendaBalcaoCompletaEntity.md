

# VendaBalcaoCompletaEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identificadorUnico** | **String** | String no formato UUID v4 | 
**idCaixa** | **Integer** |  | 
**clienteVendaId** | **Long** | ID da Venda Balcão gerado no banco local | 
**concluida** | **Boolean** |  |  [optional]
**dataVenda** | [**OffsetDateTime**](OffsetDateTime.md) | Data/Hora da Venda, no formato YYYY-MM-DD HH24:MI:SSOF:00 | 
**valorRecebido** | **Double** |  | 
**troco** | **Double** |  | 
**totalLiquido** | **Double** |  | 
**cancelada** | **Boolean** |  | 
**idEcfReducaoZ** | **Long** |  |  [optional]
**totalBruto** | **Double** |  | 
**coo** | **String** |  |  [optional]
**acrescimoD** | **Double** |  |  [optional]
**descontoD** | **Double** |  |  [optional]
**acrescimoP** | **Double** |  |  [optional]
**descontoP** | **Double** |  |  [optional]
**pafClienteNome** | **String** |  |  [optional]
**ccf** | **Integer** |  |  [optional]
**idEcf** | **Integer** |  |  [optional]
**pafClienteCpfCnpj** | **Long** |  |  [optional]
**comissionario** | **Long** |  |  [optional]
**comissaoP** | **Double** |  |  [optional]
**idCliente** | **Long** |  |  [optional]
**dataCadastro** | [**OffsetDateTime**](OffsetDateTime.md) | Data/Hora da Criação do Cupom, no formato YYYY-MM-DD HH24:MI:SSOF:00 | 
**venda** | **Boolean** |  | 
**idEntregador** | **Integer** |  |  [optional]
**fichaAtendimentoFicha** | **Integer** |  |  [optional]
**davOrigem** | **Long** |  |  [optional]
**taxaEntrega** | **Double** |  |  [optional]
**davTipoDocumentoId** | **Integer** |  |  [optional]
**ignorarQtdDescPromo** | **Boolean** |  |  [optional]
**produtoTabelaPrecoBaseId** | **Integer** |  |  [optional]
**observacao** | **String** |  |  [optional]
**trocaId** | **Long** |  |  [optional]
**formasPagamentos** | [**List&lt;SincronizarFormaPagamento&gt;**](SincronizarFormaPagamento.md) | Valor obrigatório para Vendas concluídas e não canceladas |  [optional]
**itens** | [**List&lt;SincronizarItemVenda&gt;**](SincronizarItemVenda.md) | Valor obrigatório para Vendas concluídas e não canceladas |  [optional]
**cfe** | [**SincronizarCFe**](SincronizarCFe.md) |  |  [optional]



