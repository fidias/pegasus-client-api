

# ClienteAutocompleteResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**list** | [**List&lt;ClienteAjaxAutoComplete&gt;**](ClienteAjaxAutoComplete.md) |  |  [optional]



