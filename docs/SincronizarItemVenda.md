

# SincronizarItemVenda

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idProduto** | **Integer** |  | 
**custo** | **Double** |  | 
**lucro** | **Double** |  | 
**descricao** | **String** |  | 
**quantidade** | **Double** |  | 
**preco** | **Double** |  | 
**cancelado** | **Boolean** |  | 
**pafNumero** | **Integer** |  |  [optional]
**pafAcrescimoD** | **Double** |  |  [optional]
**pafAcrescimoP** | **Double** |  |  [optional]
**pafDescontoP** | **Double** |  |  [optional]
**pafDescontoD** | **Double** |  |  [optional]
**pafTotalLiq** | **Double** |  | 
**pafTotalizadorParcial** | **String** |  |  [optional]
**pafCancQuant** | **Double** |  |  [optional]
**pafCancValor** | **Double** |  |  [optional]
**pafCancAcresc** | **Double** |  |  [optional]
**pafCasaDecimalQuant** | **Integer** |  |  [optional]
**pafCasaDecimalValor** | **Integer** |  |  [optional]
**pafUnidade** | **String** |  | 
**pafIat** | **String** |  |  [optional]
**pafIppt** | **String** |  |  [optional]
**totalBruto** | **Double** |  |  [optional]
**obs** | **String** |  |  [optional]
**clienteItemId** | **Long** |  | 
**comissao** | **Double** |  |  [optional]
**produto** | **Boolean** |  | 
**comissaoP** | **Double** |  |  [optional]
**comissaoV** | **Double** |  |  [optional]
**tributoFederalD** | **Double** |  |  [optional]
**tributoEstadualD** | **Double** |  |  [optional]
**tributoMunicipalD** | **Double** |  |  [optional]
**observacaoAdicional** | **String** |  |  [optional]
**produtoTabelaPrecoId** | **Integer** |  |  [optional]



