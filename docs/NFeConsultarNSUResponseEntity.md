

# NFeConsultarNSUResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  |  [optional]
**motivo** | **String** |  |  [optional]
**versao** | **String** |  |  [optional]
**ambiente** | **String** |  |  [optional]
**versaoAplicativo** | **String** |  |  [optional]
**dataHoraResposta** | **String** |  |  [optional]
**ultimoNsu** | **String** |  |  [optional]
**maxNsu** | **String** |  |  [optional]



