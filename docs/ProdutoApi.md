# ProdutoApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**alternarArquivado**](ProdutoApi.md#alternarArquivado) | **PUT** /api/produto/bloco-notas/{id}/alternar-arquivado | 
[**alternarDisponivel**](ProdutoApi.md#alternarDisponivel) | **PUT** /api/cfop/{cfop}/alternar-disponivel | 
[**alternarDisponivel1**](ProdutoApi.md#alternarDisponivel1) | **PUT** /api/tabela-preco/{id}/alternar-disponivel/{tipo_pagamento_id} | 
[**aplicar**](ProdutoApi.md#aplicar) | **GET** /api/produto/alterar-em-lote/{id}/aplicar | 
[**atualizar15**](ProdutoApi.md#atualizar15) | **PUT** /api/ncm/{id} | 
[**atualizar16**](ProdutoApi.md#atualizar16) | **PUT** /api/tabela-preco/{id} | 
[**autocomplete13**](ProdutoApi.md#autocomplete13) | **GET** /api/produto/genero/autocomplete | 
[**autocomplete14**](ProdutoApi.md#autocomplete14) | **GET** /api/produto/autocomplete | 
[**autocompleteOperacaoFinanceira**](ProdutoApi.md#autocompleteOperacaoFinanceira) | **GET** /api/cfop/operacao-financeira | 
[**autocompleteOperacaoFinanceiraInternos**](ProdutoApi.md#autocompleteOperacaoFinanceiraInternos) | **GET** /api/cfop/operacao-financeira/internos | 
[**carregar12**](ProdutoApi.md#carregar12) | **GET** /api/produto/configuracao/codigo-barras-balanca | 
[**carregarConfig**](ProdutoApi.md#carregarConfig) | **GET** /api/produto/fortes-fs/config | 
[**carregarSugestao**](ProdutoApi.md#carregarSugestao) | **GET** /api/sugestao-saida/cfop/{cfop} | 
[**criar20**](ProdutoApi.md#criar20) | **POST** /api/produto/bloco-notas | 
[**criar21**](ProdutoApi.md#criar21) | **POST** /api/produto/especie | 
[**criar22**](ProdutoApi.md#criar22) | **POST** /api/produto/historico-inventario | 
[**criar23**](ProdutoApi.md#criar23) | **POST** /api/produto/marca | 
[**criar24**](ProdutoApi.md#criar24) | **POST** /api/ncm | 
[**criar25**](ProdutoApi.md#criar25) | **POST** /api/produto/producao | 
[**criar26**](ProdutoApi.md#criar26) | **POST** /api/produto/producao/{producao_id}/item | 
[**criar27**](ProdutoApi.md#criar27) | **POST** /api/tabela-preco | 
[**criarProdutoEntradaNFe**](ProdutoApi.md#criarProdutoEntradaNFe) | **POST** /api/produto/entrada-nfe | 
[**deletar26**](ProdutoApi.md#deletar26) | **DELETE** /api/produto/bloco-notas/{id} | 
[**deletar27**](ProdutoApi.md#deletar27) | **DELETE** /api/ncm/{id} | 
[**deletar28**](ProdutoApi.md#deletar28) | **DELETE** /api/produto/{produto_base_id}/agregado/{produto_agregado_id} | 
[**deletar29**](ProdutoApi.md#deletar29) | **DELETE** /api/produto/{id}/combustivel | 
[**deletar30**](ProdutoApi.md#deletar30) | **DELETE** /api/produto/{produto_base_id}/ficha-tecnica/{produto_ficha_tecnica_id} | 
[**deletar31**](ProdutoApi.md#deletar31) | **DELETE** /api/produto/{id}/ipi | 
[**deletar32**](ProdutoApi.md#deletar32) | **DELETE** /api/produto/producao/{id} | 
[**deletar33**](ProdutoApi.md#deletar33) | **DELETE** /api/produto/producao/{producao_id}/item/{produto_producao_item_id} | 
[**deletar34**](ProdutoApi.md#deletar34) | **DELETE** /api/produto/{produto_id}/tabela-preco/{produto_tabela_preco_id} | 
[**deletar35**](ProdutoApi.md#deletar35) | **DELETE** /api/tabela-preco/{id} | 
[**deletar36**](ProdutoApi.md#deletar36) | **DELETE** /api/produto/{produto_id}/unidade-entrada/{unidade_id} | 
[**duplicar**](ProdutoApi.md#duplicar) | **GET** /api/produto/{id}/duplicar | 
[**exportarPorPeriodo**](ProdutoApi.md#exportarPorPeriodo) | **GET** /api/produto/fortes-fs/exportar/periodo | 
[**filtrar1**](ProdutoApi.md#filtrar1) | **GET** /api/produto/producao/{producao_id}/item/filtrar | 
[**gerarEntradaSaida1**](ProdutoApi.md#gerarEntradaSaida1) | **GET** /api/produto/producao/{id}/gerar-entrada-saida | 
[**get11**](ProdutoApi.md#get11) | **GET** /api/produto/alterar-em-lote/{id} | 
[**get12**](ProdutoApi.md#get12) | **GET** /api/ncm/{id} | 
[**get13**](ProdutoApi.md#get13) | **GET** /api/produto/producao/{id} | 
[**get14**](ProdutoApi.md#get14) | **GET** /api/tabela-preco/{id} | 
[**importar**](ProdutoApi.md#importar) | **POST** /api/produto/alterar-em-lote/importar | 
[**importarCsv**](ProdutoApi.md#importarCsv) | **POST** /api/ncm/importar/csv | 
[**index12**](ProdutoApi.md#index12) | **GET** /api/produto | 
[**index13**](ProdutoApi.md#index13) | **GET** /api/produto/producao | 
[**index14**](ProdutoApi.md#index14) | **GET** /api/tabela-preco | 
[**listar12**](ProdutoApi.md#listar12) | **GET** /api/produto/alterar-em-lote | 
[**listar13**](ProdutoApi.md#listar13) | **GET** /api/produto/bloco-notas | 
[**listar14**](ProdutoApi.md#listar14) | **GET** /api/cfop | 
[**listar15**](ProdutoApi.md#listar15) | **GET** /api/ncm | 
[**listar17**](ProdutoApi.md#listar17) | **GET** /api/produto/{produto_id}/unidade-entrada | 
[**listarSelect23**](ProdutoApi.md#listarSelect23) | **GET** /api/cfop/select2 | 
[**listarSelect2Internos**](ProdutoApi.md#listarSelect2Internos) | **GET** /api/cfop/select2/internos | 
[**modificarSituacao1**](ProdutoApi.md#modificarSituacao1) | **PUT** /api/produto/producao/{id}/alterar-status | 
[**modificarSituacao2**](ProdutoApi.md#modificarSituacao2) | **PUT** /api/produto/producao/{producao_id}/item/alterar-status/{produto_producao_item_id} | 
[**recalcularEstoque**](ProdutoApi.md#recalcularEstoque) | **GET** /api/produto/recalcular-estoque | 
[**salvar15**](ProdutoApi.md#salvar15) | **POST** /api/produto/configuracao/codigo-barras-balanca | 
[**salvar16**](ProdutoApi.md#salvar16) | **POST** /api/produto/{produto_base_id}/agregado | 
[**salvar17**](ProdutoApi.md#salvar17) | **POST** /api/produto/{id}/combustivel | 
[**salvar18**](ProdutoApi.md#salvar18) | **POST** /api/produto/{produto_base_id}/ficha-tecnica | 
[**salvar19**](ProdutoApi.md#salvar19) | **POST** /api/produto/producao/configuracao | 
[**salvar20**](ProdutoApi.md#salvar20) | **POST** /api/produto/{produto_id}/tabela-preco/{produto_tabela_preco_id} | 
[**salvar21**](ProdutoApi.md#salvar21) | **POST** /api/produto/{produto_id}/unidade-entrada | 
[**salvarCFOP**](ProdutoApi.md#salvarCFOP) | **POST** /api/sugestao-saida/cfop/{cfop} | 
[**salvarCOFINS**](ProdutoApi.md#salvarCOFINS) | **POST** /api/sugestao-saida/cfop/{cfop_entrada}/id_dest/{id_dest}/cofins | 
[**salvarConfig**](ProdutoApi.md#salvarConfig) | **POST** /api/produto/fortes-fs/config | 
[**salvarICMS**](ProdutoApi.md#salvarICMS) | **POST** /api/sugestao-saida/cfop/{cfop_entrada}/id_dest/{id_dest}/icms | 
[**salvarPIS**](ProdutoApi.md#salvarPIS) | **POST** /api/sugestao-saida/cfop/{cfop_entrada}/id_dest/{id_dest}/pis | 
[**select21**](ProdutoApi.md#select21) | **GET** /api/produto/especie/select2 | 
[**select22**](ProdutoApi.md#select22) | **GET** /api/produto/marca/select2 | 
[**verificarCodigoBarras**](ProdutoApi.md#verificarCodigoBarras) | **GET** /api/produto/verificar-codigo-barras | 
[**visualizar1**](ProdutoApi.md#visualizar1) | **GET** /api/produto/historico-inventario/visualizar | 


<a name="alternarArquivado"></a>
# **alternarArquivado**
> alternarArquivado(id, isArquivado)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer id = 56; // Integer | 
    Boolean isArquivado = true; // Boolean | 
    try {
      apiInstance.alternarArquivado(id, isArquivado);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#alternarArquivado");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **isArquivado** | **Boolean**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="alternarDisponivel"></a>
# **alternarDisponivel**
> alternarDisponivel(cfop, isDisponivel)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer cfop = 56; // Integer | 
    Boolean isDisponivel = true; // Boolean | 
    try {
      apiInstance.alternarDisponivel(cfop, isDisponivel);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#alternarDisponivel");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cfop** | **Integer**|  |
 **isDisponivel** | **Boolean**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="alternarDisponivel1"></a>
# **alternarDisponivel1**
> alternarDisponivel1(id, tipoPagamentoId, isDisponivel)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer id = 56; // Integer | 
    Integer tipoPagamentoId = 56; // Integer | 
    Boolean isDisponivel = true; // Boolean | 
    try {
      apiInstance.alternarDisponivel1(id, tipoPagamentoId, isDisponivel);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#alternarDisponivel1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **tipoPagamentoId** | **Integer**|  |
 **isDisponivel** | **Boolean**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="aplicar"></a>
# **aplicar**
> aplicar(id)



Aplicar alterações em lote, de preço, custo e lucro

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      apiInstance.aplicar(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#aplicar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**412** | Pré-condição não satisfeita  * Lote já foi aplicado. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="atualizar15"></a>
# **atualizar15**
> atualizar15(id, codigo, descricao, disponivelUso)



Requisita atualização de NCM

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Long id = 56L; // Long | 
    String codigo = "codigo_example"; // String | 
    String descricao = "descricao_example"; // String | 
    Boolean disponivelUso = true; // Boolean | 
    try {
      apiInstance.atualizar15(id, codigo, descricao, disponivelUso);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#atualizar15");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **codigo** | **String**|  |
 **descricao** | **String**|  |
 **disponivelUso** | **Boolean**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="atualizar16"></a>
# **atualizar16**
> ProdutoTabelaPreco atualizar16(id, nome, usarComoDesconto)



Requisita atualização de uma Tabela de Preço

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer id = 56; // Integer | 
    String nome = "nome_example"; // String | 
    Boolean usarComoDesconto = false; // Boolean | 
    try {
      ProdutoTabelaPreco result = apiInstance.atualizar16(id, nome, usarComoDesconto);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#atualizar16");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **nome** | **String**|  |
 **usarComoDesconto** | **Boolean**|  | [default to false]

### Return type

[**ProdutoTabelaPreco**](ProdutoTabelaPreco.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados de uma Tabela de Preço |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="autocomplete13"></a>
# **autocomplete13**
> GeneroSelect2ResponseEntity autocomplete13(termo, pagina)



Lista de Gêneros de um Produto, com suporte a busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String termo = "termo_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      GeneroSelect2ResponseEntity result = apiInstance.autocomplete13(termo, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#autocomplete13");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  |
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**GeneroSelect2ResponseEntity**](GeneroSelect2ResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de Gêneros de um Produto |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="autocomplete14"></a>
# **autocomplete14**
> ProdutoAutocompleteResponseEntity autocomplete14(termo, produtoTipoSimples, tipoProdutoId)



Solicita lista de produtos/serviços que contenham o termo solicitado na descrição, código do produto ou código de barras

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String termo = "termo_example"; // String | 
    String produtoTipoSimples = "produto"; // String | 
    List<String> tipoProdutoId = Arrays.asList(); // List<String> | 
    try {
      ProdutoAutocompleteResponseEntity result = apiInstance.autocomplete14(termo, produtoTipoSimples, tipoProdutoId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#autocomplete14");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  |
 **produtoTipoSimples** | **String**|  | [optional] [default to produto] [enum: produto, servico, ambos]
 **tipoProdutoId** | [**List&lt;String&gt;**](String.md)|  | [optional] [enum: MERCADORIA_PARA_REVENDA, MATERIA_PRIMA, EMBALAGEM, PRODUTO_EM_PROCESSO, PRODUTO_ACABADO, SUBPRODUTO, PRODUTO_INTERMEDIARIO, MATERIAL_DE_USO_E_CONSUMO, ATIVO_IMOBILIZADO, SERVICOS, OUTROS_INSUMOS, OUTRAS]

### Return type

[**ProdutoAutocompleteResponseEntity**](ProdutoAutocompleteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de produtos/serviços |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="autocompleteOperacaoFinanceira"></a>
# **autocompleteOperacaoFinanceira**
> OperacaoFinanceiraAutocompleteResponseEntity autocompleteOperacaoFinanceira(termo, grupos, pagina)



Lista de Operações Financeiras, com suporte a busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String termo = "termo_example"; // String | 
    String grupos = "grupos_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      OperacaoFinanceiraAutocompleteResponseEntity result = apiInstance.autocompleteOperacaoFinanceira(termo, grupos, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#autocompleteOperacaoFinanceira");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  |
 **grupos** | **String**|  | [optional]
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**OperacaoFinanceiraAutocompleteResponseEntity**](OperacaoFinanceiraAutocompleteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de Operações Financeiras |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="autocompleteOperacaoFinanceiraInternos"></a>
# **autocompleteOperacaoFinanceiraInternos**
> OperacaoFinanceiraAutocompleteResponseEntity autocompleteOperacaoFinanceiraInternos(termo, grupos, pagina)



Lista de Operações Financeiras usadas apenas internamente pelo sistema (sem relação com CFOP&#39;s do Governo), com suporte a busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String termo = "termo_example"; // String | 
    String grupos = "grupos_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      OperacaoFinanceiraAutocompleteResponseEntity result = apiInstance.autocompleteOperacaoFinanceiraInternos(termo, grupos, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#autocompleteOperacaoFinanceiraInternos");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  |
 **grupos** | **String**|  | [optional]
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**OperacaoFinanceiraAutocompleteResponseEntity**](OperacaoFinanceiraAutocompleteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de Operações Financeiras |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="carregar12"></a>
# **carregar12**
> CodigoBarrasBalancaConfiguracaoResponseEntity carregar12()



Retorna conjunto de configurações relacionadas a Código de Barras da Balança

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    try {
      CodigoBarrasBalancaConfiguracaoResponseEntity result = apiInstance.carregar12();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#carregar12");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CodigoBarrasBalancaConfiguracaoResponseEntity**](CodigoBarrasBalancaConfiguracaoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Conjunto de Configurações de Código de Barras da Balança |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="carregarConfig"></a>
# **carregarConfig**
> FortesFSConfiguracaoResponseEntity carregarConfig()



Retorna conjunto de configurações relacionadas ao Fortes FS

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    try {
      FortesFSConfiguracaoResponseEntity result = apiInstance.carregarConfig();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#carregarConfig");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**FortesFSConfiguracaoResponseEntity**](FortesFSConfiguracaoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Conjunto de configurações do Fortes FS |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="carregarSugestao"></a>
# **carregarSugestao**
> carregarSugestao(cfop)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer cfop = 56; // Integer | 
    try {
      apiInstance.carregarSugestao(cfop);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#carregarSugestao");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cfop** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="criar20"></a>
# **criar20**
> criar20(descricao, quantidade)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String descricao = "descricao_example"; // String | 
    DoubleParam quantidade = new DoubleParam(); // DoubleParam | 
    try {
      apiInstance.criar20(descricao, quantidade);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#criar20");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **descricao** | **String**|  |
 **quantidade** | [**DoubleParam**](DoubleParam.md)|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="criar21"></a>
# **criar21**
> Especie criar21(descricao)



Cria uma espécie

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String descricao = "descricao_example"; // String | 
    try {
      Especie result = apiInstance.criar21(descricao);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#criar21");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **descricao** | **String**|  |

### Return type

[**Especie**](Especie.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Retorna dados da espécie |  -  |
**400** | Erro ao criar espécie |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar22"></a>
# **criar22**
> criar22(dataInicial, dataFinal, filtroQtd, motivo)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    OffsetDateTime dataInicial = new OffsetDateTime(); // OffsetDateTime | Data no formato DD/MM/AAAA
    OffsetDateTime dataFinal = new OffsetDateTime(); // OffsetDateTime | Data no formato DD/MM/AAAA
    String filtroQtd = "todos"; // String | Filtro em relação a quantidade dos itens
    String motivo = "final_periodo"; // String | Indicador do motivo do Inventário
    try {
      apiInstance.criar22(dataInicial, dataFinal, filtroQtd, motivo);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#criar22");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dataInicial** | **OffsetDateTime**| Data no formato DD/MM/AAAA |
 **dataFinal** | **OffsetDateTime**| Data no formato DD/MM/AAAA |
 **filtroQtd** | **String**| Filtro em relação a quantidade dos itens | [optional] [default to todos] [enum: todos, somente_negativos, somente_positivos]
 **motivo** | **String**| Indicador do motivo do Inventário | [optional] [default to final_periodo] [enum: final_periodo, mudanca_forma_tributacao, solicitacao_baixa_cadastral, alteracao_regime_pagamento, solicitacao_fiscalizacao]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="criar23"></a>
# **criar23**
> Marca criar23(descricao)



Cria uma marca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String descricao = "descricao_example"; // String | 
    try {
      Marca result = apiInstance.criar23(descricao);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#criar23");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **descricao** | **String**|  |

### Return type

[**Marca**](Marca.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Retorna dados da marca |  -  |
**400** | Erro ao criar marca |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar24"></a>
# **criar24**
> criar24(codigo, descricao, disponivelUso)



Requisita criação de NCM

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String codigo = "codigo_example"; // String | 
    String descricao = "descricao_example"; // String | 
    Boolean disponivelUso = true; // Boolean | 
    try {
      apiInstance.criar24(codigo, descricao, disponivelUso);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#criar24");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **codigo** | **String**|  |
 **descricao** | **String**|  |
 **disponivelUso** | **Boolean**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar25"></a>
# **criar25**
> CodigoProducaoResponseEntity criar25()



Requisita criação de uma Produção

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    try {
      CodigoProducaoResponseEntity result = apiInstance.criar25();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#criar25");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CodigoProducaoResponseEntity**](CodigoProducaoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Código da Produção criada |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar26"></a>
# **criar26**
> ProdutoProducaoCompletoResponseEntity criar26(producaoId, produtoId, quantidade)



Requisita adição de um Produto a uma Produção

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer producaoId = 56; // Integer | 
    Integer produtoId = 56; // Integer | 
    Double quantidade = 3.4D; // Double | 
    try {
      ProdutoProducaoCompletoResponseEntity result = apiInstance.criar26(producaoId, produtoId, quantidade);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#criar26");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **producaoId** | **Integer**|  |
 **produtoId** | **Integer**|  |
 **quantidade** | **Double**|  |

### Return type

[**ProdutoProducaoCompletoResponseEntity**](ProdutoProducaoCompletoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Dados completos de uma Produção |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar27"></a>
# **criar27**
> ProdutoTabelaPreco criar27(nome, usarComoDesconto)



Requisita criação de uma Tabela de Preço

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String nome = "nome_example"; // String | 
    Boolean usarComoDesconto = false; // Boolean | 
    try {
      ProdutoTabelaPreco result = apiInstance.criar27(nome, usarComoDesconto);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#criar27");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nome** | **String**|  |
 **usarComoDesconto** | **Boolean**|  | [default to false]

### Return type

[**ProdutoTabelaPreco**](ProdutoTabelaPreco.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Dados de uma Tabelas de Preço |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criarProdutoEntradaNFe"></a>
# **criarProdutoEntradaNFe**
> ProdutoCriadoResponseEntity criarProdutoEntradaNFe(inlineObject92)



Solicita criação de um produto

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    InlineObject92 inlineObject92 = new InlineObject92(); // InlineObject92 | 
    try {
      ProdutoCriadoResponseEntity result = apiInstance.criarProdutoEntradaNFe(inlineObject92);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#criarProdutoEntradaNFe");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject92** | [**InlineObject92**](InlineObject92.md)|  | [optional]

### Return type

[**ProdutoCriadoResponseEntity**](ProdutoCriadoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna dados do produto criado |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar26"></a>
# **deletar26**
> deletar26(id)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      apiInstance.deletar26(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#deletar26");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="deletar27"></a>
# **deletar27**
> deletar27(id)



Requisita remoção de NCM

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      apiInstance.deletar27(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#deletar27");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar28"></a>
# **deletar28**
> ProdutoAgregadoListaResponseEntity deletar28(produtoBaseId, produtoAgregadoId)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer produtoBaseId = 56; // Integer | 
    Integer produtoAgregadoId = 56; // Integer | 
    try {
      ProdutoAgregadoListaResponseEntity result = apiInstance.deletar28(produtoBaseId, produtoAgregadoId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#deletar28");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **produtoBaseId** | **Integer**|  |
 **produtoAgregadoId** | **Integer**|  |

### Return type

[**ProdutoAgregadoListaResponseEntity**](ProdutoAgregadoListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Deleta produto agregado do produto base |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar29"></a>
# **deletar29**
> deletar29(id)



Deleta dados do combustível de um produto

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      apiInstance.deletar29(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#deletar29");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**404** | Dados do Combustível do Produto {0} não encontrados |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar30"></a>
# **deletar30**
> ProdutoFichaTecnicaListaResponseEntity deletar30(produtoBaseId, produtoFichaTecnicaId)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer produtoBaseId = 56; // Integer | 
    Integer produtoFichaTecnicaId = 56; // Integer | 
    try {
      ProdutoFichaTecnicaListaResponseEntity result = apiInstance.deletar30(produtoBaseId, produtoFichaTecnicaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#deletar30");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **produtoBaseId** | **Integer**|  |
 **produtoFichaTecnicaId** | **Integer**|  |

### Return type

[**ProdutoFichaTecnicaListaResponseEntity**](ProdutoFichaTecnicaListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Deleta Produto (Ficha Técnica) do produto base |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar31"></a>
# **deletar31**
> deletar31(id)



Deleta dados de IPI de um produto

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      apiInstance.deletar31(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#deletar31");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**404** | Dados de IPI do Produto {0} não encontrados |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar32"></a>
# **deletar32**
> deletar32(id)



Requisita remoção de uma Produção

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      apiInstance.deletar32(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#deletar32");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar33"></a>
# **deletar33**
> ProdutoProducaoCompletoResponseEntity deletar33(producaoId, produtoProducaoItemId)



Requisita remoção de um Item de uma Produção

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer producaoId = 56; // Integer | 
    Long produtoProducaoItemId = 56L; // Long | 
    try {
      ProdutoProducaoCompletoResponseEntity result = apiInstance.deletar33(producaoId, produtoProducaoItemId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#deletar33");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **producaoId** | **Integer**|  |
 **produtoProducaoItemId** | **Long**|  |

### Return type

[**ProdutoProducaoCompletoResponseEntity**](ProdutoProducaoCompletoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Dados completos de uma Produção |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar34"></a>
# **deletar34**
> ProdutoTabelaPrecoListaResponseEntity deletar34(produtoId, produtoTabelaPrecoId)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Long produtoId = 56L; // Long | 
    Integer produtoTabelaPrecoId = 56; // Integer | 
    try {
      ProdutoTabelaPrecoListaResponseEntity result = apiInstance.deletar34(produtoId, produtoTabelaPrecoId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#deletar34");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **produtoId** | **Long**|  |
 **produtoTabelaPrecoId** | **Integer**|  |

### Return type

[**ProdutoTabelaPrecoListaResponseEntity**](ProdutoTabelaPrecoListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Deleta produto da tabela de preço |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar35"></a>
# **deletar35**
> deletar35(id)



Requisita remoção de uma Tabela de Preço

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      apiInstance.deletar35(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#deletar35");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar36"></a>
# **deletar36**
> deletar36(produtoId, unidadeId)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer produtoId = 56; // Integer | 
    Integer unidadeId = 56; // Integer | 
    try {
      apiInstance.deletar36(produtoId, unidadeId);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#deletar36");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **produtoId** | **Integer**|  |
 **unidadeId** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="duplicar"></a>
# **duplicar**
> ProdutoAutocompleteResponseEntity duplicar(id)



Solicita copia de um produto a partir do id

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      ProdutoAutocompleteResponseEntity result = apiInstance.duplicar(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#duplicar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

[**ProdutoAutocompleteResponseEntity**](ProdutoAutocompleteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna id do novo produto, o qual foi duplicado |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="exportarPorPeriodo"></a>
# **exportarPorPeriodo**
> exportarPorPeriodo(documento, inicioLancamento, fimLancamento)



Exporta entradas de um período (data de lançamento) para arquivo de importação do Fortes

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    List<String> documento = Arrays.asList(); // List<String> | 
    OffsetDateTime inicioLancamento = new OffsetDateTime(); // OffsetDateTime | 
    OffsetDateTime fimLancamento = new OffsetDateTime(); // OffsetDateTime | 
    try {
      apiInstance.exportarPorPeriodo(documento, inicioLancamento, fimLancamento);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#exportarPorPeriodo");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **documento** | [**List&lt;String&gt;**](String.md)|  | [enum: nfe_entrada, nfe_saida, nfce_saida, cfe_saida]
 **inicioLancamento** | **OffsetDateTime**|  | [optional]
 **fimLancamento** | **OffsetDateTime**|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Arquivo a ser importado pelo Fortes |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="filtrar1"></a>
# **filtrar1**
> ProdutoProducaoListaResponseEntity filtrar1(producaoId, termo)



Filtra lista de Produtos de uma Produção

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer producaoId = 56; // Integer | 
    String termo = "termo_example"; // String | 
    try {
      ProdutoProducaoListaResponseEntity result = apiInstance.filtrar1(producaoId, termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#filtrar1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **producaoId** | **Integer**|  |
 **termo** | **String**|  | [optional]

### Return type

[**ProdutoProducaoListaResponseEntity**](ProdutoProducaoListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna itens do Produção |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="gerarEntradaSaida1"></a>
# **gerarEntradaSaida1**
> ProdutoProducaoCompletoResponseEntity gerarEntradaSaida1(id)



Requisita geração de notas de Entrada/Saída de uma Produção

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      ProdutoProducaoCompletoResponseEntity result = apiInstance.gerarEntradaSaida1(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#gerarEntradaSaida1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

[**ProdutoProducaoCompletoResponseEntity**](ProdutoProducaoCompletoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de uma Produção |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="get11"></a>
# **get11**
> AlterarEmLoteResponseEntity get11(id)



Obter lista de alterações e dados do lote, de preço, custo e lucro

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      AlterarEmLoteResponseEntity result = apiInstance.get11(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#get11");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

[**AlterarEmLoteResponseEntity**](AlterarEmLoteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de alterações |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="get12"></a>
# **get12**
> NCMCompletoResponseEntity get12(id)



Solicita dados de um NCM

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      NCMCompletoResponseEntity result = apiInstance.get12(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#get12");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**NCMCompletoResponseEntity**](NCMCompletoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um NCM |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="get13"></a>
# **get13**
> ProdutoProducaoCompletoResponseEntity get13(id)



Solicita uma Produção completa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      ProdutoProducaoCompletoResponseEntity result = apiInstance.get13(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#get13");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

[**ProdutoProducaoCompletoResponseEntity**](ProdutoProducaoCompletoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de uma Produção |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="get14"></a>
# **get14**
> TabelaPrecoCompleta get14(id)



Solicita uma Tabela de Preço completa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      TabelaPrecoCompleta result = apiInstance.get14(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#get14");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**TabelaPrecoCompleta**](TabelaPrecoCompleta.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de uma Tabela de Preço |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="importar"></a>
# **importar**
> AlterarEmLoteImportarResponseEntity importar(arquivo)



Importar arquivo para alteração em lote, de preço, custo e lucro

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    List<byte[]> arquivo = null; // List<byte[]> | Arquivo CSV
    try {
      AlterarEmLoteImportarResponseEntity result = apiInstance.importar(arquivo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#importar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **arquivo** | [**List&lt;byte[]&gt;**](byte[].md)| Arquivo CSV | [optional]

### Return type

[**AlterarEmLoteImportarResponseEntity**](AlterarEmLoteImportarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista com as alterações a serem aplicadas aos produtos |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="importarCsv"></a>
# **importarCsv**
> importarCsv(cUf, arquivo)



Importar arquivo para atualização do NCM e os percentuais de impostos federais, estaduais e municipais.

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer cUf = 56; // Integer | 
    List<byte[]> arquivo = null; // List<byte[]> | Arquivo CSV
    try {
      apiInstance.importarCsv(cUf, arquivo);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#importarCsv");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cUf** | **Integer**|  |
 **arquivo** | [**List&lt;byte[]&gt;**](byte[].md)| Arquivo CSV | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="index12"></a>
# **index12**
> ProdutoIndexResponseEntity index12(descricao, inicioDescricao, idFornecedor, tipoItemId, codigoBarras, referencia, idProduto, dataInicial, dataFinal, ativoInativo, pagina, ncm, quantidade)



Solicita lista de produto, com opção de busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String descricao = "descricao_example"; // String | 
    Boolean inicioDescricao = false; // Boolean | 
    Integer idFornecedor = 56; // Integer | 
    Integer tipoItemId = 56; // Integer | 
    String codigoBarras = "codigoBarras_example"; // String | 
    String referencia = "referencia_example"; // String | 
    Long idProduto = 56L; // Long | 
    OffsetDateTime dataInicial = 02/01/2019; // OffsetDateTime | Data no formato DD/MM/AAAA
    OffsetDateTime dataFinal = 02/01/2019; // OffsetDateTime | Data no formato DD/MM/AAAA
    Boolean ativoInativo = true; // Boolean | 
    Integer pagina = 1; // Integer | 
    String ncm = "ncm_example"; // String | 
    String quantidade = "quantidade_example"; // String | 
    try {
      ProdutoIndexResponseEntity result = apiInstance.index12(descricao, inicioDescricao, idFornecedor, tipoItemId, codigoBarras, referencia, idProduto, dataInicial, dataFinal, ativoInativo, pagina, ncm, quantidade);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#index12");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **descricao** | **String**|  | [optional]
 **inicioDescricao** | **Boolean**|  | [optional] [default to false]
 **idFornecedor** | **Integer**|  | [optional]
 **tipoItemId** | **Integer**|  | [optional]
 **codigoBarras** | **String**|  | [optional]
 **referencia** | **String**|  | [optional]
 **idProduto** | **Long**|  | [optional]
 **dataInicial** | **OffsetDateTime**| Data no formato DD/MM/AAAA | [optional]
 **dataFinal** | **OffsetDateTime**| Data no formato DD/MM/AAAA | [optional]
 **ativoInativo** | **Boolean**|  | [optional]
 **pagina** | **Integer**|  | [optional] [default to 1]
 **ncm** | **String**|  | [optional]
 **quantidade** | **String**|  | [optional] [enum: todos, maiorQueZero, menorQueZero, igualAZero]

### Return type

[**ProdutoIndexResponseEntity**](ProdutoIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de produtos |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="index13"></a>
# **index13**
> ProdutoProducaoIndexResponseEntity index13(dataCriacaoDe, dataCriacaoAte, criadoPor, statusId, descricao, pagina)



Solicita lista de Metadados de uma Produção, com opção de busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    OffsetDateTime dataCriacaoDe = new OffsetDateTime(); // OffsetDateTime | 
    OffsetDateTime dataCriacaoAte = new OffsetDateTime(); // OffsetDateTime | 
    Integer criadoPor = 56; // Integer | 
    String statusId = "statusId_example"; // String | 
    String descricao = "descricao_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      ProdutoProducaoIndexResponseEntity result = apiInstance.index13(dataCriacaoDe, dataCriacaoAte, criadoPor, statusId, descricao, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#index13");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dataCriacaoDe** | **OffsetDateTime**|  | [optional]
 **dataCriacaoAte** | **OffsetDateTime**|  | [optional]
 **criadoPor** | **Integer**|  | [optional]
 **statusId** | **String**|  | [optional] [enum: EM_ANDAMENTO, FINALIZADO]
 **descricao** | **String**|  | [optional]
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**ProdutoProducaoIndexResponseEntity**](ProdutoProducaoIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de Metadados de uma Produção |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="index14"></a>
# **index14**
> TabelaPrecoIndexResponseEntity index14(nome)



Solicita lista de tabelas de preço, com opção de busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String nome = "nome_example"; // String | 
    try {
      TabelaPrecoIndexResponseEntity result = apiInstance.index14(nome);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#index14");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nome** | **String**|  | [optional]

### Return type

[**TabelaPrecoIndexResponseEntity**](TabelaPrecoIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de tabelas de preço |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="listar12"></a>
# **listar12**
> AlterarEmLoteListaResponseEntity listar12()



Obter lista dos últimos lotes

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    try {
      AlterarEmLoteListaResponseEntity result = apiInstance.listar12();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#listar12");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AlterarEmLoteListaResponseEntity**](AlterarEmLoteListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de lotes |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="listar13"></a>
# **listar13**
> listar13()



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    try {
      apiInstance.listar13();
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#listar13");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="listar14"></a>
# **listar14**
> listar14(cbCfop, cfop, cbDescricao, descricao, cbGrupoCfop, grupoCfop, pagina)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Boolean cbCfop = false; // Boolean | 
    String cfop = "cfop_example"; // String | 
    Boolean cbDescricao = false; // Boolean | 
    String descricao = "descricao_example"; // String | 
    Boolean cbGrupoCfop = false; // Boolean | 
    String grupoCfop = "grupoCfop_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      apiInstance.listar14(cbCfop, cfop, cbDescricao, descricao, cbGrupoCfop, grupoCfop, pagina);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#listar14");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cbCfop** | **Boolean**|  | [optional] [default to false]
 **cfop** | **String**|  | [optional]
 **cbDescricao** | **Boolean**|  | [optional] [default to false]
 **descricao** | **String**|  | [optional]
 **cbGrupoCfop** | **Boolean**|  | [optional] [default to false]
 **grupoCfop** | **String**|  | [optional]
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="listar15"></a>
# **listar15**
> NCMIndexResponseEntity listar15(descricao, codigo, pagina)



Solicita lista de NCM, com opção de busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String descricao = "descricao_example"; // String | 
    String codigo = "codigo_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      NCMIndexResponseEntity result = apiInstance.listar15(descricao, codigo, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#listar15");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **descricao** | **String**|  | [optional]
 **codigo** | **String**|  | [optional]
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**NCMIndexResponseEntity**](NCMIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de NCM |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="listar17"></a>
# **listar17**
> listar17(produtoId)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer produtoId = 56; // Integer | 
    try {
      apiInstance.listar17(produtoId);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#listar17");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **produtoId** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="listarSelect23"></a>
# **listarSelect23**
> listarSelect23(termo, grupos, pagina, somenteHabilitados)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String termo = "termo_example"; // String | 
    String grupos = "grupos_example"; // String | 
    Integer pagina = 1; // Integer | 
    Boolean somenteHabilitados = true; // Boolean | 
    try {
      apiInstance.listarSelect23(termo, grupos, pagina, somenteHabilitados);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#listarSelect23");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  |
 **grupos** | **String**|  | [optional]
 **pagina** | **Integer**|  | [optional] [default to 1]
 **somenteHabilitados** | **Boolean**|  | [optional] [default to true]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="listarSelect2Internos"></a>
# **listarSelect2Internos**
> listarSelect2Internos(termo, grupos, pagina, somenteHabilitados)



Lista de CFOP&#39;s usados apenas internamente pelo sistema (sem relação com CFOP&#39;s do Governo), com suporte a busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String termo = "termo_example"; // String | 
    String grupos = "grupos_example"; // String | 
    Integer pagina = 1; // Integer | 
    Boolean somenteHabilitados = true; // Boolean | 
    try {
      apiInstance.listarSelect2Internos(termo, grupos, pagina, somenteHabilitados);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#listarSelect2Internos");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  |
 **grupos** | **String**|  | [optional]
 **pagina** | **Integer**|  | [optional] [default to 1]
 **somenteHabilitados** | **Boolean**|  | [optional] [default to true]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="modificarSituacao1"></a>
# **modificarSituacao1**
> ProdutoProducaoCompletoResponseEntity modificarSituacao1(id, statusId)



Requisita modificação do Status de uma Produção

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer id = 56; // Integer | 
    String statusId = "statusId_example"; // String | Status para o qual será modificado
    try {
      ProdutoProducaoCompletoResponseEntity result = apiInstance.modificarSituacao1(id, statusId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#modificarSituacao1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **statusId** | **String**| Status para o qual será modificado | [optional] [enum: EM_ANDAMENTO, FINALIZADO]

### Return type

[**ProdutoProducaoCompletoResponseEntity**](ProdutoProducaoCompletoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de uma Produção |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="modificarSituacao2"></a>
# **modificarSituacao2**
> ProdutoProducaoListaResponseEntity modificarSituacao2(producaoId, produtoProducaoItemId, itemStatusId)



Requisita modificação do Status de um item da Produção

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer producaoId = 56; // Integer | 
    Long produtoProducaoItemId = 56L; // Long | 
    Integer itemStatusId = 56; // Integer | Status para o qual será modificado
    try {
      ProdutoProducaoListaResponseEntity result = apiInstance.modificarSituacao2(producaoId, produtoProducaoItemId, itemStatusId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#modificarSituacao2");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **producaoId** | **Integer**|  |
 **produtoProducaoItemId** | **Long**|  |
 **itemStatusId** | **Integer**| Status para o qual será modificado | [optional]

### Return type

[**ProdutoProducaoListaResponseEntity**](ProdutoProducaoListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna itens do Produção |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="recalcularEstoque"></a>
# **recalcularEstoque**
> recalcularEstoque()



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    try {
      apiInstance.recalcularEstoque();
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#recalcularEstoque");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar15"></a>
# **salvar15**
> CodigoBarrasBalancaConfiguracaoResponseEntity salvar15(digitosIniciais, totalDigitos, parteADigitoInicial, parteADigitoFinal, parteBDigitoInicial, parteBDigitoFinal, parteBQuantidadeCasas, parteBUtilizarComo, tratamentoDecimal)



Salva conjunto de Configurações de Código de Barras da Balança

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String digitosIniciais = "digitosIniciais_example"; // String | 
    Integer totalDigitos = 56; // Integer | 
    Integer parteADigitoInicial = 56; // Integer | 
    Integer parteADigitoFinal = 56; // Integer | 
    Integer parteBDigitoInicial = 56; // Integer | 
    Integer parteBDigitoFinal = 56; // Integer | 
    Integer parteBQuantidadeCasas = 56; // Integer | 
    String parteBUtilizarComo = "parteBUtilizarComo_example"; // String | 
    String tratamentoDecimal = "tratamentoDecimal_example"; // String | 
    try {
      CodigoBarrasBalancaConfiguracaoResponseEntity result = apiInstance.salvar15(digitosIniciais, totalDigitos, parteADigitoInicial, parteADigitoFinal, parteBDigitoInicial, parteBDigitoFinal, parteBQuantidadeCasas, parteBUtilizarComo, tratamentoDecimal);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#salvar15");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **digitosIniciais** | **String**|  |
 **totalDigitos** | **Integer**|  |
 **parteADigitoInicial** | **Integer**|  |
 **parteADigitoFinal** | **Integer**|  |
 **parteBDigitoInicial** | **Integer**|  |
 **parteBDigitoFinal** | **Integer**|  |
 **parteBQuantidadeCasas** | **Integer**|  |
 **parteBUtilizarComo** | **String**|  | [enum: peso, valor]
 **tratamentoDecimal** | **String**|  | [enum: arredondamento, truncamento]

### Return type

[**CodigoBarrasBalancaConfiguracaoResponseEntity**](CodigoBarrasBalancaConfiguracaoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Conjunto de Configurações de Código de Barras da Balança |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar16"></a>
# **salvar16**
> ProdutoAgregadoListaResponseEntity salvar16(produtoBaseId, produtoAgregadoId, quantidade)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer produtoBaseId = 56; // Integer | 
    Integer produtoAgregadoId = 56; // Integer | 
    Double quantidade = 3.4D; // Double | 
    try {
      ProdutoAgregadoListaResponseEntity result = apiInstance.salvar16(produtoBaseId, produtoAgregadoId, quantidade);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#salvar16");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **produtoBaseId** | **Integer**|  |
 **produtoAgregadoId** | **Integer**|  |
 **quantidade** | **Double**|  |

### Return type

[**ProdutoAgregadoListaResponseEntity**](ProdutoAgregadoListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Quando Produto Base e Produto Agregado já existem, quantidade é atualizada |  -  |
**201** | Quando Produto Base e Produto Agregado não existem |  -  |
**400** | Falha na requisição  * Produto não encontrado. * Produto agregado não pode ser o mesmo produto base. * Quantidade deve ser maior que zero. * Unidade do Item não informada. * Quantidade não pode ser fracionada para este produto. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar17"></a>
# **salvar17**
> salvar17(id, cProdAnp, vPart, qTemp, pGlp, pGnn, pGni, copiarQcomParaQtemp)



Salva dados do combustível de um produto

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer id = 56; // Integer | 
    Long cProdAnp = 56L; // Long | 
    Double vPart = 3.4D; // Double | 
    Double qTemp = 3.4D; // Double | 
    Double pGlp = 3.4D; // Double | 
    Double pGnn = 3.4D; // Double | 
    Double pGni = 3.4D; // Double | 
    Boolean copiarQcomParaQtemp = false; // Boolean | 
    try {
      apiInstance.salvar17(id, cProdAnp, vPart, qTemp, pGlp, pGnn, pGni, copiarQcomParaQtemp);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#salvar17");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **cProdAnp** | **Long**|  |
 **vPart** | **Double**|  | [optional]
 **qTemp** | **Double**|  | [optional]
 **pGlp** | **Double**|  | [optional]
 **pGnn** | **Double**|  | [optional]
 **pGni** | **Double**|  | [optional]
 **copiarQcomParaQtemp** | **Boolean**|  | [optional] [default to false]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados do combustível atualizados com sucesso |  -  |
**201** | Dados do combustível criados com sucesso |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar18"></a>
# **salvar18**
> ProdutoFichaTecnicaListaResponseEntity salvar18(produtoBaseId, produtoFichaTecnicaId, quantidade)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer produtoBaseId = 56; // Integer | 
    Integer produtoFichaTecnicaId = 56; // Integer | 
    Double quantidade = 3.4D; // Double | 
    try {
      ProdutoFichaTecnicaListaResponseEntity result = apiInstance.salvar18(produtoBaseId, produtoFichaTecnicaId, quantidade);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#salvar18");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **produtoBaseId** | **Integer**|  |
 **produtoFichaTecnicaId** | **Integer**|  |
 **quantidade** | **Double**|  |

### Return type

[**ProdutoFichaTecnicaListaResponseEntity**](ProdutoFichaTecnicaListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Quando Produto Base e Produto (Ficha Técnica) já existem, quantidade é atualizada |  -  |
**201** | Quando Produto Base e Produto (Ficha Técnica) não existem |  -  |
**400** | Falha na requisição  * Produto não encontrado. * Produto (Ficha Técnica) não pode ser o mesmo produto base. * Quantidade deve ser maior que zero. * Unidade do Item não informada. * Quantidade não pode ser fracionada para este produto. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar19"></a>
# **salvar19**
> salvar19(producaoOperacaoFinanceiraEntrada, producaoOperacaoFinanceiraSaida)



Salva conjunto de configurações de Produção

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer producaoOperacaoFinanceiraEntrada = 56; // Integer | 
    Integer producaoOperacaoFinanceiraSaida = 56; // Integer | 
    try {
      apiInstance.salvar19(producaoOperacaoFinanceiraEntrada, producaoOperacaoFinanceiraSaida);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#salvar19");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **producaoOperacaoFinanceiraEntrada** | **Integer**|  | [optional]
 **producaoOperacaoFinanceiraSaida** | **Integer**|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar20"></a>
# **salvar20**
> ProdutoTabelaPrecoListaResponseEntity salvar20(produtoId, produtoTabelaPrecoId, preco, custo)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer produtoId = 56; // Integer | 
    Integer produtoTabelaPrecoId = 56; // Integer | 
    Double preco = 3.4D; // Double | 
    Double custo = 3.4D; // Double | 
    try {
      ProdutoTabelaPrecoListaResponseEntity result = apiInstance.salvar20(produtoId, produtoTabelaPrecoId, preco, custo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#salvar20");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **produtoId** | **Integer**|  |
 **produtoTabelaPrecoId** | **Integer**|  |
 **preco** | **Double**|  |
 **custo** | **Double**|  |

### Return type

[**ProdutoTabelaPrecoListaResponseEntity**](ProdutoTabelaPrecoListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Quando produto já existe na tabela de preço informada |  -  |
**201** | Quando produto foi criado na tabela de preço informada |  -  |
**400** | Falha na requisição  * Produto {0} não encontrado. |  -  |
**412** | Pré-condição não satisfeita  * Sem permissão para definir Preço abaixo ou igual ao Custo. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar21"></a>
# **salvar21**
> salvar21(produtoId, unidadeId, quantidadeEquivalente, codBarrasUnidTributavel)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer produtoId = 56; // Integer | 
    IntegerParam unidadeId = new IntegerParam(); // IntegerParam | 
    DoubleParam quantidadeEquivalente = new DoubleParam(); // DoubleParam | 
    String codBarrasUnidTributavel = "codBarrasUnidTributavel_example"; // String | 
    try {
      apiInstance.salvar21(produtoId, unidadeId, quantidadeEquivalente, codBarrasUnidTributavel);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#salvar21");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **produtoId** | **Integer**|  |
 **unidadeId** | [**IntegerParam**](IntegerParam.md)|  |
 **quantidadeEquivalente** | [**DoubleParam**](DoubleParam.md)|  |
 **codBarrasUnidTributavel** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="salvarCFOP"></a>
# **salvarCFOP**
> salvarCFOP(cfop, inlineObject100)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer cfop = 56; // Integer | 
    InlineObject100 inlineObject100 = new InlineObject100(); // InlineObject100 | 
    try {
      apiInstance.salvarCFOP(cfop, inlineObject100);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#salvarCFOP");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cfop** | **Integer**|  |
 **inlineObject100** | [**InlineObject100**](InlineObject100.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="salvarCOFINS"></a>
# **salvarCOFINS**
> salvarCOFINS(cfopEntrada, idDest, inlineObject101)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer cfopEntrada = 56; // Integer | 
    String idDest = "idDest_example"; // String | 
    InlineObject101 inlineObject101 = new InlineObject101(); // InlineObject101 | 
    try {
      apiInstance.salvarCOFINS(cfopEntrada, idDest, inlineObject101);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#salvarCOFINS");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cfopEntrada** | **Integer**|  |
 **idDest** | **String**|  | [enum: Operacao_Interna, Operacao_Interestadual, Operacao_Com_Exterior]
 **inlineObject101** | [**InlineObject101**](InlineObject101.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="salvarConfig"></a>
# **salvarConfig**
> salvarConfig(nomeEmpresa, faturaTipoDocumento)



Salva conjunto de configurações do Fortes FS

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String nomeEmpresa = "nomeEmpresa_example"; // String | 
    IntegerParam faturaTipoDocumento = new IntegerParam(); // IntegerParam | 
    try {
      apiInstance.salvarConfig(nomeEmpresa, faturaTipoDocumento);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#salvarConfig");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nomeEmpresa** | **String**|  |
 **faturaTipoDocumento** | [**IntegerParam**](IntegerParam.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvarICMS"></a>
# **salvarICMS**
> salvarICMS(cfopEntrada, idDest, inlineObject102)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer cfopEntrada = 56; // Integer | 
    String idDest = "idDest_example"; // String | 
    InlineObject102 inlineObject102 = new InlineObject102(); // InlineObject102 | 
    try {
      apiInstance.salvarICMS(cfopEntrada, idDest, inlineObject102);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#salvarICMS");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cfopEntrada** | **Integer**|  |
 **idDest** | **String**|  | [enum: Operacao_Interna, Operacao_Interestadual, Operacao_Com_Exterior]
 **inlineObject102** | [**InlineObject102**](InlineObject102.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="salvarPIS"></a>
# **salvarPIS**
> salvarPIS(cfopEntrada, idDest, inlineObject103)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    Integer cfopEntrada = 56; // Integer | 
    String idDest = "idDest_example"; // String | 
    InlineObject103 inlineObject103 = new InlineObject103(); // InlineObject103 | 
    try {
      apiInstance.salvarPIS(cfopEntrada, idDest, inlineObject103);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#salvarPIS");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cfopEntrada** | **Integer**|  |
 **idDest** | **String**|  | [enum: Operacao_Interna, Operacao_Interestadual, Operacao_Com_Exterior]
 **inlineObject103** | [**InlineObject103**](InlineObject103.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="select21"></a>
# **select21**
> EspecieSelect2ResponseEntity select21(termo, pagina)



Lista de espécies, com suporte a busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String termo = "termo_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      EspecieSelect2ResponseEntity result = apiInstance.select21(termo, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#select21");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  |
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**EspecieSelect2ResponseEntity**](EspecieSelect2ResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de espécies |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="select22"></a>
# **select22**
> MarcaSelect2ResponseEntity select22(termo, pagina)



Lista de marcas, com suporte a busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    String termo = "termo_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      MarcaSelect2ResponseEntity result = apiInstance.select22(termo, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#select22");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  |
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**MarcaSelect2ResponseEntity**](MarcaSelect2ResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de marcas |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="verificarCodigoBarras"></a>
# **verificarCodigoBarras**
> verificarCodigoBarras(codigoBarras)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    NumericOnlyParam codigoBarras = new NumericOnlyParam(); // NumericOnlyParam | 
    try {
      apiInstance.verificarCodigoBarras(codigoBarras);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#verificarCodigoBarras");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **codigoBarras** | [**NumericOnlyParam**](.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="visualizar1"></a>
# **visualizar1**
> visualizar1(dataInicial, dataFinal, motivo, filtroQtd)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ProdutoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ProdutoApi apiInstance = new ProdutoApi(defaultClient);
    OffsetDateTime dataInicial = 02/01/2019; // OffsetDateTime | Data no formato DD/MM/AAAA
    OffsetDateTime dataFinal = 02/01/2019; // OffsetDateTime | Data no formato DD/MM/AAAA
    String motivo = "motivo_example"; // String | Indicador do motivo do Inventário
    String filtroQtd = "todos"; // String | Filtro em relação a quantidade dos itens
    try {
      apiInstance.visualizar1(dataInicial, dataFinal, motivo, filtroQtd);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProdutoApi#visualizar1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dataInicial** | **OffsetDateTime**| Data no formato DD/MM/AAAA |
 **dataFinal** | **OffsetDateTime**| Data no formato DD/MM/AAAA |
 **motivo** | **String**| Indicador do motivo do Inventário | [enum: final_periodo, mudanca_forma_tributacao, solicitacao_baixa_cadastral, alteracao_regime_pagamento, solicitacao_fiscalizacao]
 **filtroQtd** | **String**| Filtro em relação a quantidade dos itens | [optional] [default to todos] [enum: todos, somente_negativos, somente_positivos]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

