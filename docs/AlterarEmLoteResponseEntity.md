

# AlterarEmLoteResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lote** | [**ProdutoAlterarEmLoteGetLote**](ProdutoAlterarEmLoteGetLote.md) |  |  [optional]
**lista** | [**List&lt;ProdutoAlterarEmLoteGetItemLista&gt;**](ProdutoAlterarEmLoteGetItemLista.md) |  |  [optional]



