

# InlineObject80

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parentId** | **Long** |  |  [optional]
**descricao** | **String** |  | 
**fluxoOperacao** | [**FluxoOperacaoEnum**](#FluxoOperacaoEnum) |  |  [optional]



## Enum: FluxoOperacaoEnum

Name | Value
---- | -----
ENTRADA | &quot;Entrada&quot;
SAIDA | &quot;Saida&quot;



