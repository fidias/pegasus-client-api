

# MDFeCarregar

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numeroMdfe** | [**NumeroMdfe**](NumeroMdfe.md) |  | 
**ambienteMdfe** | [**AmbienteMdfe**](AmbienteMdfe.md) |  | 
**ufEncerMdfe** | [**UfEncerMdfe**](UfEncerMdfe.md) |  | 
**munEncerMdfe** | [**MunEncerMdfe**](MunEncerMdfe.md) |  | 
**serieMdfe** | [**SerieMdfe**](SerieMdfe.md) |  | 
**tipoCargaPadrao** | [**TipoCargaPadrao**](TipoCargaPadrao.md) |  | 



