

# InlineObject60

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numeroNfce** | [**LongParam**](LongParam.md) |  | 
**serieNfce** | [**ShortParam**](ShortParam.md) |  | 
**idTokenCscNfce** | [**IntegerParam**](IntegerParam.md) |  | 
**codCscNfce** | **String** |  | 



