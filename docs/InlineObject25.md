

# InlineObject25

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**operacao** | [**OperacaoEnum**](#OperacaoEnum) | Define em qual dos totais a variação percentual será realizada | 
**valorTextual** | **String** |  | 



## Enum: OperacaoEnum

Name | Value
---- | -----
TOTAL | &quot;total&quot;
PRODUTOS | &quot;produtos&quot;
SERVICOS | &quot;servicos&quot;



