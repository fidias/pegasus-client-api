

# VendaSincronizarResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identificadorUnico** | **String** |  |  [optional]
**idVendaBalcao** | **Long** |  |  [optional]
**clienteVendaId** | **Long** |  |  [optional]
**caixaId** | **Integer** |  |  [optional]
**itens** | [**List&lt;ResponseSincronizarItem&gt;**](ResponseSincronizarItem.md) |  |  [optional]
**formas** | [**List&lt;ResponseSincronizarFormaPagamento&gt;**](ResponseSincronizarFormaPagamento.md) |  |  [optional]



