

# EventoCancelarMDFe

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numeroProtocolo** | **Integer** |  |  [optional]
**justificativa** | **String** |  |  [optional]
**versao** | **String** |  |  [optional]
**info** | [**Info**](Info.md) |  |  [optional]



