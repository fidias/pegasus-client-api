

# Info4

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  |  [optional]
**motivo** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**chave** | **String** |  |  [optional]
**tipoEvento** | **String** |  |  [optional]
**evento** | **String** |  |  [optional]
**dataHoraEvento** | **String** |  |  [optional]
**numProtocolo** | **String** |  |  [optional]
**emailDestinatario** | **String** |  |  [optional]



