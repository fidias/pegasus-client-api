

# ICMSAliquotaPadraoResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**aliquotas** | **String** |  |  [optional]
**eventos** | [**List&lt;ICMSAliquotaPadraoEventoItem&gt;**](ICMSAliquotaPadraoEventoItem.md) |  |  [optional]



