

# VfpeRetVerificarStatusValidador

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bin** | **String** |  | 
**parcelas** | **Long** |  | 
**donoCartao** | **String** |  | 
**tipoBandeira** | **String** |  | 
**dataExpiracao** | **String** |  | 
**codigoPagamento** | **String** |  | 
**codigoAutorizacao** | **String** |  | 
**instituicaoFinanceira** | **String** |  | 
**ultimosQuatroDigitos** | **String** |  | 



