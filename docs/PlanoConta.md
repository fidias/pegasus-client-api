

# PlanoConta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**descricao** | **String** |  |  [optional]
**parent** | [**PlanoConta**](PlanoConta.md) |  |  [optional]
**fluxoOperacao** | [**FluxoOperacaoEnum**](#FluxoOperacaoEnum) |  |  [optional]
**identificador** | **String** |  |  [optional]
**codigoSistemaExterno** | **String** |  |  [optional]
**deduzido** | **Boolean** |  |  [optional]
**classificacao** | [**ClassificacaoEnum**](#ClassificacaoEnum) |  |  [optional]



## Enum: FluxoOperacaoEnum

Name | Value
---- | -----
ENTRADA | &quot;Entrada&quot;
SAIDA | &quot;Saida&quot;



## Enum: ClassificacaoEnum

Name | Value
---- | -----
FIXAS | &quot;Fixas&quot;
VARIAVEIS | &quot;Variaveis&quot;



