

# PlanoContaEventosResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eventos** | [**List&lt;PlanoContaEventoItem&gt;**](PlanoContaEventoItem.md) |  |  [optional]



