

# CfopOperacaoFinanceiraItemAutocomplete

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**cfop** | **Long** |  | 
**natureza** | **String** |  | 
**aplicacao** | **String** |  | 
**fluxoOperacao** | [**FluxoOperacaoEnum**](#FluxoOperacaoEnum) |  | 



## Enum: FluxoOperacaoEnum

Name | Value
---- | -----
ENTRADA | &quot;Entrada&quot;
SAIDA | &quot;Saida&quot;



