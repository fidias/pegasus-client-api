# ServioApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**select23**](ServioApi.md#select23) | **GET** /api/servico/codigo-servico/select2 | 


<a name="select23"></a>
# **select23**
> CodigoServicoSelect2ResponseEntity select23(termo, pagina)



Lista de Códigos de Serviços, com suporte a busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ServioApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ServioApi apiInstance = new ServioApi(defaultClient);
    String termo = "termo_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      CodigoServicoSelect2ResponseEntity result = apiInstance.select23(termo, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ServioApi#select23");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  |
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**CodigoServicoSelect2ResponseEntity**](CodigoServicoSelect2ResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de Códigos de Serviços |  -  |
**500** | Erro desconhecido no servidor |  -  |

