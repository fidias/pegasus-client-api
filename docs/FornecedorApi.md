# FornecedorApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**autocomplete6**](FornecedorApi.md#autocomplete6) | **GET** /api/fornecedor/autocomplete | 
[**getFornecedor**](FornecedorApi.md#getFornecedor) | **GET** /api/fornecedor/{id} | 


<a name="autocomplete6"></a>
# **autocomplete6**
> FornecedorListaResponseEntity autocomplete6(termo)



Solicita lista de fornecedores que contenham o termo solicitado no nome ou nome fantasia

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.FornecedorApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    FornecedorApi apiInstance = new FornecedorApi(defaultClient);
    String termo = "termo_example"; // String | 
    try {
      FornecedorListaResponseEntity result = apiInstance.autocomplete6(termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling FornecedorApi#autocomplete6");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  | [optional]

### Return type

[**FornecedorListaResponseEntity**](FornecedorListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de fornecedores |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="getFornecedor"></a>
# **getFornecedor**
> FornecedorResponseEntity getFornecedor(id)



Solicita um fornecedores pelo ID

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.FornecedorApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    FornecedorApi apiInstance = new FornecedorApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      FornecedorResponseEntity result = apiInstance.getFornecedor(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling FornecedorApi#getFornecedor");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

[**FornecedorResponseEntity**](FornecedorResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna dados do fornecedor |  -  |
**404** | Registro não encontrado |  -  |
**500** | Erro desconhecido no servidor |  -  |

