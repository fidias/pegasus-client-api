

# ProdutoFichaTecnicaItemLista

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**produtoFichaTecnicaId** | **Long** |  | 
**descricao** | **String** |  | 
**quantidade** | **Double** |  | 
**unidade** | **String** |  | 
**custo** | **Double** |  | 



