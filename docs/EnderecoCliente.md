

# EnderecoCliente

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**titulo** | **String** |  | 
**logradouro** | **String** |  | 
**numero** | **String** |  | 
**cep** | **String** |  |  [optional]
**pontoReferencia** | **String** |  |  [optional]
**complemento** | **String** |  |  [optional]
**bairro** | [**Bairro**](Bairro.md) |  | 
**municipio** | [**Municipio**](Municipio.md) |  | 
**uf** | [**Uf**](Uf.md) |  | 
**enderecoPadrao** | **Boolean** |  | 



