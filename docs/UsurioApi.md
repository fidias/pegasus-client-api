# UsurioApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**autocomplete16**](UsurioApi.md#autocomplete16) | **GET** /api/usuario/autocomplete | 


<a name="autocomplete16"></a>
# **autocomplete16**
> UsuarioAutocompleteResponseEntity autocomplete16(termo)



Solicita lista de Usuários que contenham o termo solicitado no login/apelido

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.UsurioApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    UsurioApi apiInstance = new UsurioApi(defaultClient);
    String termo = "termo_example"; // String | 
    try {
      UsuarioAutocompleteResponseEntity result = apiInstance.autocomplete16(termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling UsurioApi#autocomplete16");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  | [optional]

### Return type

[**UsuarioAutocompleteResponseEntity**](UsuarioAutocompleteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de Usuários |  -  |
**500** | Erro desconhecido no servidor |  -  |

