

# InlineObject9

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tipoTransacaoId** | **Integer** |  | 
**valor** | **Double** |  | 
**historico** | **String** |  | 
**identificadorUnico** | **String** |  | 



