

# TipoDocumentoAutocompleteResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;TipoDocumentoAutocomplete&gt;**](TipoDocumentoAutocomplete.md) |  |  [optional]



