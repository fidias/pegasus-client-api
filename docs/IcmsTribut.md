

# IcmsTribut

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**dado** | **String** |  | 
**valor** | **Double** |  | 
**numero** | **Double** |  | 
**string** | **String** |  | 
**textoAjuda** | **String** |  |  [optional]



