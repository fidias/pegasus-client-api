

# TabelaPrecoTipoPagamento

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**nome** | **String** |  | 
**urlImg** | **String** |  | 
**cartao** | **Boolean** |  | 
**disponivel** | **Boolean** |  | 
**obs** | **String** |  |  [optional]
**modalidadeCartaoId** | **Long** |  |  [optional]
**contaId** | **Long** |  |  [optional]
**nomeConta** | **String** |  |  [optional]
**cssBgColor** | **String** |  |  [optional]
**modalidadeCartao** | **String** |  |  [optional]
**enabled** | **Boolean** |  | 



