

# PainelPlanoContaResumoResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**formasPagamento** | [**Tipo**](Tipo.md) |  |  [optional]
**itensVenda** | [**Tipo**](Tipo.md) |  |  [optional]
**despesa** | **Double** |  |  [optional]



