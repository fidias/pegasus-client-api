

# InlineObject48

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**justificativa** | **String** |  | 
**dataHoraEvento** | [**OffsetDateTime**](OffsetDateTime.md) | Data no formato DD/MM/AAAA HH:MM:SS | 



