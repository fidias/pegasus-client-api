

# InlineObject32

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**grupoMercadoriaId** | [**IntegerParam**](IntegerParam.md) |  | 
**nomeImpressora** | **String** |  | 
**espacamentoVertical** | [**IntegerParam**](IntegerParam.md) |  | 
**ecfModeloId** | [**IntegerParam**](IntegerParam.md) |  | 
**impressaoAutomatica** | **Boolean** |  | 



