

# NFeCarregarConfiguracoes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**serie** | [**Serie**](Serie.md) |  | 
**modelo** | [**Modelo**](Modelo.md) |  | 
**versao** | [**Versao**](Versao.md) |  | 
**ambiente** | [**Ambiente**](Ambiente.md) |  | 
**infAdic** | [**InfAdic**](InfAdic.md) |  | 
**numeroNfe** | [**NumeroNfe**](NumeroNfe.md) |  | 
**icmsTribut** | [**IcmsTribut**](IcmsTribut.md) |  | 
**natOperacao** | [**NatOperacao**](NatOperacao.md) |  | 
**tributSimples** | [**TributSimples**](TributSimples.md) |  | 
**importDiluirD** | [**ImportDiluirD**](ImportDiluirD.md) |  | 
**modFretePadrao** | [**ModFretePadrao**](ModFretePadrao.md) |  | 
**codCscNfce** | [**CodCscNfce**](CodCscNfce.md) |  | 
**idTokenCscNfce** | [**IdTokenCscNfce**](IdTokenCscNfce.md) |  | 
**numeroNfce** | [**NumeroNfce**](NumeroNfce.md) |  | 
**serieNfce** | [**SerieNfce**](SerieNfce.md) |  | 
**cfopVendaPadrao** | [**CfopVendaPadrao**](CfopVendaPadrao.md) |  | 
**cfopDevolucaoPadrao** | [**CfopDevolucaoPadrao**](CfopDevolucaoPadrao.md) |  | 



