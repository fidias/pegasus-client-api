# MdfEApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**adicionar1**](MdfEApi.md#adicionar1) | **POST** /api/mdfe/{mdfe_id}/inf-mun/{procedimento} | 
[**adicionar2**](MdfEApi.md#adicionar2) | **POST** /api/mdfe/{id}/inf-percurso | 
[**cancelar1**](MdfEApi.md#cancelar1) | **POST** /api/mdfe/{id}/evento/cancelar | 
[**carregar7**](MdfEApi.md#carregar7) | **GET** /api/mdfe/configuracao | 
[**carregarXml**](MdfEApi.md#carregarXml) | **GET** /api/mdfe/xml/{id} | 
[**consultarSituacao**](MdfEApi.md#consultarSituacao) | **GET** /api/mdfe/situacao/{id} | 
[**deletar11**](MdfEApi.md#deletar11) | **DELETE** /api/mdfe/{mdfe_id}/inf-mun/{procedimento}/{id} | 
[**deletar12**](MdfEApi.md#deletar12) | **DELETE** /api/mdfe/{id}/inf-percurso/{c_uf} | 
[**deletar13**](MdfEApi.md#deletar13) | **DELETE** /api/mdfe/{mdfe_id}/produto-predominante | 
[**deletar14**](MdfEApi.md#deletar14) | **DELETE** /api/mdfe/veiculo-tracao/{emi_veic_tracao_id}/proprietario | 
[**deletar15**](MdfEApi.md#deletar15) | **DELETE** /api/mdfe/{mdfe_id}/rodo/veiculo-reboque/{emi_veic_tracao_id} | 
[**deletar16**](MdfEApi.md#deletar16) | **DELETE** /api/mdfe/{mdfe_id}/rodo/ciot/{ciot} | 
[**encerrar1**](MdfEApi.md#encerrar1) | **POST** /api/mdfe/{id}/evento/encerrar | 
[**exportarDAMDFE**](MdfEApi.md#exportarDAMDFE) | **GET** /api/mdfe/exportar/pdf/{id} | 
[**exportarXML**](MdfEApi.md#exportarXML) | **GET** /api/mdfe/exportar/xml/{id} | 
[**historicoTransmissao**](MdfEApi.md#historicoTransmissao) | **GET** /api/mdfe/transmissao/{id} | 
[**incluirCondutor**](MdfEApi.md#incluirCondutor) | **POST** /api/mdfe/{id}/evento/incluir-condutor | 
[**listar3**](MdfEApi.md#listar3) | **GET** /api/mdfe/{mdfe_id}/rodo/veiculo-reboque | 
[**listar4**](MdfEApi.md#listar4) | **GET** /api/mdfe/{mdfe_id}/rodo/ciot | 
[**listarSelect21**](MdfEApi.md#listarSelect21) | **GET** /api/mdfe/instituicao-pagamento/select2 | 
[**salvar10**](MdfEApi.md#salvar10) | **POST** /api/mdfe/{mdfe_id}/rodo/veiculo-reboque | 
[**salvar11**](MdfEApi.md#salvar11) | **POST** /api/mdfe/{mdfe_id}/rodo/ciot | 
[**salvar6**](MdfEApi.md#salvar6) | **POST** /api/mdfe/configuracao | 
[**salvar7**](MdfEApi.md#salvar7) | **POST** /api/mdfe/{mdfe_id}/contas-pagar | 
[**salvar8**](MdfEApi.md#salvar8) | **POST** /api/mdfe/{mdfe_id}/produto-predominante | 
[**salvar9**](MdfEApi.md#salvar9) | **POST** /api/mdfe/veiculo-tracao/{emi_veic_tracao_id}/proprietario | 


<a name="adicionar1"></a>
# **adicionar1**
> adicionar1(mdfeId, procedimento, cMun)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long mdfeId = 56L; // Long | 
    String procedimento = "procedimento_example"; // String | 
    IntegerParam cMun = new IntegerParam(); // IntegerParam | 
    try {
      apiInstance.adicionar1(mdfeId, procedimento, cMun);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#adicionar1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mdfeId** | **Long**|  |
 **procedimento** | **String**|  | [enum: carrega, descarrega]
 **cMun** | [**IntegerParam**](IntegerParam.md)|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="adicionar2"></a>
# **adicionar2**
> adicionar2(id, inlineObject52)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long id = 56L; // Long | 
    InlineObject52 inlineObject52 = new InlineObject52(); // InlineObject52 | 
    try {
      apiInstance.adicionar2(id, inlineObject52);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#adicionar2");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **inlineObject52** | [**InlineObject52**](InlineObject52.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="cancelar1"></a>
# **cancelar1**
> EventoCancelarMDFe cancelar1(id, justificativa, dataHoraEvento)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long id = 56L; // Long | 
    String justificativa = "justificativa_example"; // String | 
    OffsetDateTime dataHoraEvento = new OffsetDateTime(); // OffsetDateTime | Data no formato DD/MM/AAAA HH:MM:SS
    try {
      EventoCancelarMDFe result = apiInstance.cancelar1(id, justificativa, dataHoraEvento);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#cancelar1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **justificativa** | **String**|  |
 **dataHoraEvento** | **OffsetDateTime**| Data no formato DD/MM/AAAA HH:MM:SS |

### Return type

[**EventoCancelarMDFe**](EventoCancelarMDFe.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Solicita cancelamento de MDF-e |  -  |
**400** | Erro na validação do XML |  -  |
**404** | MDF-e {0} não encontrado. |  -  |
**412** | Pré-condição não satisfeita  * MDF-e {0} não foi autorizado ainda. Somente podem ser encerrados MDF-e autorizados. Número do Protocolo não encontrado. * MDF-e {0} já foi encerrado. *MDF-e {0} já foi cancelado. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="carregar7"></a>
# **carregar7**
> MDFeConfiguracaoResponseEntity carregar7()



Retorna conjunto de configurações relacionadas ao MDF-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    try {
      MDFeConfiguracaoResponseEntity result = apiInstance.carregar7();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#carregar7");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**MDFeConfiguracaoResponseEntity**](MDFeConfiguracaoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Conjunto de configurações do MDF-e |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="carregarXml"></a>
# **carregarXml**
> MDFeCarregarXmlResponseEntity carregarXml(id)



Solicita XML de entrada de um MDF-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      MDFeCarregarXmlResponseEntity result = apiInstance.carregarXml(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#carregarXml");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**MDFeCarregarXmlResponseEntity**](MDFeCarregarXmlResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna XML de entrada |  -  |
**404** | Registro não encontrado  * MDF-e não encontrado. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="consultarSituacao"></a>
# **consultarSituacao**
> MDFeSituacaoResponseEntity consultarSituacao(id)



Solicita situação do MDF-e junto a Sefaz

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      MDFeSituacaoResponseEntity result = apiInstance.consultarSituacao(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#consultarSituacao");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**MDFeSituacaoResponseEntity**](MDFeSituacaoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna dados da consulta da situação |  -  |
**412** | Pré-condição não satisfeita  * Situação do MDF-e não pode ser consultada. Verifique se o MDF-e foi transmitido. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar11"></a>
# **deletar11**
> deletar11(mdfeId, procedimento, id)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long mdfeId = 56L; // Long | 
    String procedimento = "procedimento_example"; // String | 
    Long id = 56L; // Long | 
    try {
      apiInstance.deletar11(mdfeId, procedimento, id);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#deletar11");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mdfeId** | **Long**|  |
 **procedimento** | **String**|  | [enum: carrega, descarrega]
 **id** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="deletar12"></a>
# **deletar12**
> deletar12(id, cUf)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long id = 56L; // Long | 
    Integer cUf = 56; // Integer | 
    try {
      apiInstance.deletar12(id, cUf);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#deletar12");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **cUf** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="deletar13"></a>
# **deletar13**
> deletar13(mdfeId)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long mdfeId = 56L; // Long | 
    try {
      apiInstance.deletar13(mdfeId);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#deletar13");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mdfeId** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="deletar14"></a>
# **deletar14**
> deletar14(emiVeicTracaoId)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long emiVeicTracaoId = 56L; // Long | 
    try {
      apiInstance.deletar14(emiVeicTracaoId);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#deletar14");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **emiVeicTracaoId** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="deletar15"></a>
# **deletar15**
> deletar15(mdfeId, emiVeicTracaoId)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long mdfeId = 56L; // Long | 
    Integer emiVeicTracaoId = 56; // Integer | 
    try {
      apiInstance.deletar15(mdfeId, emiVeicTracaoId);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#deletar15");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mdfeId** | **Long**|  |
 **emiVeicTracaoId** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="deletar16"></a>
# **deletar16**
> deletar16(mdfeId, ciot)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long mdfeId = 56L; // Long | 
    Long ciot = 56L; // Long | 
    try {
      apiInstance.deletar16(mdfeId, ciot);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#deletar16");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mdfeId** | **Long**|  |
 **ciot** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="encerrar1"></a>
# **encerrar1**
> EventoCancelarMDFe encerrar1(id, dataEncerramento, dataHoraEvento, codigoUf, codigoMunicipio)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long id = 56L; // Long | 
    String dataEncerramento = "dataEncerramento_example"; // String | 
    OffsetDateTime dataHoraEvento = new OffsetDateTime(); // OffsetDateTime | Data no formato DD/MM/AAAA HH:MM:SS
    IntegerParam codigoUf = new IntegerParam(); // IntegerParam | 
    LongParam codigoMunicipio = new LongParam(); // LongParam | 
    try {
      EventoCancelarMDFe result = apiInstance.encerrar1(id, dataEncerramento, dataHoraEvento, codigoUf, codigoMunicipio);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#encerrar1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **dataEncerramento** | **String**|  |
 **dataHoraEvento** | **OffsetDateTime**| Data no formato DD/MM/AAAA HH:MM:SS |
 **codigoUf** | [**IntegerParam**](IntegerParam.md)|  |
 **codigoMunicipio** | [**LongParam**](LongParam.md)|  |

### Return type

[**EventoCancelarMDFe**](EventoCancelarMDFe.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Solicita encerramento de MDF-e |  -  |
**404** | MDF-e {0} não encontrado. |  -  |
**412** | Pré-condição não satisfeita  * MDF-e {0} não foi autorizado ainda. Somente podem ser encerrados MDF-e autorizados. Número do Protocolo não encontrado. * MDF-e {0} já foi encerrado. *MDF-e {0} já foi cancelado. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="exportarDAMDFE"></a>
# **exportarDAMDFE**
> exportarDAMDFE(id)



Solicita PDF de distribuição de um MDF-e (DAMDFE)

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      apiInstance.exportarDAMDFE(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#exportarDAMDFE");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/pdf

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna PDF de distribuição (DAMDFE) |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="exportarXML"></a>
# **exportarXML**
> exportarXML(id)



Solicita XML de distribuição de um MDF-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      apiInstance.exportarXML(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#exportarXML");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/xml

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna XML de distribuição |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="historicoTransmissao"></a>
# **historicoTransmissao**
> MDFeTransmissaoResponseEntity historicoTransmissao(id)



Solicita histórico da transmissão de um MDF-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      MDFeTransmissaoResponseEntity result = apiInstance.historicoTransmissao(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#historicoTransmissao");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**MDFeTransmissaoResponseEntity**](MDFeTransmissaoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna histórico da transmissão |  -  |
**404** | Registro não encontrado  * MDF-e não encontrado. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="incluirCondutor"></a>
# **incluirCondutor**
> MDFeEventoResponseEntity incluirCondutor(id, funcionarioId, dataHoraEvento)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long id = 56L; // Long | 
    IntegerParam funcionarioId = new IntegerParam(); // IntegerParam | 
    OffsetDateTime dataHoraEvento = new OffsetDateTime(); // OffsetDateTime | Data no formato DD/MM/AAAA HH:MM:SS
    try {
      MDFeEventoResponseEntity result = apiInstance.incluirCondutor(id, funcionarioId, dataHoraEvento);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#incluirCondutor");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **funcionarioId** | [**IntegerParam**](IntegerParam.md)|  |
 **dataHoraEvento** | **OffsetDateTime**| Data no formato DD/MM/AAAA HH:MM:SS |

### Return type

[**MDFeEventoResponseEntity**](MDFeEventoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Solicita inclusão de condutor no MDF-e |  -  |
**400** | Erro na validação do XML |  -  |
**412** | Pré-condição não satisfeita  * MDF-e {0} não foi autorizado ainda. Somente podem ser encerrados MDF-e autorizados. Número do Protocolo não encontrado. * Funcionário {0} não encontrado. * CPF do Funcionário {0} é obrigatório. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="listar3"></a>
# **listar3**
> listar3(mdfeId)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long mdfeId = 56L; // Long | 
    try {
      apiInstance.listar3(mdfeId);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#listar3");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mdfeId** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="listar4"></a>
# **listar4**
> listar4(mdfeId)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long mdfeId = 56L; // Long | 
    try {
      apiInstance.listar4(mdfeId);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#listar4");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mdfeId** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="listarSelect21"></a>
# **listarSelect21**
> InstituicaoPagamentoListaEntity listarSelect21(termo)



Solicita lista de instituições de pagamento contendo o termo no nome

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    String termo = "termo_example"; // String | 
    try {
      InstituicaoPagamentoListaEntity result = apiInstance.listarSelect21(termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#listarSelect21");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  | [optional]

### Return type

[**InstituicaoPagamentoListaEntity**](InstituicaoPagamentoListaEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de instituições de pagamento |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar10"></a>
# **salvar10**
> salvar10(mdfeId, emiVeicTracaoId)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long mdfeId = 56L; // Long | 
    Integer emiVeicTracaoId = 56; // Integer | 
    try {
      apiInstance.salvar10(mdfeId, emiVeicTracaoId);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#salvar10");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mdfeId** | **Long**|  |
 **emiVeicTracaoId** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="salvar11"></a>
# **salvar11**
> salvar11(mdfeId, ciot, instituicaoPagamentoId)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long mdfeId = 56L; // Long | 
    String ciot = "ciot_example"; // String | 
    IntegerParam instituicaoPagamentoId = new IntegerParam(); // IntegerParam | 
    try {
      apiInstance.salvar11(mdfeId, ciot, instituicaoPagamentoId);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#salvar11");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mdfeId** | **Long**|  |
 **ciot** | **String**|  |
 **instituicaoPagamentoId** | [**IntegerParam**](IntegerParam.md)|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="salvar6"></a>
# **salvar6**
> salvar6(serieMdfe, numeroMdfe, ambienteMdfe, localEncerramentoUf, localEncerramentoMun, tipoCargaPadrao)



Salva conjunto de configurações do MDF-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    ShortParam serieMdfe = new ShortParam(); // ShortParam | 
    LongParam numeroMdfe = new LongParam(); // LongParam | 
    LongParam ambienteMdfe = new LongParam(); // LongParam | 
    LongParam localEncerramentoUf = new LongParam(); // LongParam | 
    LongParam localEncerramentoMun = new LongParam(); // LongParam | 
    LongParam tipoCargaPadrao = new LongParam(); // LongParam | 
    try {
      apiInstance.salvar6(serieMdfe, numeroMdfe, ambienteMdfe, localEncerramentoUf, localEncerramentoMun, tipoCargaPadrao);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#salvar6");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serieMdfe** | [**ShortParam**](ShortParam.md)|  |
 **numeroMdfe** | [**LongParam**](LongParam.md)|  |
 **ambienteMdfe** | [**LongParam**](LongParam.md)|  |
 **localEncerramentoUf** | [**LongParam**](LongParam.md)|  |
 **localEncerramentoMun** | [**LongParam**](LongParam.md)|  |
 **tipoCargaPadrao** | [**LongParam**](LongParam.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar7"></a>
# **salvar7**
> MDFeContasPagarResponseEntity salvar7(mdfeId, fornecedorId, valor, vencimento, contaId, operacao, livroCaixaFormaPagamentoId)



Cria uma Conta a Pagar baseada numa Operação de um Plano de Conta, específico para o MDF-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long mdfeId = 56L; // Long | 
    Integer fornecedorId = 56; // Integer | 
    Double valor = 3.4D; // Double | 
    OffsetDateTime vencimento = new OffsetDateTime(); // OffsetDateTime | 
    Integer contaId = 56; // Integer | 
    String operacao = "operacao_example"; // String | 
    Integer livroCaixaFormaPagamentoId = 56; // Integer | 
    try {
      MDFeContasPagarResponseEntity result = apiInstance.salvar7(mdfeId, fornecedorId, valor, vencimento, contaId, operacao, livroCaixaFormaPagamentoId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#salvar7");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mdfeId** | **Long**|  |
 **fornecedorId** | **Integer**|  |
 **valor** | **Double**|  |
 **vencimento** | **OffsetDateTime**|  |
 **contaId** | **Integer**|  |
 **operacao** | **String**|  | [enum: mdfe_contas_pagar__frete, mdfe_contas_pagar__imposto_frete]
 **livroCaixaFormaPagamentoId** | **Integer**|  |

### Return type

[**MDFeContasPagarResponseEntity**](MDFeContasPagarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de Contas a Pagar relacionadas a um MDF-e |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar8"></a>
# **salvar8**
> salvar8(mdfeId, inlineObject53)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Long mdfeId = 56L; // Long | 
    InlineObject53 inlineObject53 = new InlineObject53(); // InlineObject53 | 
    try {
      apiInstance.salvar8(mdfeId, inlineObject53);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#salvar8");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mdfeId** | **Long**|  |
 **inlineObject53** | [**InlineObject53**](InlineObject53.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="salvar9"></a>
# **salvar9**
> salvar9(emiVeicTracaoId, inlineObject54)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.MdfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    MdfEApi apiInstance = new MdfEApi(defaultClient);
    Integer emiVeicTracaoId = 56; // Integer | 
    InlineObject54 inlineObject54 = new InlineObject54(); // InlineObject54 | 
    try {
      apiInstance.salvar9(emiVeicTracaoId, inlineObject54);
    } catch (ApiException e) {
      System.err.println("Exception when calling MdfEApi#salvar9");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **emiVeicTracaoId** | **Integer**|  |
 **inlineObject54** | [**InlineObject54**](InlineObject54.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

