# CentroDeCustoApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**atualizar6**](CentroDeCustoApi.md#atualizar6) | **PUT** /api/livro-caixa/centro-custo/{id} | 
[**autocomplete8**](CentroDeCustoApi.md#autocomplete8) | **GET** /api/livro-caixa/centro-custo/autocomplete | 
[**criar9**](CentroDeCustoApi.md#criar9) | **POST** /api/livro-caixa/centro-custo | 
[**deletar8**](CentroDeCustoApi.md#deletar8) | **DELETE** /api/livro-caixa/centro-custo/{id} | 
[**index6**](CentroDeCustoApi.md#index6) | **GET** /api/livro-caixa/centro-custo | 


<a name="atualizar6"></a>
# **atualizar6**
> atualizar6(id, inlineObject40)



Requisita atualização de um Centro de Custo

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CentroDeCustoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CentroDeCustoApi apiInstance = new CentroDeCustoApi(defaultClient);
    Integer id = 56; // Integer | 
    InlineObject40 inlineObject40 = new InlineObject40(); // InlineObject40 | 
    try {
      apiInstance.atualizar6(id, inlineObject40);
    } catch (ApiException e) {
      System.err.println("Exception when calling CentroDeCustoApi#atualizar6");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **inlineObject40** | [**InlineObject40**](InlineObject40.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**404** | Centro de Custo {0} não encontrado. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="autocomplete8"></a>
# **autocomplete8**
> CentroCustoAutocompleteResponseEntity autocomplete8(termo)



Solicita lista de Centro de Custo que contenham o termo solicitado na descrição

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CentroDeCustoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CentroDeCustoApi apiInstance = new CentroDeCustoApi(defaultClient);
    String termo = "termo_example"; // String | 
    try {
      CentroCustoAutocompleteResponseEntity result = apiInstance.autocomplete8(termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CentroDeCustoApi#autocomplete8");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  | [optional]

### Return type

[**CentroCustoAutocompleteResponseEntity**](CentroCustoAutocompleteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de Centro de Custo |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar9"></a>
# **criar9**
> criar9(inlineObject41)



Requisita criação de um Centro de Custo

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CentroDeCustoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CentroDeCustoApi apiInstance = new CentroDeCustoApi(defaultClient);
    InlineObject41 inlineObject41 = new InlineObject41(); // InlineObject41 | 
    try {
      apiInstance.criar9(inlineObject41);
    } catch (ApiException e) {
      System.err.println("Exception when calling CentroDeCustoApi#criar9");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject41** | [**InlineObject41**](InlineObject41.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Retorna URI do Centro de Custo criado |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar8"></a>
# **deletar8**
> deletar8(id)



Deleta Centro de Custo

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CentroDeCustoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CentroDeCustoApi apiInstance = new CentroDeCustoApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      apiInstance.deletar8(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling CentroDeCustoApi#deletar8");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**404** | Centro de Custo não encontrado |  -  |
**409** | Não foi possível deletar este Centro de Custo, pois ele é referenciado por uma Conta a Pagar/Receber/Livro de Caixa. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="index6"></a>
# **index6**
> CentroCustoIndexResponseEntity index6(descricao)



Solicita lista de Centro de Custo, com opção de busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CentroDeCustoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CentroDeCustoApi apiInstance = new CentroDeCustoApi(defaultClient);
    String descricao = "descricao_example"; // String | 
    try {
      CentroCustoIndexResponseEntity result = apiInstance.index6(descricao);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CentroDeCustoApi#index6");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **descricao** | **String**|  | [optional]

### Return type

[**CentroCustoIndexResponseEntity**](CentroCustoIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de Centro de Custo |  -  |
**500** | Erro desconhecido no servidor |  -  |

