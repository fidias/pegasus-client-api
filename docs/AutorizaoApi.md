# AutorizaoApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**login**](AutorizaoApi.md#login) | **POST** /api/auth/login | 


<a name="login"></a>
# **login**
> LoginResponseEntity login(login, senha)



Solicita token de autorização através de autenticação por login e senha

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.AutorizaoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    AutorizaoApi apiInstance = new AutorizaoApi(defaultClient);
    String login = "login_example"; // String | 
    String senha = "senha_example"; // String | 
    try {
      LoginResponseEntity result = apiInstance.login(login, senha);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AutorizaoApi#login");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | **String**|  |
 **senha** | **String**|  |

### Return type

[**LoginResponseEntity**](LoginResponseEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Usuário logado com sucesso |  * Authorization - Token JWT de autorização do usuário <br>  |
**401** | Usuário não autorizado. Login ou senha incorreta |  -  |

