

# InlineObject87

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dataInicial** | [**OffsetDateTime**](OffsetDateTime.md) | Data no formato DD/MM/AAAA | 
**dataFinal** | [**OffsetDateTime**](OffsetDateTime.md) | Data no formato DD/MM/AAAA | 
**filtroQtd** | [**FiltroQtdEnum**](#FiltroQtdEnum) | Filtro em relação a quantidade dos itens |  [optional]
**motivo** | [**MotivoEnum**](#MotivoEnum) | Indicador do motivo do Inventário |  [optional]



## Enum: FiltroQtdEnum

Name | Value
---- | -----
TODOS | &quot;todos&quot;
SOMENTE_NEGATIVOS | &quot;somente_negativos&quot;
SOMENTE_POSITIVOS | &quot;somente_positivos&quot;



## Enum: MotivoEnum

Name | Value
---- | -----
FINAL_PERIODO | &quot;final_periodo&quot;
MUDANCA_FORMA_TRIBUTACAO | &quot;mudanca_forma_tributacao&quot;
SOLICITACAO_BAIXA_CADASTRAL | &quot;solicitacao_baixa_cadastral&quot;
ALTERACAO_REGIME_PAGAMENTO | &quot;alteracao_regime_pagamento&quot;
SOLICITACAO_FISCALIZACAO | &quot;solicitacao_fiscalizacao&quot;



