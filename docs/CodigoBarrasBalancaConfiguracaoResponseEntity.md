

# CodigoBarrasBalancaConfiguracaoResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**configuracoes** | [**CodigoBarrasBalancaConfiguracao**](CodigoBarrasBalancaConfiguracao.md) |  |  [optional]



