

# ProdutoAlterarEmLoteGetLote

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**dataCriacao** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**usuarioCriacao** | [**UsuarioCriacao**](UsuarioCriacao.md) |  | 
**dataAplicacao** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**usuarioAplicacao** | [**UsuarioAplicacao**](UsuarioAplicacao.md) |  |  [optional]
**alteracaoAplicada** | **Boolean** |  | 



