# GneroApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**autocomplete13**](GneroApi.md#autocomplete13) | **GET** /api/produto/genero/autocomplete | 


<a name="autocomplete13"></a>
# **autocomplete13**
> GeneroSelect2ResponseEntity autocomplete13(termo, pagina)



Lista de Gêneros de um Produto, com suporte a busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.GneroApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    GneroApi apiInstance = new GneroApi(defaultClient);
    String termo = "termo_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      GeneroSelect2ResponseEntity result = apiInstance.autocomplete13(termo, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling GneroApi#autocomplete13");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  |
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**GeneroSelect2ResponseEntity**](GeneroSelect2ResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de Gêneros de um Produto |  -  |
**500** | Erro desconhecido no servidor |  -  |

