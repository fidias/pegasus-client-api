

# InlineObject92

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codigoBarras** | **String** |  |  [optional]
**descricao** | **String** |  | 
**codigoNcm** | **String** |  |  [optional]
**codigoCest** | **String** |  |  [optional]
**cfopEntradaPadrao** | **Integer** |  |  [optional]
**cfopSaidaLocal** | **Integer** |  |  [optional]
**cfopSaidaInterestadual** | **Integer** |  |  [optional]
**grupoMercadoriaId** | **Integer** |  | 
**unidadeComercialId** | **Integer** |  | 
**generoProdutoId** | **Integer** |  |  [optional]
**referencia** | **String** |  |  [optional]
**origem** | **Integer** |  |  [optional]



