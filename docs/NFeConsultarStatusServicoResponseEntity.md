

# NFeConsultarStatusServicoResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idAmbiente** | **Integer** |  |  [optional]
**versaoAplicativo** | **String** |  |  [optional]
**codigoStatus** | **String** |  |  [optional]
**motivo** | **String** |  |  [optional]
**codigoUF** | **Integer** |  |  [optional]
**dataHoraRecebimento** | **String** |  |  [optional]
**tempoMedio** | **Integer** |  |  [optional]
**dataHoraRetorno** | **String** |  |  [optional]
**observacao** | **String** |  |  [optional]



