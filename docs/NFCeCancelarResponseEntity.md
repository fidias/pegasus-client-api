

# NFCeCancelarResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codigoUf** | **Integer** |  |  [optional]
**chaveAcesso** | **String** |  |  [optional]
**dataHoraNFCeGerado** | **String** |  |  [optional]
**valorNFCe** | **Double** |  |  [optional]
**versaoDados** | **String** |  |  [optional]
**retEnvEvento** | **String** | Representação XML de um retorno bem sucedido de um cancelamento de NFC-e. |  [optional]



