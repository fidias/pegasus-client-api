

# InlineObject15

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienteId** | **Integer** |  | 
**descricaoObjeto** | **String** |  | 
**totalASerFinanciado** | **Double** |  | 
**qtdParcelas** | **Integer** |  | 
**valorParcela** | **Double** |  | 
**margemPercentual** | **Double** |  | 
**totalFinanciamento** | **Double** |  | 
**dataPrimeiraParcela** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**diaParaVencimento** | **Integer** |  | 
**entradaContaId** | **Integer** |  | 
**entradaPlanoContaFinanceiroId** | **Long** |  |  [optional]
**entradaLivroCaixaFormaPagamentoId** | **Integer** |  | 
**saidaContaId** | **Integer** |  | 
**saidaPlanoContaFinanceiroId** | **Long** |  |  [optional]
**saidaLivroCaixaFormaPagamentoId** | **Integer** |  | 
**isCriarContaPagar** | **Boolean** |  |  [optional]
**vencimento** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**idFornecedor** | **Integer** |  |  [optional]



