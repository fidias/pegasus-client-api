

# CFeCompleto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codigoRetorno** | **String** |  | 
**mensagem** | **String** |  | 
**codigoReferencia** | **String** |  |  [optional]
**mensagemSefaz** | **String** |  |  [optional]
**dataHoraEmissao** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**chaveConsulta** | **String** |  | 
**valorTotalCfe** | **Double** |  | 
**cpfCnpjAdquirente** | **Double** |  |  [optional]
**vfpe** | [**List&lt;VFPe&gt;**](VFPe.md) |  |  [optional]



