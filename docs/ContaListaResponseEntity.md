

# ContaListaResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;ContaAutoCompleteItem&gt;**](ContaAutoCompleteItem.md) |  |  [optional]



