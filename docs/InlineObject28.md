

# InlineObject28

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienteId** | **Integer** |  | 
**davTipo** | [**DavTipoEnum**](#DavTipoEnum) |  | 
**dataEntrega** | **String** | Data no formato DD/MM/AAAA |  [optional]
**horaEntrega** | **String** | Horário no formato HH:MM |  [optional]
**dataValidade** | **String** | Data no formato DD/MM/AAAA |  [optional]
**comissionarioId** | **Long** |  |  [optional]
**observacao** | **String** |  |  [optional]
**enderecoId** | **Long** |  |  [optional]
**ignorarQtdDescPromo** | **Boolean** |  |  [optional]



## Enum: DavTipoEnum

Name | Value
---- | -----
PE | &quot;PE&quot;
OS | &quot;OS&quot;
OC | &quot;OC&quot;
PC | &quot;PC&quot;
DAV | &quot;DAV&quot;



