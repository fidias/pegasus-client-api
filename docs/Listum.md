

# Listum

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contasReceberId** | **Long** |  | 
**historico** | **String** |  | 
**cliente** | [**Cliente**](Cliente.md) |  | 
**vencimento** | **String** |  | 
**dataCompetencia** | **String** |  | 
**valor** | **Double** |  | 
**dataPago** | **String** |  | 
**forma** | **String** |  | 
**usuario** | [**Usuario**](Usuario.md) |  | 
**emissao** | **String** |  | 
**foiPago** | **Boolean** |  | 
**doc** | **String** |  | 
**valorPago** | **Double** |  | 
**datacad** | **String** |  | 
**recebidoPor** | [**RecebidoPor**](RecebidoPor.md) |  |  [optional]
**diasAtraso** | **Long** |  | 
**descontoD** | **Double** |  | 
**codigoBarras** | **String** |  | 
**dataCobranca** | **String** |  |  [optional]
**dataEdicao** | **String** |  |  [optional]
**dataRecebimento** | **String** |  |  [optional]
**tarifa** | **Double** |  |  [optional]
**texto** | **String** |  |  [optional]
**valorOriginal** | **Double** |  | 
**valorArquivo** | **Double** |  | 
**periodicidade** | **String** |  | 
**numParcelasNaoPagas** | **Long** |  | 



