# CompraApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**carregar**](CompraApi.md#carregar) | **GET** /api/compra/configuracao | 
[**carregar1**](CompraApi.md#carregar1) | **GET** /api/entrada-saida/{entrada_saida_id}/fatura/parcelas | 
[**criar3**](CompraApi.md#criar3) | **POST** /api/entrada-saida/{entrada_saida_id}/fatura/parcelas | 
[**deletar3**](CompraApi.md#deletar3) | **DELETE** /api/entrada-saida/{entrada_saida_id}/fatura/parcelas/{id} | 
[**exportarCompraNFe**](CompraApi.md#exportarCompraNFe) | **GET** /api/compra/exportar/fortes/nfe/{chave} | 
[**exportarCompraNFePorPeriodo**](CompraApi.md#exportarCompraNFePorPeriodo) | **GET** /api/compra/exportar/fortes/periodo | 
[**salvar1**](CompraApi.md#salvar1) | **POST** /api/compra/configuracao | 


<a name="carregar"></a>
# **carregar**
> CompraConfiguracaoResponseEntity carregar()



Retorna conjunto de configurações relacionadas a Entrada/Saída

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CompraApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CompraApi apiInstance = new CompraApi(defaultClient);
    try {
      CompraConfiguracaoResponseEntity result = apiInstance.carregar();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CompraApi#carregar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CompraConfiguracaoResponseEntity**](CompraConfiguracaoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Conjunto de configurações de Entrada/Saída |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="carregar1"></a>
# **carregar1**
> CarregarParcelasResponseEntity carregar1(entradaSaidaId)



Retorna lista de parcelas relacionadas a uma Entrada/Saída

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CompraApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CompraApi apiInstance = new CompraApi(defaultClient);
    Long entradaSaidaId = 56L; // Long | 
    try {
      CarregarParcelasResponseEntity result = apiInstance.carregar1(entradaSaidaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CompraApi#carregar1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entradaSaidaId** | **Long**|  |

### Return type

[**CarregarParcelasResponseEntity**](CarregarParcelasResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de parcelas de uma Entrada/Saída |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar3"></a>
# **criar3**
> CarregarParcelasResponseEntity criar3(entradaSaidaId, dataVencimento, numeroParcela, valorParcela)



Salva uma parcela relacionada a uma Entrada/Saída

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CompraApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CompraApi apiInstance = new CompraApi(defaultClient);
    Long entradaSaidaId = 56L; // Long | 
    OffsetDateTime dataVencimento = new OffsetDateTime(); // OffsetDateTime | Data no formato DD/MM/AAAA
    String numeroParcela = "numeroParcela_example"; // String | 
    Double valorParcela = 3.4D; // Double | 
    try {
      CarregarParcelasResponseEntity result = apiInstance.criar3(entradaSaidaId, dataVencimento, numeroParcela, valorParcela);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CompraApi#criar3");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entradaSaidaId** | **Long**|  |
 **dataVencimento** | **OffsetDateTime**| Data no formato DD/MM/AAAA |
 **numeroParcela** | **String**|  |
 **valorParcela** | **Double**|  |

### Return type

[**CarregarParcelasResponseEntity**](CarregarParcelasResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de parcelas de uma Entrada/Saída |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar3"></a>
# **deletar3**
> CarregarParcelasResponseEntity deletar3(entradaSaidaId, id)



Requisita remoção de uma parcela relacionada a uma Entrada/Saída

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CompraApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CompraApi apiInstance = new CompraApi(defaultClient);
    Long entradaSaidaId = 56L; // Long | 
    Long id = 56L; // Long | 
    try {
      CarregarParcelasResponseEntity result = apiInstance.deletar3(entradaSaidaId, id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CompraApi#deletar3");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entradaSaidaId** | **Long**|  |
 **id** | **Long**|  |

### Return type

[**CarregarParcelasResponseEntity**](CarregarParcelasResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de parcelas de uma Entrada/Saída |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="exportarCompraNFe"></a>
# **exportarCompraNFe**
> exportarCompraNFe(chave)



Exporta entrada para arquivo de importação do Fortes, baseado na Chave de Acesso

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CompraApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CompraApi apiInstance = new CompraApi(defaultClient);
    String chave = "chave_example"; // String | 
    try {
      apiInstance.exportarCompraNFe(chave);
    } catch (ApiException e) {
      System.err.println("Exception when calling CompraApi#exportarCompraNFe");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chave** | **String**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Arquivo a ser importado pelo Fortes |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="exportarCompraNFePorPeriodo"></a>
# **exportarCompraNFePorPeriodo**
> exportarCompraNFePorPeriodo(inicioLancamento, fimLancamento)



Exporta entradas de um período (data de lançamento) para arquivo de importação do Fortes

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CompraApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CompraApi apiInstance = new CompraApi(defaultClient);
    OffsetDateTime inicioLancamento = new OffsetDateTime(); // OffsetDateTime | 
    OffsetDateTime fimLancamento = new OffsetDateTime(); // OffsetDateTime | 
    try {
      apiInstance.exportarCompraNFePorPeriodo(inicioLancamento, fimLancamento);
    } catch (ApiException e) {
      System.err.println("Exception when calling CompraApi#exportarCompraNFePorPeriodo");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inicioLancamento** | **OffsetDateTime**|  | [optional]
 **fimLancamento** | **OffsetDateTime**|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Arquivo a ser importado pelo Fortes |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar1"></a>
# **salvar1**
> salvar1(tipoRecolhimentoIcms, tipoSubstituicao, tipoTributacaoIpi, balanceteOperacaoFinanceiraEntrada, balanceteOperacaoFinanceiraSaida, devolucaoOperacaoFinanceiraPadrao, lucroPadrao, custoExtraPercPadrao, margemLucroPercPadrao, idGeneroPadrao)



Salva conjunto de configurações de Entrada/Saída

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CompraApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CompraApi apiInstance = new CompraApi(defaultClient);
    Integer tipoRecolhimentoIcms = 56; // Integer | 
    Integer tipoSubstituicao = 56; // Integer | 
    Integer tipoTributacaoIpi = 56; // Integer | 
    Integer balanceteOperacaoFinanceiraEntrada = 56; // Integer | 
    Integer balanceteOperacaoFinanceiraSaida = 56; // Integer | 
    Integer devolucaoOperacaoFinanceiraPadrao = 56; // Integer | 
    Double lucroPadrao = 3.4D; // Double | 
    Double custoExtraPercPadrao = 3.4D; // Double | 
    Double margemLucroPercPadrao = 3.4D; // Double | 
    Integer idGeneroPadrao = 56; // Integer | 
    try {
      apiInstance.salvar1(tipoRecolhimentoIcms, tipoSubstituicao, tipoTributacaoIpi, balanceteOperacaoFinanceiraEntrada, balanceteOperacaoFinanceiraSaida, devolucaoOperacaoFinanceiraPadrao, lucroPadrao, custoExtraPercPadrao, margemLucroPercPadrao, idGeneroPadrao);
    } catch (ApiException e) {
      System.err.println("Exception when calling CompraApi#salvar1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tipoRecolhimentoIcms** | **Integer**|  | [optional]
 **tipoSubstituicao** | **Integer**|  | [optional]
 **tipoTributacaoIpi** | **Integer**|  | [optional]
 **balanceteOperacaoFinanceiraEntrada** | **Integer**|  | [optional]
 **balanceteOperacaoFinanceiraSaida** | **Integer**|  | [optional]
 **devolucaoOperacaoFinanceiraPadrao** | **Integer**|  | [optional]
 **lucroPadrao** | **Double**|  | [optional]
 **custoExtraPercPadrao** | **Double**|  | [optional]
 **margemLucroPercPadrao** | **Double**|  | [optional]
 **idGeneroPadrao** | **Integer**|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

