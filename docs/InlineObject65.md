

# InlineObject65

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numeroNfe** | [**LongParam**](LongParam.md) |  | 
**serie** | [**ShortParam**](ShortParam.md) |  | 
**ambiente** | [**LongParam**](LongParam.md) |  | 
**infAdic** | **String** |  |  [optional]
**icmsTribut** | [**ShortParam**](ShortParam.md) |  |  [optional]
**tributSimples** | [**DoubleParam**](DoubleParam.md) |  |  [optional]
**modFretePadrao** | [**ShortParam**](ShortParam.md) |  |  [optional]
**cfopVendaPadrao** | **Integer** |  | 
**cfopDevolucaoPadrao** | **Integer** |  | 
**ultimoNsu** | **Long** |  |  [optional]



