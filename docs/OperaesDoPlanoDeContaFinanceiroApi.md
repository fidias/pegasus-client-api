# OperaesDoPlanoDeContaFinanceiroApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**criar19**](OperaesDoPlanoDeContaFinanceiroApi.md#criar19) | **POST** /api/plano-conta/{id}/operacao/{operacao_id} | 
[**deletar25**](OperaesDoPlanoDeContaFinanceiroApi.md#deletar25) | **DELETE** /api/plano-conta/{id}/operacao/{operacao_id} | 
[**index11**](OperaesDoPlanoDeContaFinanceiroApi.md#index11) | **GET** /api/plano-conta/{id}/operacao | 


<a name="criar19"></a>
# **criar19**
> PlanoContaOperacaoIndexResponseEntity criar19(id, operacaoId)



Habilita Operação para Plano de Conta Financeiro

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.OperaesDoPlanoDeContaFinanceiroApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    OperaesDoPlanoDeContaFinanceiroApi apiInstance = new OperaesDoPlanoDeContaFinanceiroApi(defaultClient);
    Long id = 56L; // Long | 
    Long operacaoId = 56L; // Long | 
    try {
      PlanoContaOperacaoIndexResponseEntity result = apiInstance.criar19(id, operacaoId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling OperaesDoPlanoDeContaFinanceiroApi#criar19");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **operacaoId** | **Long**|  |

### Return type

[**PlanoContaOperacaoIndexResponseEntity**](PlanoContaOperacaoIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Lista de Operações |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar25"></a>
# **deletar25**
> PlanoContaOperacaoIndexResponseEntity deletar25(id, operacaoId)



Desabilita Operação para Plano de Conta Financeiro

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.OperaesDoPlanoDeContaFinanceiroApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    OperaesDoPlanoDeContaFinanceiroApi apiInstance = new OperaesDoPlanoDeContaFinanceiroApi(defaultClient);
    Long id = 56L; // Long | 
    Long operacaoId = 56L; // Long | 
    try {
      PlanoContaOperacaoIndexResponseEntity result = apiInstance.deletar25(id, operacaoId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling OperaesDoPlanoDeContaFinanceiroApi#deletar25");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **operacaoId** | **Long**|  |

### Return type

[**PlanoContaOperacaoIndexResponseEntity**](PlanoContaOperacaoIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de Operações |  -  |
**404** | Plano de Conta Financeiro {0} não encontrado |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="index11"></a>
# **index11**
> PlanoContaOperacaoIndexResponseEntity index11(id)



Solicita lista de Operações de um Plano de Conta Financeiro

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.OperaesDoPlanoDeContaFinanceiroApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    OperaesDoPlanoDeContaFinanceiroApi apiInstance = new OperaesDoPlanoDeContaFinanceiroApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      PlanoContaOperacaoIndexResponseEntity result = apiInstance.index11(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling OperaesDoPlanoDeContaFinanceiroApi#index11");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**PlanoContaOperacaoIndexResponseEntity**](PlanoContaOperacaoIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de Operações |  -  |
**500** | Erro desconhecido no servidor |  -  |

