

# ItemEndereco

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**titulo** | **String** |  | 
**logradouro** | **String** |  | 
**cep** | **String** |  | 
**pontoReferencia** | **String** |  |  [optional]
**complemento** | **String** |  |  [optional]
**bairro** | [**Bairro1**](Bairro1.md) |  | 
**municipio** | [**Municipio1**](Municipio1.md) |  | 
**uf** | [**Uf1**](Uf1.md) |  | 
**numero** | **String** |  | 
**enderecoPadrao** | **Boolean** |  | 



