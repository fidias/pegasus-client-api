

# EventoIncluirFuncionario

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**versao** | **String** |  | 
**info** | [**Info2**](Info2.md) |  | 
**funcionario** | [**Funcionario**](Funcionario.md) |  | 



