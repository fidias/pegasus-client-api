# PessoaApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**adicionar3**](PessoaApi.md#adicionar3) | **POST** /api/pessoa/cpf-cnpj-existente | 
[**atualizar11**](PessoaApi.md#atualizar11) | **PUT** /api/pessoa/{pessoa_id}/endereco/{id} | 
[**atualizar12**](PessoaApi.md#atualizar12) | **PUT** /api/pessoa/{pessoa_id}/rede-social/{id} | 
[**atualizar13**](PessoaApi.md#atualizar13) | **PUT** /api/pessoa/{pessoa_id}/telefone/{id} | 
[**criar15**](PessoaApi.md#criar15) | **POST** /api/pessoa/{pessoa_id}/endereco | 
[**criar16**](PessoaApi.md#criar16) | **POST** /api/pessoa/{pessoa_id}/rede-social | 
[**criar17**](PessoaApi.md#criar17) | **POST** /api/pessoa/{pessoa_id}/telefone | 
[**definirEnderecoPadrao**](PessoaApi.md#definirEnderecoPadrao) | **GET** /api/pessoa/{pessoa_id}/definir-endereco-padrao/{id} | 
[**definirTelefonePadrao**](PessoaApi.md#definirTelefonePadrao) | **GET** /api/pessoa/{pessoa_id}/telefone/definir-telefone-padrao/{id} | 
[**deletar21**](PessoaApi.md#deletar21) | **DELETE** /api/pessoa/{pessoa_id}/endereco/{id} | 
[**deletar22**](PessoaApi.md#deletar22) | **DELETE** /api/pessoa/{pessoa_id}/rede-social/{id} | 
[**deletar23**](PessoaApi.md#deletar23) | **DELETE** /api/pessoa/{pessoa_id}/telefone/{id} | 
[**get10**](PessoaApi.md#get10) | **GET** /api/pessoa/{pessoa_id}/telefone/{id} | 
[**get8**](PessoaApi.md#get8) | **GET** /api/pessoa/{pessoa_id}/endereco/{id} | 
[**get9**](PessoaApi.md#get9) | **GET** /api/pessoa/{pessoa_id}/rede-social/{id} | 
[**listar10**](PessoaApi.md#listar10) | **GET** /api/pessoa/{pessoa_id}/rede-social | 
[**listar11**](PessoaApi.md#listar11) | **GET** /api/pessoa/{pessoa_id}/telefone | 
[**listar9**](PessoaApi.md#listar9) | **GET** /api/pessoa/{pessoa_id}/endereco | 
[**verificar**](PessoaApi.md#verificar) | **GET** /api/pessoa/cpf-cnpj-existente | 


<a name="adicionar3"></a>
# **adicionar3**
> adicionar3(inlineObject73)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    InlineObject73 inlineObject73 = new InlineObject73(); // InlineObject73 | 
    try {
      apiInstance.adicionar3(inlineObject73);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#adicionar3");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject73** | [**InlineObject73**](InlineObject73.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="atualizar11"></a>
# **atualizar11**
> EnderecoListarResponseEntity atualizar11(id, pessoaId, titulo, logradouro, numero, bairroId, pontoReferencia, complemento, cep, bairro, municipioId, municipio, estadoId)



Requisita atualização de endereço para uma pessoa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    LongParam id = new LongParam(); // LongParam | 
    IntegerParam pessoaId = new IntegerParam(); // IntegerParam | 
    String titulo = "titulo_example"; // String | 
    String logradouro = "logradouro_example"; // String | 
    String numero = "numero_example"; // String | 
    LongParam bairroId = new LongParam(); // LongParam | 
    String pontoReferencia = "pontoReferencia_example"; // String | 
    String complemento = "complemento_example"; // String | 
    NumericOnlyParam cep = new NumericOnlyParam(); // NumericOnlyParam | 
    String bairro = "bairro_example"; // String | 
    LongParam municipioId = new LongParam(); // LongParam | 
    String municipio = "municipio_example"; // String | 
    IntegerParam estadoId = new IntegerParam(); // IntegerParam | 
    try {
      EnderecoListarResponseEntity result = apiInstance.atualizar11(id, pessoaId, titulo, logradouro, numero, bairroId, pontoReferencia, complemento, cep, bairro, municipioId, municipio, estadoId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#atualizar11");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**LongParam**](.md)|  |
 **pessoaId** | [**IntegerParam**](.md)|  |
 **titulo** | **String**|  |
 **logradouro** | **String**|  |
 **numero** | **String**|  |
 **bairroId** | [**LongParam**](LongParam.md)|  |
 **pontoReferencia** | **String**|  | [optional]
 **complemento** | **String**|  | [optional]
 **cep** | [**NumericOnlyParam**](NumericOnlyParam.md)|  | [optional]
 **bairro** | **String**|  | [optional]
 **municipioId** | [**LongParam**](LongParam.md)|  | [optional]
 **municipio** | **String**|  | [optional]
 **estadoId** | [**IntegerParam**](IntegerParam.md)|  | [optional]

### Return type

[**EnderecoListarResponseEntity**](EnderecoListarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de endereços |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="atualizar12"></a>
# **atualizar12**
> RedeSocialListarResponseEntity atualizar12(id, pessoaId, titulo, url)



Requisita atualização de rede social para uma pessoa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    LongParam id = new LongParam(); // LongParam | 
    IntegerParam pessoaId = new IntegerParam(); // IntegerParam | 
    String titulo = "titulo_example"; // String | 
    String url = "url_example"; // String | 
    try {
      RedeSocialListarResponseEntity result = apiInstance.atualizar12(id, pessoaId, titulo, url);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#atualizar12");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**LongParam**](.md)|  |
 **pessoaId** | [**IntegerParam**](.md)|  |
 **titulo** | **String**|  |
 **url** | **String**|  |

### Return type

[**RedeSocialListarResponseEntity**](RedeSocialListarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de rede social |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="atualizar13"></a>
# **atualizar13**
> TelefoneListarResponseEntity atualizar13(id, pessoaId, titulo, numero, contato)



Requisita atualização de telefone para uma pessoa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    LongParam id = new LongParam(); // LongParam | 
    IntegerParam pessoaId = new IntegerParam(); // IntegerParam | 
    String titulo = "titulo_example"; // String | 
    NumericOnlyParam numero = new NumericOnlyParam(); // NumericOnlyParam | 
    String contato = "contato_example"; // String | 
    try {
      TelefoneListarResponseEntity result = apiInstance.atualizar13(id, pessoaId, titulo, numero, contato);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#atualizar13");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**LongParam**](.md)|  |
 **pessoaId** | [**IntegerParam**](.md)|  |
 **titulo** | **String**|  |
 **numero** | [**NumericOnlyParam**](NumericOnlyParam.md)|  |
 **contato** | **String**|  | [optional]

### Return type

[**TelefoneListarResponseEntity**](TelefoneListarResponseEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Lista de telefones |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar15"></a>
# **criar15**
> EnderecoListarResponseEntity criar15(pessoaId, titulo, logradouro, numero, bairroId, pontoReferencia, complemento, cep, bairro, municipioId, municipio, estadoId)



Requisita criação de endereço para uma pessoa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    IntegerParam pessoaId = new IntegerParam(); // IntegerParam | 
    String titulo = "titulo_example"; // String | 
    String logradouro = "logradouro_example"; // String | 
    String numero = "numero_example"; // String | 
    LongParam bairroId = new LongParam(); // LongParam | 
    String pontoReferencia = "pontoReferencia_example"; // String | 
    String complemento = "complemento_example"; // String | 
    NumericOnlyParam cep = new NumericOnlyParam(); // NumericOnlyParam | 
    String bairro = "bairro_example"; // String | 
    LongParam municipioId = new LongParam(); // LongParam | 
    String municipio = "municipio_example"; // String | 
    IntegerParam estadoId = new IntegerParam(); // IntegerParam | 
    try {
      EnderecoListarResponseEntity result = apiInstance.criar15(pessoaId, titulo, logradouro, numero, bairroId, pontoReferencia, complemento, cep, bairro, municipioId, municipio, estadoId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#criar15");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pessoaId** | [**IntegerParam**](.md)|  |
 **titulo** | **String**|  |
 **logradouro** | **String**|  |
 **numero** | **String**|  |
 **bairroId** | [**LongParam**](LongParam.md)|  |
 **pontoReferencia** | **String**|  | [optional]
 **complemento** | **String**|  | [optional]
 **cep** | [**NumericOnlyParam**](NumericOnlyParam.md)|  | [optional]
 **bairro** | **String**|  | [optional]
 **municipioId** | [**LongParam**](LongParam.md)|  | [optional]
 **municipio** | **String**|  | [optional]
 **estadoId** | [**IntegerParam**](IntegerParam.md)|  | [optional]

### Return type

[**EnderecoListarResponseEntity**](EnderecoListarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Lista de endereços |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar16"></a>
# **criar16**
> RedeSocialListarResponseEntity criar16(pessoaId, titulo, url)



Requisita criação de rede social para uma pessoa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    IntegerParam pessoaId = new IntegerParam(); // IntegerParam | 
    String titulo = "titulo_example"; // String | 
    String url = "url_example"; // String | 
    try {
      RedeSocialListarResponseEntity result = apiInstance.criar16(pessoaId, titulo, url);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#criar16");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pessoaId** | [**IntegerParam**](.md)|  |
 **titulo** | **String**|  |
 **url** | **String**|  |

### Return type

[**RedeSocialListarResponseEntity**](RedeSocialListarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Lista de rede social |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar17"></a>
# **criar17**
> TelefoneListarResponseEntity criar17(pessoaId, titulo, numero, contato)



Requisita criação de telefone para uma pessoa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    IntegerParam pessoaId = new IntegerParam(); // IntegerParam | 
    String titulo = "titulo_example"; // String | 
    NumericOnlyParam numero = new NumericOnlyParam(); // NumericOnlyParam | 
    String contato = "contato_example"; // String | 
    try {
      TelefoneListarResponseEntity result = apiInstance.criar17(pessoaId, titulo, numero, contato);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#criar17");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pessoaId** | [**IntegerParam**](.md)|  |
 **titulo** | **String**|  |
 **numero** | [**NumericOnlyParam**](NumericOnlyParam.md)|  |
 **contato** | **String**|  | [optional]

### Return type

[**TelefoneListarResponseEntity**](TelefoneListarResponseEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Lista de telefones |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="definirEnderecoPadrao"></a>
# **definirEnderecoPadrao**
> EnderecoListarResponseEntity definirEnderecoPadrao(pessoaId, id)



Requisita definição de endereço como padrão para uma pessoa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    IntegerParam pessoaId = new IntegerParam(); // IntegerParam | 
    LongParam id = new LongParam(); // LongParam | 
    try {
      EnderecoListarResponseEntity result = apiInstance.definirEnderecoPadrao(pessoaId, id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#definirEnderecoPadrao");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pessoaId** | [**IntegerParam**](.md)|  |
 **id** | [**LongParam**](.md)|  |

### Return type

[**EnderecoListarResponseEntity**](EnderecoListarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de endereços |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="definirTelefonePadrao"></a>
# **definirTelefonePadrao**
> TelefoneListarResponseEntity definirTelefonePadrao(pessoaId, id)



Requisita definição de telefone como padrão para uma pessoa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    IntegerParam pessoaId = new IntegerParam(); // IntegerParam | 
    LongParam id = new LongParam(); // LongParam | 
    try {
      TelefoneListarResponseEntity result = apiInstance.definirTelefonePadrao(pessoaId, id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#definirTelefonePadrao");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pessoaId** | [**IntegerParam**](.md)|  |
 **id** | [**LongParam**](.md)|  |

### Return type

[**TelefoneListarResponseEntity**](TelefoneListarResponseEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de telefones |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar21"></a>
# **deletar21**
> EnderecoListarResponseEntity deletar21(pessoaId, id)



Requisita remoção de endereço para uma pessoa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    IntegerParam pessoaId = new IntegerParam(); // IntegerParam | 
    LongParam id = new LongParam(); // LongParam | 
    try {
      EnderecoListarResponseEntity result = apiInstance.deletar21(pessoaId, id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#deletar21");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pessoaId** | [**IntegerParam**](.md)|  |
 **id** | [**LongParam**](.md)|  |

### Return type

[**EnderecoListarResponseEntity**](EnderecoListarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de endereços |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar22"></a>
# **deletar22**
> RedeSocialListarResponseEntity deletar22(pessoaId, id)



Requisita remoção de rede social para uma pessoa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    IntegerParam pessoaId = new IntegerParam(); // IntegerParam | 
    LongParam id = new LongParam(); // LongParam | 
    try {
      RedeSocialListarResponseEntity result = apiInstance.deletar22(pessoaId, id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#deletar22");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pessoaId** | [**IntegerParam**](.md)|  |
 **id** | [**LongParam**](.md)|  |

### Return type

[**RedeSocialListarResponseEntity**](RedeSocialListarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de rede social |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar23"></a>
# **deletar23**
> TelefoneListarResponseEntity deletar23(pessoaId, id)



Requisita remoção de telefone para uma pessoa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    IntegerParam pessoaId = new IntegerParam(); // IntegerParam | 
    LongParam id = new LongParam(); // LongParam | 
    try {
      TelefoneListarResponseEntity result = apiInstance.deletar23(pessoaId, id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#deletar23");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pessoaId** | [**IntegerParam**](.md)|  |
 **id** | [**LongParam**](.md)|  |

### Return type

[**TelefoneListarResponseEntity**](TelefoneListarResponseEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de telefones |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="get10"></a>
# **get10**
> TelefoneListarResponseEntity get10(pessoaId, id)



Solicita um telefone de uma pessoa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    Integer pessoaId = 56; // Integer | 
    Integer id = 56; // Integer | 
    try {
      TelefoneListarResponseEntity result = apiInstance.get10(pessoaId, id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#get10");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pessoaId** | **Integer**|  |
 **id** | **Integer**|  |

### Return type

[**TelefoneListarResponseEntity**](TelefoneListarResponseEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Telefone |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="get8"></a>
# **get8**
> EnderecoResponseEntity get8(pessoaId, id)



Solicita um endereço de uma pessoa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    Integer pessoaId = 56; // Integer | 
    Integer id = 56; // Integer | 
    try {
      EnderecoResponseEntity result = apiInstance.get8(pessoaId, id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#get8");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pessoaId** | **Integer**|  |
 **id** | **Integer**|  |

### Return type

[**EnderecoResponseEntity**](EnderecoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Endereço |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="get9"></a>
# **get9**
> RedeSocialResponseEntity get9(pessoaId, id)



Solicita um rede social de uma pessoa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    Integer pessoaId = 56; // Integer | 
    Integer id = 56; // Integer | 
    try {
      RedeSocialResponseEntity result = apiInstance.get9(pessoaId, id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#get9");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pessoaId** | **Integer**|  |
 **id** | **Integer**|  |

### Return type

[**RedeSocialResponseEntity**](RedeSocialResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Rede Social |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="listar10"></a>
# **listar10**
> RedeSocialListarResponseEntity listar10(pessoaId)



Solicita lista de redes sociais de uma pessoa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    Integer pessoaId = 56; // Integer | 
    try {
      RedeSocialListarResponseEntity result = apiInstance.listar10(pessoaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#listar10");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pessoaId** | **Integer**|  |

### Return type

[**RedeSocialListarResponseEntity**](RedeSocialListarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de rede social |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="listar11"></a>
# **listar11**
> TelefoneListarResponseEntity listar11(pessoaId)



Solicita listar de telefones de uma pessoa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    Integer pessoaId = 56; // Integer | 
    try {
      TelefoneListarResponseEntity result = apiInstance.listar11(pessoaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#listar11");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pessoaId** | **Integer**|  |

### Return type

[**TelefoneListarResponseEntity**](TelefoneListarResponseEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de telefones |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="listar9"></a>
# **listar9**
> EnderecoListarResponseEntity listar9(pessoaId)



Solicita lista de endereços de uma pessoa

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    Integer pessoaId = 56; // Integer | 
    try {
      EnderecoListarResponseEntity result = apiInstance.listar9(pessoaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#listar9");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pessoaId** | **Integer**|  |

### Return type

[**EnderecoListarResponseEntity**](EnderecoListarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de endereços |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="verificar"></a>
# **verificar**
> verificar(cpfCnpj, moduloPessoa)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PessoaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    PessoaApi apiInstance = new PessoaApi(defaultClient);
    NumericOnlyParam cpfCnpj = new NumericOnlyParam(); // NumericOnlyParam | 
    String moduloPessoa = "moduloPessoa_example"; // String | 
    try {
      apiInstance.verificar(cpfCnpj, moduloPessoa);
    } catch (ApiException e) {
      System.err.println("Exception when calling PessoaApi#verificar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cpfCnpj** | [**NumericOnlyParam**](.md)|  |
 **moduloPessoa** | **String**|  | [enum: cliente, fornecedor, funcionario, contabilista, emitente]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

