

# ItensVenda

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idproduto** | **Long** |  | 
**descricao** | **String** |  | 
**quantidade** | **Double** |  | 
**iditemVenda** | **Long** |  | 
**preco** | **Double** |  | 
**cancelado** | **Boolean** |  | 
**pafNumero** | **Long** |  | 
**pafAcrescimoD** | **Double** |  | 
**pafDescontoD** | **Double** |  | 
**pafTotalLiq** | **Double** |  | 
**codBarra** | **String** |  |  [optional]
**referencia** | **Long** |  |  [optional]
**brev** | **String** |  | 



