

# NFeListarResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;NFCeItemLista&gt;**](NFCeItemLista.md) |  |  [optional]



