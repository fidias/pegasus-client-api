

# MultipartFormDataInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**formData** | [**Map&lt;String, InputPart&gt;**](InputPart.md) |  |  [optional]
**formDataMap** | [**Map&lt;String, List&lt;InputPart&gt;&gt;**](List.md) |  |  [optional]
**parts** | [**List&lt;InputPart&gt;**](InputPart.md) |  |  [optional]
**preamble** | **String** |  |  [optional]



