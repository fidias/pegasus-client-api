

# Devolucao

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**motivo** | **String** |  |  [optional]
**emissao** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**idCliente** | **Integer** |  |  [optional]
**nome** | **String** |  |  [optional]
**vendas** | **String** |  |  [optional]
**idvendaBalcao** | **Integer** |  |  [optional]
**totalCrTroca** | **Double** |  |  [optional]
**descontadoEm** | **Integer** |  |  [optional]
**cfop** | **Integer** |  |  [optional]
**notaId** | **Integer** |  |  [optional]
**concluida** | **Boolean** |  |  [optional]
**autorizadoPor** | **Integer** |  |  [optional]



