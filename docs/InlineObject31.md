

# InlineObject31

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**modoItem** | [**ModoItemEnum**](#ModoItemEnum) |  |  [optional]
**mensagemCabecalho1** | **String** |  |  [optional]
**mensagemCabecalho2** | **String** |  |  [optional]



## Enum: ModoItemEnum

Name | Value
---- | -----
CONDENSADO | &quot;condensado&quot;
EXPANDIDO_PRECO_LIQUIDO | &quot;expandido_preco_liquido&quot;



