

# ProdutoAutocompleteResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**list** | [**List&lt;ProdutoAjaxAutoComplete&gt;**](ProdutoAjaxAutoComplete.md) |  |  [optional]



