

# MDFeContaPagarItemLista

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contasPagarId** | **Long** |  | 
**historico** | **String** |  | 
**vencimento** | **String** |  | 
**valor** | **Double** |  | 
**dataPago** | **String** |  |  [optional]
**valorPago** | **Double** |  |  [optional]
**emissao** | **String** |  | 
**foiPago** | **Boolean** |  | 
**fornecedor** | [**Fornecedor**](Fornecedor.md) |  |  [optional]



