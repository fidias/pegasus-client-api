# TaxaApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**atualizar1**](TaxaApi.md#atualizar1) | **PUT** /api/cartoes/pos/{serial_pos}/taxa/{taxa_id} | 
[**cadastrar**](TaxaApi.md#cadastrar) | **POST** /api/cartoes/pos/{serial_pos}/taxa | 
[**deletar2**](TaxaApi.md#deletar2) | **DELETE** /api/cartoes/pos/{serial_pos}/taxa/{taxa_id} | 
[**get2**](TaxaApi.md#get2) | **GET** /api/cartoes/pos/{serial_pos}/taxa/{taxa_id} | 
[**listar2**](TaxaApi.md#listar2) | **GET** /api/cartoes/pos/{serial_pos}/taxa | 


<a name="atualizar1"></a>
# **atualizar1**
> TaxaListarResponseEntity atualizar1(taxaId, serialPos, taxaP, numParcelasIni, numParcelasFim)



Requisita atualização de taxas do POS

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.TaxaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    TaxaApi apiInstance = new TaxaApi(defaultClient);
    Integer taxaId = 56; // Integer | 
    String serialPos = "serialPos_example"; // String | 
    DoubleParam taxaP = new DoubleParam(); // DoubleParam | 
    IntegerParam numParcelasIni = new IntegerParam(); // IntegerParam | 
    IntegerParam numParcelasFim = new IntegerParam(); // IntegerParam | 
    try {
      TaxaListarResponseEntity result = apiInstance.atualizar1(taxaId, serialPos, taxaP, numParcelasIni, numParcelasFim);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TaxaApi#atualizar1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taxaId** | **Integer**|  |
 **serialPos** | **String**|  |
 **taxaP** | [**DoubleParam**](DoubleParam.md)|  |
 **numParcelasIni** | [**IntegerParam**](IntegerParam.md)|  |
 **numParcelasFim** | [**IntegerParam**](IntegerParam.md)|  |

### Return type

[**TaxaListarResponseEntity**](TaxaListarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de taxas de um Serial POS |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="cadastrar"></a>
# **cadastrar**
> TaxaListarResponseEntity cadastrar(serialPos, taxaP, numParcelasIni, numParcelasFim)



Requisita criação de taxa do POS

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.TaxaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    TaxaApi apiInstance = new TaxaApi(defaultClient);
    String serialPos = "serialPos_example"; // String | 
    DoubleParam taxaP = new DoubleParam(); // DoubleParam | 
    IntegerParam numParcelasIni = new IntegerParam(); // IntegerParam | 
    IntegerParam numParcelasFim = new IntegerParam(); // IntegerParam | 
    try {
      TaxaListarResponseEntity result = apiInstance.cadastrar(serialPos, taxaP, numParcelasIni, numParcelasFim);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TaxaApi#cadastrar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serialPos** | **String**|  |
 **taxaP** | [**DoubleParam**](DoubleParam.md)|  |
 **numParcelasIni** | [**IntegerParam**](IntegerParam.md)|  |
 **numParcelasFim** | [**IntegerParam**](IntegerParam.md)|  |

### Return type

[**TaxaListarResponseEntity**](TaxaListarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Lista de taxa de um POS |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar2"></a>
# **deletar2**
> TaxaListarResponseEntity deletar2(serialPos, taxaId)



Requisita remoção de taxa de um serial POS

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.TaxaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    TaxaApi apiInstance = new TaxaApi(defaultClient);
    String serialPos = "serialPos_example"; // String | 
    Integer taxaId = 56; // Integer | 
    try {
      TaxaListarResponseEntity result = apiInstance.deletar2(serialPos, taxaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TaxaApi#deletar2");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serialPos** | **String**|  |
 **taxaId** | **Integer**|  |

### Return type

[**TaxaListarResponseEntity**](TaxaListarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de taxas de um Serial POS |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="get2"></a>
# **get2**
> TaxaResponseEntity get2(serialPos, taxaId)



Solicita uma taxa de um POS

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.TaxaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    TaxaApi apiInstance = new TaxaApi(defaultClient);
    String serialPos = "serialPos_example"; // String | 
    Integer taxaId = 56; // Integer | 
    try {
      TaxaResponseEntity result = apiInstance.get2(serialPos, taxaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TaxaApi#get2");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serialPos** | **String**|  |
 **taxaId** | **Integer**|  |

### Return type

[**TaxaResponseEntity**](TaxaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Taxa |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="listar2"></a>
# **listar2**
> TaxaListarResponseEntity listar2(serialPos)



Solicita lista de taxas de um POS

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.TaxaApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    TaxaApi apiInstance = new TaxaApi(defaultClient);
    String serialPos = "serialPos_example"; // String | 
    try {
      TaxaListarResponseEntity result = apiInstance.listar2(serialPos);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TaxaApi#listar2");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serialPos** | **String**|  |

### Return type

[**TaxaListarResponseEntity**](TaxaListarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de taxas de um POS |  -  |
**500** | Erro desconhecido no servidor |  -  |

