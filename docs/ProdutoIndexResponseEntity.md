

# ProdutoIndexResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**produtos** | [**List&lt;Produto&gt;**](Produto.md) |  |  [optional]



