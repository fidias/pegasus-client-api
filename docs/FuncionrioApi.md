# FuncionrioApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**autocomplete7**](FuncionrioApi.md#autocomplete7) | **GET** /api/funcionario/autocomplete | 


<a name="autocomplete7"></a>
# **autocomplete7**
> FuncionarioResponseEntity autocomplete7(termo)



Solicita lista de funcionários que contenham o termo solicitado no nome ou nome fantasia

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.FuncionrioApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    FuncionrioApi apiInstance = new FuncionrioApi(defaultClient);
    String termo = "termo_example"; // String | 
    try {
      FuncionarioResponseEntity result = apiInstance.autocomplete7(termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling FuncionrioApi#autocomplete7");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  | [optional]

### Return type

[**FuncionarioResponseEntity**](FuncionarioResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de funcionários |  -  |
**500** | Erro desconhecido no servidor |  -  |

