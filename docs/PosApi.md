# PosApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**atualizar**](PosApi.md#atualizar) | **PUT** /api/cartoes/pos/{serial_pos} | 
[**criar2**](PosApi.md#criar2) | **POST** /api/cartoes/pos | 
[**deletar1**](PosApi.md#deletar1) | **DELETE** /api/cartoes/pos/{serial_pos} | 
[**get1**](PosApi.md#get1) | **GET** /api/cartoes/pos/{serial_pos} | 
[**listar1**](PosApi.md#listar1) | **GET** /api/cartoes/pos | 


<a name="atualizar"></a>
# **atualizar**
> PosResponseEntity atualizar(serialPos, descricao, chaveRequisicao, idAdquirente, posicao)



Requisita atualização de POS

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PosApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PosApi apiInstance = new PosApi(defaultClient);
    String serialPos = "serialPos_example"; // String | 
    String descricao = "descricao_example"; // String | 
    String chaveRequisicao = "chaveRequisicao_example"; // String | 
    IntegerParam idAdquirente = new IntegerParam(); // IntegerParam | 
    IntegerParam posicao = new IntegerParam(); // IntegerParam | 
    try {
      PosResponseEntity result = apiInstance.atualizar(serialPos, descricao, chaveRequisicao, idAdquirente, posicao);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PosApi#atualizar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serialPos** | **String**|  |
 **descricao** | **String**|  |
 **chaveRequisicao** | **String**|  |
 **idAdquirente** | [**IntegerParam**](IntegerParam.md)|  |
 **posicao** | [**IntegerParam**](IntegerParam.md)|  | [optional]

### Return type

[**PosResponseEntity**](PosResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | POS atualizado |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar2"></a>
# **criar2**
> PosResponseEntity criar2(descricao, chaveRequisicao, idAdquirente, posicao)



Requisita criação de POS

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PosApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PosApi apiInstance = new PosApi(defaultClient);
    String descricao = "descricao_example"; // String | 
    String chaveRequisicao = "chaveRequisicao_example"; // String | 
    IntegerParam idAdquirente = new IntegerParam(); // IntegerParam | 
    IntegerParam posicao = new IntegerParam(); // IntegerParam | 
    try {
      PosResponseEntity result = apiInstance.criar2(descricao, chaveRequisicao, idAdquirente, posicao);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PosApi#criar2");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **descricao** | **String**|  |
 **chaveRequisicao** | **String**|  |
 **idAdquirente** | [**IntegerParam**](IntegerParam.md)|  |
 **posicao** | [**IntegerParam**](IntegerParam.md)|  | [optional]

### Return type

[**PosResponseEntity**](PosResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Lista de POS |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar1"></a>
# **deletar1**
> PosListarResponseEntity deletar1(serialPos)



Requisita remoção de um serial POS

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PosApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PosApi apiInstance = new PosApi(defaultClient);
    String serialPos = "serialPos_example"; // String | 
    try {
      PosListarResponseEntity result = apiInstance.deletar1(serialPos);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PosApi#deletar1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serialPos** | **String**|  |

### Return type

[**PosListarResponseEntity**](PosListarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de Serial POS |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="get1"></a>
# **get1**
> PosResponseEntity get1(serialPos)



Solicita um POS

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PosApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PosApi apiInstance = new PosApi(defaultClient);
    String serialPos = "serialPos_example"; // String | 
    try {
      PosResponseEntity result = apiInstance.get1(serialPos);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PosApi#get1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serialPos** | **String**|  |

### Return type

[**PosResponseEntity**](PosResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | POS |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="listar1"></a>
# **listar1**
> PosListarResponseEntity listar1()



Solicita lista de POS cadastrados

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PosApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PosApi apiInstance = new PosApi(defaultClient);
    try {
      PosListarResponseEntity result = apiInstance.listar1();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PosApi#listar1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**PosListarResponseEntity**](PosListarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de POS |  -  |
**500** | Erro desconhecido no servidor |  -  |

