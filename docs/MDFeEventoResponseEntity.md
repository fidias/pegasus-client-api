

# MDFeEventoResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retorno** | [**EventoIncluirFuncionario**](EventoIncluirFuncionario.md) |  |  [optional]
**leiauteXml** | **String** |  |  [optional]
**statusAtual** | **Integer** |  |  [optional]



