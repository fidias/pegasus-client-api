

# Parcela

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contasReceberId** | **Long** |  | 
**vencimento** | **String** |  | 
**valor** | **Double** |  | 
**dataPago** | **String** |  |  [optional]
**foiPago** | **Boolean** |  | 
**doc** | **String** |  |  [optional]
**valorPago** | **Double** |  | 
**recebidoPor** | [**RecebidoPor**](RecebidoPor.md) |  |  [optional]
**dataEdicao** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**dataRecebimento** | **String** |  |  [optional]
**dataCompetencia** | **String** |  | 
**formaPagamento** | [**FormaPagamento**](FormaPagamento.md) |  | 
**isJuros** | **Boolean** |  | 
**lucroParcela** | **Double** |  | 



