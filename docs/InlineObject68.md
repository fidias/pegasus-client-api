

# InlineObject68

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cProdAnp** | **Long** |  | 
**ufCons** | **Integer** |  | 
**vPart** | **Double** |  |  [optional]
**qTemp** | **Double** |  |  [optional]
**pGlp** | **Double** |  |  [optional]
**pGnn** | **Double** |  |  [optional]
**pGni** | **Double** |  |  [optional]



