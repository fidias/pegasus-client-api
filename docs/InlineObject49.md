

# InlineObject49

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dataEncerramento** | **String** |  | 
**dataHoraEvento** | [**OffsetDateTime**](OffsetDateTime.md) | Data no formato DD/MM/AAAA HH:MM:SS | 
**codigoUf** | [**IntegerParam**](IntegerParam.md) |  | 
**codigoMunicipio** | [**LongParam**](LongParam.md) |  | 



