

# ProdutoTabelaPrecoListaResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;ProdutoTabelaPrecoItem&gt;**](ProdutoTabelaPrecoItem.md) |  |  [optional]



