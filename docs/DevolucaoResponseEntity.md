

# DevolucaoResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**devolucoes** | [**List&lt;Devolucao&gt;**](Devolucao.md) |  |  [optional]



