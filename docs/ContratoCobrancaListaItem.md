

# ContratoCobrancaListaItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**dataInicial** | **String** |  | 
**dataFinal** | **String** |  | 
**descricao** | **String** |  | 
**contratoTipo** | [**ContratoTipo1**](ContratoTipo1.md) |  | 
**conta** | [**Conta1**](Conta1.md) |  | 



