

# PlanoContaListaItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**parentId** | **Long** |  |  [optional]
**descricao** | **String** |  | 
**identificador** | **String** |  |  [optional]
**fluxoOperacao** | [**FluxoOperacaoEnum**](#FluxoOperacaoEnum) |  |  [optional]
**codigoSistemaExterno** | **String** |  |  [optional]
**isDeduzido** | **Boolean** |  |  [optional]
**classificacaoId** | **Long** |  |  [optional]



## Enum: FluxoOperacaoEnum

Name | Value
---- | -----
ENTRADA | &quot;Entrada&quot;
SAIDA | &quot;Saida&quot;



