

# OperacaoFinanceiraCarregar

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cfop** | **Long** |  | 
**descricao** | **String** |  | 
**aplicacao** | **String** |  | 
**disponivelUso** | **Boolean** |  | 
**habilitado** | **Boolean** |  | 
**isGerarFatura** | **Boolean** |  | 
**isAlterarQuantidade** | **Boolean** |  | 
**isAlterarCusto** | **Boolean** |  | 
**isAlterarPreco** | **Boolean** |  | 
**hasFormaPagamento** | **Boolean** |  | 
**isAlterarIdUltimaNota** | **Boolean** |  | 



