# ContratoApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**autocomplete2**](ContratoApi.md#autocomplete2) | **GET** /api/contrato/tipo/autocomplete | 
[**criar6**](ContratoApi.md#criar6) | **POST** /api/contrato/cobranca | 
[**gerar1**](ContratoApi.md#gerar1) | **GET** /api/contrato/cobranca/{id}/gerar | 
[**get4**](ContratoApi.md#get4) | **GET** /api/contrato/cobranca/{id} | 
[**importarSPC**](ContratoApi.md#importarSPC) | **POST** /api/contrato/cobranca/{id}/importar/spc | 
[**index3**](ContratoApi.md#index3) | **GET** /api/contrato/cobranca | 
[**visualizar**](ContratoApi.md#visualizar) | **POST** /api/contrato/cobranca/visualizar | 


<a name="autocomplete2"></a>
# **autocomplete2**
> ContratoTipoAutocompleteResponseEntity autocomplete2(termo, pagina)



Solicita lista de tipos de contrato, com opção de busca por termo

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContratoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContratoApi apiInstance = new ContratoApi(defaultClient);
    String termo = "termo_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      ContratoTipoAutocompleteResponseEntity result = apiInstance.autocomplete2(termo, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContratoApi#autocomplete2");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  | [optional]
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**ContratoTipoAutocompleteResponseEntity**](ContratoTipoAutocompleteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de tipos de contrato |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar6"></a>
# **criar6**
> ContratoCobrancaResponseEntity criar6(dataInicial, dataFinal, contratoTipoId, contaId, descricao, planoContaFinanceiroId)



Criar cobrança para um Tipo de Contrato

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContratoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContratoApi apiInstance = new ContratoApi(defaultClient);
    OffsetDateTime dataInicial = new OffsetDateTime(); // OffsetDateTime | 
    OffsetDateTime dataFinal = new OffsetDateTime(); // OffsetDateTime | 
    Integer contratoTipoId = 56; // Integer | 
    Integer contaId = 56; // Integer | 
    String descricao = "descricao_example"; // String | 
    Long planoContaFinanceiroId = 56L; // Long | 
    try {
      ContratoCobrancaResponseEntity result = apiInstance.criar6(dataInicial, dataFinal, contratoTipoId, contaId, descricao, planoContaFinanceiroId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContratoApi#criar6");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dataInicial** | **OffsetDateTime**|  |
 **dataFinal** | **OffsetDateTime**|  |
 **contratoTipoId** | **Integer**|  |
 **contaId** | **Integer**|  |
 **descricao** | **String**|  | [optional]
 **planoContaFinanceiroId** | **Long**|  | [optional]

### Return type

[**ContratoCobrancaResponseEntity**](ContratoCobrancaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Retorna id da Cobrança |  -  |
**400** | Erro ao criar Cobrança |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="gerar1"></a>
# **gerar1**
> gerar1(id)



Gerar cobranças para um Tipo de Contrato

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContratoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContratoApi apiInstance = new ContratoApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      apiInstance.gerar1(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContratoApi#gerar1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**404** | Cobrança não encontrada |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="get4"></a>
# **get4**
> ContratoCobrancaCompleto get4(id)



Solicita uma Cobrança

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContratoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContratoApi apiInstance = new ContratoApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      ContratoCobrancaCompleto result = apiInstance.get4(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContratoApi#get4");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**ContratoCobrancaCompleto**](ContratoCobrancaCompleto.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de uma Cobrança |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="importarSPC"></a>
# **importarSPC**
> AlterarEmLoteImportarResponseEntity importarSPC(id, arquivo)



Importar arquivo do SPC para cobrança

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContratoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContratoApi apiInstance = new ContratoApi(defaultClient);
    Long id = 56L; // Long | 
    List<byte[]> arquivo = null; // List<byte[]> | Arquivo TXT
    try {
      AlterarEmLoteImportarResponseEntity result = apiInstance.importarSPC(id, arquivo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContratoApi#importarSPC");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **arquivo** | [**List&lt;byte[]&gt;**](byte[].md)| Arquivo TXT | [optional]

### Return type

[**AlterarEmLoteImportarResponseEntity**](AlterarEmLoteImportarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista com as cobranças |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="index3"></a>
# **index3**
> CobrancaIndexResponseEntity index3(contratoTipoId, contaId, dataInicial, dataFinal, pagina)



Solicita lista de Metadados de Cobranças, com opção de busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContratoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContratoApi apiInstance = new ContratoApi(defaultClient);
    Integer contratoTipoId = 56; // Integer | 
    Integer contaId = 56; // Integer | 
    OffsetDateTime dataInicial = 02/01/2019; // OffsetDateTime | Data no formato DD/MM/AAAA
    OffsetDateTime dataFinal = 02/01/2019; // OffsetDateTime | Data no formato DD/MM/AAAA
    Integer pagina = 1; // Integer | 
    try {
      CobrancaIndexResponseEntity result = apiInstance.index3(contratoTipoId, contaId, dataInicial, dataFinal, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContratoApi#index3");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contratoTipoId** | **Integer**|  |
 **contaId** | **Integer**|  |
 **dataInicial** | **OffsetDateTime**| Data no formato DD/MM/AAAA | [optional]
 **dataFinal** | **OffsetDateTime**| Data no formato DD/MM/AAAA | [optional]
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**CobrancaIndexResponseEntity**](CobrancaIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de Metadados de Cobranças |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="visualizar"></a>
# **visualizar**
> ContratoCobrancaVisualizarResponseEntity visualizar(dataInicial, dataFinal, contratoTipoId, contaId, descricao, planoContaFinanceiroId)



Solicita lista de visualização de Cobranças a partir do Tipo de Contrato

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ContratoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ContratoApi apiInstance = new ContratoApi(defaultClient);
    OffsetDateTime dataInicial = new OffsetDateTime(); // OffsetDateTime | 
    OffsetDateTime dataFinal = new OffsetDateTime(); // OffsetDateTime | 
    Integer contratoTipoId = 56; // Integer | 
    Integer contaId = 56; // Integer | 
    String descricao = "descricao_example"; // String | 
    Long planoContaFinanceiroId = 56L; // Long | 
    try {
      ContratoCobrancaVisualizarResponseEntity result = apiInstance.visualizar(dataInicial, dataFinal, contratoTipoId, contaId, descricao, planoContaFinanceiroId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ContratoApi#visualizar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dataInicial** | **OffsetDateTime**|  |
 **dataFinal** | **OffsetDateTime**|  |
 **contratoTipoId** | **Integer**|  |
 **contaId** | **Integer**|  |
 **descricao** | **String**|  | [optional]
 **planoContaFinanceiroId** | **Long**|  | [optional]

### Return type

[**ContratoCobrancaVisualizarResponseEntity**](ContratoCobrancaVisualizarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de Cobranças |  -  |
**500** | Erro desconhecido no servidor |  -  |

