

# InlineObject73

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**IntegerParam**](IntegerParam.md) |  | 
**moduloPessoa** | [**ModuloPessoaEnum**](#ModuloPessoaEnum) |  | 



## Enum: ModuloPessoaEnum

Name | Value
---- | -----
CLIENTE | &quot;cliente&quot;
FORNECEDOR | &quot;fornecedor&quot;
FUNCIONARIO | &quot;funcionario&quot;
CONTABILISTA | &quot;contabilista&quot;
EMITENTE | &quot;emitente&quot;



