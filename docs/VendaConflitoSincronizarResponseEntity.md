

# VendaConflitoSincronizarResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vendaInformada** | [**Venda**](Venda.md) |  |  [optional]
**vendaEncontrada** | [**Venda**](Venda.md) |  |  [optional]



