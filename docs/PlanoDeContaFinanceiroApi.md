# PlanoDeContaFinanceiroApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**atualizar14**](PlanoDeContaFinanceiroApi.md#atualizar14) | **PUT** /api/plano-conta/{id} | 
[**autocomplete12**](PlanoDeContaFinanceiroApi.md#autocomplete12) | **GET** /api/plano-conta/autocomplete | 
[**criar18**](PlanoDeContaFinanceiroApi.md#criar18) | **POST** /api/plano-conta | 
[**deletar24**](PlanoDeContaFinanceiroApi.md#deletar24) | **DELETE** /api/plano-conta/{id} | 
[**eventos**](PlanoDeContaFinanceiroApi.md#eventos) | **GET** /api/plano-conta/eventos | 
[**mover**](PlanoDeContaFinanceiroApi.md#mover) | **GET** /api/plano-conta/{id_atual}/mover-para/{id_destino} | 
[**resumo**](PlanoDeContaFinanceiroApi.md#resumo) | **GET** /api/plano-conta/painel/resumo | 


<a name="atualizar14"></a>
# **atualizar14**
> PlanoContaListaResponseEntity atualizar14(id, descricao, parentId, fluxoOperacao)



Requisita atualização de plano de conta

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PlanoDeContaFinanceiroApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PlanoDeContaFinanceiroApi apiInstance = new PlanoDeContaFinanceiroApi(defaultClient);
    Long id = 56L; // Long | 
    String descricao = "descricao_example"; // String | 
    Long parentId = 56L; // Long | 
    String fluxoOperacao = "fluxoOperacao_example"; // String | 
    try {
      PlanoContaListaResponseEntity result = apiInstance.atualizar14(id, descricao, parentId, fluxoOperacao);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PlanoDeContaFinanceiroApi#atualizar14");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **descricao** | **String**|  |
 **parentId** | **Long**|  | [optional]
 **fluxoOperacao** | **String**|  | [optional] [enum: Entrada, Saida]

### Return type

[**PlanoContaListaResponseEntity**](PlanoContaListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de plano de contas |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="autocomplete12"></a>
# **autocomplete12**
> PlanoContaAutocompleteResponseEntity autocomplete12(operacao, fluxoOperacao, termo)



Solicita lista de planos de contas financeiro que contenham o termo solicitado na descrição

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PlanoDeContaFinanceiroApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PlanoDeContaFinanceiroApi apiInstance = new PlanoDeContaFinanceiroApi(defaultClient);
    String operacao = "operacao_example"; // String | 
    String fluxoOperacao = "fluxoOperacao_example"; // String | 
    String termo = "termo_example"; // String | 
    try {
      PlanoContaAutocompleteResponseEntity result = apiInstance.autocomplete12(operacao, fluxoOperacao, termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PlanoDeContaFinanceiroApi#autocomplete12");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **operacao** | **String**|  | [optional] [enum: conta_receber__salvar, conta_pagar__salvar, mdfe_contas_pagar__frete, mdfe_contas_pagar__imposto_frete, entrada__outro_frete, entrada__outro_imposto_icms, saida__outro_frete, saida__outro_imposto_icms]
 **fluxoOperacao** | **String**|  | [optional] [enum: Entrada, Saida]
 **termo** | **String**|  | [optional]

### Return type

[**PlanoContaAutocompleteResponseEntity**](PlanoContaAutocompleteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de planos de contas financeiro |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar18"></a>
# **criar18**
> PlanoContaListaResponseEntity criar18(descricao, parentId, fluxoOperacao)



Requisita criação de plano de conta

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PlanoDeContaFinanceiroApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PlanoDeContaFinanceiroApi apiInstance = new PlanoDeContaFinanceiroApi(defaultClient);
    String descricao = "descricao_example"; // String | 
    Long parentId = 56L; // Long | 
    String fluxoOperacao = "fluxoOperacao_example"; // String | 
    try {
      PlanoContaListaResponseEntity result = apiInstance.criar18(descricao, parentId, fluxoOperacao);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PlanoDeContaFinanceiroApi#criar18");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **descricao** | **String**|  |
 **parentId** | **Long**|  | [optional]
 **fluxoOperacao** | **String**|  | [optional] [enum: Entrada, Saida]

### Return type

[**PlanoContaListaResponseEntity**](PlanoContaListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Lista de plano de contas |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar24"></a>
# **deletar24**
> PlanoContaListaResponseEntity deletar24(id)



Requisita remoção de um Plano de Conta

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PlanoDeContaFinanceiroApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PlanoDeContaFinanceiroApi apiInstance = new PlanoDeContaFinanceiroApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      PlanoContaListaResponseEntity result = apiInstance.deletar24(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PlanoDeContaFinanceiroApi#deletar24");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**PlanoContaListaResponseEntity**](PlanoContaListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de plano de contas |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="eventos"></a>
# **eventos**
> PlanoContaEventosResponseEntity eventos(body)



Solicita lista de eventos ocorridos no Plano de Conta

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PlanoDeContaFinanceiroApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PlanoDeContaFinanceiroApi apiInstance = new PlanoDeContaFinanceiroApi(defaultClient);
    Object body = null; // Object | 
    try {
      PlanoContaEventosResponseEntity result = apiInstance.eventos(body);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PlanoDeContaFinanceiroApi#eventos");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **Object**|  | [optional]

### Return type

[**PlanoContaEventosResponseEntity**](PlanoContaEventosResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de eventos |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="mover"></a>
# **mover**
> PlanoContaMoverResponseEntity mover(idAtual, idDestino)



Requisita mover o id de referência (Contas a Receber, Livro de Caixa) de um Plano de Conta para um novo id

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PlanoDeContaFinanceiroApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PlanoDeContaFinanceiroApi apiInstance = new PlanoDeContaFinanceiroApi(defaultClient);
    Long idAtual = 56L; // Long | 
    Long idDestino = 56L; // Long | 
    try {
      PlanoContaMoverResponseEntity result = apiInstance.mover(idAtual, idDestino);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PlanoDeContaFinanceiroApi#mover");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAtual** | **Long**|  |
 **idDestino** | **Long**|  |

### Return type

[**PlanoContaMoverResponseEntity**](PlanoContaMoverResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de Módulos afetados (Contas a Receber, Contas a Pagar, Livro de Caixa) |  -  |
**204** | Não houve erros e também nenhum Plano de Conta foi movido |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="resumo"></a>
# **resumo**
> PainelPlanoContaResumoResponseEntity resumo(dataInicial, dataFinal)



Solicita resumo de Formas de Pagamento num período, junto com valor das despesas não deduzidas. Ideal para ser usado como Gráfico.

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.PlanoDeContaFinanceiroApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    PlanoDeContaFinanceiroApi apiInstance = new PlanoDeContaFinanceiroApi(defaultClient);
    OffsetDateTime dataInicial = 02/01/2019 12:00:00; // OffsetDateTime | Data no formato DD/MM/AAAA HH:MM:SS
    OffsetDateTime dataFinal = 02/01/2019 12:00:00; // OffsetDateTime | Data no formato DD/MM/AAAA HH:MM:SS
    try {
      PainelPlanoContaResumoResponseEntity result = apiInstance.resumo(dataInicial, dataFinal);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PlanoDeContaFinanceiroApi#resumo");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dataInicial** | **OffsetDateTime**| Data no formato DD/MM/AAAA HH:MM:SS |
 **dataFinal** | **OffsetDateTime**| Data no formato DD/MM/AAAA HH:MM:SS |

### Return type

[**PainelPlanoContaResumoResponseEntity**](PainelPlanoContaResumoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de planos de contas financeiro |  -  |
**500** | Erro desconhecido no servidor |  -  |

