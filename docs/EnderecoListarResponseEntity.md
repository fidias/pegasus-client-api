

# EnderecoListarResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;ItemEndereco&gt;**](ItemEndereco.md) |  |  [optional]
**id** | **Long** |  |  [optional]
**enderecoPadrao** | **Long** |  |  [optional]



