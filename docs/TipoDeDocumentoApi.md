# TipoDeDocumentoApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**atualizar8**](TipoDeDocumentoApi.md#atualizar8) | **PUT** /api/livro-caixa/tipo-documento/{id} | 
[**autocomplete10**](TipoDeDocumentoApi.md#autocomplete10) | **GET** /api/livro-caixa/tipo-documento/autocomplete | 
[**criar11**](TipoDeDocumentoApi.md#criar11) | **POST** /api/livro-caixa/tipo-documento | 
[**deletar10**](TipoDeDocumentoApi.md#deletar10) | **DELETE** /api/livro-caixa/tipo-documento/{id} | 
[**index9**](TipoDeDocumentoApi.md#index9) | **GET** /api/livro-caixa/tipo-documento | 


<a name="atualizar8"></a>
# **atualizar8**
> atualizar8(id, inlineObject44)



Requisita atualização de um tipo de documento

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.TipoDeDocumentoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    TipoDeDocumentoApi apiInstance = new TipoDeDocumentoApi(defaultClient);
    Integer id = 56; // Integer | 
    InlineObject44 inlineObject44 = new InlineObject44(); // InlineObject44 | 
    try {
      apiInstance.atualizar8(id, inlineObject44);
    } catch (ApiException e) {
      System.err.println("Exception when calling TipoDeDocumentoApi#atualizar8");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **inlineObject44** | [**InlineObject44**](InlineObject44.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**404** | Tipo de Documento {0} não encontrado. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="autocomplete10"></a>
# **autocomplete10**
> TipoDocumentoAutocompleteResponseEntity autocomplete10(termo)



Solicita lista de Tipos de Documento que contenham o termo solicitado na descrição

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.TipoDeDocumentoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    TipoDeDocumentoApi apiInstance = new TipoDeDocumentoApi(defaultClient);
    String termo = "termo_example"; // String | 
    try {
      TipoDocumentoAutocompleteResponseEntity result = apiInstance.autocomplete10(termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TipoDeDocumentoApi#autocomplete10");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  | [optional]

### Return type

[**TipoDocumentoAutocompleteResponseEntity**](TipoDocumentoAutocompleteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de Tipo de Documento |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar11"></a>
# **criar11**
> criar11(inlineObject45)



Requisita criação de um tipo de documento

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.TipoDeDocumentoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    TipoDeDocumentoApi apiInstance = new TipoDeDocumentoApi(defaultClient);
    InlineObject45 inlineObject45 = new InlineObject45(); // InlineObject45 | 
    try {
      apiInstance.criar11(inlineObject45);
    } catch (ApiException e) {
      System.err.println("Exception when calling TipoDeDocumentoApi#criar11");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject45** | [**InlineObject45**](InlineObject45.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar10"></a>
# **deletar10**
> deletar10(id)



Deleta tipo de documento

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.TipoDeDocumentoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    TipoDeDocumentoApi apiInstance = new TipoDeDocumentoApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      apiInstance.deletar10(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling TipoDeDocumentoApi#deletar10");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**404** | Tipo de documento não encontrado |  -  |
**409** | Não foi possível deletar este Tipo de documento, pois ele é referenciado por uma Conta a Pagar. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="index9"></a>
# **index9**
> TipoDocumentoIndexResponseEntity index9(descricao)



Solicita lista de tipo de documento, com opção de busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.TipoDeDocumentoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    TipoDeDocumentoApi apiInstance = new TipoDeDocumentoApi(defaultClient);
    String descricao = "descricao_example"; // String | 
    try {
      TipoDocumentoIndexResponseEntity result = apiInstance.index9(descricao);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TipoDeDocumentoApi#index9");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **descricao** | **String**|  | [optional]

### Return type

[**TipoDocumentoIndexResponseEntity**](TipoDocumentoIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de tipo de documento |  -  |
**500** | Erro desconhecido no servidor |  -  |

