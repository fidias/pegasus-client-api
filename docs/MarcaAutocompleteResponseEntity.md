

# MarcaAutocompleteResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**list** | [**List&lt;DavMarcaAjaxAutoComplete&gt;**](DavMarcaAjaxAutoComplete.md) |  |  [optional]



