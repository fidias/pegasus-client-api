

# NFSeEnviarLoteResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**loteRpsXml** | **String** |  |  [optional]
**listaTransmissao** | **String** |  |  [optional]
**statusInterno** | **String** |  |  [optional]



