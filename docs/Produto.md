

# Produto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**descricao** | **String** |  | 
**lucro** | **Double** |  | 
**quantidade** | **Double** |  | 
**preco** | **Double** |  | 
**custo** | **Double** |  | 
**dataCadastro** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**dataAlteracao** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**codBarra** | **String** |  |  [optional]
**ultimaVenda** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**ativo** | **Boolean** |  | 
**referencia** | **String** |  |  [optional]
**qtdMinima** | **Double** |  | 
**ultimoFornecedor** | [**UltimoFornecedor**](UltimoFornecedor.md) |  |  [optional]
**grupoMercadoria** | [**GrupoMercadoria**](GrupoMercadoria.md) |  | 
**enderecoLogistico** | **String** |  |  [optional]
**quantidadeReservada** | **Double** |  | 
**unidadeBrev** | **String** |  | 
**criadoPorUsuario** | [**CriadoPorUsuario**](CriadoPorUsuario.md) |  |  [optional]
**alteradoPorUsuario** | [**AlteradoPorUsuario**](AlteradoPorUsuario.md) |  |  [optional]
**tabelasPreco** | [**List&lt;TabelasPreco&gt;**](TabelasPreco.md) |  |  [optional]
**ncm** | [**Ncm**](Ncm.md) |  |  [optional]
**margemLucroPercentual** | **Double** |  | 



