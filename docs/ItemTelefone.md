

# ItemTelefone

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**titulo** | **String** |  | 
**numero** | **Double** |  | 
**numeroFormatado** | **String** |  |  [optional]
**contato** | **String** |  |  [optional]
**telefonePadrao** | **Boolean** |  |  [optional]



