# AnpApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**select2**](AnpApi.md#select2) | **GET** /api/anp/produto/select2 | 


<a name="select2"></a>
# **select2**
> AnpProdutoListaResponseEntity select2(termo, pagina)



Lista tabela ANP do produto, com suporte a busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.AnpApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    AnpApi apiInstance = new AnpApi(defaultClient);
    String termo = "termo_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      AnpProdutoListaResponseEntity result = apiInstance.select2(termo, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AnpApi#select2");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  |
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**AnpProdutoListaResponseEntity**](AnpProdutoListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de produtos da tabela ANP |  -  |
**500** | Erro desconhecido no servidor |  -  |

