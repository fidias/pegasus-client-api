

# InlineObject23

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**livroCaixaFormaPagamentoId** | **Integer** |  | 
**contaId** | **Integer** |  |  [optional]
**vencimento** | **List&lt;String&gt;** |  | 
**valor** | [**List&lt;DoubleParam&gt;**](DoubleParam.md) |  | 



