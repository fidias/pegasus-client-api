

# RetEnvEvento

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  | 
**motivo** | **String** |  | 
**versao** | **String** |  | 
**ambiente** | **String** |  | 
**versaoAplicativo** | **String** |  | 
**uf** | **String** |  | 
**dataHoraRecebimento** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**protocolo** | [**Protocolo1**](Protocolo1.md) |  |  [optional]
**eventos** | [**List&lt;Evento1&gt;**](Evento1.md) |  |  [optional]



