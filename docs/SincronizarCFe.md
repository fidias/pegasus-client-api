

# SincronizarCFe

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienteVendaId** | **Long** |  | 
**versaoDadosEnt** | **String** |  | 
**cnpjSoftwareHouse** | **Long** |  | 
**signAc** | **String** |  | 
**numeroCaixa** | **Integer** |  | 
**retEnviarDadosVenda** | [**SincronizarRetEnviarDadosVenda**](SincronizarRetEnviarDadosVenda.md) |  | 
**retCancelarUltimaVenda** | [**SincronizarRetCancelarUltimaVenda**](SincronizarRetCancelarUltimaVenda.md) |  |  [optional]



