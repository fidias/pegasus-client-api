

# FormaPagamentoIndexResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;FormaPagamentoItemLista&gt;**](FormaPagamentoItemLista.md) |  |  [optional]



