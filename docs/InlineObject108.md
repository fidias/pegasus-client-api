

# InlineObject108

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**unidadeId** | [**IntegerParam**](IntegerParam.md) |  | 
**quantidadeEquivalente** | [**DoubleParam**](DoubleParam.md) |  | 
**codBarrasUnidTributavel** | **String** |  |  [optional]



