# DevolucaoApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**index4**](DevolucaoApi.md#index4) | **GET** /api/devolucao | 


<a name="index4"></a>
# **index4**
> DevolucaoResponseEntity index4(dataInicial, dataFinal, idCliente, numCaixa, numCupom, status, pagina)



Retorna lista de devoluções

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.DevolucaoApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    DevolucaoApi apiInstance = new DevolucaoApi(defaultClient);
    OffsetDateTime dataInicial = 02/01/2019; // OffsetDateTime | Data no formato DD/MM/AAAA
    OffsetDateTime dataFinal = 02/01/2019; // OffsetDateTime | Data no formato DD/MM/AAAA
    Integer idCliente = 56; // Integer | 
    Integer numCaixa = 56; // Integer | 
    Integer numCupom = 56; // Integer | 
    String status = "status_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      DevolucaoResponseEntity result = apiInstance.index4(dataInicial, dataFinal, idCliente, numCaixa, numCupom, status, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DevolucaoApi#index4");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dataInicial** | **OffsetDateTime**| Data no formato DD/MM/AAAA | [optional]
 **dataFinal** | **OffsetDateTime**| Data no formato DD/MM/AAAA | [optional]
 **idCliente** | **Integer**|  | [optional]
 **numCaixa** | **Integer**|  | [optional]
 **numCupom** | **Integer**|  | [optional]
 **status** | **String**|  | [optional] [enum: concluida, nao_concluida]
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**DevolucaoResponseEntity**](DevolucaoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de devoluções |  -  |
**500** | Erro desconhecido no servidor |  -  |

