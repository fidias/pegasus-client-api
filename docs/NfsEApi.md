# NfsEApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**arquivar1**](NfsEApi.md#arquivar1) | **PUT** /api/nfse/{id}/arquivar | 
[**atualizar10**](NfsEApi.md#atualizar10) | **PUT** /api/nfse/{id} | 
[**carregar11**](NfsEApi.md#carregar11) | **GET** /api/nfse/configuracao | 
[**consultarLoteRps**](NfsEApi.md#consultarLoteRps) | **GET** /api/nfse/{id}/evento/consultar-lote-rps | 
[**criar14**](NfsEApi.md#criar14) | **POST** /api/nfse | 
[**enviarLoteRps**](NfsEApi.md#enviarLoteRps) | **GET** /api/nfse/{id}/evento/enviar-lote-rps | 
[**get7**](NfsEApi.md#get7) | **GET** /api/nfse/{id} | 
[**getXml**](NfsEApi.md#getXml) | **GET** /api/nfse/{id}/xml | 
[**index10**](NfsEApi.md#index10) | **GET** /api/nfse | 
[**salvar14**](NfsEApi.md#salvar14) | **POST** /api/nfse/configuracao | 


<a name="arquivar1"></a>
# **arquivar1**
> arquivar1(id)



Requisita arquivar um NFS-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfsEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfsEApi apiInstance = new NfsEApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      apiInstance.arquivar1(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfsEApi#arquivar1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="atualizar10"></a>
# **atualizar10**
> NFSeCompletoResponseEntity atualizar10(id, numeroRps, dataEmissao, discriminacao, codigoServicoId, aliquotaServicos, valorServicos, tomadorServicoId, naturezaOperacaoId, municipioPrestacaoServicoId, valorDeducoes, valorPis, valorCofins, valorInss, valorCsll, valorIr, outrasRetencoes, valorIssRetido, issRetido)



Requisita atualização de um NFS-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfsEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfsEApi apiInstance = new NfsEApi(defaultClient);
    Long id = 56L; // Long | 
    Long numeroRps = 56L; // Long | 
    OffsetDateTime dataEmissao = new OffsetDateTime(); // OffsetDateTime | Data no formato DD/MM/AAAA HH:MM:SS
    String discriminacao = "discriminacao_example"; // String | 
    Integer codigoServicoId = 56; // Integer | 
    Double aliquotaServicos = 3.4D; // Double | 
    Double valorServicos = 3.4D; // Double | 
    Integer tomadorServicoId = 56; // Integer | 
    Integer naturezaOperacaoId = 56; // Integer | 
    Integer municipioPrestacaoServicoId = 56; // Integer | 
    Double valorDeducoes = 3.4D; // Double | 
    Double valorPis = 3.4D; // Double | 
    Double valorCofins = 3.4D; // Double | 
    Double valorInss = 3.4D; // Double | 
    Double valorCsll = 3.4D; // Double | 
    Double valorIr = 3.4D; // Double | 
    Double outrasRetencoes = 3.4D; // Double | 
    Double valorIssRetido = 3.4D; // Double | 
    Boolean issRetido = false; // Boolean | 
    try {
      NFSeCompletoResponseEntity result = apiInstance.atualizar10(id, numeroRps, dataEmissao, discriminacao, codigoServicoId, aliquotaServicos, valorServicos, tomadorServicoId, naturezaOperacaoId, municipioPrestacaoServicoId, valorDeducoes, valorPis, valorCofins, valorInss, valorCsll, valorIr, outrasRetencoes, valorIssRetido, issRetido);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfsEApi#atualizar10");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **numeroRps** | **Long**|  |
 **dataEmissao** | **OffsetDateTime**| Data no formato DD/MM/AAAA HH:MM:SS |
 **discriminacao** | **String**|  |
 **codigoServicoId** | **Integer**|  |
 **aliquotaServicos** | **Double**|  |
 **valorServicos** | **Double**|  |
 **tomadorServicoId** | **Integer**|  |
 **naturezaOperacaoId** | **Integer**|  |
 **municipioPrestacaoServicoId** | **Integer**|  |
 **valorDeducoes** | **Double**|  | [optional]
 **valorPis** | **Double**|  | [optional]
 **valorCofins** | **Double**|  | [optional]
 **valorInss** | **Double**|  | [optional]
 **valorCsll** | **Double**|  | [optional]
 **valorIr** | **Double**|  | [optional]
 **outrasRetencoes** | **Double**|  | [optional]
 **valorIssRetido** | **Double**|  | [optional]
 **issRetido** | **Boolean**|  | [optional] [default to false]

### Return type

[**NFSeCompletoResponseEntity**](NFSeCompletoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um NFS-e |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="carregar11"></a>
# **carregar11**
> NFSeConfiguracaoResponseEntity carregar11()



Retorna conjunto de configurações relacionadas ao NFS-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfsEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfsEApi apiInstance = new NfsEApi(defaultClient);
    try {
      NFSeConfiguracaoResponseEntity result = apiInstance.carregar11();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfsEApi#carregar11");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NFSeConfiguracaoResponseEntity**](NFSeConfiguracaoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Conjunto de configurações do NFS-e |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="consultarLoteRps"></a>
# **consultarLoteRps**
> NFSeConsultarLoteResponseEntity consultarLoteRps(id)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfsEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfsEApi apiInstance = new NfsEApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      NFSeConsultarLoteResponseEntity result = apiInstance.consultarLoteRps(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfsEApi#consultarLoteRps");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**NFSeConsultarLoteResponseEntity**](NFSeConsultarLoteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Solicita consulta da situação de Lote RPS de uma NFS-e |  -  |
**404** | Registro não encontrado  * Lote para Consulta de NFS-e não encontrado. |  -  |
**412** | Pré-condição não satisfeita  * Protocolo do Lote de Consulta de NFS-e não encontrado. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar14"></a>
# **criar14**
> NFSeResponseEntity criar14(numeroRps, dataEmissao, discriminacao, codigoServicoId, aliquotaServicos, valorServicos, tomadorServicoId, naturezaOperacaoId, municipioPrestacaoServicoId, valorDeducoes, valorPis, valorCofins, valorInss, valorCsll, valorIr, outrasRetencoes, valorIssRetido, issRetido)



Cria uma NFS-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfsEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfsEApi apiInstance = new NfsEApi(defaultClient);
    Long numeroRps = 56L; // Long | 
    OffsetDateTime dataEmissao = new OffsetDateTime(); // OffsetDateTime | Data no formato DD/MM/AAAA HH:MM:SS
    String discriminacao = "discriminacao_example"; // String | 
    Integer codigoServicoId = 56; // Integer | 
    Double aliquotaServicos = 3.4D; // Double | 
    Double valorServicos = 3.4D; // Double | 
    Integer tomadorServicoId = 56; // Integer | 
    Integer naturezaOperacaoId = 56; // Integer | 
    Integer municipioPrestacaoServicoId = 56; // Integer | 
    Double valorDeducoes = 3.4D; // Double | 
    Double valorPis = 3.4D; // Double | 
    Double valorCofins = 3.4D; // Double | 
    Double valorInss = 3.4D; // Double | 
    Double valorCsll = 3.4D; // Double | 
    Double valorIr = 3.4D; // Double | 
    Double outrasRetencoes = 3.4D; // Double | 
    Double valorIssRetido = 3.4D; // Double | 
    Boolean issRetido = false; // Boolean | 
    try {
      NFSeResponseEntity result = apiInstance.criar14(numeroRps, dataEmissao, discriminacao, codigoServicoId, aliquotaServicos, valorServicos, tomadorServicoId, naturezaOperacaoId, municipioPrestacaoServicoId, valorDeducoes, valorPis, valorCofins, valorInss, valorCsll, valorIr, outrasRetencoes, valorIssRetido, issRetido);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfsEApi#criar14");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **numeroRps** | **Long**|  |
 **dataEmissao** | **OffsetDateTime**| Data no formato DD/MM/AAAA HH:MM:SS |
 **discriminacao** | **String**|  |
 **codigoServicoId** | **Integer**|  |
 **aliquotaServicos** | **Double**|  |
 **valorServicos** | **Double**|  |
 **tomadorServicoId** | **Integer**|  |
 **naturezaOperacaoId** | **Integer**|  |
 **municipioPrestacaoServicoId** | **Integer**|  |
 **valorDeducoes** | **Double**|  | [optional]
 **valorPis** | **Double**|  | [optional]
 **valorCofins** | **Double**|  | [optional]
 **valorInss** | **Double**|  | [optional]
 **valorCsll** | **Double**|  | [optional]
 **valorIr** | **Double**|  | [optional]
 **outrasRetencoes** | **Double**|  | [optional]
 **valorIssRetido** | **Double**|  | [optional]
 **issRetido** | **Boolean**|  | [optional] [default to false]

### Return type

[**NFSeResponseEntity**](NFSeResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Retorna id da NFS-e |  -  |
**400** | Erro ao criar NFS-e |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="enviarLoteRps"></a>
# **enviarLoteRps**
> NFSeEnviarLoteResponseEntity enviarLoteRps(id)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfsEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfsEApi apiInstance = new NfsEApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      NFSeEnviarLoteResponseEntity result = apiInstance.enviarLoteRps(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfsEApi#enviarLoteRps");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**NFSeEnviarLoteResponseEntity**](NFSeEnviarLoteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Solicita envio de Lote RPS de uma NFS-e |  -  |
**400** | Erro na validação do XML |  -  |
**412** | Pré-condição não satisfeita  * NFS-e não encontrado * NFS-e &#39;&#39;{0}&#39;&#39; já foi Autorizado e não pode ser reenviado. * NFS-e &#39;&#39;{0}&#39;&#39; já foi Cancelado e não pode ser reenviado. * NFS-e &#39;&#39;{0}&#39;&#39; já foi Arquivado e não pode ser reenviado. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="get7"></a>
# **get7**
> NFSeCompletoResponseEntity get7(id)



Solicita um NFS-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfsEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfsEApi apiInstance = new NfsEApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      NFSeCompletoResponseEntity result = apiInstance.get7(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfsEApi#get7");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**NFSeCompletoResponseEntity**](NFSeCompletoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um NFS-e |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="getXml"></a>
# **getXml**
> NFSeXmlResponseEntity getXml(id)



Solicita o xml de um NFS-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfsEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfsEApi apiInstance = new NfsEApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      NFSeXmlResponseEntity result = apiInstance.getXml(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfsEApi#getXml");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**NFSeXmlResponseEntity**](NFSeXmlResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | RPS XML e XML de um NFS-e |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="index10"></a>
# **index10**
> NFSeIndexResponseEntity index10(tomadorServicoId, dataEmissaoInicial, dataEmissaoFinal, statusInternoId, pagina)



Solicita lista de NFS-e, com opção de busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfsEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfsEApi apiInstance = new NfsEApi(defaultClient);
    Long tomadorServicoId = 56L; // Long | 
    OffsetDateTime dataEmissaoInicial = 02/01/2019; // OffsetDateTime | Data no formato DD/MM/AAAA
    OffsetDateTime dataEmissaoFinal = 02/01/2019; // OffsetDateTime | Data no formato DD/MM/AAAA
    String statusInternoId = "statusInternoId_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      NFSeIndexResponseEntity result = apiInstance.index10(tomadorServicoId, dataEmissaoInicial, dataEmissaoFinal, statusInternoId, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfsEApi#index10");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tomadorServicoId** | **Long**|  | [optional]
 **dataEmissaoInicial** | **OffsetDateTime**| Data no formato DD/MM/AAAA | [optional]
 **dataEmissaoFinal** | **OffsetDateTime**| Data no formato DD/MM/AAAA | [optional]
 **statusInternoId** | **String**|  | [optional] [enum: EmDigitacao, RPSEnviado, RPSComErro, Autorizado, Cancelado, Arquivado]
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**NFSeIndexResponseEntity**](NFSeIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de NFS-e |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar14"></a>
# **salvar14**
> NFSeConfiguracaoResponseEntity salvar14(numeroLoteRps, numeroRps, serieRps, tipoRps, regimeEspecialTributacao, naturezaOperacao, codigoServicoPadrao, aliquotaServicoPadrao, codigoCnae, ambienteNfse)



Salva conjunto de configurações do NFS-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfsEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfsEApi apiInstance = new NfsEApi(defaultClient);
    Long numeroLoteRps = 56L; // Long | 
    Long numeroRps = 56L; // Long | 
    String serieRps = "serieRps_example"; // String | 
    String tipoRps = "ReciboProvisorioServicos"; // String | 
    String regimeEspecialTributacao = "NaoInformado"; // String | 
    String naturezaOperacao = "TributacaoMunicipio"; // String | 
    Long codigoServicoPadrao = 56L; // Long | 
    Double aliquotaServicoPadrao = 3.4D; // Double | 
    Long codigoCnae = 56L; // Long | 
    String ambienteNfse = "Homologacao"; // String | 
    try {
      NFSeConfiguracaoResponseEntity result = apiInstance.salvar14(numeroLoteRps, numeroRps, serieRps, tipoRps, regimeEspecialTributacao, naturezaOperacao, codigoServicoPadrao, aliquotaServicoPadrao, codigoCnae, ambienteNfse);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfsEApi#salvar14");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **numeroLoteRps** | **Long**|  |
 **numeroRps** | **Long**|  |
 **serieRps** | **String**|  |
 **tipoRps** | **String**|  | [default to ReciboProvisorioServicos] [enum: ReciboProvisorioServicos, RpsNotaFiscalConjugada, Cupom]
 **regimeEspecialTributacao** | **String**|  | [default to NaoInformado] [enum: NaoInformado, MicroempresaMunicipal, Estimativa, SociedadeProfissionais, Cooperativa, MEI, MEEPP]
 **naturezaOperacao** | **String**|  | [default to TributacaoMunicipio] [enum: TributacaoMunicipio, TributacaoForaMunicipio, Isencao, Imune, ExigibilidadeSuspensaDecisaoJudicial, ExigibilidadeSuspensaProcedimentoAdministrativo]
 **codigoServicoPadrao** | **Long**|  |
 **aliquotaServicoPadrao** | **Double**|  |
 **codigoCnae** | **Long**|  |
 **ambienteNfse** | **String**|  | [default to Homologacao] [enum: Producao, Homologacao]

### Return type

[**NFSeConfiguracaoResponseEntity**](NFSeConfiguracaoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Conjunto de configurações do NFS-e |  -  |
**500** | Erro desconhecido no servidor |  -  |

