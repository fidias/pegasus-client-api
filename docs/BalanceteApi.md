# BalanceteApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**criar**](BalanceteApi.md#criar) | **POST** /api/balancete-manual | 
[**criar1**](BalanceteApi.md#criar1) | **POST** /api/balancete-manual/{id}/produto | 
[**deletar**](BalanceteApi.md#deletar) | **DELETE** /api/balancete-manual/{id}/produto/{produto_id} | 
[**encerrar**](BalanceteApi.md#encerrar) | **GET** /api/balancete-manual/{id}/encerrar | 
[**filtrar**](BalanceteApi.md#filtrar) | **GET** /api/balancete-manual/{id}/produto/filtrar | 
[**gerarEntradaSaida**](BalanceteApi.md#gerarEntradaSaida) | **GET** /api/balancete-manual/{id}/gerar-entrada-saida | 
[**get**](BalanceteApi.md#get) | **GET** /api/balancete-manual/{id} | 
[**index**](BalanceteApi.md#index) | **GET** /api/balancete-manual | 


<a name="criar"></a>
# **criar**
> BalanceteManualCriadoResponseEntity criar(responsavelId, tipoId, motivoId)



Criar Balancete

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.BalanceteApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    BalanceteApi apiInstance = new BalanceteApi(defaultClient);
    IntegerParam responsavelId = new IntegerParam(); // IntegerParam | 
    IntegerParam tipoId = new IntegerParam(); // IntegerParam | 
    IntegerParam motivoId = new IntegerParam(); // IntegerParam | 
    try {
      BalanceteManualCriadoResponseEntity result = apiInstance.criar(responsavelId, tipoId, motivoId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling BalanceteApi#criar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **responsavelId** | [**IntegerParam**](IntegerParam.md)|  |
 **tipoId** | [**IntegerParam**](IntegerParam.md)|  |
 **motivoId** | [**IntegerParam**](IntegerParam.md)|  |

### Return type

[**BalanceteManualCriadoResponseEntity**](BalanceteManualCriadoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Retorna id do Balancete |  -  |
**400** | Erro ao criar Balancete |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar1"></a>
# **criar1**
> BalanceteManualListaResponseEntity criar1(id, produtoId, quantidade)



Adicionar Produto a um Balancete

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.BalanceteApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    BalanceteApi apiInstance = new BalanceteApi(defaultClient);
    Long id = 56L; // Long | 
    Long produtoId = 56L; // Long | 
    Double quantidade = 3.4D; // Double | 
    try {
      BalanceteManualListaResponseEntity result = apiInstance.criar1(id, produtoId, quantidade);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling BalanceteApi#criar1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **produtoId** | **Long**|  |
 **quantidade** | **Double**|  |

### Return type

[**BalanceteManualListaResponseEntity**](BalanceteManualListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Retorna itens do Balancete |  -  |
**400** |  |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar"></a>
# **deletar**
> BalanceteManualListaResponseEntity deletar(produtoId, id)



Requisita remoção de Produto de um Balancete

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.BalanceteApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    BalanceteApi apiInstance = new BalanceteApi(defaultClient);
    Long produtoId = 56L; // Long | 
    Long id = 56L; // Long | 
    try {
      BalanceteManualListaResponseEntity result = apiInstance.deletar(produtoId, id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling BalanceteApi#deletar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **produtoId** | **Long**|  |
 **id** | **Long**|  |

### Return type

[**BalanceteManualListaResponseEntity**](BalanceteManualListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna itens do Balancete |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="encerrar"></a>
# **encerrar**
> BalanceteManualCompletoResponseEntity encerrar(id)



Requisita encerramento de um Balancete (Inventário Físico)

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.BalanceteApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    BalanceteApi apiInstance = new BalanceteApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      BalanceteManualCompletoResponseEntity result = apiInstance.encerrar(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling BalanceteApi#encerrar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**BalanceteManualCompletoResponseEntity**](BalanceteManualCompletoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um Balancete |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="filtrar"></a>
# **filtrar**
> BalanceteManualListaResponseEntity filtrar(id, produto, pagina)



Filtra lista de Produtos de um Balancete

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.BalanceteApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    BalanceteApi apiInstance = new BalanceteApi(defaultClient);
    Long id = 56L; // Long | 
    String produto = "produto_example"; // String | 
    Integer pagina = 1; // Integer | 
    try {
      BalanceteManualListaResponseEntity result = apiInstance.filtrar(id, produto, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling BalanceteApi#filtrar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **produto** | **String**|  | [optional]
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**BalanceteManualListaResponseEntity**](BalanceteManualListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna itens do Balancete |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="gerarEntradaSaida"></a>
# **gerarEntradaSaida**
> BalanceteManualCompletoResponseEntity gerarEntradaSaida(id)



Requisita geração de notas de Entrada/Saída de um Balancete (Inventário Físico)

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.BalanceteApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    BalanceteApi apiInstance = new BalanceteApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      BalanceteManualCompletoResponseEntity result = apiInstance.gerarEntradaSaida(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling BalanceteApi#gerarEntradaSaida");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**BalanceteManualCompletoResponseEntity**](BalanceteManualCompletoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um Balancete |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="get"></a>
# **get**
> BalanceteManualCompletoResponseEntity get(id)



Solicita um Balancete

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.BalanceteApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    BalanceteApi apiInstance = new BalanceteApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      BalanceteManualCompletoResponseEntity result = apiInstance.get(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling BalanceteApi#get");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

[**BalanceteManualCompletoResponseEntity**](BalanceteManualCompletoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados completos de um Balancete |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="index"></a>
# **index**
> BalanceteManualIndexResponseEntity index(pagina)



Solicita lista de Metadados de Balancetes, com opção de busca

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.BalanceteApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    BalanceteApi apiInstance = new BalanceteApi(defaultClient);
    Integer pagina = 1; // Integer | 
    try {
      BalanceteManualIndexResponseEntity result = apiInstance.index(pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling BalanceteApi#index");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**BalanceteManualIndexResponseEntity**](BalanceteManualIndexResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de Metadados de Balancetes |  -  |
**500** | Erro desconhecido no servidor |  -  |

