

# ListarConfigComandaResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**configuracoes** | [**List&lt;Configuracao&gt;**](Configuracao.md) |  |  [optional]
**servicoImpressaoExecutando** | **Boolean** |  |  [optional]



