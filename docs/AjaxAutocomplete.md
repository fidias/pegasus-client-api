

# AjaxAutocomplete

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**value** | **String** |  | 
**cpfCnpjFormatado** | **String** |  |  [optional]



