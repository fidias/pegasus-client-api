# ClienteApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**autocomplete**](ClienteApi.md#autocomplete) | **GET** /api/cliente/autocomplete | 
[**clienteCriarCredito**](ClienteApi.md#clienteCriarCredito) | **POST** /api/cliente/{cliente_id}/credito | 
[**clienteReservarLimiteCredito**](ClienteApi.md#clienteReservarLimiteCredito) | **POST** /api/cliente/{cliente_id}/reservar-limite-credito | 
[**getTabelaPreco**](ClienteApi.md#getTabelaPreco) | **GET** /api/cliente/{id}/tabela-preco | 
[**index1**](ClienteApi.md#index1) | **GET** /api/cliente/{cliente_id}/credito | 


<a name="autocomplete"></a>
# **autocomplete**
> ClienteAutocompleteResponseEntity autocomplete(termo)



Solicita lista de clientes que contenham o termo solicitado no nome ou nome fantasia

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ClienteApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ClienteApi apiInstance = new ClienteApi(defaultClient);
    String termo = "termo_example"; // String | 
    try {
      ClienteAutocompleteResponseEntity result = apiInstance.autocomplete(termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ClienteApi#autocomplete");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  | [optional]

### Return type

[**ClienteAutocompleteResponseEntity**](ClienteAutocompleteResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna lista de clientes |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="clienteCriarCredito"></a>
# **clienteCriarCredito**
> ClienteCreditoCriarResponseEntity clienteCriarCredito(clienteId, tipoTransacaoId, valor, historico, identificadorUnico)



Requisita utilização de Crédito/Débito de um Cliente

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ClienteApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ClienteApi apiInstance = new ClienteApi(defaultClient);
    Integer clienteId = 56; // Integer | 
    Integer tipoTransacaoId = 56; // Integer | 
    Double valor = 3.4D; // Double | 
    String historico = "historico_example"; // String | 
    String identificadorUnico = "identificadorUnico_example"; // String | 
    try {
      ClienteCreditoCriarResponseEntity result = apiInstance.clienteCriarCredito(clienteId, tipoTransacaoId, valor, historico, identificadorUnico);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ClienteApi#clienteCriarCredito");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clienteId** | **Integer**|  |
 **tipoTransacaoId** | **Integer**|  |
 **valor** | **Double**|  |
 **historico** | **String**|  |
 **identificadorUnico** | **String**|  |

### Return type

[**ClienteCreditoCriarResponseEntity**](ClienteCreditoCriarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Código (ID) do Crédito/Débito solicitado |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="clienteReservarLimiteCredito"></a>
# **clienteReservarLimiteCredito**
> clienteReservarLimiteCredito(clienteId, identificadorUnico, valorSolicitado)



Requisita reservar de Limite de Crédito para um Cliente

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ClienteApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ClienteApi apiInstance = new ClienteApi(defaultClient);
    Integer clienteId = 56; // Integer | 
    String identificadorUnico = "identificadorUnico_example"; // String | 
    Double valorSolicitado = 3.4D; // Double | 
    try {
      apiInstance.clienteReservarLimiteCredito(clienteId, identificadorUnico, valorSolicitado);
    } catch (ApiException e) {
      System.err.println("Exception when calling ClienteApi#clienteReservarLimiteCredito");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clienteId** | **Integer**|  |
 **identificadorUnico** | **String**|  |
 **valorSolicitado** | **Double**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="getTabelaPreco"></a>
# **getTabelaPreco**
> ProdutoTabelaPreco getTabelaPreco(id)



Solicita a Tabela de Preço do Cliente em questão

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ClienteApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ClienteApi apiInstance = new ClienteApi(defaultClient);
    Integer id = 56; // Integer | 
    try {
      ProdutoTabelaPreco result = apiInstance.getTabelaPreco(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ClienteApi#getTabelaPreco");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |

### Return type

[**ProdutoTabelaPreco**](ProdutoTabelaPreco.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna Tabela de Preço do Cliente |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="index1"></a>
# **index1**
> ClienteCreditoListaResponseEntity index1(clienteId, pagina)



Solicita a Lista de crédito usado e recebido do Cliente

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.ClienteApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    ClienteApi apiInstance = new ClienteApi(defaultClient);
    Integer clienteId = 56; // Integer | 
    Integer pagina = 1; // Integer | 
    try {
      ClienteCreditoListaResponseEntity result = apiInstance.index1(clienteId, pagina);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ClienteApi#index1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clienteId** | **Integer**|  |
 **pagina** | **Integer**|  | [optional] [default to 1]

### Return type

[**ClienteCreditoListaResponseEntity**](ClienteCreditoListaResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna Lista de crédito usado e recebido do Cliente |  -  |
**500** | Erro desconhecido no servidor |  -  |

