

# InlineObject66

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pagamentoFormaId** | [**IntegerParam**](IntegerParam.md) |  | 
**valor** | [**DoubleParam**](DoubleParam.md) |  | 
**bandeiraId** | [**IntegerParam**](IntegerParam.md) |  |  [optional]
**numeroAutorizacaoOperacao** | **String** |  |  [optional]
**descricaoMeioPagamento** | **String** |  |  [optional]



