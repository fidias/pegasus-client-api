

# InlineObject93

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cProdAnp** | **Long** |  | 
**vPart** | **Double** |  |  [optional]
**qTemp** | **Double** |  |  [optional]
**pGlp** | **Double** |  |  [optional]
**pGnn** | **Double** |  |  [optional]
**pGni** | **Double** |  |  [optional]
**copiarQcomParaQtemp** | **Boolean** |  |  [optional]



