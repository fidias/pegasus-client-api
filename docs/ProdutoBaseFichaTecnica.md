

# ProdutoBaseFichaTecnica

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**custo** | **Double** |  |  [optional]
**margemLucroPercentual** | **Double** |  |  [optional]
**preco** | **Double** |  |  [optional]
**lucro** | **Double** |  |  [optional]



