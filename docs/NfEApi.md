# NfEApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**aplicarAliquotaPadrao**](NfEApi.md#aplicarAliquotaPadrao) | **GET** /api/nfe/icms/{cst}/emitente/{emitente_id}/aplicar-aliquota-padrao | 
[**atualizar9**](NfEApi.md#atualizar9) | **PUT** /api/nfe/{nfe_id}/pagamento/{id} | 
[**autocomplete11**](NfEApi.md#autocomplete11) | **GET** /api/nfe/destinatario/autocomplete | 
[**carregar10**](NfEApi.md#carregar10) | **GET** /api/nfe/{nfe_id}/produto/{id}/combustivel | 
[**carregar8**](NfEApi.md#carregar8) | **GET** /api/nfe/configuracao | 
[**carregar9**](NfEApi.md#carregar9) | **GET** /api/nfe/{nfe_id}/pagamento/{id} | 
[**carregarAliquotaPadrao**](NfEApi.md#carregarAliquotaPadrao) | **GET** /api/nfe/icms/{cst}/emitente/{emitente_id}/aliquota-padrao | 
[**carregarAliquotaPadrao1**](NfEApi.md#carregarAliquotaPadrao1) | **GET** /api/nfe/icms/{cst}/aliquota-padrao | 
[**consultarChaveNFe**](NfEApi.md#consultarChaveNFe) | **GET** /api/nfe/distribuicao-dfe/cons-ch-nfe | 
[**consultarNSU**](NfEApi.md#consultarNSU) | **GET** /api/nfe/distribuicao-dfe/cons-nsu | 
[**consultarStatusServico**](NfEApi.md#consultarStatusServico) | **GET** /api/nfe/consultar-status-servico | 
[**criar12**](NfEApi.md#criar12) | **POST** /api/nfe/{nfe_id}/pagamento | 
[**criar13**](NfEApi.md#criar13) | **POST** /api/nfe/{nfe_id}/ref/nf | 
[**deletar17**](NfEApi.md#deletar17) | **DELETE** /api/nfe/{nfe_id}/produto/{id}/ipi | 
[**deletar18**](NfEApi.md#deletar18) | **DELETE** /api/nfe/{nfe_id}/pagamento/{id} | 
[**deletar19**](NfEApi.md#deletar19) | **DELETE** /api/nfe/{nfe_id}/produto/{id}/combustivel | 
[**deletar20**](NfEApi.md#deletar20) | **DELETE** /api/nfe/{nfe_id}/ref/nf/{id} | 
[**deletarDadosCertificado**](NfEApi.md#deletarDadosCertificado) | **DELETE** /api/nfe/configuracao/dados-certificado | 
[**exportarEntradaSaida**](NfEApi.md#exportarEntradaSaida) | **GET** /api/nfe/{nfe_id}/exportar/entrada-saida | 
[**listar6**](NfEApi.md#listar6) | **GET** /api/nfe/manifestacao-destinatario | 
[**listar7**](NfEApi.md#listar7) | **GET** /api/nfe/{nfe_id}/pagamento | 
[**listar8**](NfEApi.md#listar8) | **GET** /api/nfe/{nfe_id}/ref/nf | 
[**listarEnquadramentoLegalSelect2**](NfEApi.md#listarEnquadramentoLegalSelect2) | **GET** /api/nfe/ipi/enquadramento-legal/select2 | 
[**listarSelect22**](NfEApi.md#listarSelect22) | **GET** /api/nfe/natureza-operacao/select2 | 
[**listarSeloControleSelect2**](NfEApi.md#listarSeloControleSelect2) | **GET** /api/nfe/ipi/selo-controle/select2 | 
[**resumoAliquotaPadrao**](NfEApi.md#resumoAliquotaPadrao) | **GET** /api/nfe/icms/{cst}/emitente/{emitente_id}/resumo-aliquota-padrao | 
[**salvar12**](NfEApi.md#salvar12) | **POST** /api/nfe/configuracao | 
[**salvar13**](NfEApi.md#salvar13) | **POST** /api/nfe/{nfe_id}/produto/{id}/combustivel | 
[**salvarAliquotaPadrao**](NfEApi.md#salvarAliquotaPadrao) | **POST** /api/nfe/icms/{cst}/emitente/{emitente_id}/aliquota-padrao | 
[**salvarDadosCertificado**](NfEApi.md#salvarDadosCertificado) | **POST** /api/nfe/configuracao/dados-certificado | 
[**transmitirESalvar**](NfEApi.md#transmitirESalvar) | **POST** /api/nfe/manifestacao-destinatario | 


<a name="aplicarAliquotaPadrao"></a>
# **aplicarAliquotaPadrao**
> ICMSAplicarAliquotaPadraoResponseEntity aplicarAliquotaPadrao(cst, emitenteId)



Aplica, nos Produtos, as alíquotas de um determinado ICMS, baseado em seu Código de Situação Tributária (CST).

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    String cst = "cst_example"; // String | 
    Integer emitenteId = 56; // Integer | 
    try {
      ICMSAplicarAliquotaPadraoResponseEntity result = apiInstance.aplicarAliquotaPadrao(cst, emitenteId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#aplicarAliquotaPadrao");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cst** | **String**|  |
 **emitenteId** | **Integer**|  |

### Return type

[**ICMSAplicarAliquotaPadraoResponseEntity**](ICMSAplicarAliquotaPadraoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Eventos ocorridos na Alíquota Padrão do Emitente. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="atualizar9"></a>
# **atualizar9**
> atualizar9(id, nfeId, pagamentoFormaId, valor, bandeiraId, numeroAutorizacaoOperacao, descricaoMeioPagamento)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    Long id = 56L; // Long | 
    Long nfeId = 56L; // Long | 
    IntegerParam pagamentoFormaId = new IntegerParam(); // IntegerParam | 
    DoubleParam valor = new DoubleParam(); // DoubleParam | 
    IntegerParam bandeiraId = new IntegerParam(); // IntegerParam | 
    String numeroAutorizacaoOperacao = "numeroAutorizacaoOperacao_example"; // String | 
    String descricaoMeioPagamento = "descricaoMeioPagamento_example"; // String | 
    try {
      apiInstance.atualizar9(id, nfeId, pagamentoFormaId, valor, bandeiraId, numeroAutorizacaoOperacao, descricaoMeioPagamento);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#atualizar9");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **nfeId** | **Long**|  |
 **pagamentoFormaId** | [**IntegerParam**](IntegerParam.md)|  |
 **valor** | [**DoubleParam**](DoubleParam.md)|  |
 **bandeiraId** | [**IntegerParam**](IntegerParam.md)|  | [optional]
 **numeroAutorizacaoOperacao** | **String**|  | [optional]
 **descricaoMeioPagamento** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="autocomplete11"></a>
# **autocomplete11**
> NFeDestinatarioListResponseEntity autocomplete11(termo)



Autocomplete para buscar Destinatários, seja Cliente ou Fornecedor.

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    String termo = "termo_example"; // String | 
    try {
      NFeDestinatarioListResponseEntity result = apiInstance.autocomplete11(termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#autocomplete11");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  | [optional]

### Return type

[**NFeDestinatarioListResponseEntity**](NFeDestinatarioListResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de Destinatários |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="carregar10"></a>
# **carregar10**
> DetCombustivelResponseEntity carregar10(nfeId, id)



Solicita dados do combustível de um produto

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    Long nfeId = 56L; // Long | 
    Long id = 56L; // Long | 
    try {
      DetCombustivelResponseEntity result = apiInstance.carregar10(nfeId, id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#carregar10");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nfeId** | **Long**|  |
 **id** | **Long**|  |

### Return type

[**DetCombustivelResponseEntity**](DetCombustivelResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados do combustível |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="carregar8"></a>
# **carregar8**
> NFeConfiguracaoResponseEntity carregar8()



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    try {
      NFeConfiguracaoResponseEntity result = apiInstance.carregar8();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#carregar8");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NFeConfiguracaoResponseEntity**](NFeConfiguracaoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Retorna conjunto de configurações relacionadas ao NF-e |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="carregar9"></a>
# **carregar9**
> carregar9(nfeId, id)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    Long nfeId = 56L; // Long | 
    Long id = 56L; // Long | 
    try {
      apiInstance.carregar9(nfeId, id);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#carregar9");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nfeId** | **Long**|  |
 **id** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="carregarAliquotaPadrao"></a>
# **carregarAliquotaPadrao**
> ICMSAliquotaPadraoResponseEntity carregarAliquotaPadrao(cst, emitenteId)



Busca todas as alíquotas relecionadas a um ICMS, baseada em seu Código de Situação Tributária (CST)

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    String cst = "cst_example"; // String | 
    Integer emitenteId = 56; // Integer | 
    try {
      ICMSAliquotaPadraoResponseEntity result = apiInstance.carregarAliquotaPadrao(cst, emitenteId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#carregarAliquotaPadrao");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cst** | **String**|  |
 **emitenteId** | **Integer**|  |

### Return type

[**ICMSAliquotaPadraoResponseEntity**](ICMSAliquotaPadraoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Conjunto de alíquotas relacionadas ao ICMS |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="carregarAliquotaPadrao1"></a>
# **carregarAliquotaPadrao1**
> ICMSAliquotaPadraoResponseEntity carregarAliquotaPadrao1(cst)



Busca todas as alíquotas relecionadas a um ICMS, baseada em seu Código de Situação Tributária (CST)

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    String cst = "cst_example"; // String | 
    try {
      ICMSAliquotaPadraoResponseEntity result = apiInstance.carregarAliquotaPadrao1(cst);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#carregarAliquotaPadrao1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cst** | **String**|  |

### Return type

[**ICMSAliquotaPadraoResponseEntity**](ICMSAliquotaPadraoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Conjunto de alíquotas relacionadas ao ICMS |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="consultarChaveNFe"></a>
# **consultarChaveNFe**
> consultarChaveNFe(chaveNfe)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    NfEApi apiInstance = new NfEApi(defaultClient);
    String chaveNfe = "chaveNfe_example"; // String | 
    try {
      apiInstance.consultarChaveNFe(chaveNfe);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#consultarChaveNFe");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chaveNfe** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="consultarNSU"></a>
# **consultarNSU**
> NFeConsultarNSUResponseEntity consultarNSU(tipoPesquisa, nsu)



Realiza Consulta de Documentos através do NSU.

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    NfEApi apiInstance = new NfEApi(defaultClient);
    String tipoPesquisa = "tipoPesquisa_example"; // String | 
    Long nsu = 0lL; // Long | 
    try {
      NFeConsultarNSUResponseEntity result = apiInstance.consultarNSU(tipoPesquisa, nsu);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#consultarNSU");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tipoPesquisa** | **String**|  | [enum: ultimo_nsu, sem_nsu, nsu_especifico]
 **nsu** | **Long**|  | [optional] [default to 0l]

### Return type

[**NFeConsultarNSUResponseEntity**](NFeConsultarNSUResponseEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Resposta do Web Service em relação ao NSU requisitado. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="consultarStatusServico"></a>
# **consultarStatusServico**
> NFeConsultarStatusServicoResponseEntity consultarStatusServico()



Requisita consulta do status do serviço na Sefaz

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    try {
      NFeConsultarStatusServicoResponseEntity result = apiInstance.consultarStatusServico();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#consultarStatusServico");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NFeConsultarStatusServicoResponseEntity**](NFeConsultarStatusServicoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados do status do serviço |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="criar12"></a>
# **criar12**
> criar12(nfeId, pagamentoFormaId, valor, bandeiraId, numeroAutorizacaoOperacao, descricaoMeioPagamento)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    Long nfeId = 56L; // Long | 
    IntegerParam pagamentoFormaId = new IntegerParam(); // IntegerParam | 
    DoubleParam valor = new DoubleParam(); // DoubleParam | 
    IntegerParam bandeiraId = new IntegerParam(); // IntegerParam | 
    String numeroAutorizacaoOperacao = "numeroAutorizacaoOperacao_example"; // String | 
    String descricaoMeioPagamento = "descricaoMeioPagamento_example"; // String | 
    try {
      apiInstance.criar12(nfeId, pagamentoFormaId, valor, bandeiraId, numeroAutorizacaoOperacao, descricaoMeioPagamento);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#criar12");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nfeId** | **Long**|  |
 **pagamentoFormaId** | [**IntegerParam**](IntegerParam.md)|  |
 **valor** | [**DoubleParam**](DoubleParam.md)|  |
 **bandeiraId** | [**IntegerParam**](IntegerParam.md)|  | [optional]
 **numeroAutorizacaoOperacao** | **String**|  | [optional]
 **descricaoMeioPagamento** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="criar13"></a>
# **criar13**
> criar13(nfeId, codigoUfEmitente, dataEmissao, cnpjEmitente, serie, numeroDocumentoFiscal, modelo)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    NfEApi apiInstance = new NfEApi(defaultClient);
    Long nfeId = 56L; // Long | 
    IntegerParam codigoUfEmitente = new IntegerParam(); // IntegerParam | 
    String dataEmissao = "dataEmissao_example"; // String | 
    NumericOnlyParam cnpjEmitente = new NumericOnlyParam(); // NumericOnlyParam | 
    IntegerParam serie = new IntegerParam(); // IntegerParam | 
    LongParam numeroDocumentoFiscal = new LongParam(); // LongParam | 
    Integer modelo = 56; // Integer | 
    try {
      apiInstance.criar13(nfeId, codigoUfEmitente, dataEmissao, cnpjEmitente, serie, numeroDocumentoFiscal, modelo);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#criar13");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nfeId** | **Long**|  |
 **codigoUfEmitente** | [**IntegerParam**](IntegerParam.md)|  |
 **dataEmissao** | **String**|  |
 **cnpjEmitente** | [**NumericOnlyParam**](NumericOnlyParam.md)|  |
 **serie** | [**IntegerParam**](IntegerParam.md)|  |
 **numeroDocumentoFiscal** | [**LongParam**](LongParam.md)|  |
 **modelo** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="deletar17"></a>
# **deletar17**
> deletar17(nfeId, id)



Deleta dados de IPI de um item da NF-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    Long nfeId = 56L; // Long | 
    Long id = 56L; // Long | 
    try {
      apiInstance.deletar17(nfeId, id);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#deletar17");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nfeId** | **Long**|  |
 **id** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**404** | Dados de IPI do Produto {0} não encontrados |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar18"></a>
# **deletar18**
> deletar18(nfeId, id)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    Long nfeId = 56L; // Long | 
    Long id = 56L; // Long | 
    try {
      apiInstance.deletar18(nfeId, id);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#deletar18");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nfeId** | **Long**|  |
 **id** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="deletar19"></a>
# **deletar19**
> deletar19(nfeId, id)



Deleta dados do combustível de um produto

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    Long nfeId = 56L; // Long | 
    Long id = 56L; // Long | 
    try {
      apiInstance.deletar19(nfeId, id);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#deletar19");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nfeId** | **Long**|  |
 **id** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**404** | Dados do Combustível do Produto {0} não encontrados |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="deletar20"></a>
# **deletar20**
> deletar20(nfeId, id)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    NfEApi apiInstance = new NfEApi(defaultClient);
    Long nfeId = 56L; // Long | 
    Long id = 56L; // Long | 
    try {
      apiInstance.deletar20(nfeId, id);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#deletar20");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nfeId** | **Long**|  |
 **id** | **Long**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="deletarDadosCertificado"></a>
# **deletarDadosCertificado**
> deletarDadosCertificado(caminhoBaseCertCli)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    String caminhoBaseCertCli = "caminhoBaseCertCli_example"; // String | 
    try {
      apiInstance.deletarDadosCertificado(caminhoBaseCertCli);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#deletarDadosCertificado");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caminhoBaseCertCli** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="exportarEntradaSaida"></a>
# **exportarEntradaSaida**
> exportarEntradaSaida(nfeId)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    Long nfeId = 56L; // Long | 
    try {
      apiInstance.exportarEntradaSaida(nfeId);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#exportarEntradaSaida");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nfeId** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="listar6"></a>
# **listar6**
> listar6(pagina, chaveNfe, manifestacaoTipoId)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    NfEApi apiInstance = new NfEApi(defaultClient);
    Integer pagina = 1; // Integer | 
    String chaveNfe = "chaveNfe_example"; // String | 
    Integer manifestacaoTipoId = 56; // Integer | 
    try {
      apiInstance.listar6(pagina, chaveNfe, manifestacaoTipoId);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#listar6");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagina** | **Integer**|  | [optional] [default to 1]
 **chaveNfe** | **String**|  | [optional]
 **manifestacaoTipoId** | **Integer**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="listar7"></a>
# **listar7**
> listar7(nfeId)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    Long nfeId = 56L; // Long | 
    try {
      apiInstance.listar7(nfeId);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#listar7");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nfeId** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="listar8"></a>
# **listar8**
> listar8(nfeId)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    NfEApi apiInstance = new NfEApi(defaultClient);
    Long nfeId = 56L; // Long | 
    try {
      apiInstance.listar8(nfeId);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#listar8");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nfeId** | **Long**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="listarEnquadramentoLegalSelect2"></a>
# **listarEnquadramentoLegalSelect2**
> EnquadramentoLegalListResponseEntity listarEnquadramentoLegalSelect2(termo)



Pesquisa código de Enquadramento Legal do IPI

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    String termo = "termo_example"; // String | 
    try {
      EnquadramentoLegalListResponseEntity result = apiInstance.listarEnquadramentoLegalSelect2(termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#listarEnquadramentoLegalSelect2");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  | [optional]

### Return type

[**EnquadramentoLegalListResponseEntity**](EnquadramentoLegalListResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista Código de Enquadramento Legal do IPI |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="listarSelect22"></a>
# **listarSelect22**
> NFeNaturezaOperacaoListResponseEntity listarSelect22(tipoOperacao, identificadorDestino, termo)



Pesquisa Naturezas da Operação baseadas no Tipo de Operação e Destino restrito à NF-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    String tipoOperacao = "tipoOperacao_example"; // String | 
    Integer identificadorDestino = 56; // Integer | Valor do Destino
    String termo = "termo_example"; // String | 
    try {
      NFeNaturezaOperacaoListResponseEntity result = apiInstance.listarSelect22(tipoOperacao, identificadorDestino, termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#listarSelect22");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tipoOperacao** | **String**|  | [enum: Entrada, Saida]
 **identificadorDestino** | **Integer**| Valor do Destino |
 **termo** | **String**|  | [optional]

### Return type

[**NFeNaturezaOperacaoListResponseEntity**](NFeNaturezaOperacaoListResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Naturezas da Operação restrito à NF-e |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="listarSeloControleSelect2"></a>
# **listarSeloControleSelect2**
> SeloControleListResponseEntity listarSeloControleSelect2(termo)



Pesquisa código do selo de controle IPI

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    String termo = "termo_example"; // String | 
    try {
      SeloControleListResponseEntity result = apiInstance.listarSeloControleSelect2(termo);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#listarSeloControleSelect2");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **termo** | **String**|  | [optional]

### Return type

[**SeloControleListResponseEntity**](SeloControleListResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista Código do selo de controle IPI |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="resumoAliquotaPadrao"></a>
# **resumoAliquotaPadrao**
> ICMSResumoAliquotaPadraoResponseEntity resumoAliquotaPadrao(cst, emitenteId)



Requisita resumo de quantos produtos serão alterados caso sejam aplicadas as alíquotas de um determinado ICMS, baseado em seu Código de Situação Tributária (CST).

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    String cst = "cst_example"; // String | 
    Integer emitenteId = 56; // Integer | 
    try {
      ICMSResumoAliquotaPadraoResponseEntity result = apiInstance.resumoAliquotaPadrao(cst, emitenteId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#resumoAliquotaPadrao");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cst** | **String**|  |
 **emitenteId** | **Integer**|  |

### Return type

[**ICMSResumoAliquotaPadraoResponseEntity**](ICMSResumoAliquotaPadraoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Resumo da quantidade de produtos que serão alterados. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar12"></a>
# **salvar12**
> salvar12(numeroNfe, serie, ambiente, cfopVendaPadrao, cfopDevolucaoPadrao, infAdic, icmsTribut, tributSimples, modFretePadrao, ultimoNsu)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    LongParam numeroNfe = new LongParam(); // LongParam | 
    ShortParam serie = new ShortParam(); // ShortParam | 
    LongParam ambiente = new LongParam(); // LongParam | 
    Integer cfopVendaPadrao = 56; // Integer | 
    Integer cfopDevolucaoPadrao = 56; // Integer | 
    String infAdic = "infAdic_example"; // String | 
    ShortParam icmsTribut = new ShortParam(); // ShortParam | 
    DoubleParam tributSimples = new DoubleParam(); // DoubleParam | 
    ShortParam modFretePadrao = new ShortParam(); // ShortParam | 
    Long ultimoNsu = 0lL; // Long | 
    try {
      apiInstance.salvar12(numeroNfe, serie, ambiente, cfopVendaPadrao, cfopDevolucaoPadrao, infAdic, icmsTribut, tributSimples, modFretePadrao, ultimoNsu);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#salvar12");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **numeroNfe** | [**LongParam**](LongParam.md)|  |
 **serie** | [**ShortParam**](ShortParam.md)|  |
 **ambiente** | [**LongParam**](LongParam.md)|  |
 **cfopVendaPadrao** | **Integer**|  |
 **cfopDevolucaoPadrao** | **Integer**|  |
 **infAdic** | **String**|  | [optional]
 **icmsTribut** | [**ShortParam**](ShortParam.md)|  | [optional]
 **tributSimples** | [**DoubleParam**](DoubleParam.md)|  | [optional]
 **modFretePadrao** | [**ShortParam**](ShortParam.md)|  | [optional]
 **ultimoNsu** | **Long**|  | [optional] [default to 0l]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar13"></a>
# **salvar13**
> salvar13(id, nfeId, cProdAnp, ufCons, vPart, qTemp, pGlp, pGnn, pGni)



Salva dados do combustível de um produto

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    Long id = 56L; // Long | 
    Long nfeId = 56L; // Long | 
    Long cProdAnp = 56L; // Long | 
    Integer ufCons = 56; // Integer | 
    Double vPart = 3.4D; // Double | 
    Double qTemp = 3.4D; // Double | 
    Double pGlp = 3.4D; // Double | 
    Double pGnn = 3.4D; // Double | 
    Double pGni = 3.4D; // Double | 
    try {
      apiInstance.salvar13(id, nfeId, cProdAnp, ufCons, vPart, qTemp, pGlp, pGnn, pGni);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#salvar13");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |
 **nfeId** | **Long**|  |
 **cProdAnp** | **Long**|  |
 **ufCons** | **Integer**|  |
 **vPart** | **Double**|  | [optional]
 **qTemp** | **Double**|  | [optional]
 **pGlp** | **Double**|  | [optional]
 **pGnn** | **Double**|  | [optional]
 **pGni** | **Double**|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dados do combustível atualizados com sucesso |  -  |
**201** | Dados do combustível criados com sucesso |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvarAliquotaPadrao"></a>
# **salvarAliquotaPadrao**
> ICMSAliquotaPadraoResponseEntity salvarAliquotaPadrao(cst, emitenteId)



Salva o valor padrão para as alíquotas de uma Situação Tributária. Utilize o método GET deste endpoint para saber quais as alíquotas são válidas para salvar. Valores que não estão relacionados ao CST serão ignorados.

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    String cst = "cst_example"; // String | 
    Integer emitenteId = 56; // Integer | 
    try {
      ICMSAliquotaPadraoResponseEntity result = apiInstance.salvarAliquotaPadrao(cst, emitenteId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#salvarAliquotaPadrao");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cst** | **String**|  |
 **emitenteId** | **Integer**|  |

### Return type

[**ICMSAliquotaPadraoResponseEntity**](ICMSAliquotaPadraoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Conjunto de alíquotas relacionadas ao ICMS |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvarDadosCertificado"></a>
# **salvarDadosCertificado**
> salvarDadosCertificado(parts, preamble)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    NfEApi apiInstance = new NfEApi(defaultClient);
    List<InputPart> parts = new InputPart(); // List<InputPart> | 
    String preamble = "preamble_example"; // String | 
    try {
      apiInstance.salvarDadosCertificado(parts, preamble);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#salvarDadosCertificado");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parts** | [**List&lt;InputPart&gt;**](InputPart.md)|  | [optional]
 **preamble** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

<a name="transmitirESalvar"></a>
# **transmitirESalvar**
> transmitirESalvar(chaveNfe, manifestacaoTipoId, justificativa)



### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.NfEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");

    NfEApi apiInstance = new NfEApi(defaultClient);
    String chaveNfe = "chaveNfe_example"; // String | 
    Integer manifestacaoTipoId = 56; // Integer | 
    String justificativa = "justificativa_example"; // String | 
    try {
      apiInstance.transmitirESalvar(chaveNfe, manifestacaoTipoId, justificativa);
    } catch (ApiException e) {
      System.err.println("Exception when calling NfEApi#transmitirESalvar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chaveNfe** | **String**|  |
 **manifestacaoTipoId** | **Integer**|  |
 **justificativa** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

