

# VendaResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**venda** | [**VendaCompleta**](VendaCompleta.md) |  |  [optional]
**cfe** | [**CFeCompleto**](CFeCompleto.md) |  |  [optional]
**dav** | **String** |  |  [optional]



