

# Info

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**versaoAplicativo** | **String** |  |  [optional]
**uf** | **String** |  |  [optional]
**motivo** | **String** |  |  [optional]
**evento** | **String** |  |  [optional]
**ambiente** | **String** |  |  [optional]
**chave** | **String** |  |  [optional]
**dataHoraEvento** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**tipoEvento** | **String** |  |  [optional]
**numProtocolo** | **String** |  |  [optional]
**status** | **String** |  |  [optional]
**numSequencia** | **String** |  |  [optional]



