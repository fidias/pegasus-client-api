

# VFPe

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**vfpeEnviarPagamento** | [**VfpeEnviarPagamento**](VfpeEnviarPagamento.md) |  | 
**vfpeRetEnviarPagamento** | [**VfpeRetEnviarPagamento**](VfpeRetEnviarPagamento.md) |  | 
**vfpeRetVerificarStatusValidador** | [**VfpeRetVerificarStatusValidador**](VfpeRetVerificarStatusValidador.md) |  | 
**vfpeRetRespostaFiscal** | [**VfpeRetRespostaFiscal**](VfpeRetRespostaFiscal.md) |  | 



