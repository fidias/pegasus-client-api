

# InlineObject22

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numeroCte** | [**LongParam**](LongParam.md) |  | 
**ambienteCte** | [**LongParam**](LongParam.md) |  | 
**localEmissaoUf** | [**LongParam**](LongParam.md) |  | 
**localEmissaoMun** | [**LongParam**](LongParam.md) |  | 
**cfopPadrao** | [**LongParam**](LongParam.md) |  | 



