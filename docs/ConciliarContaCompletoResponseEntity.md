

# ConciliarContaCompletoResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**dados** | **String** |  |  [optional]
**itens** | **String** |  |  [optional]
**eventos** | **String** |  |  [optional]
**listaLivroCaixa** | **String** |  |  [optional]



