

# InlineObject46

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**serieMdfe** | [**ShortParam**](ShortParam.md) |  | 
**numeroMdfe** | [**LongParam**](LongParam.md) |  | 
**ambienteMdfe** | [**LongParam**](LongParam.md) |  | 
**localEncerramentoUf** | [**LongParam**](LongParam.md) |  | 
**localEncerramentoMun** | [**LongParam**](LongParam.md) |  | 
**tipoCargaPadrao** | [**LongParam**](LongParam.md) |  |  [optional]



