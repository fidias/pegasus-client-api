

# CFe

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clienteVendaId** | **Long** |  | 
**versaoDadosEnt** | **String** |  | 
**cnpjSoftwareHouse** | **Long** |  | 
**signAc** | **String** |  | 
**numeroCaixa** | **Integer** |  | 
**retEnviarDadosVenda** | [**RetEnviarDadosVenda**](RetEnviarDadosVenda.md) |  | 
**retCancelarUltimaVenda** | [**RetCancelarUltimaVenda**](RetCancelarUltimaVenda.md) |  |  [optional]



