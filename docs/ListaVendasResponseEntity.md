

# ListaVendasResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vendas** | [**List&lt;Venda&gt;**](Venda.md) |  |  [optional]



