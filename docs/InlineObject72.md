

# InlineObject72

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numeroLoteRps** | **Long** |  | 
**numeroRps** | **Long** |  | 
**serieRps** | **String** |  | 
**tipoRps** | [**TipoRpsEnum**](#TipoRpsEnum) |  | 
**regimeEspecialTributacao** | [**RegimeEspecialTributacaoEnum**](#RegimeEspecialTributacaoEnum) |  | 
**naturezaOperacao** | [**NaturezaOperacaoEnum**](#NaturezaOperacaoEnum) |  | 
**codigoServicoPadrao** | **Long** |  | 
**aliquotaServicoPadrao** | **Double** |  | 
**codigoCnae** | **Long** |  | 
**ambienteNfse** | [**AmbienteNfseEnum**](#AmbienteNfseEnum) |  | 



## Enum: TipoRpsEnum

Name | Value
---- | -----
RECIBOPROVISORIOSERVICOS | &quot;ReciboProvisorioServicos&quot;
RPSNOTAFISCALCONJUGADA | &quot;RpsNotaFiscalConjugada&quot;
CUPOM | &quot;Cupom&quot;



## Enum: RegimeEspecialTributacaoEnum

Name | Value
---- | -----
NAOINFORMADO | &quot;NaoInformado&quot;
MICROEMPRESAMUNICIPAL | &quot;MicroempresaMunicipal&quot;
ESTIMATIVA | &quot;Estimativa&quot;
SOCIEDADEPROFISSIONAIS | &quot;SociedadeProfissionais&quot;
COOPERATIVA | &quot;Cooperativa&quot;
MEI | &quot;MEI&quot;
MEEPP | &quot;MEEPP&quot;



## Enum: NaturezaOperacaoEnum

Name | Value
---- | -----
TRIBUTACAOMUNICIPIO | &quot;TributacaoMunicipio&quot;
TRIBUTACAOFORAMUNICIPIO | &quot;TributacaoForaMunicipio&quot;
ISENCAO | &quot;Isencao&quot;
IMUNE | &quot;Imune&quot;
EXIGIBILIDADESUSPENSADECISAOJUDICIAL | &quot;ExigibilidadeSuspensaDecisaoJudicial&quot;
EXIGIBILIDADESUSPENSAPROCEDIMENTOADMINISTRATIVO | &quot;ExigibilidadeSuspensaProcedimentoAdministrativo&quot;



## Enum: AmbienteNfseEnum

Name | Value
---- | -----
PRODUCAO | &quot;Producao&quot;
HOMOLOGACAO | &quot;Homologacao&quot;



