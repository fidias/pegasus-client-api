

# VendaCompleta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  | 
**identificadorUnico** | **String** |  |  [optional]
**dataVenda** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**valorRecebido** | **Double** |  | 
**troco** | **Double** |  | 
**totalLiquido** | **Double** |  | 
**totalBruto** | **Double** |  | 
**acrescimoD** | **Double** |  | 
**descontoD** | **Double** |  | 
**cliente** | [**Cliente**](Cliente.md) |  |  [optional]
**usuario** | [**Usuario**](Usuario.md) |  | 
**idcaixa** | **Long** |  | 
**clienteVendaId** | **Long** |  | 
**comissionario** | [**Comissionario**](Comissionario.md) |  |  [optional]
**desabilitarImportarNfe** | **Boolean** |  | 
**nfeId** | **Long** |  |  [optional]
**cancelada** | **Boolean** |  | 
**itensVenda** | [**List&lt;ItensVenda&gt;**](ItensVenda.md) |  | 
**formasPagamento** | [**List&lt;FormasPagamento&gt;**](FormasPagamento.md) |  | 
**ignorarQtdDescPromo** | **Boolean** |  | 



