

# InlineObject39

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**objetoId** | **Long** |  |  [optional]
**placa** | **String** |  | 
**modelo** | **String** |  |  [optional]
**marcaId** | **Integer** |  | 
**anoFabricacao** | **String** |  |  [optional]
**renavam** | **String** |  |  [optional]
**infoExtra** | **String** |  |  [optional]



