

# InlineObject53

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tipoCargaId** | **Integer** |  | 
**produtoPredominante** | **String** |  | 
**gtin** | **String** |  |  [optional]
**ncmId** | **Long** |  |  [optional]
**localCarregaCep** | **String** |  |  [optional]
**localDescarregaCep** | **String** |  |  [optional]



