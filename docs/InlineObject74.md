

# InlineObject74

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**titulo** | **String** |  | 
**logradouro** | **String** |  | 
**numero** | **String** |  | 
**pontoReferencia** | **String** |  |  [optional]
**complemento** | **String** |  |  [optional]
**cep** | [**NumericOnlyParam**](NumericOnlyParam.md) |  |  [optional]
**bairroId** | [**LongParam**](LongParam.md) |  | 
**bairro** | **String** |  |  [optional]
**municipioId** | [**LongParam**](LongParam.md) |  |  [optional]
**municipio** | **String** |  |  [optional]
**estadoId** | [**IntegerParam**](IntegerParam.md) |  |  [optional]



