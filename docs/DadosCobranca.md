

# DadosCobranca

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**situacao** | **String** |  | 
**token** | **String** |  | 
**urlCobranca** | **String** |  |  [optional]
**vencimento** | **String** |  | 
**atualizadoEm** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**tokenLink** | **String** |  | 
**diasDiferenca** | **Long** |  | 
**isAtualizado** | **Boolean** |  | 



