

# InputPart

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contentTypeFromMessage** | **Boolean** |  |  [optional]
**mediaType** | [**InputPartMediaType**](InputPartMediaType.md) |  |  [optional]
**bodyAsString** | **String** |  |  [optional]
**headers** | [**Map&lt;String, List&lt;String&gt;&gt;**](List.md) |  |  [optional]



