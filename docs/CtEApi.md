# CtEApi

All URIs are relative to *http://localhost:8081/pegasus*

Method | HTTP request | Description
------------- | ------------- | -------------
[**carregar4**](CtEApi.md#carregar4) | **GET** /api/cte/configuracao | 
[**download**](CtEApi.md#download) | **GET** /api/cte/evento/download/{id} | 
[**gerarFaturamento**](CtEApi.md#gerarFaturamento) | **POST** /api/cte/{cte_id}/faturamento | 
[**listarFaturamento**](CtEApi.md#listarFaturamento) | **GET** /api/cte/{cte_id}/faturamento | 
[**listarPorCTeId**](CtEApi.md#listarPorCTeId) | **GET** /api/cte/evento/listar/{cte_id} | 
[**salvar3**](CtEApi.md#salvar3) | **POST** /api/cte/configuracao | 


<a name="carregar4"></a>
# **carregar4**
> CTeConfiguracaoResponseEntity carregar4()



Retorna conjunto de configurações relacionadas ao CT-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CtEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CtEApi apiInstance = new CtEApi(defaultClient);
    try {
      CTeConfiguracaoResponseEntity result = apiInstance.carregar4();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CtEApi#carregar4");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CTeConfiguracaoResponseEntity**](CTeConfiguracaoResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Conjunto de configurações do CT-e |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="download"></a>
# **download**
> download(id)



Retorna arquivo XML do evento de um CT-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CtEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CtEApi apiInstance = new CtEApi(defaultClient);
    Long id = 56L; // Long | 
    try {
      apiInstance.download(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling CtEApi#download");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/xml, */*

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Arquivo XML do evento |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="gerarFaturamento"></a>
# **gerarFaturamento**
> CTeFaturamentoListResponseEntity gerarFaturamento(cteId, livroCaixaFormaPagamentoId, vencimento, valor, contaId)



Solicita geração de um faturamento de CT-e, definindo um conjunto de vencimentos e seus respectivos valores

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CtEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CtEApi apiInstance = new CtEApi(defaultClient);
    Integer cteId = 56; // Integer | 
    Integer livroCaixaFormaPagamentoId = 56; // Integer | 
    List<String> vencimento = "vencimento_example"; // List<String> | 
    List<DoubleParam> valor = new DoubleParam(); // List<DoubleParam> | 
    Integer contaId = 56; // Integer | 
    try {
      CTeFaturamentoListResponseEntity result = apiInstance.gerarFaturamento(cteId, livroCaixaFormaPagamentoId, vencimento, valor, contaId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CtEApi#gerarFaturamento");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cteId** | **Integer**|  |
 **livroCaixaFormaPagamentoId** | **Integer**|  |
 **vencimento** | [**List&lt;String&gt;**](String.md)|  |
 **valor** | [**List&lt;DoubleParam&gt;**](DoubleParam.md)|  |
 **contaId** | **Integer**|  | [optional]

### Return type

[**CTeFaturamentoListResponseEntity**](CTeFaturamentoListResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de faturamentos gerados |  -  |
**412** | Pré-condição não satisfeita  * Faturamento para este CT-e já foi gerado. * Faturamento só pode ser gerado para CT-e autorizado. |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="listarFaturamento"></a>
# **listarFaturamento**
> CTeFaturamentoListResponseEntity listarFaturamento(cteId)



Solicita lista de faturamentos de um CT-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CtEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CtEApi apiInstance = new CtEApi(defaultClient);
    Long cteId = 56L; // Long | 
    try {
      CTeFaturamentoListResponseEntity result = apiInstance.listarFaturamento(cteId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CtEApi#listarFaturamento");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cteId** | **Long**|  |

### Return type

[**CTeFaturamentoListResponseEntity**](CTeFaturamentoListResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de faturamentos do CT-e |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="listarPorCTeId"></a>
# **listarPorCTeId**
> CTeEventoListarResponseEntity listarPorCTeId(cteId)



Retorna lista de eventos de um CT-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CtEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CtEApi apiInstance = new CtEApi(defaultClient);
    Long cteId = 56L; // Long | 
    try {
      CTeEventoListarResponseEntity result = apiInstance.listarPorCTeId(cteId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling CtEApi#listarPorCTeId");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cteId** | **Long**|  |

### Return type

[**CTeEventoListarResponseEntity**](CTeEventoListarResponseEntity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Lista de eventos de um CT-e |  -  |
**500** | Erro desconhecido no servidor |  -  |

<a name="salvar3"></a>
# **salvar3**
> salvar3(numeroCte, ambienteCte, localEmissaoUf, localEmissaoMun, cfopPadrao)



Salva conjunto de configurações do CT-e

### Example
```java
// Import classes:
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.auth.*;
import br.com.fidias.pegasus_client.models.*;
import br.com.fidias.pegasus_client.api.CtEApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8081/pegasus");
    
    // Configure HTTP basic authorization: bearerAuth
    HttpBasicAuth bearerAuth = (HttpBasicAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setUsername("YOUR USERNAME");
    bearerAuth.setPassword("YOUR PASSWORD");

    CtEApi apiInstance = new CtEApi(defaultClient);
    LongParam numeroCte = new LongParam(); // LongParam | 
    LongParam ambienteCte = new LongParam(); // LongParam | 
    LongParam localEmissaoUf = new LongParam(); // LongParam | 
    LongParam localEmissaoMun = new LongParam(); // LongParam | 
    LongParam cfopPadrao = new LongParam(); // LongParam | 
    try {
      apiInstance.salvar3(numeroCte, ambienteCte, localEmissaoUf, localEmissaoMun, cfopPadrao);
    } catch (ApiException e) {
      System.err.println("Exception when calling CtEApi#salvar3");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **numeroCte** | [**LongParam**](LongParam.md)|  |
 **ambienteCte** | [**LongParam**](LongParam.md)|  |
 **localEmissaoUf** | [**LongParam**](LongParam.md)|  |
 **localEmissaoMun** | [**LongParam**](LongParam.md)|  |
 **cfopPadrao** | [**LongParam**](LongParam.md)|  |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Sem conteúdo de retorno |  -  |
**500** | Erro desconhecido no servidor |  -  |

