

# AlterarEmLoteListaResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;ProdutoAlterarEmLoteItemLista&gt;**](ProdutoAlterarEmLoteItemLista.md) |  |  [optional]



