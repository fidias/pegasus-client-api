

# InlineObject70

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numeroRps** | **Long** |  | 
**dataEmissao** | [**OffsetDateTime**](OffsetDateTime.md) | Data no formato DD/MM/AAAA HH:MM:SS | 
**discriminacao** | **String** |  | 
**codigoServicoId** | **Integer** |  | 
**aliquotaServicos** | **Double** |  | 
**valorServicos** | **Double** |  | 
**valorDeducoes** | **Double** |  |  [optional]
**valorPis** | **Double** |  |  [optional]
**valorCofins** | **Double** |  |  [optional]
**valorInss** | **Double** |  |  [optional]
**valorCsll** | **Double** |  |  [optional]
**valorIr** | **Double** |  |  [optional]
**outrasRetencoes** | **Double** |  |  [optional]
**valorIssRetido** | **Double** |  |  [optional]
**issRetido** | **Boolean** |  |  [optional]
**tomadorServicoId** | **Integer** |  | 
**naturezaOperacaoId** | **Integer** |  | 
**municipioPrestacaoServicoId** | **Integer** |  | 



