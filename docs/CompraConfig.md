

# CompraConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tipoTributacaoIcms** | [**TipoTributacaoIcms**](TipoTributacaoIcms.md) |  | 
**tipoRecolhimentoIcms** | [**TipoRecolhimentoIcms**](TipoRecolhimentoIcms.md) |  | 
**tipoSubstituicao** | [**TipoSubstituicao**](TipoSubstituicao.md) |  | 
**tipoTributacaoIpi** | [**TipoTributacaoIpi**](TipoTributacaoIpi.md) |  | 
**balanceteOperacaoFinanceiraEntrada** | [**BalanceteOperacaoFinanceiraEntrada**](BalanceteOperacaoFinanceiraEntrada.md) |  | 
**balanceteOperacaoFinanceiraSaida** | [**BalanceteOperacaoFinanceiraSaida**](BalanceteOperacaoFinanceiraSaida.md) |  | 
**devolucaoOperacaoFinanceiraPadrao** | [**DevolucaoOperacaoFinanceiraPadrao**](DevolucaoOperacaoFinanceiraPadrao.md) |  | 
**lucroPadrao** | [**LucroPadrao**](LucroPadrao.md) |  | 
**custoExtraPercPadrao** | [**CustoExtraPercPadrao**](CustoExtraPercPadrao.md) |  | 
**margemLucroPercPadrao** | [**MargemLucroPercPadrao**](MargemLucroPercPadrao.md) |  | 



