

# InlineObject12

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dataVencimento** | [**OffsetDateTime**](OffsetDateTime.md) | Data no formato DD/MM/AAAA | 
**numeroParcela** | **String** |  | 
**valorParcela** | **Double** |  | 



