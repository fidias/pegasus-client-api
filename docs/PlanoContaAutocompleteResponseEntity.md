

# PlanoContaAutocompleteResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lista** | [**List&lt;PlanoContaAutocompleteItem&gt;**](PlanoContaAutocompleteItem.md) |  |  [optional]



