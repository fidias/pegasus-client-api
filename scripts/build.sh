#!/usr/bin/env bash

set -o nounset
set -o errexit
set -o pipefail

mv openapi.json{,~}
rm -R tmp-build
mkdir tmp-build
curl -O http://localhost:8081/pegasus/api/openapi.json

npx openapi-generator generate \
    --input-spec openapi.json \
    --generator-name java \
    --group-id br.com.fidias \
    --artifact-id pegasus-client-api \
    --output tmp-build \
    --api-package br.com.fidias.pegasus_client.api \
    --model-package br.com.fidias.pegasus_client.model \
    --config api-config.json \
    --artifact-version 1.0.0 \
    --skip-validate-spec \
    --minimal-update

cp tmp-build/README.md README.md
cp tmp-build/docs/*.md docs
cp -r tmp-build/src/** src

git apply ApiClient.java.patch
