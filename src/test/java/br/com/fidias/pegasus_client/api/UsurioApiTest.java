/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.api;

import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.model.ExceptionEntity;
import br.com.fidias.pegasus_client.model.UsuarioAutocompleteResponseEntity;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for UsurioApi
 */
@Ignore
public class UsurioApiTest {

    private final UsurioApi api = new UsurioApi();

    
    /**
     * 
     *
     * Solicita lista de Usuários que contenham o termo solicitado no login/apelido
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void autocomplete16Test() throws ApiException {
        String termo = null;
        UsuarioAutocompleteResponseEntity response = api.autocomplete16(termo);

        // TODO: test validations
    }
    
}
