/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.api;

import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.model.DefinirClienteResponseEntity;
import br.com.fidias.pegasus_client.model.ExceptionEntity;
import br.com.fidias.pegasus_client.model.ListaVendasResponseEntity;
import br.com.fidias.pegasus_client.model.NFSeImportarVendaResponseEntity;
import br.com.fidias.pegasus_client.model.NFeResponseEntity;
import java.time.OffsetDateTime;
import br.com.fidias.pegasus_client.model.VendaBalcaoCompletaEntity;
import br.com.fidias.pegasus_client.model.VendaConflitoSincronizarResponseEntity;
import br.com.fidias.pegasus_client.model.VendaResponseEntity;
import br.com.fidias.pegasus_client.model.VendaSincronizarResponseEntity;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for CaixaApi
 */
@Ignore
public class CaixaApiTest {

    private final CaixaApi api = new CaixaApi();

    
    /**
     * 
     *
     * Define um cliente para a venda
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void definirClienteTest() throws ApiException {
        Long vendaId = null;
        Integer clienteId = null;
        DefinirClienteResponseEntity response = api.definirCliente(vendaId, clienteId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Importa uma Venda para NFS-e
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void importarNFSeTest() throws ApiException {
        Long vendaId = null;
        NFSeImportarVendaResponseEntity response = api.importarNFSe(vendaId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Importa uma Venda para NF-e
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void importarNFeTest() throws ApiException {
        Long vendaId = null;
        NFeResponseEntity response = api.importarNFe(vendaId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Recupera uma venda através do ID
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void lerTest() throws ApiException {
        Long vendaId = null;
        VendaResponseEntity response = api.ler(vendaId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Solicita lista de vendas de um caixa, em uma determinada data
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void listarTest() throws ApiException {
        Integer caixaId = null;
        OffsetDateTime dataVenda = null;
        ListaVendasResponseEntity response = api.listar(caixaId, dataVenda);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Solicita a sincronização de uma Venda
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void vendaSincronizarTest() throws ApiException {
        VendaBalcaoCompletaEntity vendaBalcaoCompletaEntity = null;
        VendaSincronizarResponseEntity response = api.vendaSincronizar(vendaBalcaoCompletaEntity);

        // TODO: test validations
    }
    
}
