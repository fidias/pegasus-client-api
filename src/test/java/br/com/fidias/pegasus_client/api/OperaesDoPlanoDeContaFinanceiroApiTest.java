/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.api;

import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.model.ExceptionEntity;
import br.com.fidias.pegasus_client.model.PlanoContaOperacaoIndexResponseEntity;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for OperaesDoPlanoDeContaFinanceiroApi
 */
@Ignore
public class OperaesDoPlanoDeContaFinanceiroApiTest {

    private final OperaesDoPlanoDeContaFinanceiroApi api = new OperaesDoPlanoDeContaFinanceiroApi();

    
    /**
     * 
     *
     * Habilita Operação para Plano de Conta Financeiro
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void criar19Test() throws ApiException {
        Long id = null;
        Long operacaoId = null;
        PlanoContaOperacaoIndexResponseEntity response = api.criar19(id, operacaoId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Desabilita Operação para Plano de Conta Financeiro
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void deletar25Test() throws ApiException {
        Long id = null;
        Long operacaoId = null;
        PlanoContaOperacaoIndexResponseEntity response = api.deletar25(id, operacaoId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Solicita lista de Operações de um Plano de Conta Financeiro
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void index11Test() throws ApiException {
        Long id = null;
        PlanoContaOperacaoIndexResponseEntity response = api.index11(id);

        // TODO: test validations
    }
    
}
