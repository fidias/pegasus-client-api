/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.api;

import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.model.DoubleParam;
import br.com.fidias.pegasus_client.model.ExceptionEntity;
import br.com.fidias.pegasus_client.model.IntegerParam;
import br.com.fidias.pegasus_client.model.TaxaListarResponseEntity;
import br.com.fidias.pegasus_client.model.TaxaResponseEntity;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for TaxaApi
 */
@Ignore
public class TaxaApiTest {

    private final TaxaApi api = new TaxaApi();

    
    /**
     * 
     *
     * Requisita atualização de taxas do POS
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void atualizar1Test() throws ApiException {
        Integer taxaId = null;
        String serialPos = null;
        DoubleParam taxaP = null;
        IntegerParam numParcelasIni = null;
        IntegerParam numParcelasFim = null;
        TaxaListarResponseEntity response = api.atualizar1(taxaId, serialPos, taxaP, numParcelasIni, numParcelasFim);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Requisita criação de taxa do POS
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void cadastrarTest() throws ApiException {
        String serialPos = null;
        DoubleParam taxaP = null;
        IntegerParam numParcelasIni = null;
        IntegerParam numParcelasFim = null;
        TaxaListarResponseEntity response = api.cadastrar(serialPos, taxaP, numParcelasIni, numParcelasFim);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Requisita remoção de taxa de um serial POS
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void deletar2Test() throws ApiException {
        String serialPos = null;
        Integer taxaId = null;
        TaxaListarResponseEntity response = api.deletar2(serialPos, taxaId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Solicita uma taxa de um POS
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void get2Test() throws ApiException {
        String serialPos = null;
        Integer taxaId = null;
        TaxaResponseEntity response = api.get2(serialPos, taxaId);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Solicita lista de taxas de um POS
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void listar2Test() throws ApiException {
        String serialPos = null;
        TaxaListarResponseEntity response = api.listar2(serialPos);

        // TODO: test validations
    }
    
}
