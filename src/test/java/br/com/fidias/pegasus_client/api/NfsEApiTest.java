/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.api;

import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.model.ErrorHandlerEntity;
import br.com.fidias.pegasus_client.model.ExceptionEntity;
import br.com.fidias.pegasus_client.model.NFSeCompletoResponseEntity;
import br.com.fidias.pegasus_client.model.NFSeConfiguracaoResponseEntity;
import br.com.fidias.pegasus_client.model.NFSeConsultarLoteResponseEntity;
import br.com.fidias.pegasus_client.model.NFSeEnviarLoteResponseEntity;
import br.com.fidias.pegasus_client.model.NFSeIndexResponseEntity;
import br.com.fidias.pegasus_client.model.NFSeResponseEntity;
import br.com.fidias.pegasus_client.model.NFSeXmlResponseEntity;
import java.time.OffsetDateTime;
import br.com.fidias.pegasus_client.model.WarningEntity;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for NfsEApi
 */
@Ignore
public class NfsEApiTest {

    private final NfsEApi api = new NfsEApi();

    
    /**
     * 
     *
     * Requisita arquivar um NFS-e
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void arquivar1Test() throws ApiException {
        Long id = null;
        api.arquivar1(id);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Requisita atualização de um NFS-e
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void atualizar10Test() throws ApiException {
        Long id = null;
        Long numeroRps = null;
        OffsetDateTime dataEmissao = null;
        String discriminacao = null;
        Integer codigoServicoId = null;
        Double aliquotaServicos = null;
        Double valorServicos = null;
        Integer tomadorServicoId = null;
        Integer naturezaOperacaoId = null;
        Integer municipioPrestacaoServicoId = null;
        Double valorDeducoes = null;
        Double valorPis = null;
        Double valorCofins = null;
        Double valorInss = null;
        Double valorCsll = null;
        Double valorIr = null;
        Double outrasRetencoes = null;
        Double valorIssRetido = null;
        Boolean issRetido = null;
        NFSeCompletoResponseEntity response = api.atualizar10(id, numeroRps, dataEmissao, discriminacao, codigoServicoId, aliquotaServicos, valorServicos, tomadorServicoId, naturezaOperacaoId, municipioPrestacaoServicoId, valorDeducoes, valorPis, valorCofins, valorInss, valorCsll, valorIr, outrasRetencoes, valorIssRetido, issRetido);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Retorna conjunto de configurações relacionadas ao NFS-e
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void carregar11Test() throws ApiException {
        NFSeConfiguracaoResponseEntity response = api.carregar11();

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void consultarLoteRpsTest() throws ApiException {
        Long id = null;
        NFSeConsultarLoteResponseEntity response = api.consultarLoteRps(id);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Cria uma NFS-e
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void criar14Test() throws ApiException {
        Long numeroRps = null;
        OffsetDateTime dataEmissao = null;
        String discriminacao = null;
        Integer codigoServicoId = null;
        Double aliquotaServicos = null;
        Double valorServicos = null;
        Integer tomadorServicoId = null;
        Integer naturezaOperacaoId = null;
        Integer municipioPrestacaoServicoId = null;
        Double valorDeducoes = null;
        Double valorPis = null;
        Double valorCofins = null;
        Double valorInss = null;
        Double valorCsll = null;
        Double valorIr = null;
        Double outrasRetencoes = null;
        Double valorIssRetido = null;
        Boolean issRetido = null;
        NFSeResponseEntity response = api.criar14(numeroRps, dataEmissao, discriminacao, codigoServicoId, aliquotaServicos, valorServicos, tomadorServicoId, naturezaOperacaoId, municipioPrestacaoServicoId, valorDeducoes, valorPis, valorCofins, valorInss, valorCsll, valorIr, outrasRetencoes, valorIssRetido, issRetido);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void enviarLoteRpsTest() throws ApiException {
        Long id = null;
        NFSeEnviarLoteResponseEntity response = api.enviarLoteRps(id);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Solicita um NFS-e
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void get7Test() throws ApiException {
        Long id = null;
        NFSeCompletoResponseEntity response = api.get7(id);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Solicita o xml de um NFS-e
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getXmlTest() throws ApiException {
        Long id = null;
        NFSeXmlResponseEntity response = api.getXml(id);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Solicita lista de NFS-e, com opção de busca
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void index10Test() throws ApiException {
        Long tomadorServicoId = null;
        OffsetDateTime dataEmissaoInicial = null;
        OffsetDateTime dataEmissaoFinal = null;
        String statusInternoId = null;
        Integer pagina = null;
        NFSeIndexResponseEntity response = api.index10(tomadorServicoId, dataEmissaoInicial, dataEmissaoFinal, statusInternoId, pagina);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * Salva conjunto de configurações do NFS-e
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void salvar14Test() throws ApiException {
        Long numeroLoteRps = null;
        Long numeroRps = null;
        String serieRps = null;
        String tipoRps = null;
        String regimeEspecialTributacao = null;
        String naturezaOperacao = null;
        Long codigoServicoPadrao = null;
        Double aliquotaServicoPadrao = null;
        Long codigoCnae = null;
        String ambienteNfse = null;
        NFSeConfiguracaoResponseEntity response = api.salvar14(numeroLoteRps, numeroRps, serieRps, tipoRps, regimeEspecialTributacao, naturezaOperacao, codigoServicoPadrao, aliquotaServicoPadrao, codigoCnae, ambienteNfse);

        // TODO: test validations
    }
    
}
