/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import br.com.fidias.pegasus_client.model.IntegerParam;
import br.com.fidias.pegasus_client.model.LongParam;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for InlineObject49
 */
public class InlineObject49Test {
    private final InlineObject49 model = new InlineObject49();

    /**
     * Model tests for InlineObject49
     */
    @Test
    public void testInlineObject49() {
        // TODO: test InlineObject49
    }

    /**
     * Test the property 'dataEncerramento'
     */
    @Test
    public void dataEncerramentoTest() {
        // TODO: test dataEncerramento
    }

    /**
     * Test the property 'dataHoraEvento'
     */
    @Test
    public void dataHoraEventoTest() {
        // TODO: test dataHoraEvento
    }

    /**
     * Test the property 'codigoUf'
     */
    @Test
    public void codigoUfTest() {
        // TODO: test codigoUf
    }

    /**
     * Test the property 'codigoMunicipio'
     */
    @Test
    public void codigoMunicipioTest() {
        // TODO: test codigoMunicipio
    }

}
