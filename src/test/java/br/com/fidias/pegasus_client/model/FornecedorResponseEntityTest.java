/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import br.com.fidias.pegasus_client.model.Fornecedor;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for FornecedorResponseEntity
 */
public class FornecedorResponseEntityTest {
    private final FornecedorResponseEntity model = new FornecedorResponseEntity();

    /**
     * Model tests for FornecedorResponseEntity
     */
    @Test
    public void testFornecedorResponseEntity() {
        // TODO: test FornecedorResponseEntity
    }

    /**
     * Test the property 'fornecedor'
     */
    @Test
    public void fornecedorTest() {
        // TODO: test fornecedor
    }

}
