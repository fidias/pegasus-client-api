/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import br.com.fidias.pegasus_client.model.IntegerParam;
import br.com.fidias.pegasus_client.model.LongParam;
import br.com.fidias.pegasus_client.model.NumericOnlyParam;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for InlineObject69
 */
public class InlineObject69Test {
    private final InlineObject69 model = new InlineObject69();

    /**
     * Model tests for InlineObject69
     */
    @Test
    public void testInlineObject69() {
        // TODO: test InlineObject69
    }

    /**
     * Test the property 'codigoUfEmitente'
     */
    @Test
    public void codigoUfEmitenteTest() {
        // TODO: test codigoUfEmitente
    }

    /**
     * Test the property 'dataEmissao'
     */
    @Test
    public void dataEmissaoTest() {
        // TODO: test dataEmissao
    }

    /**
     * Test the property 'cnpjEmitente'
     */
    @Test
    public void cnpjEmitenteTest() {
        // TODO: test cnpjEmitente
    }

    /**
     * Test the property 'serie'
     */
    @Test
    public void serieTest() {
        // TODO: test serie
    }

    /**
     * Test the property 'numeroDocumentoFiscal'
     */
    @Test
    public void numeroDocumentoFiscalTest() {
        // TODO: test numeroDocumentoFiscal
    }

    /**
     * Test the property 'modelo'
     */
    @Test
    public void modeloTest() {
        // TODO: test modelo
    }

}
