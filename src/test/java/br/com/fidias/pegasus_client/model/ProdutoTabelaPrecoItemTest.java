/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for ProdutoTabelaPrecoItem
 */
public class ProdutoTabelaPrecoItemTest {
    private final ProdutoTabelaPrecoItem model = new ProdutoTabelaPrecoItem();

    /**
     * Model tests for ProdutoTabelaPrecoItem
     */
    @Test
    public void testProdutoTabelaPrecoItem() {
        // TODO: test ProdutoTabelaPrecoItem
    }

    /**
     * Test the property 'produtoTabelaPrecoId'
     */
    @Test
    public void produtoTabelaPrecoIdTest() {
        // TODO: test produtoTabelaPrecoId
    }

    /**
     * Test the property 'nome'
     */
    @Test
    public void nomeTest() {
        // TODO: test nome
    }

    /**
     * Test the property 'produtoId'
     */
    @Test
    public void produtoIdTest() {
        // TODO: test produtoId
    }

    /**
     * Test the property 'margemP'
     */
    @Test
    public void margemPTest() {
        // TODO: test margemP
    }

    /**
     * Test the property 'preco'
     */
    @Test
    public void precoTest() {
        // TODO: test preco
    }

    /**
     * Test the property 'itemId'
     */
    @Test
    public void itemIdTest() {
        // TODO: test itemId
    }

}
