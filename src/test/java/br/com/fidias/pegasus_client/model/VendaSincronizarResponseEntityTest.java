/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import br.com.fidias.pegasus_client.model.ResponseSincronizarFormaPagamento;
import br.com.fidias.pegasus_client.model.ResponseSincronizarItem;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for VendaSincronizarResponseEntity
 */
public class VendaSincronizarResponseEntityTest {
    private final VendaSincronizarResponseEntity model = new VendaSincronizarResponseEntity();

    /**
     * Model tests for VendaSincronizarResponseEntity
     */
    @Test
    public void testVendaSincronizarResponseEntity() {
        // TODO: test VendaSincronizarResponseEntity
    }

    /**
     * Test the property 'identificadorUnico'
     */
    @Test
    public void identificadorUnicoTest() {
        // TODO: test identificadorUnico
    }

    /**
     * Test the property 'idVendaBalcao'
     */
    @Test
    public void idVendaBalcaoTest() {
        // TODO: test idVendaBalcao
    }

    /**
     * Test the property 'clienteVendaId'
     */
    @Test
    public void clienteVendaIdTest() {
        // TODO: test clienteVendaId
    }

    /**
     * Test the property 'caixaId'
     */
    @Test
    public void caixaIdTest() {
        // TODO: test caixaId
    }

    /**
     * Test the property 'itens'
     */
    @Test
    public void itensTest() {
        // TODO: test itens
    }

    /**
     * Test the property 'formas'
     */
    @Test
    public void formasTest() {
        // TODO: test formas
    }

}
