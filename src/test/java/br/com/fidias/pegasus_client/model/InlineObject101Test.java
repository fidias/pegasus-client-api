/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for InlineObject101
 */
public class InlineObject101Test {
    private final InlineObject101 model = new InlineObject101();

    /**
     * Model tests for InlineObject101
     */
    @Test
    public void testInlineObject101() {
        // TODO: test InlineObject101
    }

    /**
     * Test the property 'jsonCofins'
     */
    @Test
    public void jsonCofinsTest() {
        // TODO: test jsonCofins
    }

}
