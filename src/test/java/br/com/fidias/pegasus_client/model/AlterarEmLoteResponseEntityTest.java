/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import br.com.fidias.pegasus_client.model.ProdutoAlterarEmLoteGetItemLista;
import br.com.fidias.pegasus_client.model.ProdutoAlterarEmLoteGetLote;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for AlterarEmLoteResponseEntity
 */
public class AlterarEmLoteResponseEntityTest {
    private final AlterarEmLoteResponseEntity model = new AlterarEmLoteResponseEntity();

    /**
     * Model tests for AlterarEmLoteResponseEntity
     */
    @Test
    public void testAlterarEmLoteResponseEntity() {
        // TODO: test AlterarEmLoteResponseEntity
    }

    /**
     * Test the property 'lote'
     */
    @Test
    public void loteTest() {
        // TODO: test lote
    }

    /**
     * Test the property 'lista'
     */
    @Test
    public void listaTest() {
        // TODO: test lista
    }

}
