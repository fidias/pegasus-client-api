/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for NFCeCancelarResponseEntity
 */
public class NFCeCancelarResponseEntityTest {
    private final NFCeCancelarResponseEntity model = new NFCeCancelarResponseEntity();

    /**
     * Model tests for NFCeCancelarResponseEntity
     */
    @Test
    public void testNFCeCancelarResponseEntity() {
        // TODO: test NFCeCancelarResponseEntity
    }

    /**
     * Test the property 'codigoUf'
     */
    @Test
    public void codigoUfTest() {
        // TODO: test codigoUf
    }

    /**
     * Test the property 'chaveAcesso'
     */
    @Test
    public void chaveAcessoTest() {
        // TODO: test chaveAcesso
    }

    /**
     * Test the property 'dataHoraNFCeGerado'
     */
    @Test
    public void dataHoraNFCeGeradoTest() {
        // TODO: test dataHoraNFCeGerado
    }

    /**
     * Test the property 'valorNFCe'
     */
    @Test
    public void valorNFCeTest() {
        // TODO: test valorNFCe
    }

    /**
     * Test the property 'versaoDados'
     */
    @Test
    public void versaoDadosTest() {
        // TODO: test versaoDados
    }

    /**
     * Test the property 'retEnvEvento'
     */
    @Test
    public void retEnvEventoTest() {
        // TODO: test retEnvEvento
    }

}
