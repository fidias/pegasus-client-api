/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for Totais
 */
public class TotaisTest {
    private final Totais model = new Totais();

    /**
     * Model tests for Totais
     */
    @Test
    public void testTotais() {
        // TODO: test Totais
    }

    /**
     * Test the property 'totalProduto'
     */
    @Test
    public void totalProdutoTest() {
        // TODO: test totalProduto
    }

    /**
     * Test the property 'totalServico'
     */
    @Test
    public void totalServicoTest() {
        // TODO: test totalServico
    }

    /**
     * Test the property 'totalBrutoProduto'
     */
    @Test
    public void totalBrutoProdutoTest() {
        // TODO: test totalBrutoProduto
    }

    /**
     * Test the property 'totalBrutoServico'
     */
    @Test
    public void totalBrutoServicoTest() {
        // TODO: test totalBrutoServico
    }

    /**
     * Test the property 'totalLiquido'
     */
    @Test
    public void totalLiquidoTest() {
        // TODO: test totalLiquido
    }

    /**
     * Test the property 'totalBruto'
     */
    @Test
    public void totalBrutoTest() {
        // TODO: test totalBruto
    }

    /**
     * Test the property 'variacaoD'
     */
    @Test
    public void variacaoDTest() {
        // TODO: test variacaoD
    }

    /**
     * Test the property 'variacaoProdutoD'
     */
    @Test
    public void variacaoProdutoDTest() {
        // TODO: test variacaoProdutoD
    }

    /**
     * Test the property 'variacaoServicoD'
     */
    @Test
    public void variacaoServicoDTest() {
        // TODO: test variacaoServicoD
    }

}
