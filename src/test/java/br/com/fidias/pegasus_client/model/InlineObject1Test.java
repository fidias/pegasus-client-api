/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import br.com.fidias.pegasus_client.model.IntegerParam;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for InlineObject1
 */
public class InlineObject1Test {
    private final InlineObject1 model = new InlineObject1();

    /**
     * Model tests for InlineObject1
     */
    @Test
    public void testInlineObject1() {
        // TODO: test InlineObject1
    }

    /**
     * Test the property 'responsavelId'
     */
    @Test
    public void responsavelIdTest() {
        // TODO: test responsavelId
    }

    /**
     * Test the property 'tipoId'
     */
    @Test
    public void tipoIdTest() {
        // TODO: test tipoId
    }

    /**
     * Test the property 'motivoId'
     */
    @Test
    public void motivoIdTest() {
        // TODO: test motivoId
    }

}
