/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import br.com.fidias.pegasus_client.model.Destinatario;
import br.com.fidias.pegasus_client.model.Situacao;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for NFCeItemLista
 */
public class NFCeItemListaTest {
    private final NFCeItemLista model = new NFCeItemLista();

    /**
     * Model tests for NFCeItemLista
     */
    @Test
    public void testNFCeItemLista() {
        // TODO: test NFCeItemLista
    }

    /**
     * Test the property 'id'
     */
    @Test
    public void idTest() {
        // TODO: test id
    }

    /**
     * Test the property 'anoMes'
     */
    @Test
    public void anoMesTest() {
        // TODO: test anoMes
    }

    /**
     * Test the property 'mod'
     */
    @Test
    public void modTest() {
        // TODO: test mod
    }

    /**
     * Test the property 'serie'
     */
    @Test
    public void serieTest() {
        // TODO: test serie
    }

    /**
     * Test the property 'nNf'
     */
    @Test
    public void nNfTest() {
        // TODO: test nNf
    }

    /**
     * Test the property 'tpEmis'
     */
    @Test
    public void tpEmisTest() {
        // TODO: test tpEmis
    }

    /**
     * Test the property 'cNf'
     */
    @Test
    public void cNfTest() {
        // TODO: test cNf
    }

    /**
     * Test the property 'tpAmb'
     */
    @Test
    public void tpAmbTest() {
        // TODO: test tpAmb
    }

    /**
     * Test the property 'cUf'
     */
    @Test
    public void cUfTest() {
        // TODO: test cUf
    }

    /**
     * Test the property 'versao'
     */
    @Test
    public void versaoTest() {
        // TODO: test versao
    }

    /**
     * Test the property 'destinatario'
     */
    @Test
    public void destinatarioTest() {
        // TODO: test destinatario
    }

    /**
     * Test the property 'situacao'
     */
    @Test
    public void situacaoTest() {
        // TODO: test situacao
    }

    /**
     * Test the property 'valorTotal'
     */
    @Test
    public void valorTotalTest() {
        // TODO: test valorTotal
    }

    /**
     * Test the property 'dhEmi'
     */
    @Test
    public void dhEmiTest() {
        // TODO: test dhEmi
    }

}
