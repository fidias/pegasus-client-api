/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import br.com.fidias.pegasus_client.model.DoubleParam;
import br.com.fidias.pegasus_client.model.IntegerParam;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for InlineObject7
 */
public class InlineObject7Test {
    private final InlineObject7 model = new InlineObject7();

    /**
     * Model tests for InlineObject7
     */
    @Test
    public void testInlineObject7() {
        // TODO: test InlineObject7
    }

    /**
     * Test the property 'taxaP'
     */
    @Test
    public void taxaPTest() {
        // TODO: test taxaP
    }

    /**
     * Test the property 'numParcelasIni'
     */
    @Test
    public void numParcelasIniTest() {
        // TODO: test numParcelasIni
    }

    /**
     * Test the property 'numParcelasFim'
     */
    @Test
    public void numParcelasFimTest() {
        // TODO: test numParcelasFim
    }

}
