/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for InlineObject96
 */
public class InlineObject96Test {
    private final InlineObject96 model = new InlineObject96();

    /**
     * Model tests for InlineObject96
     */
    @Test
    public void testInlineObject96() {
        // TODO: test InlineObject96
    }

    /**
     * Test the property 'producaoOperacaoFinanceiraEntrada'
     */
    @Test
    public void producaoOperacaoFinanceiraEntradaTest() {
        // TODO: test producaoOperacaoFinanceiraEntrada
    }

    /**
     * Test the property 'producaoOperacaoFinanceiraSaida'
     */
    @Test
    public void producaoOperacaoFinanceiraSaidaTest() {
        // TODO: test producaoOperacaoFinanceiraSaida
    }

}
