/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for InlineObject8
 */
public class InlineObject8Test {
    private final InlineObject8 model = new InlineObject8();

    /**
     * Model tests for InlineObject8
     */
    @Test
    public void testInlineObject8() {
        // TODO: test InlineObject8
    }

    /**
     * Test the property 'identificadorUnico'
     */
    @Test
    public void identificadorUnicoTest() {
        // TODO: test identificadorUnico
    }

    /**
     * Test the property 'valorSolicitado'
     */
    @Test
    public void valorSolicitadoTest() {
        // TODO: test valorSolicitado
    }

}
