/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import br.com.fidias.pegasus_client.model.Usuario1;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for DavModificarSituacao
 */
public class DavModificarSituacaoTest {
    private final DavModificarSituacao model = new DavModificarSituacao();

    /**
     * Model tests for DavModificarSituacao
     */
    @Test
    public void testDavModificarSituacao() {
        // TODO: test DavModificarSituacao
    }

    /**
     * Test the property 'id'
     */
    @Test
    public void idTest() {
        // TODO: test id
    }

    /**
     * Test the property 'dataEdicao'
     */
    @Test
    public void dataEdicaoTest() {
        // TODO: test dataEdicao
    }

    /**
     * Test the property 'statusId'
     */
    @Test
    public void statusIdTest() {
        // TODO: test statusId
    }

    /**
     * Test the property 'usuario'
     */
    @Test
    public void usuarioTest() {
        // TODO: test usuario
    }

}
