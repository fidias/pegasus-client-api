/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.api;

import br.com.fidias.pegasus_client.ApiCallback;
import br.com.fidias.pegasus_client.ApiClient;
import br.com.fidias.pegasus_client.ApiException;
import br.com.fidias.pegasus_client.ApiResponse;
import br.com.fidias.pegasus_client.Configuration;
import br.com.fidias.pegasus_client.Pair;
import br.com.fidias.pegasus_client.ProgressRequestBody;
import br.com.fidias.pegasus_client.ProgressResponseBody;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;


import br.com.fidias.pegasus_client.model.ExceptionEntity;
import br.com.fidias.pegasus_client.model.IntegerParam;
import br.com.fidias.pegasus_client.model.PosListarResponseEntity;
import br.com.fidias.pegasus_client.model.PosResponseEntity;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PosApi {
    private ApiClient localVarApiClient;

    public PosApi() {
        this(Configuration.getDefaultApiClient());
    }

    public PosApi(ApiClient apiClient) {
        this.localVarApiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return localVarApiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.localVarApiClient = apiClient;
    }

    /**
     * Build call for atualizar
     * @param serialPos  (required)
     * @param descricao  (required)
     * @param chaveRequisicao  (required)
     * @param idAdquirente  (required)
     * @param posicao  (optional)
     * @param _callback Callback for upload/download progress
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> POS atualizado </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call atualizarCall(String serialPos, String descricao, String chaveRequisicao, IntegerParam idAdquirente, IntegerParam posicao, final ApiCallback _callback) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/api/cartoes/pos/{serial_pos}"
            .replaceAll("\\{" + "serial_pos" + "\\}", localVarApiClient.escapeString(serialPos.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        Map<String, Object> localVarFormParams = new HashMap<String, Object>();
        if (descricao != null) {
            localVarFormParams.put("descricao", descricao);
        }

        if (chaveRequisicao != null) {
            localVarFormParams.put("chave_requisicao", chaveRequisicao);
        }

        if (idAdquirente != null) {
            localVarFormParams.put("id_adquirente", idAdquirente);
        }

        if (posicao != null) {
            localVarFormParams.put("posicao", posicao);
        }

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = localVarApiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) {
            localVarHeaderParams.put("Accept", localVarAccept);
        }

        final String[] localVarContentTypes = {
            "application/x-www-form-urlencoded"
        };
        final String localVarContentType = localVarApiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        String[] localVarAuthNames = new String[] { "bearerAuth" };
        return localVarApiClient.buildCall(localVarPath, "PUT", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, _callback);
    }

    @SuppressWarnings("rawtypes")
    private okhttp3.Call atualizarValidateBeforeCall(String serialPos, String descricao, String chaveRequisicao, IntegerParam idAdquirente, IntegerParam posicao, final ApiCallback _callback) throws ApiException {
        
        // verify the required parameter 'serialPos' is set
        if (serialPos == null) {
            throw new ApiException("Missing the required parameter 'serialPos' when calling atualizar(Async)");
        }
        
        // verify the required parameter 'descricao' is set
        if (descricao == null) {
            throw new ApiException("Missing the required parameter 'descricao' when calling atualizar(Async)");
        }
        
        // verify the required parameter 'chaveRequisicao' is set
        if (chaveRequisicao == null) {
            throw new ApiException("Missing the required parameter 'chaveRequisicao' when calling atualizar(Async)");
        }
        
        // verify the required parameter 'idAdquirente' is set
        if (idAdquirente == null) {
            throw new ApiException("Missing the required parameter 'idAdquirente' when calling atualizar(Async)");
        }
        

        okhttp3.Call localVarCall = atualizarCall(serialPos, descricao, chaveRequisicao, idAdquirente, posicao, _callback);
        return localVarCall;

    }

    /**
     * 
     * Requisita atualização de POS
     * @param serialPos  (required)
     * @param descricao  (required)
     * @param chaveRequisicao  (required)
     * @param idAdquirente  (required)
     * @param posicao  (optional)
     * @return PosResponseEntity
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> POS atualizado </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public PosResponseEntity atualizar(String serialPos, String descricao, String chaveRequisicao, IntegerParam idAdquirente, IntegerParam posicao) throws ApiException {
        ApiResponse<PosResponseEntity> localVarResp = atualizarWithHttpInfo(serialPos, descricao, chaveRequisicao, idAdquirente, posicao);
        return localVarResp.getData();
    }

    /**
     * 
     * Requisita atualização de POS
     * @param serialPos  (required)
     * @param descricao  (required)
     * @param chaveRequisicao  (required)
     * @param idAdquirente  (required)
     * @param posicao  (optional)
     * @return ApiResponse&lt;PosResponseEntity&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> POS atualizado </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public ApiResponse<PosResponseEntity> atualizarWithHttpInfo(String serialPos, String descricao, String chaveRequisicao, IntegerParam idAdquirente, IntegerParam posicao) throws ApiException {
        okhttp3.Call localVarCall = atualizarValidateBeforeCall(serialPos, descricao, chaveRequisicao, idAdquirente, posicao, null);
        Type localVarReturnType = new TypeToken<PosResponseEntity>(){}.getType();
        return localVarApiClient.execute(localVarCall, localVarReturnType);
    }

    /**
     *  (asynchronously)
     * Requisita atualização de POS
     * @param serialPos  (required)
     * @param descricao  (required)
     * @param chaveRequisicao  (required)
     * @param idAdquirente  (required)
     * @param posicao  (optional)
     * @param _callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> POS atualizado </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call atualizarAsync(String serialPos, String descricao, String chaveRequisicao, IntegerParam idAdquirente, IntegerParam posicao, final ApiCallback<PosResponseEntity> _callback) throws ApiException {

        okhttp3.Call localVarCall = atualizarValidateBeforeCall(serialPos, descricao, chaveRequisicao, idAdquirente, posicao, _callback);
        Type localVarReturnType = new TypeToken<PosResponseEntity>(){}.getType();
        localVarApiClient.executeAsync(localVarCall, localVarReturnType, _callback);
        return localVarCall;
    }
    /**
     * Build call for criar2
     * @param descricao  (required)
     * @param chaveRequisicao  (required)
     * @param idAdquirente  (required)
     * @param posicao  (optional)
     * @param _callback Callback for upload/download progress
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 201 </td><td> Lista de POS </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call criar2Call(String descricao, String chaveRequisicao, IntegerParam idAdquirente, IntegerParam posicao, final ApiCallback _callback) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/api/cartoes/pos";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        Map<String, Object> localVarFormParams = new HashMap<String, Object>();
        if (descricao != null) {
            localVarFormParams.put("descricao", descricao);
        }

        if (chaveRequisicao != null) {
            localVarFormParams.put("chave_requisicao", chaveRequisicao);
        }

        if (idAdquirente != null) {
            localVarFormParams.put("id_adquirente", idAdquirente);
        }

        if (posicao != null) {
            localVarFormParams.put("posicao", posicao);
        }

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = localVarApiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) {
            localVarHeaderParams.put("Accept", localVarAccept);
        }

        final String[] localVarContentTypes = {
            "application/x-www-form-urlencoded"
        };
        final String localVarContentType = localVarApiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        String[] localVarAuthNames = new String[] { "bearerAuth" };
        return localVarApiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, _callback);
    }

    @SuppressWarnings("rawtypes")
    private okhttp3.Call criar2ValidateBeforeCall(String descricao, String chaveRequisicao, IntegerParam idAdquirente, IntegerParam posicao, final ApiCallback _callback) throws ApiException {
        
        // verify the required parameter 'descricao' is set
        if (descricao == null) {
            throw new ApiException("Missing the required parameter 'descricao' when calling criar2(Async)");
        }
        
        // verify the required parameter 'chaveRequisicao' is set
        if (chaveRequisicao == null) {
            throw new ApiException("Missing the required parameter 'chaveRequisicao' when calling criar2(Async)");
        }
        
        // verify the required parameter 'idAdquirente' is set
        if (idAdquirente == null) {
            throw new ApiException("Missing the required parameter 'idAdquirente' when calling criar2(Async)");
        }
        

        okhttp3.Call localVarCall = criar2Call(descricao, chaveRequisicao, idAdquirente, posicao, _callback);
        return localVarCall;

    }

    /**
     * 
     * Requisita criação de POS
     * @param descricao  (required)
     * @param chaveRequisicao  (required)
     * @param idAdquirente  (required)
     * @param posicao  (optional)
     * @return PosResponseEntity
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 201 </td><td> Lista de POS </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public PosResponseEntity criar2(String descricao, String chaveRequisicao, IntegerParam idAdquirente, IntegerParam posicao) throws ApiException {
        ApiResponse<PosResponseEntity> localVarResp = criar2WithHttpInfo(descricao, chaveRequisicao, idAdquirente, posicao);
        return localVarResp.getData();
    }

    /**
     * 
     * Requisita criação de POS
     * @param descricao  (required)
     * @param chaveRequisicao  (required)
     * @param idAdquirente  (required)
     * @param posicao  (optional)
     * @return ApiResponse&lt;PosResponseEntity&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 201 </td><td> Lista de POS </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public ApiResponse<PosResponseEntity> criar2WithHttpInfo(String descricao, String chaveRequisicao, IntegerParam idAdquirente, IntegerParam posicao) throws ApiException {
        okhttp3.Call localVarCall = criar2ValidateBeforeCall(descricao, chaveRequisicao, idAdquirente, posicao, null);
        Type localVarReturnType = new TypeToken<PosResponseEntity>(){}.getType();
        return localVarApiClient.execute(localVarCall, localVarReturnType);
    }

    /**
     *  (asynchronously)
     * Requisita criação de POS
     * @param descricao  (required)
     * @param chaveRequisicao  (required)
     * @param idAdquirente  (required)
     * @param posicao  (optional)
     * @param _callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 201 </td><td> Lista de POS </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call criar2Async(String descricao, String chaveRequisicao, IntegerParam idAdquirente, IntegerParam posicao, final ApiCallback<PosResponseEntity> _callback) throws ApiException {

        okhttp3.Call localVarCall = criar2ValidateBeforeCall(descricao, chaveRequisicao, idAdquirente, posicao, _callback);
        Type localVarReturnType = new TypeToken<PosResponseEntity>(){}.getType();
        localVarApiClient.executeAsync(localVarCall, localVarReturnType, _callback);
        return localVarCall;
    }
    /**
     * Build call for deletar1
     * @param serialPos  (required)
     * @param _callback Callback for upload/download progress
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Lista de Serial POS </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call deletar1Call(String serialPos, final ApiCallback _callback) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/api/cartoes/pos/{serial_pos}"
            .replaceAll("\\{" + "serial_pos" + "\\}", localVarApiClient.escapeString(serialPos.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        Map<String, Object> localVarFormParams = new HashMap<String, Object>();
        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = localVarApiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) {
            localVarHeaderParams.put("Accept", localVarAccept);
        }

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = localVarApiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        String[] localVarAuthNames = new String[] { "bearerAuth" };
        return localVarApiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, _callback);
    }

    @SuppressWarnings("rawtypes")
    private okhttp3.Call deletar1ValidateBeforeCall(String serialPos, final ApiCallback _callback) throws ApiException {
        
        // verify the required parameter 'serialPos' is set
        if (serialPos == null) {
            throw new ApiException("Missing the required parameter 'serialPos' when calling deletar1(Async)");
        }
        

        okhttp3.Call localVarCall = deletar1Call(serialPos, _callback);
        return localVarCall;

    }

    /**
     * 
     * Requisita remoção de um serial POS
     * @param serialPos  (required)
     * @return PosListarResponseEntity
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Lista de Serial POS </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public PosListarResponseEntity deletar1(String serialPos) throws ApiException {
        ApiResponse<PosListarResponseEntity> localVarResp = deletar1WithHttpInfo(serialPos);
        return localVarResp.getData();
    }

    /**
     * 
     * Requisita remoção de um serial POS
     * @param serialPos  (required)
     * @return ApiResponse&lt;PosListarResponseEntity&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Lista de Serial POS </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public ApiResponse<PosListarResponseEntity> deletar1WithHttpInfo(String serialPos) throws ApiException {
        okhttp3.Call localVarCall = deletar1ValidateBeforeCall(serialPos, null);
        Type localVarReturnType = new TypeToken<PosListarResponseEntity>(){}.getType();
        return localVarApiClient.execute(localVarCall, localVarReturnType);
    }

    /**
     *  (asynchronously)
     * Requisita remoção de um serial POS
     * @param serialPos  (required)
     * @param _callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Lista de Serial POS </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call deletar1Async(String serialPos, final ApiCallback<PosListarResponseEntity> _callback) throws ApiException {

        okhttp3.Call localVarCall = deletar1ValidateBeforeCall(serialPos, _callback);
        Type localVarReturnType = new TypeToken<PosListarResponseEntity>(){}.getType();
        localVarApiClient.executeAsync(localVarCall, localVarReturnType, _callback);
        return localVarCall;
    }
    /**
     * Build call for get1
     * @param serialPos  (required)
     * @param _callback Callback for upload/download progress
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> POS </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call get1Call(String serialPos, final ApiCallback _callback) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/api/cartoes/pos/{serial_pos}"
            .replaceAll("\\{" + "serial_pos" + "\\}", localVarApiClient.escapeString(serialPos.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        Map<String, Object> localVarFormParams = new HashMap<String, Object>();
        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = localVarApiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) {
            localVarHeaderParams.put("Accept", localVarAccept);
        }

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = localVarApiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        String[] localVarAuthNames = new String[] { "bearerAuth" };
        return localVarApiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, _callback);
    }

    @SuppressWarnings("rawtypes")
    private okhttp3.Call get1ValidateBeforeCall(String serialPos, final ApiCallback _callback) throws ApiException {
        
        // verify the required parameter 'serialPos' is set
        if (serialPos == null) {
            throw new ApiException("Missing the required parameter 'serialPos' when calling get1(Async)");
        }
        

        okhttp3.Call localVarCall = get1Call(serialPos, _callback);
        return localVarCall;

    }

    /**
     * 
     * Solicita um POS
     * @param serialPos  (required)
     * @return PosResponseEntity
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> POS </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public PosResponseEntity get1(String serialPos) throws ApiException {
        ApiResponse<PosResponseEntity> localVarResp = get1WithHttpInfo(serialPos);
        return localVarResp.getData();
    }

    /**
     * 
     * Solicita um POS
     * @param serialPos  (required)
     * @return ApiResponse&lt;PosResponseEntity&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> POS </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public ApiResponse<PosResponseEntity> get1WithHttpInfo(String serialPos) throws ApiException {
        okhttp3.Call localVarCall = get1ValidateBeforeCall(serialPos, null);
        Type localVarReturnType = new TypeToken<PosResponseEntity>(){}.getType();
        return localVarApiClient.execute(localVarCall, localVarReturnType);
    }

    /**
     *  (asynchronously)
     * Solicita um POS
     * @param serialPos  (required)
     * @param _callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> POS </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call get1Async(String serialPos, final ApiCallback<PosResponseEntity> _callback) throws ApiException {

        okhttp3.Call localVarCall = get1ValidateBeforeCall(serialPos, _callback);
        Type localVarReturnType = new TypeToken<PosResponseEntity>(){}.getType();
        localVarApiClient.executeAsync(localVarCall, localVarReturnType, _callback);
        return localVarCall;
    }
    /**
     * Build call for listar1
     * @param _callback Callback for upload/download progress
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Lista de POS </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call listar1Call(final ApiCallback _callback) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/api/cartoes/pos";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        Map<String, Object> localVarFormParams = new HashMap<String, Object>();
        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = localVarApiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) {
            localVarHeaderParams.put("Accept", localVarAccept);
        }

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = localVarApiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        String[] localVarAuthNames = new String[] { "bearerAuth" };
        return localVarApiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, _callback);
    }

    @SuppressWarnings("rawtypes")
    private okhttp3.Call listar1ValidateBeforeCall(final ApiCallback _callback) throws ApiException {
        

        okhttp3.Call localVarCall = listar1Call(_callback);
        return localVarCall;

    }

    /**
     * 
     * Solicita lista de POS cadastrados
     * @return PosListarResponseEntity
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Lista de POS </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public PosListarResponseEntity listar1() throws ApiException {
        ApiResponse<PosListarResponseEntity> localVarResp = listar1WithHttpInfo();
        return localVarResp.getData();
    }

    /**
     * 
     * Solicita lista de POS cadastrados
     * @return ApiResponse&lt;PosListarResponseEntity&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Lista de POS </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public ApiResponse<PosListarResponseEntity> listar1WithHttpInfo() throws ApiException {
        okhttp3.Call localVarCall = listar1ValidateBeforeCall(null);
        Type localVarReturnType = new TypeToken<PosListarResponseEntity>(){}.getType();
        return localVarApiClient.execute(localVarCall, localVarReturnType);
    }

    /**
     *  (asynchronously)
     * Solicita lista de POS cadastrados
     * @param _callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Lista de POS </td><td>  -  </td></tr>
        <tr><td> 500 </td><td> Erro desconhecido no servidor </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call listar1Async(final ApiCallback<PosListarResponseEntity> _callback) throws ApiException {

        okhttp3.Call localVarCall = listar1ValidateBeforeCall(_callback);
        Type localVarReturnType = new TypeToken<PosListarResponseEntity>(){}.getType();
        localVarApiClient.executeAsync(localVarCall, localVarReturnType, _callback);
        return localVarCall;
    }
}
