/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;

/**
 * ItemBalanceteManual
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class ItemBalanceteManual {
  public static final String SERIALIZED_NAME_ID = "id";
  @SerializedName(SERIALIZED_NAME_ID)
  private Long id;

  public static final String SERIALIZED_NAME_DATA_CONTAGEM = "data_contagem";
  @SerializedName(SERIALIZED_NAME_DATA_CONTAGEM)
  private OffsetDateTime dataContagem;

  public static final String SERIALIZED_NAME_RESPONSAVEL_ID = "responsavel_id";
  @SerializedName(SERIALIZED_NAME_RESPONSAVEL_ID)
  private Long responsavelId;

  public static final String SERIALIZED_NAME_RESPONSAVEL = "responsavel";
  @SerializedName(SERIALIZED_NAME_RESPONSAVEL)
  private String responsavel;

  public static final String SERIALIZED_NAME_TIPO_ID = "tipo_id";
  @SerializedName(SERIALIZED_NAME_TIPO_ID)
  private Long tipoId;

  public static final String SERIALIZED_NAME_TIPO_DESCRICAO = "tipo_descricao";
  @SerializedName(SERIALIZED_NAME_TIPO_DESCRICAO)
  private String tipoDescricao;

  public static final String SERIALIZED_NAME_MOTIVO_INVENTARIO_ID = "motivo_inventario_id";
  @SerializedName(SERIALIZED_NAME_MOTIVO_INVENTARIO_ID)
  private Long motivoInventarioId;

  public static final String SERIALIZED_NAME_MOTIVO_SPED = "motivo_sped";
  @SerializedName(SERIALIZED_NAME_MOTIVO_SPED)
  private String motivoSped;


  public ItemBalanceteManual id(Long id) {
    
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(required = true, value = "")

  public Long getId() {
    return id;
  }



  public void setId(Long id) {
    this.id = id;
  }


  public ItemBalanceteManual dataContagem(OffsetDateTime dataContagem) {
    
    this.dataContagem = dataContagem;
    return this;
  }

   /**
   * Get dataContagem
   * @return dataContagem
  **/
  @ApiModelProperty(required = true, value = "")

  public OffsetDateTime getDataContagem() {
    return dataContagem;
  }



  public void setDataContagem(OffsetDateTime dataContagem) {
    this.dataContagem = dataContagem;
  }


  public ItemBalanceteManual responsavelId(Long responsavelId) {
    
    this.responsavelId = responsavelId;
    return this;
  }

   /**
   * Get responsavelId
   * @return responsavelId
  **/
  @ApiModelProperty(required = true, value = "")

  public Long getResponsavelId() {
    return responsavelId;
  }



  public void setResponsavelId(Long responsavelId) {
    this.responsavelId = responsavelId;
  }


  public ItemBalanceteManual responsavel(String responsavel) {
    
    this.responsavel = responsavel;
    return this;
  }

   /**
   * Get responsavel
   * @return responsavel
  **/
  @ApiModelProperty(required = true, value = "")

  public String getResponsavel() {
    return responsavel;
  }



  public void setResponsavel(String responsavel) {
    this.responsavel = responsavel;
  }


  public ItemBalanceteManual tipoId(Long tipoId) {
    
    this.tipoId = tipoId;
    return this;
  }

   /**
   * Get tipoId
   * @return tipoId
  **/
  @ApiModelProperty(required = true, value = "")

  public Long getTipoId() {
    return tipoId;
  }



  public void setTipoId(Long tipoId) {
    this.tipoId = tipoId;
  }


  public ItemBalanceteManual tipoDescricao(String tipoDescricao) {
    
    this.tipoDescricao = tipoDescricao;
    return this;
  }

   /**
   * Get tipoDescricao
   * @return tipoDescricao
  **/
  @ApiModelProperty(required = true, value = "")

  public String getTipoDescricao() {
    return tipoDescricao;
  }



  public void setTipoDescricao(String tipoDescricao) {
    this.tipoDescricao = tipoDescricao;
  }


  public ItemBalanceteManual motivoInventarioId(Long motivoInventarioId) {
    
    this.motivoInventarioId = motivoInventarioId;
    return this;
  }

   /**
   * Get motivoInventarioId
   * @return motivoInventarioId
  **/
  @ApiModelProperty(required = true, value = "")

  public Long getMotivoInventarioId() {
    return motivoInventarioId;
  }



  public void setMotivoInventarioId(Long motivoInventarioId) {
    this.motivoInventarioId = motivoInventarioId;
  }


  public ItemBalanceteManual motivoSped(String motivoSped) {
    
    this.motivoSped = motivoSped;
    return this;
  }

   /**
   * Get motivoSped
   * @return motivoSped
  **/
  @ApiModelProperty(required = true, value = "")

  public String getMotivoSped() {
    return motivoSped;
  }



  public void setMotivoSped(String motivoSped) {
    this.motivoSped = motivoSped;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ItemBalanceteManual itemBalanceteManual = (ItemBalanceteManual) o;
    return Objects.equals(this.id, itemBalanceteManual.id) &&
        Objects.equals(this.dataContagem, itemBalanceteManual.dataContagem) &&
        Objects.equals(this.responsavelId, itemBalanceteManual.responsavelId) &&
        Objects.equals(this.responsavel, itemBalanceteManual.responsavel) &&
        Objects.equals(this.tipoId, itemBalanceteManual.tipoId) &&
        Objects.equals(this.tipoDescricao, itemBalanceteManual.tipoDescricao) &&
        Objects.equals(this.motivoInventarioId, itemBalanceteManual.motivoInventarioId) &&
        Objects.equals(this.motivoSped, itemBalanceteManual.motivoSped);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, dataContagem, responsavelId, responsavel, tipoId, tipoDescricao, motivoInventarioId, motivoSped);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ItemBalanceteManual {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    dataContagem: ").append(toIndentedString(dataContagem)).append("\n");
    sb.append("    responsavelId: ").append(toIndentedString(responsavelId)).append("\n");
    sb.append("    responsavel: ").append(toIndentedString(responsavel)).append("\n");
    sb.append("    tipoId: ").append(toIndentedString(tipoId)).append("\n");
    sb.append("    tipoDescricao: ").append(toIndentedString(tipoDescricao)).append("\n");
    sb.append("    motivoInventarioId: ").append(toIndentedString(motivoInventarioId)).append("\n");
    sb.append("    motivoSped: ").append(toIndentedString(motivoSped)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

