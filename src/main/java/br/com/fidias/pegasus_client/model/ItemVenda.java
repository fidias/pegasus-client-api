/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import br.com.fidias.pegasus_client.model.Cest;
import br.com.fidias.pegasus_client.model.Cfop;
import br.com.fidias.pegasus_client.model.PafUnidade;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * ItemVenda
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class ItemVenda {
  public static final String SERIALIZED_NAME_ID_PRODUTO = "idProduto";
  @SerializedName(SERIALIZED_NAME_ID_PRODUTO)
  private Long idProduto;

  public static final String SERIALIZED_NAME_TIPO_ID = "tipoId";
  @SerializedName(SERIALIZED_NAME_TIPO_ID)
  private Integer tipoId;

  public static final String SERIALIZED_NAME_DESCRICAO = "descricao";
  @SerializedName(SERIALIZED_NAME_DESCRICAO)
  private String descricao;

  public static final String SERIALIZED_NAME_ID_NCM = "idNcm";
  @SerializedName(SERIALIZED_NAME_ID_NCM)
  private Long idNcm;

  public static final String SERIALIZED_NAME_CODIGO_NCM = "codigoNcm";
  @SerializedName(SERIALIZED_NAME_CODIGO_NCM)
  private String codigoNcm;

  public static final String SERIALIZED_NAME_QUANTIDADE = "quantidade";
  @SerializedName(SERIALIZED_NAME_QUANTIDADE)
  private Double quantidade;

  public static final String SERIALIZED_NAME_PRECO = "preco";
  @SerializedName(SERIALIZED_NAME_PRECO)
  private Double preco;

  public static final String SERIALIZED_NAME_CODIGO_BARRA = "codigoBarra";
  @SerializedName(SERIALIZED_NAME_CODIGO_BARRA)
  private String codigoBarra;

  public static final String SERIALIZED_NAME_PAF_DESC_DINHEIRO = "pafDescDinheiro";
  @SerializedName(SERIALIZED_NAME_PAF_DESC_DINHEIRO)
  private Double pafDescDinheiro;

  public static final String SERIALIZED_NAME_PAF_ACRESC_DINHEIRO = "pafAcrescDinheiro";
  @SerializedName(SERIALIZED_NAME_PAF_ACRESC_DINHEIRO)
  private Double pafAcrescDinheiro;

  public static final String SERIALIZED_NAME_CFOP = "cfop";
  @SerializedName(SERIALIZED_NAME_CFOP)
  private Cfop cfop;

  public static final String SERIALIZED_NAME_PAF_UNIDADE = "pafUnidade";
  @SerializedName(SERIALIZED_NAME_PAF_UNIDADE)
  private PafUnidade pafUnidade;

  public static final String SERIALIZED_NAME_CEST = "cest";
  @SerializedName(SERIALIZED_NAME_CEST)
  private Cest cest;

  public static final String SERIALIZED_NAME_IMPOSTO_I_C_M_S = "impostoICMS";
  @SerializedName(SERIALIZED_NAME_IMPOSTO_I_C_M_S)
  private String impostoICMS;

  public static final String SERIALIZED_NAME_IMPOSTO_P_I_S = "impostoPIS";
  @SerializedName(SERIALIZED_NAME_IMPOSTO_P_I_S)
  private String impostoPIS;

  public static final String SERIALIZED_NAME_IMPOSTO_C_O_F_I_N_S = "impostoCOFINS";
  @SerializedName(SERIALIZED_NAME_IMPOSTO_C_O_F_I_N_S)
  private String impostoCOFINS;

  public static final String SERIALIZED_NAME_TOTAL_APROXIMADO_TRIBUTOS = "totalAproximadoTributos";
  @SerializedName(SERIALIZED_NAME_TOTAL_APROXIMADO_TRIBUTOS)
  private Double totalAproximadoTributos;

  public static final String SERIALIZED_NAME_IAT = "iat";
  @SerializedName(SERIALIZED_NAME_IAT)
  private String iat;

  public static final String SERIALIZED_NAME_PAF_CASA_DECIMAL_QUANT = "pafCasaDecimalQuant";
  @SerializedName(SERIALIZED_NAME_PAF_CASA_DECIMAL_QUANT)
  private Integer pafCasaDecimalQuant;

  public static final String SERIALIZED_NAME_PAF_CASA_DECIMAL_VALOR = "pafCasaDecimalValor";
  @SerializedName(SERIALIZED_NAME_PAF_CASA_DECIMAL_VALOR)
  private Integer pafCasaDecimalValor;


  public ItemVenda idProduto(Long idProduto) {
    
    this.idProduto = idProduto;
    return this;
  }

   /**
   * Get idProduto
   * @return idProduto
  **/
  @ApiModelProperty(required = true, value = "")

  public Long getIdProduto() {
    return idProduto;
  }



  public void setIdProduto(Long idProduto) {
    this.idProduto = idProduto;
  }


  public ItemVenda tipoId(Integer tipoId) {
    
    this.tipoId = tipoId;
    return this;
  }

   /**
   * Get tipoId
   * @return tipoId
  **/
  @ApiModelProperty(required = true, value = "")

  public Integer getTipoId() {
    return tipoId;
  }



  public void setTipoId(Integer tipoId) {
    this.tipoId = tipoId;
  }


  public ItemVenda descricao(String descricao) {
    
    this.descricao = descricao;
    return this;
  }

   /**
   * Get descricao
   * @return descricao
  **/
  @ApiModelProperty(required = true, value = "")

  public String getDescricao() {
    return descricao;
  }



  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }


  public ItemVenda idNcm(Long idNcm) {
    
    this.idNcm = idNcm;
    return this;
  }

   /**
   * Get idNcm
   * @return idNcm
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Long getIdNcm() {
    return idNcm;
  }



  public void setIdNcm(Long idNcm) {
    this.idNcm = idNcm;
  }


  public ItemVenda codigoNcm(String codigoNcm) {
    
    this.codigoNcm = codigoNcm;
    return this;
  }

   /**
   * Get codigoNcm
   * @return codigoNcm
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getCodigoNcm() {
    return codigoNcm;
  }



  public void setCodigoNcm(String codigoNcm) {
    this.codigoNcm = codigoNcm;
  }


  public ItemVenda quantidade(Double quantidade) {
    
    this.quantidade = quantidade;
    return this;
  }

   /**
   * Get quantidade
   * @return quantidade
  **/
  @ApiModelProperty(required = true, value = "")

  public Double getQuantidade() {
    return quantidade;
  }



  public void setQuantidade(Double quantidade) {
    this.quantidade = quantidade;
  }


  public ItemVenda preco(Double preco) {
    
    this.preco = preco;
    return this;
  }

   /**
   * Get preco
   * @return preco
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Double getPreco() {
    return preco;
  }



  public void setPreco(Double preco) {
    this.preco = preco;
  }


  public ItemVenda codigoBarra(String codigoBarra) {
    
    this.codigoBarra = codigoBarra;
    return this;
  }

   /**
   * Get codigoBarra
   * @return codigoBarra
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getCodigoBarra() {
    return codigoBarra;
  }



  public void setCodigoBarra(String codigoBarra) {
    this.codigoBarra = codigoBarra;
  }


  public ItemVenda pafDescDinheiro(Double pafDescDinheiro) {
    
    this.pafDescDinheiro = pafDescDinheiro;
    return this;
  }

   /**
   * Get pafDescDinheiro
   * @return pafDescDinheiro
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Double getPafDescDinheiro() {
    return pafDescDinheiro;
  }



  public void setPafDescDinheiro(Double pafDescDinheiro) {
    this.pafDescDinheiro = pafDescDinheiro;
  }


  public ItemVenda pafAcrescDinheiro(Double pafAcrescDinheiro) {
    
    this.pafAcrescDinheiro = pafAcrescDinheiro;
    return this;
  }

   /**
   * Get pafAcrescDinheiro
   * @return pafAcrescDinheiro
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Double getPafAcrescDinheiro() {
    return pafAcrescDinheiro;
  }



  public void setPafAcrescDinheiro(Double pafAcrescDinheiro) {
    this.pafAcrescDinheiro = pafAcrescDinheiro;
  }


  public ItemVenda cfop(Cfop cfop) {
    
    this.cfop = cfop;
    return this;
  }

   /**
   * Get cfop
   * @return cfop
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Cfop getCfop() {
    return cfop;
  }



  public void setCfop(Cfop cfop) {
    this.cfop = cfop;
  }


  public ItemVenda pafUnidade(PafUnidade pafUnidade) {
    
    this.pafUnidade = pafUnidade;
    return this;
  }

   /**
   * Get pafUnidade
   * @return pafUnidade
  **/
  @ApiModelProperty(required = true, value = "")

  public PafUnidade getPafUnidade() {
    return pafUnidade;
  }



  public void setPafUnidade(PafUnidade pafUnidade) {
    this.pafUnidade = pafUnidade;
  }


  public ItemVenda cest(Cest cest) {
    
    this.cest = cest;
    return this;
  }

   /**
   * Get cest
   * @return cest
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Cest getCest() {
    return cest;
  }



  public void setCest(Cest cest) {
    this.cest = cest;
  }


  public ItemVenda impostoICMS(String impostoICMS) {
    
    this.impostoICMS = impostoICMS;
    return this;
  }

   /**
   * Valor do imposto ICMS em JSON
   * @return impostoICMS
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Valor do imposto ICMS em JSON")

  public String getImpostoICMS() {
    return impostoICMS;
  }



  public void setImpostoICMS(String impostoICMS) {
    this.impostoICMS = impostoICMS;
  }


  public ItemVenda impostoPIS(String impostoPIS) {
    
    this.impostoPIS = impostoPIS;
    return this;
  }

   /**
   * Valor do imposto PIS em JSON
   * @return impostoPIS
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Valor do imposto PIS em JSON")

  public String getImpostoPIS() {
    return impostoPIS;
  }



  public void setImpostoPIS(String impostoPIS) {
    this.impostoPIS = impostoPIS;
  }


  public ItemVenda impostoCOFINS(String impostoCOFINS) {
    
    this.impostoCOFINS = impostoCOFINS;
    return this;
  }

   /**
   * Valor do imposto COFINS em JSON
   * @return impostoCOFINS
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Valor do imposto COFINS em JSON")

  public String getImpostoCOFINS() {
    return impostoCOFINS;
  }



  public void setImpostoCOFINS(String impostoCOFINS) {
    this.impostoCOFINS = impostoCOFINS;
  }


  public ItemVenda totalAproximadoTributos(Double totalAproximadoTributos) {
    
    this.totalAproximadoTributos = totalAproximadoTributos;
    return this;
  }

   /**
   * Get totalAproximadoTributos
   * @return totalAproximadoTributos
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Double getTotalAproximadoTributos() {
    return totalAproximadoTributos;
  }



  public void setTotalAproximadoTributos(Double totalAproximadoTributos) {
    this.totalAproximadoTributos = totalAproximadoTributos;
  }


  public ItemVenda iat(String iat) {
    
    this.iat = iat;
    return this;
  }

   /**
   * Get iat
   * @return iat
  **/
  @ApiModelProperty(required = true, value = "")

  public String getIat() {
    return iat;
  }



  public void setIat(String iat) {
    this.iat = iat;
  }


  public ItemVenda pafCasaDecimalQuant(Integer pafCasaDecimalQuant) {
    
    this.pafCasaDecimalQuant = pafCasaDecimalQuant;
    return this;
  }

   /**
   * Get pafCasaDecimalQuant
   * @return pafCasaDecimalQuant
  **/
  @ApiModelProperty(required = true, value = "")

  public Integer getPafCasaDecimalQuant() {
    return pafCasaDecimalQuant;
  }



  public void setPafCasaDecimalQuant(Integer pafCasaDecimalQuant) {
    this.pafCasaDecimalQuant = pafCasaDecimalQuant;
  }


  public ItemVenda pafCasaDecimalValor(Integer pafCasaDecimalValor) {
    
    this.pafCasaDecimalValor = pafCasaDecimalValor;
    return this;
  }

   /**
   * Get pafCasaDecimalValor
   * @return pafCasaDecimalValor
  **/
  @ApiModelProperty(required = true, value = "")

  public Integer getPafCasaDecimalValor() {
    return pafCasaDecimalValor;
  }



  public void setPafCasaDecimalValor(Integer pafCasaDecimalValor) {
    this.pafCasaDecimalValor = pafCasaDecimalValor;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ItemVenda itemVenda = (ItemVenda) o;
    return Objects.equals(this.idProduto, itemVenda.idProduto) &&
        Objects.equals(this.tipoId, itemVenda.tipoId) &&
        Objects.equals(this.descricao, itemVenda.descricao) &&
        Objects.equals(this.idNcm, itemVenda.idNcm) &&
        Objects.equals(this.codigoNcm, itemVenda.codigoNcm) &&
        Objects.equals(this.quantidade, itemVenda.quantidade) &&
        Objects.equals(this.preco, itemVenda.preco) &&
        Objects.equals(this.codigoBarra, itemVenda.codigoBarra) &&
        Objects.equals(this.pafDescDinheiro, itemVenda.pafDescDinheiro) &&
        Objects.equals(this.pafAcrescDinheiro, itemVenda.pafAcrescDinheiro) &&
        Objects.equals(this.cfop, itemVenda.cfop) &&
        Objects.equals(this.pafUnidade, itemVenda.pafUnidade) &&
        Objects.equals(this.cest, itemVenda.cest) &&
        Objects.equals(this.impostoICMS, itemVenda.impostoICMS) &&
        Objects.equals(this.impostoPIS, itemVenda.impostoPIS) &&
        Objects.equals(this.impostoCOFINS, itemVenda.impostoCOFINS) &&
        Objects.equals(this.totalAproximadoTributos, itemVenda.totalAproximadoTributos) &&
        Objects.equals(this.iat, itemVenda.iat) &&
        Objects.equals(this.pafCasaDecimalQuant, itemVenda.pafCasaDecimalQuant) &&
        Objects.equals(this.pafCasaDecimalValor, itemVenda.pafCasaDecimalValor);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idProduto, tipoId, descricao, idNcm, codigoNcm, quantidade, preco, codigoBarra, pafDescDinheiro, pafAcrescDinheiro, cfop, pafUnidade, cest, impostoICMS, impostoPIS, impostoCOFINS, totalAproximadoTributos, iat, pafCasaDecimalQuant, pafCasaDecimalValor);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ItemVenda {\n");
    sb.append("    idProduto: ").append(toIndentedString(idProduto)).append("\n");
    sb.append("    tipoId: ").append(toIndentedString(tipoId)).append("\n");
    sb.append("    descricao: ").append(toIndentedString(descricao)).append("\n");
    sb.append("    idNcm: ").append(toIndentedString(idNcm)).append("\n");
    sb.append("    codigoNcm: ").append(toIndentedString(codigoNcm)).append("\n");
    sb.append("    quantidade: ").append(toIndentedString(quantidade)).append("\n");
    sb.append("    preco: ").append(toIndentedString(preco)).append("\n");
    sb.append("    codigoBarra: ").append(toIndentedString(codigoBarra)).append("\n");
    sb.append("    pafDescDinheiro: ").append(toIndentedString(pafDescDinheiro)).append("\n");
    sb.append("    pafAcrescDinheiro: ").append(toIndentedString(pafAcrescDinheiro)).append("\n");
    sb.append("    cfop: ").append(toIndentedString(cfop)).append("\n");
    sb.append("    pafUnidade: ").append(toIndentedString(pafUnidade)).append("\n");
    sb.append("    cest: ").append(toIndentedString(cest)).append("\n");
    sb.append("    impostoICMS: ").append(toIndentedString(impostoICMS)).append("\n");
    sb.append("    impostoPIS: ").append(toIndentedString(impostoPIS)).append("\n");
    sb.append("    impostoCOFINS: ").append(toIndentedString(impostoCOFINS)).append("\n");
    sb.append("    totalAproximadoTributos: ").append(toIndentedString(totalAproximadoTributos)).append("\n");
    sb.append("    iat: ").append(toIndentedString(iat)).append("\n");
    sb.append("    pafCasaDecimalQuant: ").append(toIndentedString(pafCasaDecimalQuant)).append("\n");
    sb.append("    pafCasaDecimalValor: ").append(toIndentedString(pafCasaDecimalValor)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

