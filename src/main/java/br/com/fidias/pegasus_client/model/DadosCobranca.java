/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;

/**
 * DadosCobranca
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class DadosCobranca {
  public static final String SERIALIZED_NAME_SITUACAO = "situacao";
  @SerializedName(SERIALIZED_NAME_SITUACAO)
  private String situacao;

  public static final String SERIALIZED_NAME_TOKEN = "token";
  @SerializedName(SERIALIZED_NAME_TOKEN)
  private String token;

  public static final String SERIALIZED_NAME_URL_COBRANCA = "url_cobranca";
  @SerializedName(SERIALIZED_NAME_URL_COBRANCA)
  private String urlCobranca;

  public static final String SERIALIZED_NAME_VENCIMENTO = "vencimento";
  @SerializedName(SERIALIZED_NAME_VENCIMENTO)
  private String vencimento;

  public static final String SERIALIZED_NAME_ATUALIZADO_EM = "atualizado_em";
  @SerializedName(SERIALIZED_NAME_ATUALIZADO_EM)
  private OffsetDateTime atualizadoEm;

  public static final String SERIALIZED_NAME_TOKEN_LINK = "token_link";
  @SerializedName(SERIALIZED_NAME_TOKEN_LINK)
  private String tokenLink;

  public static final String SERIALIZED_NAME_DIAS_DIFERENCA = "dias_diferenca";
  @SerializedName(SERIALIZED_NAME_DIAS_DIFERENCA)
  private Long diasDiferenca;

  public static final String SERIALIZED_NAME_IS_ATUALIZADO = "is_atualizado";
  @SerializedName(SERIALIZED_NAME_IS_ATUALIZADO)
  private Boolean isAtualizado;


  public DadosCobranca situacao(String situacao) {
    
    this.situacao = situacao;
    return this;
  }

   /**
   * Get situacao
   * @return situacao
  **/
  @ApiModelProperty(required = true, value = "")

  public String getSituacao() {
    return situacao;
  }



  public void setSituacao(String situacao) {
    this.situacao = situacao;
  }


  public DadosCobranca token(String token) {
    
    this.token = token;
    return this;
  }

   /**
   * Get token
   * @return token
  **/
  @ApiModelProperty(required = true, value = "")

  public String getToken() {
    return token;
  }



  public void setToken(String token) {
    this.token = token;
  }


  public DadosCobranca urlCobranca(String urlCobranca) {
    
    this.urlCobranca = urlCobranca;
    return this;
  }

   /**
   * Get urlCobranca
   * @return urlCobranca
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getUrlCobranca() {
    return urlCobranca;
  }



  public void setUrlCobranca(String urlCobranca) {
    this.urlCobranca = urlCobranca;
  }


  public DadosCobranca vencimento(String vencimento) {
    
    this.vencimento = vencimento;
    return this;
  }

   /**
   * Get vencimento
   * @return vencimento
  **/
  @ApiModelProperty(required = true, value = "")

  public String getVencimento() {
    return vencimento;
  }



  public void setVencimento(String vencimento) {
    this.vencimento = vencimento;
  }


  public DadosCobranca atualizadoEm(OffsetDateTime atualizadoEm) {
    
    this.atualizadoEm = atualizadoEm;
    return this;
  }

   /**
   * Get atualizadoEm
   * @return atualizadoEm
  **/
  @ApiModelProperty(required = true, value = "")

  public OffsetDateTime getAtualizadoEm() {
    return atualizadoEm;
  }



  public void setAtualizadoEm(OffsetDateTime atualizadoEm) {
    this.atualizadoEm = atualizadoEm;
  }


  public DadosCobranca tokenLink(String tokenLink) {
    
    this.tokenLink = tokenLink;
    return this;
  }

   /**
   * Get tokenLink
   * @return tokenLink
  **/
  @ApiModelProperty(required = true, value = "")

  public String getTokenLink() {
    return tokenLink;
  }



  public void setTokenLink(String tokenLink) {
    this.tokenLink = tokenLink;
  }


  public DadosCobranca diasDiferenca(Long diasDiferenca) {
    
    this.diasDiferenca = diasDiferenca;
    return this;
  }

   /**
   * Get diasDiferenca
   * @return diasDiferenca
  **/
  @ApiModelProperty(required = true, value = "")

  public Long getDiasDiferenca() {
    return diasDiferenca;
  }



  public void setDiasDiferenca(Long diasDiferenca) {
    this.diasDiferenca = diasDiferenca;
  }


  public DadosCobranca isAtualizado(Boolean isAtualizado) {
    
    this.isAtualizado = isAtualizado;
    return this;
  }

   /**
   * Get isAtualizado
   * @return isAtualizado
  **/
  @ApiModelProperty(required = true, value = "")

  public Boolean getIsAtualizado() {
    return isAtualizado;
  }



  public void setIsAtualizado(Boolean isAtualizado) {
    this.isAtualizado = isAtualizado;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DadosCobranca dadosCobranca = (DadosCobranca) o;
    return Objects.equals(this.situacao, dadosCobranca.situacao) &&
        Objects.equals(this.token, dadosCobranca.token) &&
        Objects.equals(this.urlCobranca, dadosCobranca.urlCobranca) &&
        Objects.equals(this.vencimento, dadosCobranca.vencimento) &&
        Objects.equals(this.atualizadoEm, dadosCobranca.atualizadoEm) &&
        Objects.equals(this.tokenLink, dadosCobranca.tokenLink) &&
        Objects.equals(this.diasDiferenca, dadosCobranca.diasDiferenca) &&
        Objects.equals(this.isAtualizado, dadosCobranca.isAtualizado);
  }

  @Override
  public int hashCode() {
    return Objects.hash(situacao, token, urlCobranca, vencimento, atualizadoEm, tokenLink, diasDiferenca, isAtualizado);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DadosCobranca {\n");
    sb.append("    situacao: ").append(toIndentedString(situacao)).append("\n");
    sb.append("    token: ").append(toIndentedString(token)).append("\n");
    sb.append("    urlCobranca: ").append(toIndentedString(urlCobranca)).append("\n");
    sb.append("    vencimento: ").append(toIndentedString(vencimento)).append("\n");
    sb.append("    atualizadoEm: ").append(toIndentedString(atualizadoEm)).append("\n");
    sb.append("    tokenLink: ").append(toIndentedString(tokenLink)).append("\n");
    sb.append("    diasDiferenca: ").append(toIndentedString(diasDiferenca)).append("\n");
    sb.append("    isAtualizado: ").append(toIndentedString(isAtualizado)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

