/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import br.com.fidias.pegasus_client.model.Cliente;
import br.com.fidias.pegasus_client.model.Comissionario;
import br.com.fidias.pegasus_client.model.FormasPagamento;
import br.com.fidias.pegasus_client.model.ItensVenda;
import br.com.fidias.pegasus_client.model.Usuario;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * VendaCompleta
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class VendaCompleta {
  public static final String SERIALIZED_NAME_ID = "id";
  @SerializedName(SERIALIZED_NAME_ID)
  private Long id;

  public static final String SERIALIZED_NAME_IDENTIFICADOR_UNICO = "identificador_unico";
  @SerializedName(SERIALIZED_NAME_IDENTIFICADOR_UNICO)
  private String identificadorUnico;

  public static final String SERIALIZED_NAME_DATA_VENDA = "data_venda";
  @SerializedName(SERIALIZED_NAME_DATA_VENDA)
  private OffsetDateTime dataVenda;

  public static final String SERIALIZED_NAME_VALOR_RECEBIDO = "valor_recebido";
  @SerializedName(SERIALIZED_NAME_VALOR_RECEBIDO)
  private Double valorRecebido;

  public static final String SERIALIZED_NAME_TROCO = "troco";
  @SerializedName(SERIALIZED_NAME_TROCO)
  private Double troco;

  public static final String SERIALIZED_NAME_TOTAL_LIQUIDO = "total_liquido";
  @SerializedName(SERIALIZED_NAME_TOTAL_LIQUIDO)
  private Double totalLiquido;

  public static final String SERIALIZED_NAME_TOTAL_BRUTO = "total_bruto";
  @SerializedName(SERIALIZED_NAME_TOTAL_BRUTO)
  private Double totalBruto;

  public static final String SERIALIZED_NAME_ACRESCIMO_D = "acrescimo_d";
  @SerializedName(SERIALIZED_NAME_ACRESCIMO_D)
  private Double acrescimoD;

  public static final String SERIALIZED_NAME_DESCONTO_D = "desconto_d";
  @SerializedName(SERIALIZED_NAME_DESCONTO_D)
  private Double descontoD;

  public static final String SERIALIZED_NAME_CLIENTE = "cliente";
  @SerializedName(SERIALIZED_NAME_CLIENTE)
  private Cliente cliente;

  public static final String SERIALIZED_NAME_USUARIO = "usuario";
  @SerializedName(SERIALIZED_NAME_USUARIO)
  private Usuario usuario;

  public static final String SERIALIZED_NAME_IDCAIXA = "idcaixa";
  @SerializedName(SERIALIZED_NAME_IDCAIXA)
  private Long idcaixa;

  public static final String SERIALIZED_NAME_CLIENTE_VENDA_ID = "cliente_venda_id";
  @SerializedName(SERIALIZED_NAME_CLIENTE_VENDA_ID)
  private Long clienteVendaId;

  public static final String SERIALIZED_NAME_COMISSIONARIO = "comissionario";
  @SerializedName(SERIALIZED_NAME_COMISSIONARIO)
  private Comissionario comissionario;

  public static final String SERIALIZED_NAME_DESABILITAR_IMPORTAR_NFE = "desabilitar_importar_nfe";
  @SerializedName(SERIALIZED_NAME_DESABILITAR_IMPORTAR_NFE)
  private Boolean desabilitarImportarNfe;

  public static final String SERIALIZED_NAME_NFE_ID = "nfe_id";
  @SerializedName(SERIALIZED_NAME_NFE_ID)
  private Long nfeId;

  public static final String SERIALIZED_NAME_CANCELADA = "cancelada";
  @SerializedName(SERIALIZED_NAME_CANCELADA)
  private Boolean cancelada;

  public static final String SERIALIZED_NAME_ITENS_VENDA = "itens_venda";
  @SerializedName(SERIALIZED_NAME_ITENS_VENDA)
  private List<ItensVenda> itensVenda = new ArrayList<>();

  public static final String SERIALIZED_NAME_FORMAS_PAGAMENTO = "formas_pagamento";
  @SerializedName(SERIALIZED_NAME_FORMAS_PAGAMENTO)
  private List<FormasPagamento> formasPagamento = new ArrayList<>();

  public static final String SERIALIZED_NAME_IGNORAR_QTD_DESC_PROMO = "ignorar_qtd_desc_promo";
  @SerializedName(SERIALIZED_NAME_IGNORAR_QTD_DESC_PROMO)
  private Boolean ignorarQtdDescPromo;


  public VendaCompleta id(Long id) {
    
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(required = true, value = "")

  public Long getId() {
    return id;
  }



  public void setId(Long id) {
    this.id = id;
  }


  public VendaCompleta identificadorUnico(String identificadorUnico) {
    
    this.identificadorUnico = identificadorUnico;
    return this;
  }

   /**
   * Get identificadorUnico
   * @return identificadorUnico
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getIdentificadorUnico() {
    return identificadorUnico;
  }



  public void setIdentificadorUnico(String identificadorUnico) {
    this.identificadorUnico = identificadorUnico;
  }


  public VendaCompleta dataVenda(OffsetDateTime dataVenda) {
    
    this.dataVenda = dataVenda;
    return this;
  }

   /**
   * Get dataVenda
   * @return dataVenda
  **/
  @ApiModelProperty(required = true, value = "")

  public OffsetDateTime getDataVenda() {
    return dataVenda;
  }



  public void setDataVenda(OffsetDateTime dataVenda) {
    this.dataVenda = dataVenda;
  }


  public VendaCompleta valorRecebido(Double valorRecebido) {
    
    this.valorRecebido = valorRecebido;
    return this;
  }

   /**
   * Get valorRecebido
   * @return valorRecebido
  **/
  @ApiModelProperty(required = true, value = "")

  public Double getValorRecebido() {
    return valorRecebido;
  }



  public void setValorRecebido(Double valorRecebido) {
    this.valorRecebido = valorRecebido;
  }


  public VendaCompleta troco(Double troco) {
    
    this.troco = troco;
    return this;
  }

   /**
   * Get troco
   * @return troco
  **/
  @ApiModelProperty(required = true, value = "")

  public Double getTroco() {
    return troco;
  }



  public void setTroco(Double troco) {
    this.troco = troco;
  }


  public VendaCompleta totalLiquido(Double totalLiquido) {
    
    this.totalLiquido = totalLiquido;
    return this;
  }

   /**
   * Get totalLiquido
   * @return totalLiquido
  **/
  @ApiModelProperty(required = true, value = "")

  public Double getTotalLiquido() {
    return totalLiquido;
  }



  public void setTotalLiquido(Double totalLiquido) {
    this.totalLiquido = totalLiquido;
  }


  public VendaCompleta totalBruto(Double totalBruto) {
    
    this.totalBruto = totalBruto;
    return this;
  }

   /**
   * Get totalBruto
   * @return totalBruto
  **/
  @ApiModelProperty(required = true, value = "")

  public Double getTotalBruto() {
    return totalBruto;
  }



  public void setTotalBruto(Double totalBruto) {
    this.totalBruto = totalBruto;
  }


  public VendaCompleta acrescimoD(Double acrescimoD) {
    
    this.acrescimoD = acrescimoD;
    return this;
  }

   /**
   * Get acrescimoD
   * @return acrescimoD
  **/
  @ApiModelProperty(required = true, value = "")

  public Double getAcrescimoD() {
    return acrescimoD;
  }



  public void setAcrescimoD(Double acrescimoD) {
    this.acrescimoD = acrescimoD;
  }


  public VendaCompleta descontoD(Double descontoD) {
    
    this.descontoD = descontoD;
    return this;
  }

   /**
   * Get descontoD
   * @return descontoD
  **/
  @ApiModelProperty(required = true, value = "")

  public Double getDescontoD() {
    return descontoD;
  }



  public void setDescontoD(Double descontoD) {
    this.descontoD = descontoD;
  }


  public VendaCompleta cliente(Cliente cliente) {
    
    this.cliente = cliente;
    return this;
  }

   /**
   * Get cliente
   * @return cliente
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Cliente getCliente() {
    return cliente;
  }



  public void setCliente(Cliente cliente) {
    this.cliente = cliente;
  }


  public VendaCompleta usuario(Usuario usuario) {
    
    this.usuario = usuario;
    return this;
  }

   /**
   * Get usuario
   * @return usuario
  **/
  @ApiModelProperty(required = true, value = "")

  public Usuario getUsuario() {
    return usuario;
  }



  public void setUsuario(Usuario usuario) {
    this.usuario = usuario;
  }


  public VendaCompleta idcaixa(Long idcaixa) {
    
    this.idcaixa = idcaixa;
    return this;
  }

   /**
   * Get idcaixa
   * @return idcaixa
  **/
  @ApiModelProperty(required = true, value = "")

  public Long getIdcaixa() {
    return idcaixa;
  }



  public void setIdcaixa(Long idcaixa) {
    this.idcaixa = idcaixa;
  }


  public VendaCompleta clienteVendaId(Long clienteVendaId) {
    
    this.clienteVendaId = clienteVendaId;
    return this;
  }

   /**
   * Get clienteVendaId
   * @return clienteVendaId
  **/
  @ApiModelProperty(required = true, value = "")

  public Long getClienteVendaId() {
    return clienteVendaId;
  }



  public void setClienteVendaId(Long clienteVendaId) {
    this.clienteVendaId = clienteVendaId;
  }


  public VendaCompleta comissionario(Comissionario comissionario) {
    
    this.comissionario = comissionario;
    return this;
  }

   /**
   * Get comissionario
   * @return comissionario
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Comissionario getComissionario() {
    return comissionario;
  }



  public void setComissionario(Comissionario comissionario) {
    this.comissionario = comissionario;
  }


  public VendaCompleta desabilitarImportarNfe(Boolean desabilitarImportarNfe) {
    
    this.desabilitarImportarNfe = desabilitarImportarNfe;
    return this;
  }

   /**
   * Get desabilitarImportarNfe
   * @return desabilitarImportarNfe
  **/
  @ApiModelProperty(required = true, value = "")

  public Boolean getDesabilitarImportarNfe() {
    return desabilitarImportarNfe;
  }



  public void setDesabilitarImportarNfe(Boolean desabilitarImportarNfe) {
    this.desabilitarImportarNfe = desabilitarImportarNfe;
  }


  public VendaCompleta nfeId(Long nfeId) {
    
    this.nfeId = nfeId;
    return this;
  }

   /**
   * Get nfeId
   * @return nfeId
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Long getNfeId() {
    return nfeId;
  }



  public void setNfeId(Long nfeId) {
    this.nfeId = nfeId;
  }


  public VendaCompleta cancelada(Boolean cancelada) {
    
    this.cancelada = cancelada;
    return this;
  }

   /**
   * Get cancelada
   * @return cancelada
  **/
  @ApiModelProperty(required = true, value = "")

  public Boolean getCancelada() {
    return cancelada;
  }



  public void setCancelada(Boolean cancelada) {
    this.cancelada = cancelada;
  }


  public VendaCompleta itensVenda(List<ItensVenda> itensVenda) {
    
    this.itensVenda = itensVenda;
    return this;
  }

  public VendaCompleta addItensVendaItem(ItensVenda itensVendaItem) {
    this.itensVenda.add(itensVendaItem);
    return this;
  }

   /**
   * Get itensVenda
   * @return itensVenda
  **/
  @ApiModelProperty(required = true, value = "")

  public List<ItensVenda> getItensVenda() {
    return itensVenda;
  }



  public void setItensVenda(List<ItensVenda> itensVenda) {
    this.itensVenda = itensVenda;
  }


  public VendaCompleta formasPagamento(List<FormasPagamento> formasPagamento) {
    
    this.formasPagamento = formasPagamento;
    return this;
  }

  public VendaCompleta addFormasPagamentoItem(FormasPagamento formasPagamentoItem) {
    this.formasPagamento.add(formasPagamentoItem);
    return this;
  }

   /**
   * Get formasPagamento
   * @return formasPagamento
  **/
  @ApiModelProperty(required = true, value = "")

  public List<FormasPagamento> getFormasPagamento() {
    return formasPagamento;
  }



  public void setFormasPagamento(List<FormasPagamento> formasPagamento) {
    this.formasPagamento = formasPagamento;
  }


  public VendaCompleta ignorarQtdDescPromo(Boolean ignorarQtdDescPromo) {
    
    this.ignorarQtdDescPromo = ignorarQtdDescPromo;
    return this;
  }

   /**
   * Get ignorarQtdDescPromo
   * @return ignorarQtdDescPromo
  **/
  @ApiModelProperty(required = true, value = "")

  public Boolean getIgnorarQtdDescPromo() {
    return ignorarQtdDescPromo;
  }



  public void setIgnorarQtdDescPromo(Boolean ignorarQtdDescPromo) {
    this.ignorarQtdDescPromo = ignorarQtdDescPromo;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VendaCompleta vendaCompleta = (VendaCompleta) o;
    return Objects.equals(this.id, vendaCompleta.id) &&
        Objects.equals(this.identificadorUnico, vendaCompleta.identificadorUnico) &&
        Objects.equals(this.dataVenda, vendaCompleta.dataVenda) &&
        Objects.equals(this.valorRecebido, vendaCompleta.valorRecebido) &&
        Objects.equals(this.troco, vendaCompleta.troco) &&
        Objects.equals(this.totalLiquido, vendaCompleta.totalLiquido) &&
        Objects.equals(this.totalBruto, vendaCompleta.totalBruto) &&
        Objects.equals(this.acrescimoD, vendaCompleta.acrescimoD) &&
        Objects.equals(this.descontoD, vendaCompleta.descontoD) &&
        Objects.equals(this.cliente, vendaCompleta.cliente) &&
        Objects.equals(this.usuario, vendaCompleta.usuario) &&
        Objects.equals(this.idcaixa, vendaCompleta.idcaixa) &&
        Objects.equals(this.clienteVendaId, vendaCompleta.clienteVendaId) &&
        Objects.equals(this.comissionario, vendaCompleta.comissionario) &&
        Objects.equals(this.desabilitarImportarNfe, vendaCompleta.desabilitarImportarNfe) &&
        Objects.equals(this.nfeId, vendaCompleta.nfeId) &&
        Objects.equals(this.cancelada, vendaCompleta.cancelada) &&
        Objects.equals(this.itensVenda, vendaCompleta.itensVenda) &&
        Objects.equals(this.formasPagamento, vendaCompleta.formasPagamento) &&
        Objects.equals(this.ignorarQtdDescPromo, vendaCompleta.ignorarQtdDescPromo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, identificadorUnico, dataVenda, valorRecebido, troco, totalLiquido, totalBruto, acrescimoD, descontoD, cliente, usuario, idcaixa, clienteVendaId, comissionario, desabilitarImportarNfe, nfeId, cancelada, itensVenda, formasPagamento, ignorarQtdDescPromo);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class VendaCompleta {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    identificadorUnico: ").append(toIndentedString(identificadorUnico)).append("\n");
    sb.append("    dataVenda: ").append(toIndentedString(dataVenda)).append("\n");
    sb.append("    valorRecebido: ").append(toIndentedString(valorRecebido)).append("\n");
    sb.append("    troco: ").append(toIndentedString(troco)).append("\n");
    sb.append("    totalLiquido: ").append(toIndentedString(totalLiquido)).append("\n");
    sb.append("    totalBruto: ").append(toIndentedString(totalBruto)).append("\n");
    sb.append("    acrescimoD: ").append(toIndentedString(acrescimoD)).append("\n");
    sb.append("    descontoD: ").append(toIndentedString(descontoD)).append("\n");
    sb.append("    cliente: ").append(toIndentedString(cliente)).append("\n");
    sb.append("    usuario: ").append(toIndentedString(usuario)).append("\n");
    sb.append("    idcaixa: ").append(toIndentedString(idcaixa)).append("\n");
    sb.append("    clienteVendaId: ").append(toIndentedString(clienteVendaId)).append("\n");
    sb.append("    comissionario: ").append(toIndentedString(comissionario)).append("\n");
    sb.append("    desabilitarImportarNfe: ").append(toIndentedString(desabilitarImportarNfe)).append("\n");
    sb.append("    nfeId: ").append(toIndentedString(nfeId)).append("\n");
    sb.append("    cancelada: ").append(toIndentedString(cancelada)).append("\n");
    sb.append("    itensVenda: ").append(toIndentedString(itensVenda)).append("\n");
    sb.append("    formasPagamento: ").append(toIndentedString(formasPagamento)).append("\n");
    sb.append("    ignorarQtdDescPromo: ").append(toIndentedString(ignorarQtdDescPromo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

