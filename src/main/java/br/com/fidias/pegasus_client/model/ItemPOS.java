/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * ItemPOS
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class ItemPOS {
  public static final String SERIALIZED_NAME_SERIAL_POS = "serial_pos";
  @SerializedName(SERIALIZED_NAME_SERIAL_POS)
  private String serialPos;

  public static final String SERIALIZED_NAME_DESCRICAO = "descricao";
  @SerializedName(SERIALIZED_NAME_DESCRICAO)
  private String descricao;

  public static final String SERIALIZED_NAME_ADQUIRENTE_ID = "adquirente_id";
  @SerializedName(SERIALIZED_NAME_ADQUIRENTE_ID)
  private Integer adquirenteId;

  public static final String SERIALIZED_NAME_CHAVE_REQUISICAO = "chave_requisicao";
  @SerializedName(SERIALIZED_NAME_CHAVE_REQUISICAO)
  private String chaveRequisicao;

  public static final String SERIALIZED_NAME_ADQUIRENTE = "adquirente";
  @SerializedName(SERIALIZED_NAME_ADQUIRENTE)
  private String adquirente;

  public static final String SERIALIZED_NAME_CNPJ = "cnpj";
  @SerializedName(SERIALIZED_NAME_CNPJ)
  private String cnpj;

  public static final String SERIALIZED_NAME_CODIGO_ADQUIRENTE = "codigo_adquirente";
  @SerializedName(SERIALIZED_NAME_CODIGO_ADQUIRENTE)
  private Integer codigoAdquirente;


  public ItemPOS serialPos(String serialPos) {
    
    this.serialPos = serialPos;
    return this;
  }

   /**
   * Get serialPos
   * @return serialPos
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getSerialPos() {
    return serialPos;
  }



  public void setSerialPos(String serialPos) {
    this.serialPos = serialPos;
  }


  public ItemPOS descricao(String descricao) {
    
    this.descricao = descricao;
    return this;
  }

   /**
   * Get descricao
   * @return descricao
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getDescricao() {
    return descricao;
  }



  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }


  public ItemPOS adquirenteId(Integer adquirenteId) {
    
    this.adquirenteId = adquirenteId;
    return this;
  }

   /**
   * Get adquirenteId
   * @return adquirenteId
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Integer getAdquirenteId() {
    return adquirenteId;
  }



  public void setAdquirenteId(Integer adquirenteId) {
    this.adquirenteId = adquirenteId;
  }


  public ItemPOS chaveRequisicao(String chaveRequisicao) {
    
    this.chaveRequisicao = chaveRequisicao;
    return this;
  }

   /**
   * Get chaveRequisicao
   * @return chaveRequisicao
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getChaveRequisicao() {
    return chaveRequisicao;
  }



  public void setChaveRequisicao(String chaveRequisicao) {
    this.chaveRequisicao = chaveRequisicao;
  }


  public ItemPOS adquirente(String adquirente) {
    
    this.adquirente = adquirente;
    return this;
  }

   /**
   * Get adquirente
   * @return adquirente
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getAdquirente() {
    return adquirente;
  }



  public void setAdquirente(String adquirente) {
    this.adquirente = adquirente;
  }


  public ItemPOS cnpj(String cnpj) {
    
    this.cnpj = cnpj;
    return this;
  }

   /**
   * Get cnpj
   * @return cnpj
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getCnpj() {
    return cnpj;
  }



  public void setCnpj(String cnpj) {
    this.cnpj = cnpj;
  }


  public ItemPOS codigoAdquirente(Integer codigoAdquirente) {
    
    this.codigoAdquirente = codigoAdquirente;
    return this;
  }

   /**
   * Get codigoAdquirente
   * @return codigoAdquirente
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Integer getCodigoAdquirente() {
    return codigoAdquirente;
  }



  public void setCodigoAdquirente(Integer codigoAdquirente) {
    this.codigoAdquirente = codigoAdquirente;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ItemPOS itemPOS = (ItemPOS) o;
    return Objects.equals(this.serialPos, itemPOS.serialPos) &&
        Objects.equals(this.descricao, itemPOS.descricao) &&
        Objects.equals(this.adquirenteId, itemPOS.adquirenteId) &&
        Objects.equals(this.chaveRequisicao, itemPOS.chaveRequisicao) &&
        Objects.equals(this.adquirente, itemPOS.adquirente) &&
        Objects.equals(this.cnpj, itemPOS.cnpj) &&
        Objects.equals(this.codigoAdquirente, itemPOS.codigoAdquirente);
  }

  @Override
  public int hashCode() {
    return Objects.hash(serialPos, descricao, adquirenteId, chaveRequisicao, adquirente, cnpj, codigoAdquirente);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ItemPOS {\n");
    sb.append("    serialPos: ").append(toIndentedString(serialPos)).append("\n");
    sb.append("    descricao: ").append(toIndentedString(descricao)).append("\n");
    sb.append("    adquirenteId: ").append(toIndentedString(adquirenteId)).append("\n");
    sb.append("    chaveRequisicao: ").append(toIndentedString(chaveRequisicao)).append("\n");
    sb.append("    adquirente: ").append(toIndentedString(adquirente)).append("\n");
    sb.append("    cnpj: ").append(toIndentedString(cnpj)).append("\n");
    sb.append("    codigoAdquirente: ").append(toIndentedString(codigoAdquirente)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

