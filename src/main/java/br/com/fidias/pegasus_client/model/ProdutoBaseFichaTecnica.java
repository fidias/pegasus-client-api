/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * ProdutoBaseFichaTecnica
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class ProdutoBaseFichaTecnica {
  public static final String SERIALIZED_NAME_CUSTO = "custo";
  @SerializedName(SERIALIZED_NAME_CUSTO)
  private Double custo;

  public static final String SERIALIZED_NAME_MARGEM_LUCRO_PERCENTUAL = "margemLucroPercentual";
  @SerializedName(SERIALIZED_NAME_MARGEM_LUCRO_PERCENTUAL)
  private Double margemLucroPercentual;

  public static final String SERIALIZED_NAME_PRECO = "preco";
  @SerializedName(SERIALIZED_NAME_PRECO)
  private Double preco;

  public static final String SERIALIZED_NAME_LUCRO = "lucro";
  @SerializedName(SERIALIZED_NAME_LUCRO)
  private Double lucro;


  public ProdutoBaseFichaTecnica custo(Double custo) {
    
    this.custo = custo;
    return this;
  }

   /**
   * Get custo
   * @return custo
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Double getCusto() {
    return custo;
  }



  public void setCusto(Double custo) {
    this.custo = custo;
  }


  public ProdutoBaseFichaTecnica margemLucroPercentual(Double margemLucroPercentual) {
    
    this.margemLucroPercentual = margemLucroPercentual;
    return this;
  }

   /**
   * Get margemLucroPercentual
   * @return margemLucroPercentual
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Double getMargemLucroPercentual() {
    return margemLucroPercentual;
  }



  public void setMargemLucroPercentual(Double margemLucroPercentual) {
    this.margemLucroPercentual = margemLucroPercentual;
  }


  public ProdutoBaseFichaTecnica preco(Double preco) {
    
    this.preco = preco;
    return this;
  }

   /**
   * Get preco
   * @return preco
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Double getPreco() {
    return preco;
  }



  public void setPreco(Double preco) {
    this.preco = preco;
  }


  public ProdutoBaseFichaTecnica lucro(Double lucro) {
    
    this.lucro = lucro;
    return this;
  }

   /**
   * Get lucro
   * @return lucro
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Double getLucro() {
    return lucro;
  }



  public void setLucro(Double lucro) {
    this.lucro = lucro;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProdutoBaseFichaTecnica produtoBaseFichaTecnica = (ProdutoBaseFichaTecnica) o;
    return Objects.equals(this.custo, produtoBaseFichaTecnica.custo) &&
        Objects.equals(this.margemLucroPercentual, produtoBaseFichaTecnica.margemLucroPercentual) &&
        Objects.equals(this.preco, produtoBaseFichaTecnica.preco) &&
        Objects.equals(this.lucro, produtoBaseFichaTecnica.lucro);
  }

  @Override
  public int hashCode() {
    return Objects.hash(custo, margemLucroPercentual, preco, lucro);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProdutoBaseFichaTecnica {\n");
    sb.append("    custo: ").append(toIndentedString(custo)).append("\n");
    sb.append("    margemLucroPercentual: ").append(toIndentedString(margemLucroPercentual)).append("\n");
    sb.append("    preco: ").append(toIndentedString(preco)).append("\n");
    sb.append("    lucro: ").append(toIndentedString(lucro)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

