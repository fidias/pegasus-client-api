/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * InlineObject25
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class InlineObject25 {
  /**
   * Define em qual dos totais a variação percentual será realizada
   */
  @JsonAdapter(OperacaoEnum.Adapter.class)
  public enum OperacaoEnum {
    TOTAL("total"),
    
    PRODUTOS("produtos"),
    
    SERVICOS("servicos");

    private String value;

    OperacaoEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static OperacaoEnum fromValue(String value) {
      for (OperacaoEnum b : OperacaoEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }

    public static class Adapter extends TypeAdapter<OperacaoEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final OperacaoEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public OperacaoEnum read(final JsonReader jsonReader) throws IOException {
        String value =  jsonReader.nextString();
        return OperacaoEnum.fromValue(value);
      }
    }
  }

  public static final String SERIALIZED_NAME_OPERACAO = "operacao";
  @SerializedName(SERIALIZED_NAME_OPERACAO)
  private OperacaoEnum operacao;

  public static final String SERIALIZED_NAME_VALOR_TEXTUAL = "valor_textual";
  @SerializedName(SERIALIZED_NAME_VALOR_TEXTUAL)
  private String valorTextual;


  public InlineObject25 operacao(OperacaoEnum operacao) {
    
    this.operacao = operacao;
    return this;
  }

   /**
   * Define em qual dos totais a variação percentual será realizada
   * @return operacao
  **/
  @ApiModelProperty(required = true, value = "Define em qual dos totais a variação percentual será realizada")

  public OperacaoEnum getOperacao() {
    return operacao;
  }



  public void setOperacao(OperacaoEnum operacao) {
    this.operacao = operacao;
  }


  public InlineObject25 valorTextual(String valorTextual) {
    
    this.valorTextual = valorTextual;
    return this;
  }

   /**
   * Get valorTextual
   * @return valorTextual
  **/
  @ApiModelProperty(required = true, value = "")

  public String getValorTextual() {
    return valorTextual;
  }



  public void setValorTextual(String valorTextual) {
    this.valorTextual = valorTextual;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineObject25 inlineObject25 = (InlineObject25) o;
    return Objects.equals(this.operacao, inlineObject25.operacao) &&
        Objects.equals(this.valorTextual, inlineObject25.valorTextual);
  }

  @Override
  public int hashCode() {
    return Objects.hash(operacao, valorTextual);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineObject25 {\n");
    sb.append("    operacao: ").append(toIndentedString(operacao)).append("\n");
    sb.append("    valorTextual: ").append(toIndentedString(valorTextual)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

