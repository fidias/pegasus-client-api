/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import br.com.fidias.pegasus_client.model.NFeTransmissaoListaItem;
import br.com.fidias.pegasus_client.model.RetEnvEvento;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * NFeConsultarProtocoloResponseEntity
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class NFeConsultarProtocoloResponseEntity {
  public static final String SERIALIZED_NAME_LIST = "list";
  @SerializedName(SERIALIZED_NAME_LIST)
  private List<NFeTransmissaoListaItem> list = null;

  public static final String SERIALIZED_NAME_RETORNO = "retorno";
  @SerializedName(SERIALIZED_NAME_RETORNO)
  private RetEnvEvento retorno;

  public static final String SERIALIZED_NAME_SCHEMA_XML = "schema_xml";
  @SerializedName(SERIALIZED_NAME_SCHEMA_XML)
  private String schemaXml;


  public NFeConsultarProtocoloResponseEntity list(List<NFeTransmissaoListaItem> list) {
    
    this.list = list;
    return this;
  }

  public NFeConsultarProtocoloResponseEntity addListItem(NFeTransmissaoListaItem listItem) {
    if (this.list == null) {
      this.list = new ArrayList<>();
    }
    this.list.add(listItem);
    return this;
  }

   /**
   * Get list
   * @return list
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public List<NFeTransmissaoListaItem> getList() {
    return list;
  }



  public void setList(List<NFeTransmissaoListaItem> list) {
    this.list = list;
  }


  public NFeConsultarProtocoloResponseEntity retorno(RetEnvEvento retorno) {
    
    this.retorno = retorno;
    return this;
  }

   /**
   * Get retorno
   * @return retorno
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public RetEnvEvento getRetorno() {
    return retorno;
  }



  public void setRetorno(RetEnvEvento retorno) {
    this.retorno = retorno;
  }


  public NFeConsultarProtocoloResponseEntity schemaXml(String schemaXml) {
    
    this.schemaXml = schemaXml;
    return this;
  }

   /**
   * Get schemaXml
   * @return schemaXml
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getSchemaXml() {
    return schemaXml;
  }



  public void setSchemaXml(String schemaXml) {
    this.schemaXml = schemaXml;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NFeConsultarProtocoloResponseEntity nfeConsultarProtocoloResponseEntity = (NFeConsultarProtocoloResponseEntity) o;
    return Objects.equals(this.list, nfeConsultarProtocoloResponseEntity.list) &&
        Objects.equals(this.retorno, nfeConsultarProtocoloResponseEntity.retorno) &&
        Objects.equals(this.schemaXml, nfeConsultarProtocoloResponseEntity.schemaXml);
  }

  @Override
  public int hashCode() {
    return Objects.hash(list, retorno, schemaXml);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NFeConsultarProtocoloResponseEntity {\n");
    sb.append("    list: ").append(toIndentedString(list)).append("\n");
    sb.append("    retorno: ").append(toIndentedString(retorno)).append("\n");
    sb.append("    schemaXml: ").append(toIndentedString(schemaXml)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

