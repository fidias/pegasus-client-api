/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import br.com.fidias.pegasus_client.model.Ambiente;
import br.com.fidias.pegasus_client.model.CfopDevolucaoPadrao;
import br.com.fidias.pegasus_client.model.CfopVendaPadrao;
import br.com.fidias.pegasus_client.model.CodCscNfce;
import br.com.fidias.pegasus_client.model.IcmsTribut;
import br.com.fidias.pegasus_client.model.IdTokenCscNfce;
import br.com.fidias.pegasus_client.model.ImportDiluirD;
import br.com.fidias.pegasus_client.model.InfAdic;
import br.com.fidias.pegasus_client.model.ModFretePadrao;
import br.com.fidias.pegasus_client.model.Modelo;
import br.com.fidias.pegasus_client.model.NatOperacao;
import br.com.fidias.pegasus_client.model.NumeroNfce;
import br.com.fidias.pegasus_client.model.NumeroNfe;
import br.com.fidias.pegasus_client.model.Serie;
import br.com.fidias.pegasus_client.model.SerieNfce;
import br.com.fidias.pegasus_client.model.TributSimples;
import br.com.fidias.pegasus_client.model.Versao;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * NFeCarregarConfiguracoes
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class NFeCarregarConfiguracoes {
  public static final String SERIALIZED_NAME_SERIE = "serie";
  @SerializedName(SERIALIZED_NAME_SERIE)
  private Serie serie;

  public static final String SERIALIZED_NAME_MODELO = "modelo";
  @SerializedName(SERIALIZED_NAME_MODELO)
  private Modelo modelo;

  public static final String SERIALIZED_NAME_VERSAO = "versao";
  @SerializedName(SERIALIZED_NAME_VERSAO)
  private Versao versao;

  public static final String SERIALIZED_NAME_AMBIENTE = "ambiente";
  @SerializedName(SERIALIZED_NAME_AMBIENTE)
  private Ambiente ambiente;

  public static final String SERIALIZED_NAME_INF_ADIC = "inf_adic";
  @SerializedName(SERIALIZED_NAME_INF_ADIC)
  private InfAdic infAdic;

  public static final String SERIALIZED_NAME_NUMERO_NFE = "numero_nfe";
  @SerializedName(SERIALIZED_NAME_NUMERO_NFE)
  private NumeroNfe numeroNfe;

  public static final String SERIALIZED_NAME_ICMS_TRIBUT = "icms_tribut";
  @SerializedName(SERIALIZED_NAME_ICMS_TRIBUT)
  private IcmsTribut icmsTribut;

  public static final String SERIALIZED_NAME_NAT_OPERACAO = "nat_operacao";
  @SerializedName(SERIALIZED_NAME_NAT_OPERACAO)
  private NatOperacao natOperacao;

  public static final String SERIALIZED_NAME_TRIBUT_SIMPLES = "tribut_simples";
  @SerializedName(SERIALIZED_NAME_TRIBUT_SIMPLES)
  private TributSimples tributSimples;

  public static final String SERIALIZED_NAME_IMPORT_DILUIR_D = "import_diluir_d";
  @SerializedName(SERIALIZED_NAME_IMPORT_DILUIR_D)
  private ImportDiluirD importDiluirD;

  public static final String SERIALIZED_NAME_MOD_FRETE_PADRAO = "mod_frete_padrao";
  @SerializedName(SERIALIZED_NAME_MOD_FRETE_PADRAO)
  private ModFretePadrao modFretePadrao;

  public static final String SERIALIZED_NAME_COD_CSC_NFCE = "cod_csc_nfce";
  @SerializedName(SERIALIZED_NAME_COD_CSC_NFCE)
  private CodCscNfce codCscNfce;

  public static final String SERIALIZED_NAME_ID_TOKEN_CSC_NFCE = "id_token_csc_nfce";
  @SerializedName(SERIALIZED_NAME_ID_TOKEN_CSC_NFCE)
  private IdTokenCscNfce idTokenCscNfce;

  public static final String SERIALIZED_NAME_NUMERO_NFCE = "numero_nfce";
  @SerializedName(SERIALIZED_NAME_NUMERO_NFCE)
  private NumeroNfce numeroNfce;

  public static final String SERIALIZED_NAME_SERIE_NFCE = "serie_nfce";
  @SerializedName(SERIALIZED_NAME_SERIE_NFCE)
  private SerieNfce serieNfce;

  public static final String SERIALIZED_NAME_CFOP_VENDA_PADRAO = "cfop_venda_padrao";
  @SerializedName(SERIALIZED_NAME_CFOP_VENDA_PADRAO)
  private CfopVendaPadrao cfopVendaPadrao;

  public static final String SERIALIZED_NAME_CFOP_DEVOLUCAO_PADRAO = "cfop_devolucao_padrao";
  @SerializedName(SERIALIZED_NAME_CFOP_DEVOLUCAO_PADRAO)
  private CfopDevolucaoPadrao cfopDevolucaoPadrao;


  public NFeCarregarConfiguracoes serie(Serie serie) {
    
    this.serie = serie;
    return this;
  }

   /**
   * Get serie
   * @return serie
  **/
  @ApiModelProperty(required = true, value = "")

  public Serie getSerie() {
    return serie;
  }



  public void setSerie(Serie serie) {
    this.serie = serie;
  }


  public NFeCarregarConfiguracoes modelo(Modelo modelo) {
    
    this.modelo = modelo;
    return this;
  }

   /**
   * Get modelo
   * @return modelo
  **/
  @ApiModelProperty(required = true, value = "")

  public Modelo getModelo() {
    return modelo;
  }



  public void setModelo(Modelo modelo) {
    this.modelo = modelo;
  }


  public NFeCarregarConfiguracoes versao(Versao versao) {
    
    this.versao = versao;
    return this;
  }

   /**
   * Get versao
   * @return versao
  **/
  @ApiModelProperty(required = true, value = "")

  public Versao getVersao() {
    return versao;
  }



  public void setVersao(Versao versao) {
    this.versao = versao;
  }


  public NFeCarregarConfiguracoes ambiente(Ambiente ambiente) {
    
    this.ambiente = ambiente;
    return this;
  }

   /**
   * Get ambiente
   * @return ambiente
  **/
  @ApiModelProperty(required = true, value = "")

  public Ambiente getAmbiente() {
    return ambiente;
  }



  public void setAmbiente(Ambiente ambiente) {
    this.ambiente = ambiente;
  }


  public NFeCarregarConfiguracoes infAdic(InfAdic infAdic) {
    
    this.infAdic = infAdic;
    return this;
  }

   /**
   * Get infAdic
   * @return infAdic
  **/
  @ApiModelProperty(required = true, value = "")

  public InfAdic getInfAdic() {
    return infAdic;
  }



  public void setInfAdic(InfAdic infAdic) {
    this.infAdic = infAdic;
  }


  public NFeCarregarConfiguracoes numeroNfe(NumeroNfe numeroNfe) {
    
    this.numeroNfe = numeroNfe;
    return this;
  }

   /**
   * Get numeroNfe
   * @return numeroNfe
  **/
  @ApiModelProperty(required = true, value = "")

  public NumeroNfe getNumeroNfe() {
    return numeroNfe;
  }



  public void setNumeroNfe(NumeroNfe numeroNfe) {
    this.numeroNfe = numeroNfe;
  }


  public NFeCarregarConfiguracoes icmsTribut(IcmsTribut icmsTribut) {
    
    this.icmsTribut = icmsTribut;
    return this;
  }

   /**
   * Get icmsTribut
   * @return icmsTribut
  **/
  @ApiModelProperty(required = true, value = "")

  public IcmsTribut getIcmsTribut() {
    return icmsTribut;
  }



  public void setIcmsTribut(IcmsTribut icmsTribut) {
    this.icmsTribut = icmsTribut;
  }


  public NFeCarregarConfiguracoes natOperacao(NatOperacao natOperacao) {
    
    this.natOperacao = natOperacao;
    return this;
  }

   /**
   * Get natOperacao
   * @return natOperacao
  **/
  @ApiModelProperty(required = true, value = "")

  public NatOperacao getNatOperacao() {
    return natOperacao;
  }



  public void setNatOperacao(NatOperacao natOperacao) {
    this.natOperacao = natOperacao;
  }


  public NFeCarregarConfiguracoes tributSimples(TributSimples tributSimples) {
    
    this.tributSimples = tributSimples;
    return this;
  }

   /**
   * Get tributSimples
   * @return tributSimples
  **/
  @ApiModelProperty(required = true, value = "")

  public TributSimples getTributSimples() {
    return tributSimples;
  }



  public void setTributSimples(TributSimples tributSimples) {
    this.tributSimples = tributSimples;
  }


  public NFeCarregarConfiguracoes importDiluirD(ImportDiluirD importDiluirD) {
    
    this.importDiluirD = importDiluirD;
    return this;
  }

   /**
   * Get importDiluirD
   * @return importDiluirD
  **/
  @ApiModelProperty(required = true, value = "")

  public ImportDiluirD getImportDiluirD() {
    return importDiluirD;
  }



  public void setImportDiluirD(ImportDiluirD importDiluirD) {
    this.importDiluirD = importDiluirD;
  }


  public NFeCarregarConfiguracoes modFretePadrao(ModFretePadrao modFretePadrao) {
    
    this.modFretePadrao = modFretePadrao;
    return this;
  }

   /**
   * Get modFretePadrao
   * @return modFretePadrao
  **/
  @ApiModelProperty(required = true, value = "")

  public ModFretePadrao getModFretePadrao() {
    return modFretePadrao;
  }



  public void setModFretePadrao(ModFretePadrao modFretePadrao) {
    this.modFretePadrao = modFretePadrao;
  }


  public NFeCarregarConfiguracoes codCscNfce(CodCscNfce codCscNfce) {
    
    this.codCscNfce = codCscNfce;
    return this;
  }

   /**
   * Get codCscNfce
   * @return codCscNfce
  **/
  @ApiModelProperty(required = true, value = "")

  public CodCscNfce getCodCscNfce() {
    return codCscNfce;
  }



  public void setCodCscNfce(CodCscNfce codCscNfce) {
    this.codCscNfce = codCscNfce;
  }


  public NFeCarregarConfiguracoes idTokenCscNfce(IdTokenCscNfce idTokenCscNfce) {
    
    this.idTokenCscNfce = idTokenCscNfce;
    return this;
  }

   /**
   * Get idTokenCscNfce
   * @return idTokenCscNfce
  **/
  @ApiModelProperty(required = true, value = "")

  public IdTokenCscNfce getIdTokenCscNfce() {
    return idTokenCscNfce;
  }



  public void setIdTokenCscNfce(IdTokenCscNfce idTokenCscNfce) {
    this.idTokenCscNfce = idTokenCscNfce;
  }


  public NFeCarregarConfiguracoes numeroNfce(NumeroNfce numeroNfce) {
    
    this.numeroNfce = numeroNfce;
    return this;
  }

   /**
   * Get numeroNfce
   * @return numeroNfce
  **/
  @ApiModelProperty(required = true, value = "")

  public NumeroNfce getNumeroNfce() {
    return numeroNfce;
  }



  public void setNumeroNfce(NumeroNfce numeroNfce) {
    this.numeroNfce = numeroNfce;
  }


  public NFeCarregarConfiguracoes serieNfce(SerieNfce serieNfce) {
    
    this.serieNfce = serieNfce;
    return this;
  }

   /**
   * Get serieNfce
   * @return serieNfce
  **/
  @ApiModelProperty(required = true, value = "")

  public SerieNfce getSerieNfce() {
    return serieNfce;
  }



  public void setSerieNfce(SerieNfce serieNfce) {
    this.serieNfce = serieNfce;
  }


  public NFeCarregarConfiguracoes cfopVendaPadrao(CfopVendaPadrao cfopVendaPadrao) {
    
    this.cfopVendaPadrao = cfopVendaPadrao;
    return this;
  }

   /**
   * Get cfopVendaPadrao
   * @return cfopVendaPadrao
  **/
  @ApiModelProperty(required = true, value = "")

  public CfopVendaPadrao getCfopVendaPadrao() {
    return cfopVendaPadrao;
  }



  public void setCfopVendaPadrao(CfopVendaPadrao cfopVendaPadrao) {
    this.cfopVendaPadrao = cfopVendaPadrao;
  }


  public NFeCarregarConfiguracoes cfopDevolucaoPadrao(CfopDevolucaoPadrao cfopDevolucaoPadrao) {
    
    this.cfopDevolucaoPadrao = cfopDevolucaoPadrao;
    return this;
  }

   /**
   * Get cfopDevolucaoPadrao
   * @return cfopDevolucaoPadrao
  **/
  @ApiModelProperty(required = true, value = "")

  public CfopDevolucaoPadrao getCfopDevolucaoPadrao() {
    return cfopDevolucaoPadrao;
  }



  public void setCfopDevolucaoPadrao(CfopDevolucaoPadrao cfopDevolucaoPadrao) {
    this.cfopDevolucaoPadrao = cfopDevolucaoPadrao;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NFeCarregarConfiguracoes nfeCarregarConfiguracoes = (NFeCarregarConfiguracoes) o;
    return Objects.equals(this.serie, nfeCarregarConfiguracoes.serie) &&
        Objects.equals(this.modelo, nfeCarregarConfiguracoes.modelo) &&
        Objects.equals(this.versao, nfeCarregarConfiguracoes.versao) &&
        Objects.equals(this.ambiente, nfeCarregarConfiguracoes.ambiente) &&
        Objects.equals(this.infAdic, nfeCarregarConfiguracoes.infAdic) &&
        Objects.equals(this.numeroNfe, nfeCarregarConfiguracoes.numeroNfe) &&
        Objects.equals(this.icmsTribut, nfeCarregarConfiguracoes.icmsTribut) &&
        Objects.equals(this.natOperacao, nfeCarregarConfiguracoes.natOperacao) &&
        Objects.equals(this.tributSimples, nfeCarregarConfiguracoes.tributSimples) &&
        Objects.equals(this.importDiluirD, nfeCarregarConfiguracoes.importDiluirD) &&
        Objects.equals(this.modFretePadrao, nfeCarregarConfiguracoes.modFretePadrao) &&
        Objects.equals(this.codCscNfce, nfeCarregarConfiguracoes.codCscNfce) &&
        Objects.equals(this.idTokenCscNfce, nfeCarregarConfiguracoes.idTokenCscNfce) &&
        Objects.equals(this.numeroNfce, nfeCarregarConfiguracoes.numeroNfce) &&
        Objects.equals(this.serieNfce, nfeCarregarConfiguracoes.serieNfce) &&
        Objects.equals(this.cfopVendaPadrao, nfeCarregarConfiguracoes.cfopVendaPadrao) &&
        Objects.equals(this.cfopDevolucaoPadrao, nfeCarregarConfiguracoes.cfopDevolucaoPadrao);
  }

  @Override
  public int hashCode() {
    return Objects.hash(serie, modelo, versao, ambiente, infAdic, numeroNfe, icmsTribut, natOperacao, tributSimples, importDiluirD, modFretePadrao, codCscNfce, idTokenCscNfce, numeroNfce, serieNfce, cfopVendaPadrao, cfopDevolucaoPadrao);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NFeCarregarConfiguracoes {\n");
    sb.append("    serie: ").append(toIndentedString(serie)).append("\n");
    sb.append("    modelo: ").append(toIndentedString(modelo)).append("\n");
    sb.append("    versao: ").append(toIndentedString(versao)).append("\n");
    sb.append("    ambiente: ").append(toIndentedString(ambiente)).append("\n");
    sb.append("    infAdic: ").append(toIndentedString(infAdic)).append("\n");
    sb.append("    numeroNfe: ").append(toIndentedString(numeroNfe)).append("\n");
    sb.append("    icmsTribut: ").append(toIndentedString(icmsTribut)).append("\n");
    sb.append("    natOperacao: ").append(toIndentedString(natOperacao)).append("\n");
    sb.append("    tributSimples: ").append(toIndentedString(tributSimples)).append("\n");
    sb.append("    importDiluirD: ").append(toIndentedString(importDiluirD)).append("\n");
    sb.append("    modFretePadrao: ").append(toIndentedString(modFretePadrao)).append("\n");
    sb.append("    codCscNfce: ").append(toIndentedString(codCscNfce)).append("\n");
    sb.append("    idTokenCscNfce: ").append(toIndentedString(idTokenCscNfce)).append("\n");
    sb.append("    numeroNfce: ").append(toIndentedString(numeroNfce)).append("\n");
    sb.append("    serieNfce: ").append(toIndentedString(serieNfce)).append("\n");
    sb.append("    cfopVendaPadrao: ").append(toIndentedString(cfopVendaPadrao)).append("\n");
    sb.append("    cfopDevolucaoPadrao: ").append(toIndentedString(cfopDevolucaoPadrao)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

