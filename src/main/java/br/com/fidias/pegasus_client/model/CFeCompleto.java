/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import br.com.fidias.pegasus_client.model.VFPe;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * CFeCompleto
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class CFeCompleto {
  public static final String SERIALIZED_NAME_CODIGO_RETORNO = "codigo_retorno";
  @SerializedName(SERIALIZED_NAME_CODIGO_RETORNO)
  private String codigoRetorno;

  public static final String SERIALIZED_NAME_MENSAGEM = "mensagem";
  @SerializedName(SERIALIZED_NAME_MENSAGEM)
  private String mensagem;

  public static final String SERIALIZED_NAME_CODIGO_REFERENCIA = "codigo_referencia";
  @SerializedName(SERIALIZED_NAME_CODIGO_REFERENCIA)
  private String codigoReferencia;

  public static final String SERIALIZED_NAME_MENSAGEM_SEFAZ = "mensagem_sefaz";
  @SerializedName(SERIALIZED_NAME_MENSAGEM_SEFAZ)
  private String mensagemSefaz;

  public static final String SERIALIZED_NAME_DATA_HORA_EMISSAO = "data_hora_emissao";
  @SerializedName(SERIALIZED_NAME_DATA_HORA_EMISSAO)
  private OffsetDateTime dataHoraEmissao;

  public static final String SERIALIZED_NAME_CHAVE_CONSULTA = "chave_consulta";
  @SerializedName(SERIALIZED_NAME_CHAVE_CONSULTA)
  private String chaveConsulta;

  public static final String SERIALIZED_NAME_VALOR_TOTAL_CFE = "valor_total_cfe";
  @SerializedName(SERIALIZED_NAME_VALOR_TOTAL_CFE)
  private Double valorTotalCfe;

  public static final String SERIALIZED_NAME_CPF_CNPJ_ADQUIRENTE = "cpf_cnpj_adquirente";
  @SerializedName(SERIALIZED_NAME_CPF_CNPJ_ADQUIRENTE)
  private Double cpfCnpjAdquirente;

  public static final String SERIALIZED_NAME_VFPE = "vfpe";
  @SerializedName(SERIALIZED_NAME_VFPE)
  private List<VFPe> vfpe = null;


  public CFeCompleto codigoRetorno(String codigoRetorno) {
    
    this.codigoRetorno = codigoRetorno;
    return this;
  }

   /**
   * Get codigoRetorno
   * @return codigoRetorno
  **/
  @ApiModelProperty(required = true, value = "")

  public String getCodigoRetorno() {
    return codigoRetorno;
  }



  public void setCodigoRetorno(String codigoRetorno) {
    this.codigoRetorno = codigoRetorno;
  }


  public CFeCompleto mensagem(String mensagem) {
    
    this.mensagem = mensagem;
    return this;
  }

   /**
   * Get mensagem
   * @return mensagem
  **/
  @ApiModelProperty(required = true, value = "")

  public String getMensagem() {
    return mensagem;
  }



  public void setMensagem(String mensagem) {
    this.mensagem = mensagem;
  }


  public CFeCompleto codigoReferencia(String codigoReferencia) {
    
    this.codigoReferencia = codigoReferencia;
    return this;
  }

   /**
   * Get codigoReferencia
   * @return codigoReferencia
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getCodigoReferencia() {
    return codigoReferencia;
  }



  public void setCodigoReferencia(String codigoReferencia) {
    this.codigoReferencia = codigoReferencia;
  }


  public CFeCompleto mensagemSefaz(String mensagemSefaz) {
    
    this.mensagemSefaz = mensagemSefaz;
    return this;
  }

   /**
   * Get mensagemSefaz
   * @return mensagemSefaz
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getMensagemSefaz() {
    return mensagemSefaz;
  }



  public void setMensagemSefaz(String mensagemSefaz) {
    this.mensagemSefaz = mensagemSefaz;
  }


  public CFeCompleto dataHoraEmissao(OffsetDateTime dataHoraEmissao) {
    
    this.dataHoraEmissao = dataHoraEmissao;
    return this;
  }

   /**
   * Get dataHoraEmissao
   * @return dataHoraEmissao
  **/
  @ApiModelProperty(required = true, value = "")

  public OffsetDateTime getDataHoraEmissao() {
    return dataHoraEmissao;
  }



  public void setDataHoraEmissao(OffsetDateTime dataHoraEmissao) {
    this.dataHoraEmissao = dataHoraEmissao;
  }


  public CFeCompleto chaveConsulta(String chaveConsulta) {
    
    this.chaveConsulta = chaveConsulta;
    return this;
  }

   /**
   * Get chaveConsulta
   * @return chaveConsulta
  **/
  @ApiModelProperty(required = true, value = "")

  public String getChaveConsulta() {
    return chaveConsulta;
  }



  public void setChaveConsulta(String chaveConsulta) {
    this.chaveConsulta = chaveConsulta;
  }


  public CFeCompleto valorTotalCfe(Double valorTotalCfe) {
    
    this.valorTotalCfe = valorTotalCfe;
    return this;
  }

   /**
   * Get valorTotalCfe
   * @return valorTotalCfe
  **/
  @ApiModelProperty(required = true, value = "")

  public Double getValorTotalCfe() {
    return valorTotalCfe;
  }



  public void setValorTotalCfe(Double valorTotalCfe) {
    this.valorTotalCfe = valorTotalCfe;
  }


  public CFeCompleto cpfCnpjAdquirente(Double cpfCnpjAdquirente) {
    
    this.cpfCnpjAdquirente = cpfCnpjAdquirente;
    return this;
  }

   /**
   * Get cpfCnpjAdquirente
   * @return cpfCnpjAdquirente
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Double getCpfCnpjAdquirente() {
    return cpfCnpjAdquirente;
  }



  public void setCpfCnpjAdquirente(Double cpfCnpjAdquirente) {
    this.cpfCnpjAdquirente = cpfCnpjAdquirente;
  }


  public CFeCompleto vfpe(List<VFPe> vfpe) {
    
    this.vfpe = vfpe;
    return this;
  }

  public CFeCompleto addVfpeItem(VFPe vfpeItem) {
    if (this.vfpe == null) {
      this.vfpe = new ArrayList<>();
    }
    this.vfpe.add(vfpeItem);
    return this;
  }

   /**
   * Get vfpe
   * @return vfpe
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public List<VFPe> getVfpe() {
    return vfpe;
  }



  public void setVfpe(List<VFPe> vfpe) {
    this.vfpe = vfpe;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CFeCompleto cfeCompleto = (CFeCompleto) o;
    return Objects.equals(this.codigoRetorno, cfeCompleto.codigoRetorno) &&
        Objects.equals(this.mensagem, cfeCompleto.mensagem) &&
        Objects.equals(this.codigoReferencia, cfeCompleto.codigoReferencia) &&
        Objects.equals(this.mensagemSefaz, cfeCompleto.mensagemSefaz) &&
        Objects.equals(this.dataHoraEmissao, cfeCompleto.dataHoraEmissao) &&
        Objects.equals(this.chaveConsulta, cfeCompleto.chaveConsulta) &&
        Objects.equals(this.valorTotalCfe, cfeCompleto.valorTotalCfe) &&
        Objects.equals(this.cpfCnpjAdquirente, cfeCompleto.cpfCnpjAdquirente) &&
        Objects.equals(this.vfpe, cfeCompleto.vfpe);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codigoRetorno, mensagem, codigoReferencia, mensagemSefaz, dataHoraEmissao, chaveConsulta, valorTotalCfe, cpfCnpjAdquirente, vfpe);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CFeCompleto {\n");
    sb.append("    codigoRetorno: ").append(toIndentedString(codigoRetorno)).append("\n");
    sb.append("    mensagem: ").append(toIndentedString(mensagem)).append("\n");
    sb.append("    codigoReferencia: ").append(toIndentedString(codigoReferencia)).append("\n");
    sb.append("    mensagemSefaz: ").append(toIndentedString(mensagemSefaz)).append("\n");
    sb.append("    dataHoraEmissao: ").append(toIndentedString(dataHoraEmissao)).append("\n");
    sb.append("    chaveConsulta: ").append(toIndentedString(chaveConsulta)).append("\n");
    sb.append("    valorTotalCfe: ").append(toIndentedString(valorTotalCfe)).append("\n");
    sb.append("    cpfCnpjAdquirente: ").append(toIndentedString(cpfCnpjAdquirente)).append("\n");
    sb.append("    vfpe: ").append(toIndentedString(vfpe)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

