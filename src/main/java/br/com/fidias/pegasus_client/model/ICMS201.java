/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * ICMS201
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class ICMS201 {
  public static final String SERIALIZED_NAME_P_I_C_M_S_S_T = "pICMSST";
  @SerializedName(SERIALIZED_NAME_P_I_C_M_S_S_T)
  private Double pICMSST;

  public static final String SERIALIZED_NAME_P_M_V_A_S_T = "pMVAST";
  @SerializedName(SERIALIZED_NAME_P_M_V_A_S_T)
  private Double pMVAST;

  public static final String SERIALIZED_NAME_P_RED_B_C_S_T = "pRedBCST";
  @SerializedName(SERIALIZED_NAME_P_RED_B_C_S_T)
  private Double pRedBCST;

  public static final String SERIALIZED_NAME_P_CRED_S_N = "pCredSN";
  @SerializedName(SERIALIZED_NAME_P_CRED_S_N)
  private Double pCredSN;


  public ICMS201 pICMSST(Double pICMSST) {
    
    this.pICMSST = pICMSST;
    return this;
  }

   /**
   * Get pICMSST
   * @return pICMSST
  **/
  @ApiModelProperty(required = true, value = "")

  public Double getpICMSST() {
    return pICMSST;
  }



  public void setpICMSST(Double pICMSST) {
    this.pICMSST = pICMSST;
  }


  public ICMS201 pMVAST(Double pMVAST) {
    
    this.pMVAST = pMVAST;
    return this;
  }

   /**
   * Get pMVAST
   * @return pMVAST
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Double getpMVAST() {
    return pMVAST;
  }



  public void setpMVAST(Double pMVAST) {
    this.pMVAST = pMVAST;
  }


  public ICMS201 pRedBCST(Double pRedBCST) {
    
    this.pRedBCST = pRedBCST;
    return this;
  }

   /**
   * Get pRedBCST
   * @return pRedBCST
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Double getpRedBCST() {
    return pRedBCST;
  }



  public void setpRedBCST(Double pRedBCST) {
    this.pRedBCST = pRedBCST;
  }


  public ICMS201 pCredSN(Double pCredSN) {
    
    this.pCredSN = pCredSN;
    return this;
  }

   /**
   * Get pCredSN
   * @return pCredSN
  **/
  @ApiModelProperty(required = true, value = "")

  public Double getpCredSN() {
    return pCredSN;
  }



  public void setpCredSN(Double pCredSN) {
    this.pCredSN = pCredSN;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ICMS201 ICMS201 = (ICMS201) o;
    return Objects.equals(this.pICMSST, ICMS201.pICMSST) &&
        Objects.equals(this.pMVAST, ICMS201.pMVAST) &&
        Objects.equals(this.pRedBCST, ICMS201.pRedBCST) &&
        Objects.equals(this.pCredSN, ICMS201.pCredSN);
  }

  @Override
  public int hashCode() {
    return Objects.hash(pICMSST, pMVAST, pRedBCST, pCredSN);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ICMS201 {\n");
    sb.append("    pICMSST: ").append(toIndentedString(pICMSST)).append("\n");
    sb.append("    pMVAST: ").append(toIndentedString(pMVAST)).append("\n");
    sb.append("    pRedBCST: ").append(toIndentedString(pRedBCST)).append("\n");
    sb.append("    pCredSN: ").append(toIndentedString(pCredSN)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

