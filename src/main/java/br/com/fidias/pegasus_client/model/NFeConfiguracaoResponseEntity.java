/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import br.com.fidias.pegasus_client.model.NFeCarregarConfiguracoes;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * NFeConfiguracaoResponseEntity
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class NFeConfiguracaoResponseEntity {
  public static final String SERIALIZED_NAME_CONFIGURACOES_NFE = "configuracoes_nfe";
  @SerializedName(SERIALIZED_NAME_CONFIGURACOES_NFE)
  private NFeCarregarConfiguracoes configuracoesNfe;

  public static final String SERIALIZED_NAME_CONFIGURACOES_DIST = "configuracoes_dist";
  @SerializedName(SERIALIZED_NAME_CONFIGURACOES_DIST)
  private String configuracoesDist;


  public NFeConfiguracaoResponseEntity configuracoesNfe(NFeCarregarConfiguracoes configuracoesNfe) {
    
    this.configuracoesNfe = configuracoesNfe;
    return this;
  }

   /**
   * Get configuracoesNfe
   * @return configuracoesNfe
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public NFeCarregarConfiguracoes getConfiguracoesNfe() {
    return configuracoesNfe;
  }



  public void setConfiguracoesNfe(NFeCarregarConfiguracoes configuracoesNfe) {
    this.configuracoesNfe = configuracoesNfe;
  }


  public NFeConfiguracaoResponseEntity configuracoesDist(String configuracoesDist) {
    
    this.configuracoesDist = configuracoesDist;
    return this;
  }

   /**
   * Get configuracoesDist
   * @return configuracoesDist
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getConfiguracoesDist() {
    return configuracoesDist;
  }



  public void setConfiguracoesDist(String configuracoesDist) {
    this.configuracoesDist = configuracoesDist;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NFeConfiguracaoResponseEntity nfeConfiguracaoResponseEntity = (NFeConfiguracaoResponseEntity) o;
    return Objects.equals(this.configuracoesNfe, nfeConfiguracaoResponseEntity.configuracoesNfe) &&
        Objects.equals(this.configuracoesDist, nfeConfiguracaoResponseEntity.configuracoesDist);
  }

  @Override
  public int hashCode() {
    return Objects.hash(configuracoesNfe, configuracoesDist);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NFeConfiguracaoResponseEntity {\n");
    sb.append("    configuracoesNfe: ").append(toIndentedString(configuracoesNfe)).append("\n");
    sb.append("    configuracoesDist: ").append(toIndentedString(configuracoesDist)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

