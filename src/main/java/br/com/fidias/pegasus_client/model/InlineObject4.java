/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import br.com.fidias.pegasus_client.model.IntegerParam;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * InlineObject4
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class InlineObject4 {
  public static final String SERIALIZED_NAME_DESCRICAO = "descricao";
  @SerializedName(SERIALIZED_NAME_DESCRICAO)
  private String descricao;

  public static final String SERIALIZED_NAME_CHAVE_REQUISICAO = "chave_requisicao";
  @SerializedName(SERIALIZED_NAME_CHAVE_REQUISICAO)
  private String chaveRequisicao;

  public static final String SERIALIZED_NAME_ID_ADQUIRENTE = "id_adquirente";
  @SerializedName(SERIALIZED_NAME_ID_ADQUIRENTE)
  private IntegerParam idAdquirente;

  public static final String SERIALIZED_NAME_POSICAO = "posicao";
  @SerializedName(SERIALIZED_NAME_POSICAO)
  private IntegerParam posicao;


  public InlineObject4 descricao(String descricao) {
    
    this.descricao = descricao;
    return this;
  }

   /**
   * Get descricao
   * @return descricao
  **/
  @ApiModelProperty(required = true, value = "")

  public String getDescricao() {
    return descricao;
  }



  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }


  public InlineObject4 chaveRequisicao(String chaveRequisicao) {
    
    this.chaveRequisicao = chaveRequisicao;
    return this;
  }

   /**
   * Get chaveRequisicao
   * @return chaveRequisicao
  **/
  @ApiModelProperty(required = true, value = "")

  public String getChaveRequisicao() {
    return chaveRequisicao;
  }



  public void setChaveRequisicao(String chaveRequisicao) {
    this.chaveRequisicao = chaveRequisicao;
  }


  public InlineObject4 idAdquirente(IntegerParam idAdquirente) {
    
    this.idAdquirente = idAdquirente;
    return this;
  }

   /**
   * Get idAdquirente
   * @return idAdquirente
  **/
  @ApiModelProperty(required = true, value = "")

  public IntegerParam getIdAdquirente() {
    return idAdquirente;
  }



  public void setIdAdquirente(IntegerParam idAdquirente) {
    this.idAdquirente = idAdquirente;
  }


  public InlineObject4 posicao(IntegerParam posicao) {
    
    this.posicao = posicao;
    return this;
  }

   /**
   * Get posicao
   * @return posicao
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public IntegerParam getPosicao() {
    return posicao;
  }



  public void setPosicao(IntegerParam posicao) {
    this.posicao = posicao;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineObject4 inlineObject4 = (InlineObject4) o;
    return Objects.equals(this.descricao, inlineObject4.descricao) &&
        Objects.equals(this.chaveRequisicao, inlineObject4.chaveRequisicao) &&
        Objects.equals(this.idAdquirente, inlineObject4.idAdquirente) &&
        Objects.equals(this.posicao, inlineObject4.posicao);
  }

  @Override
  public int hashCode() {
    return Objects.hash(descricao, chaveRequisicao, idAdquirente, posicao);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineObject4 {\n");
    sb.append("    descricao: ").append(toIndentedString(descricao)).append("\n");
    sb.append("    chaveRequisicao: ").append(toIndentedString(chaveRequisicao)).append("\n");
    sb.append("    idAdquirente: ").append(toIndentedString(idAdquirente)).append("\n");
    sb.append("    posicao: ").append(toIndentedString(posicao)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

