/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import br.com.fidias.pegasus_client.model.IntegerParam;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * InlineObject52
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class InlineObject52 {
  public static final String SERIALIZED_NAME_C_UF = "c_uf";
  @SerializedName(SERIALIZED_NAME_C_UF)
  private IntegerParam cUf;


  public InlineObject52 cUf(IntegerParam cUf) {
    
    this.cUf = cUf;
    return this;
  }

   /**
   * Get cUf
   * @return cUf
  **/
  @ApiModelProperty(required = true, value = "")

  public IntegerParam getcUf() {
    return cUf;
  }



  public void setcUf(IntegerParam cUf) {
    this.cUf = cUf;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineObject52 inlineObject52 = (InlineObject52) o;
    return Objects.equals(this.cUf, inlineObject52.cUf);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cUf);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineObject52 {\n");
    sb.append("    cUf: ").append(toIndentedString(cUf)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

