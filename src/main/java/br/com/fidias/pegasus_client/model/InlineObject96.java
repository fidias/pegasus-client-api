/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * InlineObject96
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class InlineObject96 {
  public static final String SERIALIZED_NAME_PRODUCAO_OPERACAO_FINANCEIRA_ENTRADA = "producao_operacao_financeira_entrada";
  @SerializedName(SERIALIZED_NAME_PRODUCAO_OPERACAO_FINANCEIRA_ENTRADA)
  private Integer producaoOperacaoFinanceiraEntrada;

  public static final String SERIALIZED_NAME_PRODUCAO_OPERACAO_FINANCEIRA_SAIDA = "producao_operacao_financeira_saida";
  @SerializedName(SERIALIZED_NAME_PRODUCAO_OPERACAO_FINANCEIRA_SAIDA)
  private Integer producaoOperacaoFinanceiraSaida;


  public InlineObject96 producaoOperacaoFinanceiraEntrada(Integer producaoOperacaoFinanceiraEntrada) {
    
    this.producaoOperacaoFinanceiraEntrada = producaoOperacaoFinanceiraEntrada;
    return this;
  }

   /**
   * Get producaoOperacaoFinanceiraEntrada
   * @return producaoOperacaoFinanceiraEntrada
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Integer getProducaoOperacaoFinanceiraEntrada() {
    return producaoOperacaoFinanceiraEntrada;
  }



  public void setProducaoOperacaoFinanceiraEntrada(Integer producaoOperacaoFinanceiraEntrada) {
    this.producaoOperacaoFinanceiraEntrada = producaoOperacaoFinanceiraEntrada;
  }


  public InlineObject96 producaoOperacaoFinanceiraSaida(Integer producaoOperacaoFinanceiraSaida) {
    
    this.producaoOperacaoFinanceiraSaida = producaoOperacaoFinanceiraSaida;
    return this;
  }

   /**
   * Get producaoOperacaoFinanceiraSaida
   * @return producaoOperacaoFinanceiraSaida
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Integer getProducaoOperacaoFinanceiraSaida() {
    return producaoOperacaoFinanceiraSaida;
  }



  public void setProducaoOperacaoFinanceiraSaida(Integer producaoOperacaoFinanceiraSaida) {
    this.producaoOperacaoFinanceiraSaida = producaoOperacaoFinanceiraSaida;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineObject96 inlineObject96 = (InlineObject96) o;
    return Objects.equals(this.producaoOperacaoFinanceiraEntrada, inlineObject96.producaoOperacaoFinanceiraEntrada) &&
        Objects.equals(this.producaoOperacaoFinanceiraSaida, inlineObject96.producaoOperacaoFinanceiraSaida);
  }

  @Override
  public int hashCode() {
    return Objects.hash(producaoOperacaoFinanceiraEntrada, producaoOperacaoFinanceiraSaida);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineObject96 {\n");
    sb.append("    producaoOperacaoFinanceiraEntrada: ").append(toIndentedString(producaoOperacaoFinanceiraEntrada)).append("\n");
    sb.append("    producaoOperacaoFinanceiraSaida: ").append(toIndentedString(producaoOperacaoFinanceiraSaida)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

