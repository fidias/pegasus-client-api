/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * ICMSResumoAliquotaPadraoResponseEntity
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class ICMSResumoAliquotaPadraoResponseEntity {
  public static final String SERIALIZED_NAME_CST = "cst";
  @SerializedName(SERIALIZED_NAME_CST)
  private String cst;

  public static final String SERIALIZED_NAME_COUNT = "count";
  @SerializedName(SERIALIZED_NAME_COUNT)
  private Long count;


  public ICMSResumoAliquotaPadraoResponseEntity cst(String cst) {
    
    this.cst = cst;
    return this;
  }

   /**
   * Get cst
   * @return cst
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getCst() {
    return cst;
  }



  public void setCst(String cst) {
    this.cst = cst;
  }


  public ICMSResumoAliquotaPadraoResponseEntity count(Long count) {
    
    this.count = count;
    return this;
  }

   /**
   * Get count
   * @return count
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Long getCount() {
    return count;
  }



  public void setCount(Long count) {
    this.count = count;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ICMSResumoAliquotaPadraoResponseEntity icMSResumoAliquotaPadraoResponseEntity = (ICMSResumoAliquotaPadraoResponseEntity) o;
    return Objects.equals(this.cst, icMSResumoAliquotaPadraoResponseEntity.cst) &&
        Objects.equals(this.count, icMSResumoAliquotaPadraoResponseEntity.count);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cst, count);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ICMSResumoAliquotaPadraoResponseEntity {\n");
    sb.append("    cst: ").append(toIndentedString(cst)).append("\n");
    sb.append("    count: ").append(toIndentedString(count)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

