/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import br.com.fidias.pegasus_client.model.Sistema;
import br.com.fidias.pegasus_client.model.TipoEvento;
import br.com.fidias.pegasus_client.model.Usuario2;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;

/**
 * Evento
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class Evento {
  public static final String SERIALIZED_NAME_ID = "id";
  @SerializedName(SERIALIZED_NAME_ID)
  private Long id;

  public static final String SERIALIZED_NAME_USUARIO = "usuario";
  @SerializedName(SERIALIZED_NAME_USUARIO)
  private Usuario2 usuario;

  public static final String SERIALIZED_NAME_DATA_EVENTO = "data_evento";
  @SerializedName(SERIALIZED_NAME_DATA_EVENTO)
  private OffsetDateTime dataEvento;

  public static final String SERIALIZED_NAME_TIPO_EVENTO = "tipo_evento";
  @SerializedName(SERIALIZED_NAME_TIPO_EVENTO)
  private TipoEvento tipoEvento;

  public static final String SERIALIZED_NAME_MENSAGEM_AUXILIAR = "mensagem_auxiliar";
  @SerializedName(SERIALIZED_NAME_MENSAGEM_AUXILIAR)
  private String mensagemAuxiliar;

  public static final String SERIALIZED_NAME_SISTEMA = "sistema";
  @SerializedName(SERIALIZED_NAME_SISTEMA)
  private Sistema sistema;


  public Evento id(Long id) {
    
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(required = true, value = "")

  public Long getId() {
    return id;
  }



  public void setId(Long id) {
    this.id = id;
  }


  public Evento usuario(Usuario2 usuario) {
    
    this.usuario = usuario;
    return this;
  }

   /**
   * Get usuario
   * @return usuario
  **/
  @ApiModelProperty(required = true, value = "")

  public Usuario2 getUsuario() {
    return usuario;
  }



  public void setUsuario(Usuario2 usuario) {
    this.usuario = usuario;
  }


  public Evento dataEvento(OffsetDateTime dataEvento) {
    
    this.dataEvento = dataEvento;
    return this;
  }

   /**
   * Get dataEvento
   * @return dataEvento
  **/
  @ApiModelProperty(required = true, value = "")

  public OffsetDateTime getDataEvento() {
    return dataEvento;
  }



  public void setDataEvento(OffsetDateTime dataEvento) {
    this.dataEvento = dataEvento;
  }


  public Evento tipoEvento(TipoEvento tipoEvento) {
    
    this.tipoEvento = tipoEvento;
    return this;
  }

   /**
   * Get tipoEvento
   * @return tipoEvento
  **/
  @ApiModelProperty(required = true, value = "")

  public TipoEvento getTipoEvento() {
    return tipoEvento;
  }



  public void setTipoEvento(TipoEvento tipoEvento) {
    this.tipoEvento = tipoEvento;
  }


  public Evento mensagemAuxiliar(String mensagemAuxiliar) {
    
    this.mensagemAuxiliar = mensagemAuxiliar;
    return this;
  }

   /**
   * Get mensagemAuxiliar
   * @return mensagemAuxiliar
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getMensagemAuxiliar() {
    return mensagemAuxiliar;
  }



  public void setMensagemAuxiliar(String mensagemAuxiliar) {
    this.mensagemAuxiliar = mensagemAuxiliar;
  }


  public Evento sistema(Sistema sistema) {
    
    this.sistema = sistema;
    return this;
  }

   /**
   * Get sistema
   * @return sistema
  **/
  @ApiModelProperty(required = true, value = "")

  public Sistema getSistema() {
    return sistema;
  }



  public void setSistema(Sistema sistema) {
    this.sistema = sistema;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Evento evento = (Evento) o;
    return Objects.equals(this.id, evento.id) &&
        Objects.equals(this.usuario, evento.usuario) &&
        Objects.equals(this.dataEvento, evento.dataEvento) &&
        Objects.equals(this.tipoEvento, evento.tipoEvento) &&
        Objects.equals(this.mensagemAuxiliar, evento.mensagemAuxiliar) &&
        Objects.equals(this.sistema, evento.sistema);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, usuario, dataEvento, tipoEvento, mensagemAuxiliar, sistema);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Evento {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    usuario: ").append(toIndentedString(usuario)).append("\n");
    sb.append("    dataEvento: ").append(toIndentedString(dataEvento)).append("\n");
    sb.append("    tipoEvento: ").append(toIndentedString(tipoEvento)).append("\n");
    sb.append("    mensagemAuxiliar: ").append(toIndentedString(mensagemAuxiliar)).append("\n");
    sb.append("    sistema: ").append(toIndentedString(sistema)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

