/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import br.com.fidias.pegasus_client.model.Bairro;
import br.com.fidias.pegasus_client.model.Municipio;
import br.com.fidias.pegasus_client.model.Uf;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * Endereco
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class Endereco {
  public static final String SERIALIZED_NAME_ID = "id";
  @SerializedName(SERIALIZED_NAME_ID)
  private Long id;

  public static final String SERIALIZED_NAME_TITULO = "titulo";
  @SerializedName(SERIALIZED_NAME_TITULO)
  private String titulo;

  public static final String SERIALIZED_NAME_LOGRADOURO = "logradouro";
  @SerializedName(SERIALIZED_NAME_LOGRADOURO)
  private String logradouro;

  public static final String SERIALIZED_NAME_CEP = "cep";
  @SerializedName(SERIALIZED_NAME_CEP)
  private String cep;

  public static final String SERIALIZED_NAME_PONTO_REFERENCIA = "ponto_referencia";
  @SerializedName(SERIALIZED_NAME_PONTO_REFERENCIA)
  private String pontoReferencia;

  public static final String SERIALIZED_NAME_COMPLEMENTO = "complemento";
  @SerializedName(SERIALIZED_NAME_COMPLEMENTO)
  private String complemento;

  public static final String SERIALIZED_NAME_BAIRRO = "bairro";
  @SerializedName(SERIALIZED_NAME_BAIRRO)
  private Bairro bairro;

  public static final String SERIALIZED_NAME_MUNICIPIO = "municipio";
  @SerializedName(SERIALIZED_NAME_MUNICIPIO)
  private Municipio municipio;

  public static final String SERIALIZED_NAME_UF = "uf";
  @SerializedName(SERIALIZED_NAME_UF)
  private Uf uf;

  public static final String SERIALIZED_NAME_NUMERO = "numero";
  @SerializedName(SERIALIZED_NAME_NUMERO)
  private String numero;


  public Endereco id(Long id) {
    
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(required = true, value = "")

  public Long getId() {
    return id;
  }



  public void setId(Long id) {
    this.id = id;
  }


  public Endereco titulo(String titulo) {
    
    this.titulo = titulo;
    return this;
  }

   /**
   * Get titulo
   * @return titulo
  **/
  @ApiModelProperty(required = true, value = "")

  public String getTitulo() {
    return titulo;
  }



  public void setTitulo(String titulo) {
    this.titulo = titulo;
  }


  public Endereco logradouro(String logradouro) {
    
    this.logradouro = logradouro;
    return this;
  }

   /**
   * Get logradouro
   * @return logradouro
  **/
  @ApiModelProperty(required = true, value = "")

  public String getLogradouro() {
    return logradouro;
  }



  public void setLogradouro(String logradouro) {
    this.logradouro = logradouro;
  }


  public Endereco cep(String cep) {
    
    this.cep = cep;
    return this;
  }

   /**
   * Get cep
   * @return cep
  **/
  @ApiModelProperty(required = true, value = "")

  public String getCep() {
    return cep;
  }



  public void setCep(String cep) {
    this.cep = cep;
  }


  public Endereco pontoReferencia(String pontoReferencia) {
    
    this.pontoReferencia = pontoReferencia;
    return this;
  }

   /**
   * Get pontoReferencia
   * @return pontoReferencia
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getPontoReferencia() {
    return pontoReferencia;
  }



  public void setPontoReferencia(String pontoReferencia) {
    this.pontoReferencia = pontoReferencia;
  }


  public Endereco complemento(String complemento) {
    
    this.complemento = complemento;
    return this;
  }

   /**
   * Get complemento
   * @return complemento
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getComplemento() {
    return complemento;
  }



  public void setComplemento(String complemento) {
    this.complemento = complemento;
  }


  public Endereco bairro(Bairro bairro) {
    
    this.bairro = bairro;
    return this;
  }

   /**
   * Get bairro
   * @return bairro
  **/
  @ApiModelProperty(required = true, value = "")

  public Bairro getBairro() {
    return bairro;
  }



  public void setBairro(Bairro bairro) {
    this.bairro = bairro;
  }


  public Endereco municipio(Municipio municipio) {
    
    this.municipio = municipio;
    return this;
  }

   /**
   * Get municipio
   * @return municipio
  **/
  @ApiModelProperty(required = true, value = "")

  public Municipio getMunicipio() {
    return municipio;
  }



  public void setMunicipio(Municipio municipio) {
    this.municipio = municipio;
  }


  public Endereco uf(Uf uf) {
    
    this.uf = uf;
    return this;
  }

   /**
   * Get uf
   * @return uf
  **/
  @ApiModelProperty(required = true, value = "")

  public Uf getUf() {
    return uf;
  }



  public void setUf(Uf uf) {
    this.uf = uf;
  }


  public Endereco numero(String numero) {
    
    this.numero = numero;
    return this;
  }

   /**
   * Get numero
   * @return numero
  **/
  @ApiModelProperty(required = true, value = "")

  public String getNumero() {
    return numero;
  }



  public void setNumero(String numero) {
    this.numero = numero;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Endereco endereco = (Endereco) o;
    return Objects.equals(this.id, endereco.id) &&
        Objects.equals(this.titulo, endereco.titulo) &&
        Objects.equals(this.logradouro, endereco.logradouro) &&
        Objects.equals(this.cep, endereco.cep) &&
        Objects.equals(this.pontoReferencia, endereco.pontoReferencia) &&
        Objects.equals(this.complemento, endereco.complemento) &&
        Objects.equals(this.bairro, endereco.bairro) &&
        Objects.equals(this.municipio, endereco.municipio) &&
        Objects.equals(this.uf, endereco.uf) &&
        Objects.equals(this.numero, endereco.numero);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, titulo, logradouro, cep, pontoReferencia, complemento, bairro, municipio, uf, numero);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Endereco {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    titulo: ").append(toIndentedString(titulo)).append("\n");
    sb.append("    logradouro: ").append(toIndentedString(logradouro)).append("\n");
    sb.append("    cep: ").append(toIndentedString(cep)).append("\n");
    sb.append("    pontoReferencia: ").append(toIndentedString(pontoReferencia)).append("\n");
    sb.append("    complemento: ").append(toIndentedString(complemento)).append("\n");
    sb.append("    bairro: ").append(toIndentedString(bairro)).append("\n");
    sb.append("    municipio: ").append(toIndentedString(municipio)).append("\n");
    sb.append("    uf: ").append(toIndentedString(uf)).append("\n");
    sb.append("    numero: ").append(toIndentedString(numero)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

