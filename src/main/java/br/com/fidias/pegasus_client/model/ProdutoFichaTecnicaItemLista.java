/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * ProdutoFichaTecnicaItemLista
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class ProdutoFichaTecnicaItemLista {
  public static final String SERIALIZED_NAME_PRODUTO_FICHA_TECNICA_ID = "produto_ficha_tecnica_id";
  @SerializedName(SERIALIZED_NAME_PRODUTO_FICHA_TECNICA_ID)
  private Long produtoFichaTecnicaId;

  public static final String SERIALIZED_NAME_DESCRICAO = "descricao";
  @SerializedName(SERIALIZED_NAME_DESCRICAO)
  private String descricao;

  public static final String SERIALIZED_NAME_QUANTIDADE = "quantidade";
  @SerializedName(SERIALIZED_NAME_QUANTIDADE)
  private Double quantidade;

  public static final String SERIALIZED_NAME_UNIDADE = "unidade";
  @SerializedName(SERIALIZED_NAME_UNIDADE)
  private String unidade;

  public static final String SERIALIZED_NAME_CUSTO = "custo";
  @SerializedName(SERIALIZED_NAME_CUSTO)
  private Double custo;


  public ProdutoFichaTecnicaItemLista produtoFichaTecnicaId(Long produtoFichaTecnicaId) {
    
    this.produtoFichaTecnicaId = produtoFichaTecnicaId;
    return this;
  }

   /**
   * Get produtoFichaTecnicaId
   * @return produtoFichaTecnicaId
  **/
  @ApiModelProperty(required = true, value = "")

  public Long getProdutoFichaTecnicaId() {
    return produtoFichaTecnicaId;
  }



  public void setProdutoFichaTecnicaId(Long produtoFichaTecnicaId) {
    this.produtoFichaTecnicaId = produtoFichaTecnicaId;
  }


  public ProdutoFichaTecnicaItemLista descricao(String descricao) {
    
    this.descricao = descricao;
    return this;
  }

   /**
   * Get descricao
   * @return descricao
  **/
  @ApiModelProperty(required = true, value = "")

  public String getDescricao() {
    return descricao;
  }



  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }


  public ProdutoFichaTecnicaItemLista quantidade(Double quantidade) {
    
    this.quantidade = quantidade;
    return this;
  }

   /**
   * Get quantidade
   * @return quantidade
  **/
  @ApiModelProperty(required = true, value = "")

  public Double getQuantidade() {
    return quantidade;
  }



  public void setQuantidade(Double quantidade) {
    this.quantidade = quantidade;
  }


  public ProdutoFichaTecnicaItemLista unidade(String unidade) {
    
    this.unidade = unidade;
    return this;
  }

   /**
   * Get unidade
   * @return unidade
  **/
  @ApiModelProperty(required = true, value = "")

  public String getUnidade() {
    return unidade;
  }



  public void setUnidade(String unidade) {
    this.unidade = unidade;
  }


  public ProdutoFichaTecnicaItemLista custo(Double custo) {
    
    this.custo = custo;
    return this;
  }

   /**
   * Get custo
   * @return custo
  **/
  @ApiModelProperty(required = true, value = "")

  public Double getCusto() {
    return custo;
  }



  public void setCusto(Double custo) {
    this.custo = custo;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProdutoFichaTecnicaItemLista produtoFichaTecnicaItemLista = (ProdutoFichaTecnicaItemLista) o;
    return Objects.equals(this.produtoFichaTecnicaId, produtoFichaTecnicaItemLista.produtoFichaTecnicaId) &&
        Objects.equals(this.descricao, produtoFichaTecnicaItemLista.descricao) &&
        Objects.equals(this.quantidade, produtoFichaTecnicaItemLista.quantidade) &&
        Objects.equals(this.unidade, produtoFichaTecnicaItemLista.unidade) &&
        Objects.equals(this.custo, produtoFichaTecnicaItemLista.custo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(produtoFichaTecnicaId, descricao, quantidade, unidade, custo);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProdutoFichaTecnicaItemLista {\n");
    sb.append("    produtoFichaTecnicaId: ").append(toIndentedString(produtoFichaTecnicaId)).append("\n");
    sb.append("    descricao: ").append(toIndentedString(descricao)).append("\n");
    sb.append("    quantidade: ").append(toIndentedString(quantidade)).append("\n");
    sb.append("    unidade: ").append(toIndentedString(unidade)).append("\n");
    sb.append("    custo: ").append(toIndentedString(custo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

