/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * InlineObject61
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class InlineObject61 {
  public static final String SERIALIZED_NAME_ID_CAIXA = "id_caixa";
  @SerializedName(SERIALIZED_NAME_ID_CAIXA)
  private Integer idCaixa;

  public static final String SERIALIZED_NAME_CLIENTE_VENDA_ID = "cliente_venda_id";
  @SerializedName(SERIALIZED_NAME_CLIENTE_VENDA_ID)
  private Long clienteVendaId;

  public static final String SERIALIZED_NAME_JUSTIFICATIVA = "justificativa";
  @SerializedName(SERIALIZED_NAME_JUSTIFICATIVA)
  private String justificativa;


  public InlineObject61 idCaixa(Integer idCaixa) {
    
    this.idCaixa = idCaixa;
    return this;
  }

   /**
   * Get idCaixa
   * minimum: 1
   * @return idCaixa
  **/
  @ApiModelProperty(required = true, value = "")

  public Integer getIdCaixa() {
    return idCaixa;
  }



  public void setIdCaixa(Integer idCaixa) {
    this.idCaixa = idCaixa;
  }


  public InlineObject61 clienteVendaId(Long clienteVendaId) {
    
    this.clienteVendaId = clienteVendaId;
    return this;
  }

   /**
   * Get clienteVendaId
   * minimum: 1
   * @return clienteVendaId
  **/
  @ApiModelProperty(required = true, value = "")

  public Long getClienteVendaId() {
    return clienteVendaId;
  }



  public void setClienteVendaId(Long clienteVendaId) {
    this.clienteVendaId = clienteVendaId;
  }


  public InlineObject61 justificativa(String justificativa) {
    
    this.justificativa = justificativa;
    return this;
  }

   /**
   * Get justificativa
   * @return justificativa
  **/
  @ApiModelProperty(required = true, value = "")

  public String getJustificativa() {
    return justificativa;
  }



  public void setJustificativa(String justificativa) {
    this.justificativa = justificativa;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineObject61 inlineObject61 = (InlineObject61) o;
    return Objects.equals(this.idCaixa, inlineObject61.idCaixa) &&
        Objects.equals(this.clienteVendaId, inlineObject61.clienteVendaId) &&
        Objects.equals(this.justificativa, inlineObject61.justificativa);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idCaixa, clienteVendaId, justificativa);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineObject61 {\n");
    sb.append("    idCaixa: ").append(toIndentedString(idCaixa)).append("\n");
    sb.append("    clienteVendaId: ").append(toIndentedString(clienteVendaId)).append("\n");
    sb.append("    justificativa: ").append(toIndentedString(justificativa)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

