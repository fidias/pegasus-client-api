/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * Tipo
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class Tipo {
  public static final String SERIALIZED_NAME_PRODUTO = "produto";
  @SerializedName(SERIALIZED_NAME_PRODUTO)
  private String produto;

  public static final String SERIALIZED_NAME_SERVICO = "servico";
  @SerializedName(SERIALIZED_NAME_SERVICO)
  private String servico;


  public Tipo produto(String produto) {
    
    this.produto = produto;
    return this;
  }

   /**
   * Get produto
   * @return produto
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getProduto() {
    return produto;
  }



  public void setProduto(String produto) {
    this.produto = produto;
  }


  public Tipo servico(String servico) {
    
    this.servico = servico;
    return this;
  }

   /**
   * Get servico
   * @return servico
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getServico() {
    return servico;
  }



  public void setServico(String servico) {
    this.servico = servico;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Tipo tipo = (Tipo) o;
    return Objects.equals(this.produto, tipo.produto) &&
        Objects.equals(this.servico, tipo.servico);
  }

  @Override
  public int hashCode() {
    return Objects.hash(produto, servico);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Tipo {\n");
    sb.append("    produto: ").append(toIndentedString(produto)).append("\n");
    sb.append("    servico: ").append(toIndentedString(servico)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

