/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;

/**
 * ProdutoProducao
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class ProdutoProducao {
  public static final String SERIALIZED_NAME_PRODUTO_PRODUCAO_ID = "produto_producao_id";
  @SerializedName(SERIALIZED_NAME_PRODUTO_PRODUCAO_ID)
  private Long produtoProducaoId;

  public static final String SERIALIZED_NAME_DATA_EDICAO = "data_edicao";
  @SerializedName(SERIALIZED_NAME_DATA_EDICAO)
  private OffsetDateTime dataEdicao;


  public ProdutoProducao produtoProducaoId(Long produtoProducaoId) {
    
    this.produtoProducaoId = produtoProducaoId;
    return this;
  }

   /**
   * Get produtoProducaoId
   * @return produtoProducaoId
  **/
  @ApiModelProperty(required = true, value = "")

  public Long getProdutoProducaoId() {
    return produtoProducaoId;
  }



  public void setProdutoProducaoId(Long produtoProducaoId) {
    this.produtoProducaoId = produtoProducaoId;
  }


  public ProdutoProducao dataEdicao(OffsetDateTime dataEdicao) {
    
    this.dataEdicao = dataEdicao;
    return this;
  }

   /**
   * Get dataEdicao
   * @return dataEdicao
  **/
  @ApiModelProperty(required = true, value = "")

  public OffsetDateTime getDataEdicao() {
    return dataEdicao;
  }



  public void setDataEdicao(OffsetDateTime dataEdicao) {
    this.dataEdicao = dataEdicao;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProdutoProducao produtoProducao = (ProdutoProducao) o;
    return Objects.equals(this.produtoProducaoId, produtoProducao.produtoProducaoId) &&
        Objects.equals(this.dataEdicao, produtoProducao.dataEdicao);
  }

  @Override
  public int hashCode() {
    return Objects.hash(produtoProducaoId, dataEdicao);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProdutoProducao {\n");
    sb.append("    produtoProducaoId: ").append(toIndentedString(produtoProducaoId)).append("\n");
    sb.append("    dataEdicao: ").append(toIndentedString(dataEdicao)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

