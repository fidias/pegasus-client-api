/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * AlterarEmLoteImportarFormEntity
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class AlterarEmLoteImportarFormEntity {
  public static final String SERIALIZED_NAME_ARQUIVO = "arquivo";
  @SerializedName(SERIALIZED_NAME_ARQUIVO)
  private List<byte[]> arquivo = null;


  public AlterarEmLoteImportarFormEntity arquivo(List<byte[]> arquivo) {
    
    this.arquivo = arquivo;
    return this;
  }

  public AlterarEmLoteImportarFormEntity addArquivoItem(byte[] arquivoItem) {
    if (this.arquivo == null) {
      this.arquivo = new ArrayList<>();
    }
    this.arquivo.add(arquivoItem);
    return this;
  }

   /**
   * Arquivo CSV
   * @return arquivo
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Arquivo CSV")

  public List<byte[]> getArquivo() {
    return arquivo;
  }



  public void setArquivo(List<byte[]> arquivo) {
    this.arquivo = arquivo;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AlterarEmLoteImportarFormEntity alterarEmLoteImportarFormEntity = (AlterarEmLoteImportarFormEntity) o;
    return Objects.equals(this.arquivo, alterarEmLoteImportarFormEntity.arquivo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(arquivo);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AlterarEmLoteImportarFormEntity {\n");
    sb.append("    arquivo: ").append(toIndentedString(arquivo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

