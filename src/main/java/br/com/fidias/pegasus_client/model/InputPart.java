/*
 * Pegasus API
 * API Rest para consumir serviços do Pegasus Web
 *
 * The version of the OpenAPI document: 5
 * Contact: fidiascom@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package br.com.fidias.pegasus_client.model;

import java.util.Objects;
import java.util.Arrays;
import br.com.fidias.pegasus_client.model.InputPartMediaType;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * InputPart
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-12T10:21:55.140-03:00[America/Fortaleza]")
public class InputPart {
  public static final String SERIALIZED_NAME_CONTENT_TYPE_FROM_MESSAGE = "contentTypeFromMessage";
  @SerializedName(SERIALIZED_NAME_CONTENT_TYPE_FROM_MESSAGE)
  private Boolean contentTypeFromMessage;

  public static final String SERIALIZED_NAME_MEDIA_TYPE = "mediaType";
  @SerializedName(SERIALIZED_NAME_MEDIA_TYPE)
  private InputPartMediaType mediaType;

  public static final String SERIALIZED_NAME_BODY_AS_STRING = "bodyAsString";
  @SerializedName(SERIALIZED_NAME_BODY_AS_STRING)
  private String bodyAsString;

  public static final String SERIALIZED_NAME_HEADERS = "headers";
  @SerializedName(SERIALIZED_NAME_HEADERS)
  private Map<String, List<String>> headers = null;


  public InputPart contentTypeFromMessage(Boolean contentTypeFromMessage) {
    
    this.contentTypeFromMessage = contentTypeFromMessage;
    return this;
  }

   /**
   * Get contentTypeFromMessage
   * @return contentTypeFromMessage
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Boolean getContentTypeFromMessage() {
    return contentTypeFromMessage;
  }



  public void setContentTypeFromMessage(Boolean contentTypeFromMessage) {
    this.contentTypeFromMessage = contentTypeFromMessage;
  }


  public InputPart mediaType(InputPartMediaType mediaType) {
    
    this.mediaType = mediaType;
    return this;
  }

   /**
   * Get mediaType
   * @return mediaType
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public InputPartMediaType getMediaType() {
    return mediaType;
  }



  public void setMediaType(InputPartMediaType mediaType) {
    this.mediaType = mediaType;
  }


  public InputPart bodyAsString(String bodyAsString) {
    
    this.bodyAsString = bodyAsString;
    return this;
  }

   /**
   * Get bodyAsString
   * @return bodyAsString
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getBodyAsString() {
    return bodyAsString;
  }



  public void setBodyAsString(String bodyAsString) {
    this.bodyAsString = bodyAsString;
  }


  public InputPart headers(Map<String, List<String>> headers) {
    
    this.headers = headers;
    return this;
  }

  public InputPart putHeadersItem(String key, List<String> headersItem) {
    if (this.headers == null) {
      this.headers = new HashMap<>();
    }
    this.headers.put(key, headersItem);
    return this;
  }

   /**
   * Get headers
   * @return headers
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Map<String, List<String>> getHeaders() {
    return headers;
  }



  public void setHeaders(Map<String, List<String>> headers) {
    this.headers = headers;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InputPart inputPart = (InputPart) o;
    return Objects.equals(this.contentTypeFromMessage, inputPart.contentTypeFromMessage) &&
        Objects.equals(this.mediaType, inputPart.mediaType) &&
        Objects.equals(this.bodyAsString, inputPart.bodyAsString) &&
        Objects.equals(this.headers, inputPart.headers);
  }

  @Override
  public int hashCode() {
    return Objects.hash(contentTypeFromMessage, mediaType, bodyAsString, headers);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InputPart {\n");
    sb.append("    contentTypeFromMessage: ").append(toIndentedString(contentTypeFromMessage)).append("\n");
    sb.append("    mediaType: ").append(toIndentedString(mediaType)).append("\n");
    sb.append("    bodyAsString: ").append(toIndentedString(bodyAsString)).append("\n");
    sb.append("    headers: ").append(toIndentedString(headers)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

